<?php
return
[
    'module_title'=>'    Pages     ',
    'page_title_create'=>  'add new page   ',
    'page_title_edit'=>  '   edit page  ',
    'title'=>'page title',
    'desc'=>'page description',
    'validation_messages'=>[
        'title.*.required' =>'من فضلك قم بكتابة    اسم الصفحة',
        'title.*.max'=>'اقصى عدد للاحرف هو 255 ',
        'desc.*.required' =>'من فضلك قم بكتابة وصف الصفحة'
    ]
];
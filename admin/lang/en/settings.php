<?php
return
[
    'module_title'=>'    اقسام المشاريع     ',
    'page_title_create'=>  'اضافة    قسم جديد   ',
    'page_title_edit'=>  'Edit settings',
    'title'=>'   اسم القسم',
    'projects_count'=>'    المشاريع  ',
    'validation_messages'=>[
        'title.*.required' =>'من فضلك قم بكتابة    اسم القسم',
        'title.*.max'=>'اقصى عدد للاحرف هو 255 '
    ]
];
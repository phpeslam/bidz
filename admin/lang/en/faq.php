<?php
return
[
    'module_title'=>'faq',
    'page_title_create'=> 'add new question  ',
    'page_title_edit'=>  '  edit question ',
    'title'=>'question title',
    'desc'=>'question description',
    'validation_messages'=>[
        'title.*.required' =>'please add question title',
        'title.*.max'=>'max input length 255 ',
        'desc.*.required' =>'please add description to the question'
    ]
];
<?php
return [
    'site_title' => '     Bidz',
    'complete_warning' => '  Please complete the fields   ',
    'edit' => ' edit',
    'delete' => 'delete',
    'view' => 'view',
    'close' => 'close',
    'create' => 'create',
    'back' => 'back',
    'save' => 'save',
    'action' => 'action',
    'created_at' => 'created at',
    'updated_at' => 'updated at',
    'status_not_allowed' => ' Invalid selection ',
    'saved_successfully' => 'Successfully saved   ',
    'deleted_successfully' => 'Successfully deleted',
    'status_successfully_1' => '      Successfully posted    ',
    'status_successfully_2' => '  Publishing successfully canceled      ',
    'not_found_row' => '   Record does not exist ',
    'bootbox_delete_confirm' => ' Do you want to complete the deletion?',
    'is_active'=>'  active Status ',
    'is_active_0'=>' deactivated ',
    'is_active_1'=>' active  ',

    'logout'=>' logout  ',
    'welcome'=>'    welcome,  ',
    'profile'=>' Profile  ',

    'bootbox' => [
        'OK' => 'OK',
        'CANCEL' => 'CANCEL',
        'CONFIRM' => 'CONFIRM'
    ],
    'datatables' => [
        "sProcessing" => "جارٍ التحميل...",
        "sLengthMenu" => "أظهر _MENU_ مدخلات",
        "sZeroRecords" => "لم يعثر على أية سجلات",
        "sInfo" => "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
        "sInfoEmpty" => "يعرض 0 إلى 0 من أصل 0 سجل",
        "sInfoFiltered" => "(منتقاة من مجموع _MAX_ مُدخل)",
        "sInfoPostFix" => "",
        "sSearch" => "ابحث:",
        "sUrl" => "",
        "oPaginate" => [
            "sFirst" => "الأول",
            "sPrevious" => "السابق",
            "sNext" => "التالي",
            "sLast" => "الأخير"
        ]
    ],
    'admin-home'=>'Home',
    'faq-index'=>'Faq',
    'slider-index'=>' الاسليدر  ',
    'user-index'=>'  مسئولى الموقع ',
    'review-index'=>'  اراء العملاء ',
    'client-index'=>'   عملائنا   ',
    'service-index'=>' الخدمات  ',
    'article-index'=>' المقالات  ',
    'team-index'=>'  فريق العمل ',
    'page-index'=>'     الصفحات الثابته ',
    'category-index'=>'      اقسام المشاريع   ',
    'project-index'=>'        المشاريع ',



];
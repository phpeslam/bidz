<?php
return
    [
        'profile'=>'الصفحة الشخصية',

        'upload-error'=>'خطأ في التحميل',
        'mail-sent'=>'تم إرسال البريد الإلكتروني بنجاح',
        'your_activation_code'=>'رمز التنشيط الخاص بك هو',
        'your_activation_code_sent'=>'تم إرسال رمز التنشيط الخاص بك إلى الهاتف المحمول',
        'undefined'=>'غير معروف',
        'type-mobile-activation-code'=>'الرجاء كتابة رمز التنشيط الخاص بالجوال',
        'type-email-activation-code'=>'الرجاء إدخال رمز التفعيل ',
        'mobile-activated'=>'Mobile activated',
        'email-activated'=>'تم تنشيط البريد الإلكتروني ',
        'Unactivated-activation'=>'نظام تنشيط غير نشط ',
        'registered'=>'مسجّل بنجاح ',
        'email-link-sent'=>'link sent to your email',
        'Email-incorrect'=>'البريد الإلكتروني غير صحيح',
        'Password-required'=>'كلمة المرور مطلوبة ',
        'Password-email-required'=>'بريدك الإلكتروني أو كلمة المرور غير صحيحة ',
        'bid-add'=>'تمت إضافة "عرض التسعير بنجاح ',
        'wait-approve'=> 'تمت إضافة العرض بنجاح وينتظر الموافقة على العرض',
        'your-bidder'=> 'تم إرسال مزايدتك بنجاح',
        'error-bidding'=> 'خطأ ، حاول إرسال مقدم العرض الخاص بك مرة أخرى',
        'error-added'=> 'error , added-before',
        'added-follow'=> 'added to your follow successfully',
        'success-delete'=> 'تم الحذف بنجاح',
        'success-message'=> 'your ,message sent successfully',
        'error-message'=> 'error , try to send your message again',


        'member is provider he canot bid'=> ' العضو هو مقدم العرض الذي يمكنه تقديمه.',
        'member is owner this auction'=> 'العضو مالك هذا المزاد.',
        'auction not active'=> 'المزاد غير نشط.',
        'secc-bidder'=> 'secceed-bidder.',
        'please add balance'=> 'الرجاء إضافة رصيد.',
        'Auction is close'=> 'المزاد قريب.',
        'The :price least minimum_bid'=> 'The :price least minimum_bid',

    ];
<?php
return [
    'site_title' => '     Bidz',
    'complete_warning' => '  من فضلك قم باكمال الحقول   ',
    'edit' => ' تحرير',
    'delete' => 'حذف',
    'view' => 'عرض',
    'close' => 'اغلاق',
    'create' => 'انشاء جديد',
    'back' => 'رجوع',
    'save' => 'حـفظ',
    'action' => 'الاحداث',
    'created_at' => 'تاريخ الانشاء',
    'updated_at' => 'تاريخ التحديث',
    'status_not_allowed' => ' اختيار غير صحيح  ',
    'saved_successfully' => 'تم الحفظ بنجاح',
    'deleted_successfully' => ' تم الحذف بنجاح    ',
    'status_successfully_1' => '      تم النشر بنجاح    ',
    'status_successfully_2' => '      تم الغاء النشر بنجاح    ',
    'not_found_row' => '     سجل غير موجود ',
    'bootbox_delete_confirm' => ' هل تريد اتمام عملية الحذف ؟',
    'is_active'=>'  حالة التفعيل ',
    'is_active_0'=>'  غير مفعل ',
    'is_active_1'=>' مفعل  ',

    'logout'=>' تسجيل خروج  ',
    'welcome'=>'    مرحبا,  ',
    'profile'=>' ملفى الشخصى  ',

    'bootbox' => [
        'OK' => 'نعم',
        'CANCEL' => 'الغاء',
        'CONFIRM' => 'تاكيد'
    ],
    'datatables' => [
        "sProcessing" => "جارٍ التحميل...",
        "sLengthMenu" => "أظهر _MENU_ مدخلات",
        "sZeroRecords" => "لم يعثر على أية سجلات",
        "sInfo" => "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
        "sInfoEmpty" => "يعرض 0 إلى 0 من أصل 0 سجل",
        "sInfoFiltered" => "(منتقاة من مجموع _MAX_ مُدخل)",
        "sInfoPostFix" => "",
        "sSearch" => "ابحث:",
        "sUrl" => "",
        "oPaginate" => [
            "sFirst" => "الأول",
            "sPrevious" => "السابق",
            "sNext" => "التالي",
            "sLast" => "الأخير"
        ]
    ],
    'admin-home'=>'الرئيسية',
    'faq-index'=>'   الاسئلة الشائعة',
    'slider-index'=>' الاسليدر  ',
    'user-index'=>'  مسئولى الموقع ',
    'review-index'=>'  اراء العملاء ',
    'client-index'=>'   عملائنا   ',
    'service-index'=>' الخدمات  ',
    'article-index'=>' المقالات  ',
    'team-index'=>'  فريق العمل ',
    'page-index'=>'     الصفحات الثابته ',
    'category-index'=>'      اقسام المشاريع   ',
    'project-index'=>'        المشاريع ',



];
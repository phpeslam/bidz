<?php
return
[
    'module_title'=>'   الاسئلة الشائعة   ',
    'page_title_create'=>  'اضافة سؤال جديد   ',
    'page_title_edit'=>  '    تحرير سؤال   ',
    'title'=>'عنوان السؤال',
    'desc'=>'وصف السؤال',
    'validation_messages'=>[
        'title.*.required' =>'من فضلك قم بكتابة عنوان السؤال',
        'title.*.max'=>'اقصى عدد للاحرف هو 255 ',
        'desc.*.required' =>'من فضلك قم بكتابة وصف السؤال'
    ]
];
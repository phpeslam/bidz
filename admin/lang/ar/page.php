<?php
return
[
    'module_title'=>'    عنوان الصفحة     ',
    'page_title_create'=>  'اضافة صفحة جديدة   ',
    'page_title_edit'=>  '    تحرير الصفحة   ',
    'title'=>'عنوان الصفحة',
    'desc'=>'وصف الصفحة',
    'validation_messages'=>[
        'title.*.required' =>'من فضلك قم بكتابة    اسم الصفحة',
        'title.*.max'=>'اقصى عدد للاحرف هو 255 ',
        'desc.*.required' =>'من فضلك قم بكتابة وصف الصفحة'
    ]
];
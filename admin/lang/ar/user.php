<?php
return [
  'module_title'=>'مدراء الموقع',
  'page_title_create'=>'  مدير جديد',
  'page_title_edit'=>'تحرير مدير',
  'name'=>'الاسم',
   'current_password_msg'=>'كلمة المرور الحالية غير صحيحة',
   'current_password'=>'كلمة المرور الحاليه',
  'password'=>'كلمة المرور',
  'all'=>'       عرض جميع المدراء ',
  'masjed_haram'=>'       مشرف عام ',
  'masjed_nabawy'=>'        مشرف مقالات',
  'admin'=>'  مدير الموقع',
  'user_roles'=>' المجموعة',
  'password_confirm'=>'تاكيد كلمة المرور',
  'email'=>'البريد الالكترونى',

];
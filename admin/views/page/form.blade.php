@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $page_title }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            @include('parts.notification')
            <form class="form-horizontal" role="form" method="POST" action="{{ route('page-store') }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id" value="{{$id or ''}}"/>
                <div class="m-portlet__body">
                    <ul class="nav nav-tabs" role="tablist">
                        @foreach(getLanguages() as $code=>$lang)
                            <li class="nav-item">
                                <a class="nav-link @if($code == 'en')active  @endif" data-toggle="tab" href="#t{{$lang['id']}}">
                                    {{ $lang['text'] }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach(getLanguages() as $k=>$lang)
                            <div class="tab-pane @if($lang['id'] == '2')active  @endif" id="t{{$lang['id']}}"  role="tabpanel">
                                <div class="form-group {{ $errors->has('title.'.$lang['id']) ? ' has-error' : '' }}">
                                    <label class="col-sm-12 control-label no-padding-right" for="title{{$lang['id']}}">
                                        {{trans('page.title')}}
                                    </label>

                                    <div class="col-sm-12">
                                        <input type="text" dir="{{ $lang['direction'] }}" id="title{{$lang['id']}}"
                                               placeholder="{{trans('page.title')}}"  name="title[{{$lang['id']}}]"
                                               value="{{ old('title.'.$lang['id'],((isset($title[$lang['id']]))?$title[$lang['id']]:'')) }}"
                                               class="form-control" />
                                        @if ($errors->has('title.'.$lang['id']))
                                            <div class="help-block">
                                                <strong>{{ $errors->first('title.'.$lang['id']) }}</strong>
                                            </div>
                                        @endif
                                    </div>

                                </div>

                                <div class="form-group {{ $errors->has('desc.'.$lang['id']) ? ' has-error' : '' }}">
                                    <label class="col-sm-12 control-label no-padding-right" for="desc{{$lang['id']}}">
                                        {{trans('page.desc')}}
                                    </label>

                                    <div class="col-sm-12">
                                        <textarea dir="{{ $lang['direction'] }}" id="desc{{$lang['id']}}" placeholder="{{trans('page.desc')}}"  name="desc[{{$lang['id']}}]" class="form-control has-editor" >{{ old('desc.'.$lang['id'],((isset($desc[$lang['id']]))?$desc[$lang['id']]:'')) }}</textarea>
                                        @if ($errors->has('desc.'.$lang['id']))
                                            <div class="help-block">
                                                <strong>{{ $errors->first('desc.'.$lang['id']) }}</strong>
                                            </div>
                                        @endif
                                    </div>
                                </div>

                            </div>
                        @endforeach
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    {{trans('global.save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('page_footer')
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        $('textarea.has-editor').ckeditor();

    </script>
@endpush

@extends('layouts.master')
@section('page_title',trans('page.module_title'))

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{trans('page.module_title')}}
                    </h3>
                </div>
            </div>

        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">

                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..."
                                           id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{route('page-create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-plus"></i>
													<span>
														create new
													</span>
												</span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <div class="m_datatable" id="ajax_data"> </div>
            <!--end: Datatable -->
        </div>
    </div>
    {{--<div class="row">--}}
        {{--<div class="col-xs-12">--}}
            {{--<h3 class="header smaller lighter blue">--}}
                {{--{{trans('page.title')}}--}}
                {{--<a href="{{ route('page-create') }}" class="@if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif btn btn-mini btn-warning"><i class="fa fa-plus"></i> {{ trans('global.create') }}</a>--}}
            {{--</h3>--}}

            {{--<div>--}}
                {{--@include('parts.notification')--}}
                {{--<table id="pages-table" class="table table-bordered">--}}
                    {{--<thead>--}}

                    {{--<tr>--}}

                        {{--<th style="width: 20px;">#</th>--}}

                        {{--<th>{{trans('page.title')}}</th>--}}



                        {{--<th style="width: 200px;">{{trans('global.action')}}</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                {{--</table>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection

@push('page_footer')
<script type="text/javascript">
    var page_show = '{{route('page-show')}}';
</script>
<script type="text/javascript" src="{{asset('js/admin/page/index.js')}}"></script>
@endpush

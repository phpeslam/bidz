@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $page_title }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
                    @include('parts.notification')
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('BankTransaction-store') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" id="id" value="{{$id or ''}}"/>
                        <div class="m-portlet__body">
                            
                            <div class="tab-content">
                                
                                <div class="tab-pane active" id="t"  role="tabpanel">

                                    <div class="form-group {{ $errors->has('price') ? ' has-error' : '' }}">
                                        <label class="col-sm-12 control-label no-padding-right" for="title">
                                            price
                                        </label>

                                        <div class="col-sm-12">
                                            <input type="text" dir="" id="title"
                                                   placeholder="price"  name="price"
                                                   class="form-control " />
                                            @if ($errors->has('price'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('price') }}</strong>
                                                </div>
                                            @endif
                                        </div>

                                    </div>
                                    <input type='hidden' name='id' value='{{$id}}'>
                                    <!-- <div class="form-group {{ $errors->has('user_id') ? ' has-error' : '' }}">
                                        <label class="col-sm-12 control-label no-padding-right" for="title">
                                            User
                                        </label>

                                        <div class="col-sm-12">

                                                   <select class="form-control" name="user_id" id="user_id">
                                                        
                                                        @foreach(\App\Model\Member::where('is_active',1)->get() as $member)
                                                            <option value="{{ $member->id }}">{{ $member->first_name . ' ' . $member->last_name }}</option>
                                                        @endforeach
                                                    </select>

                                            @if ($errors->has('user_id'))
                                                <div class="help-block">
                                                    <strong>{{ $errors->first('user_id') }}</strong>
                                                </div>
                                            @endif
                                        </div>
                                    </div> -->

                                    

                                </div>
                                
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-save"></i>
                                                {{trans('global.save')}}
                                            </button>
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </form>
        </div>
    </div>
@endsection
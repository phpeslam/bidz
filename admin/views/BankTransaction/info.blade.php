@extends('layouts.master')
@section('page_title', $page_title)

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{$page_title}}
                    </h3>
                </div>

            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-section__content">
                <div class="row">
                    <div class="col-md-5">
                        <table class="table table-striped m-table">
                            <tbody>
                            <tr>
                                <th scope="row">
                                    type
                                </th>
                                <?php $type = array(
                                  '1' => 'Add Balance',
                                  '2' => 'Insurance',
                                  '3' => 'Special Ads',
                                );?>
                                <td>
                                    {{$type[$info->type]}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    date
                                </th>
                                <td>
                                    {{$info->date}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    User
                                </th>
                                <td>
                                    @if($info->userID)
                                        <?php $userdata = getMemperName($info->userID) ?>
                                        <a href="{{route('client-view',['id'=>$userdata->id])}}"
                                           title=""
                                           target="_blank"> {{ $userdata->first_name .' '. $userdata->last_name}}</a>
                                    @else
                                        Guest
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    value
                                </th>
                                <td>
                                    {{$info->value}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    auction
                                </th>
                                <td>
                                    {{$info->auctionID}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    Duration
                                </th>
                                <td>
                                    {{$info->duration}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    created at
                                </th>
                                <td>
                                    {{$info->create_at}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    type balance
                                </th>
                                <td>
                                    {{$info->type_balance}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    case balance
                                </th>
                                <td>
                                    {{$info->case_balance}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-7">
                        <img src="{{asset('storage/'.($info->image ? 'banktransaction/'.$info->image :'global/no_image_found.png'))}}" alt="image">
                    </div>
                </div>
            </div>
        </div>
        @if($info->case_balance !='deposited')
        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
            
                <a href="{{route('BankTransaction-active',['id'=>$id])}}" class="btn btn-info btn-sm" title="Active"><button ><i class="fa fa-save"></i>Active</button> </a>
                <a href="{{route('BankTransaction')}}" class="btn btn-danger btn-sm" title="Cencel"><button><i class="fa fa-bin"></i>Cancel</button> </a>
            </div>
        </div>
        @endif
    </div>
@endsection
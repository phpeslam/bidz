<a href="{{route('client-view',['id'=>$id])}}" class="btn btn-info btn-sm" title="{{trans('global.view')}}"><i class="fa fa-eye"></i> </a>
<a href="{{route('client-edit',['id'=>$id])}}" class="btn btn-success btn-sm" title="{{trans('global.edit')}}"><i class="fa fa-pencil-square-o"></i> </a>
<a href="{{route('client-delete',['id'=>$id])}}" class="btn btn-danger delete_row btn-sm " title="{{trans('global.delete')}}"><i class="fa fa-trash"></i>  </a>
<a href="{{route('BankTransaction-create',['id'=>$id])}}" class="btn btn-primary btn-sm " title="Add Balance"><i class="fa fa-plus-square"></i>  </a>
@php
$mem = \App\Model\Member::find($id);
@endphp
@if($membership_type == 2 && $mem->is_active == 0 )
<a href="{{route('active-client',['id'=>$id])}}" class="btn btn-primary btn-sm " title="Active"><i class="fa fa-user"></i>  </a>
@elseif($membership_type == 2 && $mem->is_active == 1 )
<a href="{{route('active-client',['id'=>$id])}}" class="btn btn-danger btn-sm " title="Active"><i class="fa fa-ban"></i>  </a>
@endif
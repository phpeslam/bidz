@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{ $page_title }}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            @include('parts.notification')

            @if(Session::has('message'))
                <p class="alert m-alert--default">
                    {{ Session::get('message') }}
                </p>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ route('client-add') }}">
                {{ csrf_field() }}
                <input type="hidden" name="id" id="id" value="{{$id or ''}}"/>
                <div class="m-portlet__body">
                    {{--<input type="hidden" value="{{ old('membership_type',1) }}" id="membership_type"--}}
                    {{--name=""/>--}}
                    <div class="form-group m-form__group row">
                        <label for="" class="col-2 col-form-label">
                            Type
                        </label>
                        <div class="col-4 m-checkbox-inline">

                            <label class="m-checkbox">
                                <input value="1" name="membership_type" type="radio" class="checkbox"
                                       @if(old('membership_type') == 1) checked="checked" @endif/>
                                client
                                <span></span>
                            </label>
                        </div>
                        <div class="col-4 m-checkbox-inline">
                            <label class="m-checkbox">
                                <input value="2" name="membership_type" type="radio" class="checkbox"
                                       @if(old('membership_type') == 2) checked="checked" @endif/>
                                agancy
                                <span></span>
                            </label>

                        </div>
                    </div>
                    <!-- Control -->
                    <div class="row">

                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('first_name') ? ' error' : '' }}">first name</label>
                            <input class="form-control {{ $errors->has('first_name') ? ' error' : '' }}" type="text"
                                   placeholder="first name" value="{{ old('first_name') }}" name="first_name"
                                   id="first_name"/>
                            @if ($errors->has('first_name'))
                                <div class="error">
                                    {{ $errors->first('first_name') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('last_name') ? ' error' : '' }}"> last name</label>
                            <input class="form-control {{ $errors->has('last_name') ? ' error' : '' }}" type="text"
                                   placeholder="last name" value="{{ old('last_name') }}" name="last_name"
                                   id="last_name"/>
                            @if ($errors->has('last_name'))
                                <div class="error">
                                    {{ $errors->first('last_name') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('email') ? ' error' : '' }}"> email </label>
                            <input class="form-control {{ $errors->has('email') ? ' error' : '' }}" type="email"
                                   placeholder="email" dir="ltr" value="{{ old('email') }}" name="email" id="email"/>
                            @if ($errors->has('email'))
                                <div class="error">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('email_confirmation') ? ' error' : '' }}"> email
                                confirmation</label>
                            <input class="form-control {{ $errors->has('email_confirmation') ? ' error' : '' }}"
                                   type="email" placeholder=" email confirmation"
                                   value="{{ old('email_confirmation') }}" dir="ltr" name="email_confirmation"
                                   id="email_confirmation"/>
                            @if ($errors->has('email_confirmation'))
                                <div class="error">
                                    {{ $errors->first('email_confirmation') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('password') ? ' error' : '' }}">password</label>
                            <input class="form-control {{ $errors->has('password') ? ' error' : '' }}" type="password"
                                   placeholder="password" name="password" id="password"/>
                            @if ($errors->has('password'))
                                <div class="error">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('password_confirmation') ? ' error' : '' }}"> password
                                confirmation</label>
                            <input class="form-control {{ $errors->has('password_confirmation') ? ' error' : '' }}"
                                   type="password" placeholder="password confirmation" name="password_confirmation"
                                   id="password_confirmation"/>
                            @if ($errors->has('password_confirmation'))
                                <div class="error">
                                    {{ $errors->first('password_confirmation') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="  col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('country_id') ? ' error' : '' }}">country</label>
                            <select class="form-control {{ $errors->has('country_id') ? ' error' : '' }}"
                                    name="country_id" id="country_id">
                                <option value="">select country</option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}"
                                            @if(old('country_id') == $country->id) selected="selected" @endif>{{ $country->currentLanguage->title }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country_id'))
                                <div class="error">
                                    {{ $errors->first('country_id') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('city_id') ? ' error' : '' }}">city</label>
                            <select class="form-control {{ $errors->has('city_id') ? ' error' : '' }}" name="city_id"
                                    id="city_id"
                                    @if(isset($cities) && count($cities)>0)  @else disabled="disabled" @endif>
                                <option value="">select city</option>
                                @if(isset($cities) && count($cities)>0)
                                    @foreach($cities as $city)
                                        <option value="{{ $city['id'] }}"
                                                @if(old('city_id') == $city['id']) selected="selected" @endif>{{ $city['title'] }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @if ($errors->has('city_id'))
                                <div class="error">
                                    {{ $errors->first('city_id') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('phone_number') ? ' error' : '' }}">phone number</label>
                            <input class="form-control {{ $errors->has('phone_number') ? ' error' : '' }}" type="tel"
                                   dir="ltr" placeholder="phone number" value="{{ old('phone_number') }}"
                                   name="phone_number" id="phone_number">
                            @if ($errors->has('phone_number'))
                                <div class="error">
                                    {{ $errors->first('phone_number') }}
                                </div>
                            @endif
                        </div>
                        <!-- Control -->
                        <div class="col-sm-12 col-md-6">
                            <label class=" {{ $errors->has('address') ? ' error' : '' }}">address</label>
                            <input class="form-control {{ $errors->has('address') ? ' error' : '' }}" type="text"
                                   placeholder="address" value="{{ old('address') }}" name="address" id="address">
                            @if ($errors->has('address'))
                                <div class="error">
                                    {{ $errors->first('address') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <br>

                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            {{trans('global.save')}}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@push('page_footer')
    <script type="text/javascript">
        $(function(){
            $('#country_id').change(function(){
                $('#city_id').html('').attr('disabled',true);
                var country_id = $(this).val();
                if(country_id != '') {
                    $.get('{{ route('front-register-cities') }}', {
                        country_id:country_id
                    },function(result){
                        if(result.length>0){
                            $('select#city_id').html('<option value="">select city</option>');
                            $.each(result,function (x,item) {
                                $('#city_id').append('<option value="'+item.id+'">'+item.title+'</option>');
                            });
                            $('#city_id').removeAttr('disabled');
                        }
                    },'json');
                }
            });
        });


    </script>
@endpush
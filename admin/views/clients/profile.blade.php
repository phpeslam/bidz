@extends('layouts.master')
@section('page_title', $page_title)

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                <i class="fa fa-user"></i> {{$page_title}}
                            </h3>
                        </div>
                        <span class="pull-right m-badge @if($info->membership_type == 1) m-badge--brand @else m-badge--success @endif m-badge--wide">@if($info->membership_type == 1)
                                Owner @else Agency @endif</span>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-section__content">
                        <table class="table table-striped m-table">
                            <tbody>
                            <tr>
                                <th scope="row">
                                    first name
                                </th>
                                <td>
                                    {{$info->first_name}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    last name
                                </th>
                                <td>
                                    {{$info->last_name}}
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    phone
                                </th>
                                <td>
                                    {{$info->phone_number}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    email
                                </th>
                                <td>
                                    {{$info->email}}
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    country
                                </th>
                                <td>
                                    {{getCountryName($info->country_id)}}
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    city
                                </th>
                                <td>
                                    {{getCityName($info->city_id)}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    address
                                </th>
                                <td>
                                    {{$info->address}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <!--begin:: Widgets/Last Updates-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__body">
                    <!--begin::widget 12-->
                    <div class="m-widget4">
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-interface-3"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														#User Auction
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
														{{ $countUserAuctions }}
													</span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-folder-4"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														#Follow Auctions
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
                                                    {{ $countFollowAuctions }}
													</span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-line-graph"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														Balance
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
														{{ $balance }}
													</span>
                            </div>
                        </div>
                        <!-- <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-diagram"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														A Programming Language
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
														+19
													</span>
                            </div>
                        </div> -->


                    </div>
                    <!--end::Widget 12-->
                </div>
            </div>
            <!--end:: Widgets/Last Updates-->
        </div>
    </div>
    <div class="row">
    @if($membership_type == 2)
    <div class="col-md-12">
        <img src="{{asset('/uploads/markets/'.$market_image)}}" alt="image">
    </div>
    @endif
    </div>
    <div class="m-portlet m-portlet--full-height ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Auctions
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm"
                    role="tablist">
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget5_tab1_content"
                           role="tab">
                            Active Auctions
                        </a>
                    </li>
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab2_content" role="tab">
                            closed Auctions
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin::Content-->
            <div class="tab-content">
                <div class="tab-pane active" id="m_widget5_tab1_content" aria-expanded="true">
                    <!--begin::m-widget5-->
                    <div class="m-widget5">

                        @foreach($activeAuctions as $auction)
                            <div class="m-widget5__item">
                                <div class="m-widget5__pic">
                                    <img class="m-widget7__img" src="{{$auction->image}}" alt="{{$auction->title}}">
                                </div>
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        <a href="{{route('auction-view',$auction->id)}}">{{$auction->title}}</a>
                                    </h4>

                                    <span class="m-widget5__desc">
															   {{$auction->description}}
															</span>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            <span class="m-badge {{($auction->bid_type == 1) ?  'm-badge--brand' : 'm-badge--warning'}} m-badge--wide"><b>Bide Type : {{($auction->bid_type == 1) ?  'Highest Price' : 'Client agree'}}</b></span>
                                        </span>
                                        <span class="m-widget5__info-label">
																	views :
																</span>
                                        <span class="m-widget5__info-date m--font-info">
																	<i class="fa fa-eye"></i> {{$auction->views ?? 0}}
																</span>
                                        <span class="m-widget5__info-label">
																	start at:
																</span>
                                        <span class="m-widget5__info-date m--font-info">
																{{$auction->start_at}}
																</span>
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
															<span class="m-widget5__number">
															   {{$auction->minimum_bid}}
															</span>
                                    <br>
                                    <span class="m-widget5__sales">
																minimum bid
															</span>
                                </div>
                                <div class="m-widget5__stats2">
															<span class="m-widget5__number">
																{{	$auction->bid_days}}
															</span><br>
                                    <span class="m-widget5__votes">bid days</span>
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <!--end::m-widget5-->
                </div>
                <div class="tab-pane" id="m_widget5_tab2_content" aria-expanded="false">
                    <!--begin::m-widget5-->
                    <div class="m-widget5">
                        @foreach($closedAuctions as $auction)
                            <div class="m-widget5__item">
                                <div class="m-widget5__pic">
                                    <img class="m-widget7__img" src="{{$auction->image}}" alt="{{$auction->title}}">
                                </div>
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        <a href="{{route('auction-view',$auction->id)}}">{{$auction->title}}</a>
                                    </h4>

                                    <span class="m-widget5__desc">
															   {{$auction->description}}
															</span>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            <span class="m-badge {{($auction->bid_type == 1) ?  'm-badge--brand' : 'm-badge--warning'}} m-badge--wide"><b>Bide Type : {{($auction->bid_type == 1) ?  'Highest Price' : 'Client agree'}}</b></span>
                                        </span>
                                        <span class="m-widget5__info-label">
																	views :
																</span>
                                        <span class="m-widget5__info-date m--font-info">
																	<i class="fa fa-eye"></i> {{$auction->views ?? 0}}
																</span>
                                        <span class="m-widget5__info-label">
																	start at:
																</span>
                                        <span class="m-widget5__info-date m--font-info">
																{{$auction->start_at}}
																</span>
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
															<span class="m-widget5__number">
															   {{$auction->minimum_bid}}
															</span>
                                    <br>
                                    <span class="m-widget5__sales">
																minimum bid
															</span>
                                </div>
                                <div class="m-widget5__stats2">
															<span class="m-widget5__number">
																{{	$auction->bid_days}}
															</span><br>
                                    <span class="m-widget5__votes">bid days</span>
                                </div>

                            </div>
                        @endforeach
                    </div>
                    <!--end::m-widget5-->
                </div>
            </div>
            <!--end::Content-->
        </div>
    </div>
@endsection
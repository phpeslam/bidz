@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ $page_title }}
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        @include('parts.notification')
        <form class="form-horizontal" role="form" method="POST" action="{{ route('partner-store') }}"  enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" id="id" value="{{$id or ''}}"/>
            <div class="m-portlet__body">
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-sm-12 control-label no-padding-right" for="name">
                        name
                    </label>
                    <div class="col-sm-12">
                        <input type="text" id="name"
                               placeholder="name"  name="name"
                               value="{{ old('name',((isset($name))?$name:'')) }}"
                               class="form-control" />
                        @if ($errors->has('name'))
                        <div class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                    <label class="col-sm-12 control-label no-padding-right" for="name">
                        Image
                    </label>
                    <div class="col-sm-12">
                        <input type="file" id="image"
                               placeholder="image"  name="image"
                               value=""
                               class="form-control" />
                        @if ($errors->has('image'))
                        <div class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i>
                        {{trans('global.save')}}
                    </button>
                </div>
            </div>
    </div>
</form>
</div>
</div>
@endsection



@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    {{ $page_title }} <a href="{{route('auction-view',['id'=>$info->auctionID])}}"></a>
                </h3>
            </div>
        </div>
    </div>
    <div class="m-portlet__body">
        @include('parts.notification')
        <form class="form-horizontal" role="form" method="POST" action="{{ route('adsSlider-store') }}" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="id" id="id" value="{{$info->id}}"/>
            <div class="m-portlet__body">
                <div class="row">
                    <div class="col-md-4">
                        <lable>Start date</lable>
                        <input type="date" dir="ltr" placeholder="" name="start"
                               value="{{ old('start') ?? $info->start_at }}"
                               class="form-control "/>
                        @if ($errors->has('start'))
                        <div class="help-block">
                            <strong>{{ $errors->first('start') }}</strong>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <lable>End date</lable>
                        <input type="date" dir="ltr" placeholder="" name="end"
                               value="{{ old('end') ?? $info->end_at }}"
                               class="form-control "/>
                        @if ($errors->has('end'))
                        <div class="help-block">
                            <strong>{{ $errors->first('end') }}</strong>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <lable>Active</lable>
                        <br>
                        <span class="m-switch m-switch--lg m-switch--icon">
                            <label>
                                <?php $active = $info->is_active; ?>
                                <input type="checkbox" name="switchActive" @if($active) checked="checked" @endif value="1" id="switch-active">
                                       <span></span>
                            </label>
                        </span>
                    </div>
                    <div class="col-md-4">
                        <lable>image</lable>
                        <input type="file" dir="ltr" placeholder="" name="image"
                               value=""
                               class="form-control "/>
                        @if ($errors->has('image'))
                        <div class="help-block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </div>
                        @endif
                    </div>
                </div>
                <br>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            {{trans('global.save')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection
@push('page_footer')
<script type="text/javascript">
    $(function () {
        $('#switch-active').on('click', function () {
            var attr = $(this).attr('checked');
            if (typeof attr !== typeof undefined && attr !== false) {
                $(this).removeAttr("checked");
            } else {
                $(this).attr('checked', 'checked');
            }
        });
    });
</script>
@endpush
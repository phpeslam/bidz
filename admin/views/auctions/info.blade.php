@extends('layouts.master')
@section('page_title', $page_title)

@section('content')
    <style>
        .red {
            color: #e40000;
            font-size: 21px;
        }

        .blue {
            color: #24a5f1;
            font-size: 21px;
        }
    </style>
    <div class="row">
        <div class="col-md-8">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Aucation : {{$page_title}}
                            </h3>
                        </div>
                        <span class="m-badge {{($info->bid_type == 1) ?  'm-badge--brand' : 'm-badge--warning'}}  m-badge--wide"><b>Bide Type : {{($info->bid_type == 1) ?  'Highest Price' : 'Client agree'}}</b></span>

                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-section__content">
                        <table class="table table-striped m-table">
                            <tbody>
                            <tr>
                                <th scope="row">
                                    Title
                                </th>
                                <td>
                                    {{$info->title}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    Description
                                </th>
                                <td>
                                    {{$info->description}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    User
                                </th>
                                <td>
                                    <?php $userdata = getMemperName($info->member_id) ?>
                                    <a href="{{route('client-view',['id'=>$userdata->id])}}"
                                       title=""
                                       target="_blank"> {{ $userdata->first_name .' '. $userdata->last_name}}</a>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    Minimum Bid
                                </th>
                                <td>
                                    {{$info->minimum_bid}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    Bid Days
                                </th>
                                <td>
                                    {{$info->bid_days}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    Start date
                                </th>
                                <td>
                                    {{$info->start_at}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    end date
                                </th>
                                <td>
                                    {{ date('Y-m-d H:i:s', strtotime($info->start_at. ' + '.$info->bid_days.'days'))}}
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <!--begin:: Widgets/Last Updates-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__body">
                    <!--begin::widget 12-->
                    <div class="m-widget4">
                    @if($info->isWork == 1)
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-profile"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														Ads
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
                                                        <a href="{{route('adsSlider-create',['id'=>$info->id])}}"
                                                          class="btn btn-sm btn-success" target="_blank"> add</a>
													</span>
                            </div>
                        </div>
                    @endif    
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-interface-3"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														Agancy
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
														  <?php $providerdata = getMemperName($info->provider_id) ?>
                                                        <a href="{{route('client-view',['id'=>$providerdata->id])}}"
                                                           target="_blank"> {{ $providerdata->first_name}}</a>
													</span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-folder-4"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
													provider accept
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
													<span class="fa @if($info->provider_accept )    fa-check-circle blue @else    fa-times-circle red @endif"></span>
													</span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-multimedia-2"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
													is active
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
                                                        <span class="fa @if($info->is_active )    fa-check-circle blue @else    fa-times-circle red @endif"></span>
													</span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-medical"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
														views
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
													{{$info->views ?? 0}}
													</span>
                            </div>
                        </div>
                        <div class="m-widget4__item">
                            <div class="m-widget4__ext">
													<span class="m-widget4__icon m--font-brand">
														<i class="flaticon-diagram"></i>
													</span>
                            </div>
                            <div class="m-widget4__info">
													<span class="m-widget4__text">
                                                         Current Price
													</span>
                            </div>
                            <div class="m-widget4__ext">
													<span class="m-widget4__number m--font-info">
													{{$info->views ?? 0}}
													</span>
                            </div>
                        </div>

                    </div>
                    <!--end::Widget 12-->
                </div>
            </div>
            <!--end:: Widgets/Last Updates-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="m-portlet m-portlet--mobile">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Specifications
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="m-section__content">
                        <div class="row">
                            <div class="col-md-6"><b>Car </b></div>
                            <div class="col-md-6">{{getMarkaName($info->category_id)}}</div>

                            <div class="col-md-6"><b>model </b></div>
                            <div class="col-md-6">{{getModelName($info->sub_category_id)}}</div>

                            <div class="col-md-6"><b>car type</b></div>
                            <div class="col-md-6">{{getCarTypeName($info->car_type_id)}}</div>

                            <div class="col-md-6"><b>color </b></div>
                            <div class="col-md-6">{{getColorName($info->car_color_id)}}</div>


                            <div class="col-md-6"><b>engine </b></div>
                            <div class="col-md-6">{{getEngineTypeName($info->engine_type_id)}}</div>

                            <div class="col-md-6"><b> engine size </b></div>
                            <div class="col-md-6">{{$info->engine_size}}</div>

                            <div class="col-md-6"><b> fuel </b></div>
                            <div class="col-md-6">{{getFuelName($info->fuel_id)}}</div>

                            <div class="col-md-6"><b>import </b></div>
                            <div class="col-md-6">{{getImportName($info->import_id)}}</div>

                            <div class="col-md-6"><b>motion vector </b></div>
                            <div class="col-md-6">{{getMotionVectorName($info->motion_vector_id)}}</div>

                            <div class="col-md-6"><b>specification </b></div>
                            <div class="col-md-6">{{getSpecificationName($info->specification_id)}}</div>

                            <div class="col-md-6"><b>manufacturing year </b></div>
                            <div class="col-md-6">{{$info->manufacturing_year}}</div>

                            <div class="col-md-6"><b>vaild form </b></div>
                            <div class="col-md-6"><span
                                        class="fa @if($info->vaild_form )    fa-check-circle blue @else    fa-times-circle red @endif"></span>
                            </div>

                            <div class="col-md-6"><b>periodic inspection</b></div>
                            <div class="col-md-6"><span
                                        class="fa @if($info->is_periodic_inspection)    fa-check-circle  blue @else fa-times-circle red @endif"></span>
                            </div>

                            <div class="col-md-6"><b>Mileagen </b></div>
                            <div class="col-md-6">{{$info->Mileage ?? 0}} </div>

                            <div class="col-md-6"><b>chassis no </b></div>
                            <div class="col-md-6">{{$info->chassis_no}} </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin::widget 12-->
        <div class="col-md-7">
            <!--begin:: Widgets/Last Updates-->
            <div class="m-portlet m-portlet--full-height ">
                <div class="m-portlet__body">
                    <div class="m-widget4">
                        <h4>images</h4>
                    </div>
                    @if(!empty($images))
                        @if(count($images) < 2)
                            <img src="{{$images[0]['image']}}" alt="">
                        @else

                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    @foreach($images as $image)
                                        <div class="carousel-item active">
                                            <img class="d-block w-100" src="{{$image['image']}}" alt="{{$info->title}}">
                                        </div>
                                    @endforeach
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                                   data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                                   data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        @endif
                    @endif


                </div>
            </div>
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Other Specifications
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="m-section__content">
                <div class="row">
                    @if (empty($OtherSpecification))
                        <P> there is no Other Specification data to show </P>
                    @endif
                    @foreach($OtherSpecification as $ospacific)
                        <div class="col-md-4"><span
                                    class="la la-check"></span> {{getOtherSpecificationName($ospacific['other_specification_id'])}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.master')
@section('page_title', $page_title)

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{$page_title}}
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                @if(Session::has('message'))
                    <p class="alert alert-info">
                        {{ Session::get('message') }}
                    </p>
                @endif
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">

                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..."
                                           id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <div class="m_datatable" id="ajax_data"> </div>
            <!--end: Datatable -->
        </div>
    </div>
@endsection


@push('page_footer')
    <script type="text/javascript">
        var auction_show = '{{route('auction-show',['active'=>$Auctions_Active,'pending' => $pending])}}';
    </script>
    <script type="text/javascript" src="{{asset('js/admin/auctions/index.js')}}"></script>
@endpush
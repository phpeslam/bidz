<a href="{{route('auction-view',['id'=>$id])}}" class="btn btn-info btn-sm" title="{{trans('global.view')}} "><i class="fa fa-eye"></i> </a>
<a href="{{route('auction-delete',['id'=>$id])}}" class="btn btn-danger delete_row btn-sm " title="{{trans('global.delete')}}"><i class="fa fa-trash"></i>  </a>

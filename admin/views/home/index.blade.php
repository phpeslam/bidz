@extends('layouts.master')
@section('page_title', '')

@section('content')

<div class="row">
    <div class="col-xl-3">
        <!--begin:: Widgets/Inbound Bandwidth-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 300px">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="javascript:void(0)" >Total bids</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Widget5-->
                <div class="m-widget20">
                    <div class="m-widget20__number" style="color:#010001">
                        {{$auctions}}
                    </div>
                    <div class="m-widget20__chart" style="height:160px;">
                        <canvas id="m_chart_bandwidth1"></canvas>
                    </div>
                </div>
                <!--end::Widget 5-->
            </div>
        </div>
    </div>

    <div class="col-xl-3">
        <!--begin:: Widgets/Inbound Bandwidth-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 300px">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="{{route('auctions-active')}}" >Active bids</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Widget5-->
                <div class="m-widget20">
                    <div class="m-widget20__number" style="color:#46C870">
                        {{$opened_auctions}}
                    </div>
                    <div class="m-widget20__chart" style="height:160px;">
                        <canvas id="m_chart_bandwidth3"></canvas>
                    </div>
                </div>
                <!--end::Widget 5-->
            </div>
        </div>
    </div>   

    <div class="col-xl-3">
        <!--begin:: Widgets/Inbound Bandwidth-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 300px">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="{{route('auctions-pending')}}" >Pending bids</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Widget5-->
                <div class="m-widget20">
                    <div class="m-widget20__number" style="color:#FCBD00">
                        {{$pending_auctions}}
                    </div>
                    <div class="m-widget20__chart" style="height:160px;">
                        <canvas id="m_chart_bandwidth4"></canvas>
                    </div>
                </div>
                <!--end::Widget 5-->
            </div>
        </div>
    </div>  
    <div class="col-xl-3">
        <!--begin:: Widgets/Inbound Bandwidth-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--half-height m-portlet--fit " style="min-height: 300px">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <a href="{{route('auctions-closed')}}" >Closed bids</a>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Widget5-->
                <div class="m-widget20">
                    <div class="m-widget20__number" style="color:#F25E73">
                        {{$closed_auctions}}
                    </div>
                    <div class="m-widget20__chart" style="height:160px;">
                        <canvas id="m_chart_bandwidth2"></canvas>
                    </div>
                </div>
                <!--end::Widget 5-->
            </div>
        </div>
    </div>  
</div>
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">

                    <div class="col-xl-6">
                        <!--begin:: Widgets/Profit Share-->
                        <div class="m-widget14">
                            <div class="m-widget14__header">
                                <h3 class="m-widget14__title">
                                    Memberships statistics
                                </h3>
                            </div>
                            <div class="row  align-items-center">
                                <div class="col">
                                    <div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
                                        <div class="m-widget14__stat">
                                            {{$total_members}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="m-widget14__legends">
                                        <div class="m-widget14__legend">
                                            <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                            <span class="m-widget14__legend-text">
                                                {{$admins}} Admins
                                            </span>
                                        </div>
                                        <div class="m-widget14__legend">
                                            <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                            <span class="m-widget14__legend-text">
                                                {{$partners}} Partners
                                            </span>
                                        </div>
                                        <div class="m-widget14__legend">
                                            <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                            <span class="m-widget14__legend-text">
                                                {{$members}} Members
                                            </span>
                                        </div>
                                        <div class="m-widget14__legend">
                                            <span class="m-widget14__legend-bullet m--bg-success"></span>
                                            <span class="m-widget14__legend-text">
                                                {{$markets}} Showrooms
                                            </span>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Profit Share-->
                    </div>
                    <div class="col-xl-6">
                        <!--begin:: Widgets/Profit Share-->
                        <div class="m-widget14">
                            <div class="m-widget14__header">
                                <h3 class="m-widget14__title">
                                    Ads & Payments
                                </h3>
                            </div>
                            <div class="row  align-items-center">
                                <div class="col">
                                    <div id="m_chart_profit_share2" class="m-widget14__chart" style="height: 160px">
                                        <div class="m-widget14__stat">
                                            {{$total}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="m-widget14__legends">
                                        <div class="m-widget14__legend">
                                            <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                            <span class="m-widget14__legend-text">
                                                {{$ads}} Ads
                                            </span>
                                        </div>
                                        <div class="m-widget14__legend">
                                            <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                            <span class="m-widget14__legend-text">
                                                {{$bank}} Bank transactions
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Profit Share-->
                    </div>
                </div>
            </div>
        </div>
        <!--End::Section-->
    </div>
@endsection

@push('page_footer')

@endpush

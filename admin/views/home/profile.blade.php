@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                    <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    {{$page_title}}
                </h3>
            </div>
        </div>
    </div>
    @include('parts.notification')
    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin-profile-save') }}"  >
        {{ csrf_field() }}
        <input type="hidden" name="id" id="id" value="{{$id or ''}}" />
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name" class="col-md-4 control-label">{{trans('user.name')}}</label>

            <div class="col-md-6">
                <input id="name" type="text" class="form-control" name="name" value="{{ \Auth::user()->name }}" required autofocus>

                @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">{{trans('user.email')}}</label>

            <div class="col-md-6">
                <input id="email" type="email" dir="ltr" class="form-control" name="email" value="{{ \Auth::user()->email }}" required>

                @if ($errors->has('email'))
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                @endif
            </div>
        </div>



        <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }}">
            <label for="current_password" class="col-md-4 control-label">{{trans('user.current_password')}}</label>

            <div class="col-md-6">
                <input id="current_password" type="password" class="form-control" name="current_password"  >

                @if ($errors->has('current_password'))
                <span class="help-block">
                    <strong>{{ $errors->first('current_password') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">{{trans('user.password')}}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password"  >

                @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group">
            <label for="password-confirm" class="col-md-4 control-label">{{trans('user.password_confirm')}}</label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  >
            </div>
        </div>


        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-save"></i>
                    {{trans('global.save')}}
                </button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
@endsection


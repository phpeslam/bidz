@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
<!--begin::Portlet-->
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <span class="m-portlet__head-icon m--hide">
                    <i class="la la-gear"></i>
                </span>
                <h3 class="m-portlet__head-text">
                    Update {{$row->key}}
                </h3>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right"  method="POST" action="{{ route('settings-store') }}" >
        <div class="m-portlet__body">
            {{ csrf_field() }}
            <input type="hidden" name="id" id="id" value="{{$row->id or ''}}" />
            <div class="form-group"></div>
            <div class="form-group m-form__group row">
                <label for="example-text-input" class="col-2 col-form-label">
                    Value
                </label>
                <div class="col-10">
                    <input class="form-control m-input" type="text" name="value"  id="value" value="{{ $row->value }}">
  
                </div>
            </div>

        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2"></div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-success">
                            Submit
                        </button>
                        <button type="reset" class="btn btn-secondary">
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
{{--<!--end::Portlet-->--}}
{{--<div class="row">--}}
{{--<div class="col-xs-12">--}}
{{--<h3 class="header smaller lighter blue">--}}
{{--{{ $page_title }}--}}
{{--<a href="{{route('user-index')}}" class="@if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif btn btn-mini btn-warning"><i class="fa fa-repeat"></i> {{trans('global.back')}} </a>--}}
{{--</h3>--}}

{{--<div>--}}
{{--@include('parts.notification')--}}
{{--<form class="form-horizontal" role="form" method="POST" action="{{ route('user-store') }}"  >--}}
{{--{{ csrf_field() }}--}}
{{--<input type="hidden" name="id" id="id" value="{{$id or ''}}" />--}}
{{--<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">--}}
{{--<label for="name" class="col-md-4 control-label">{{trans('user.name')}}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="name" type="text" class="form-control" name="name" value="{{ old('name',(isset($name))?$name:'') }}" required autofocus>--}}

{{--@if ($errors->has('name'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('name') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--<label for="email" class="col-md-4 control-label">{{trans('user.email')}}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="email" type="email" dir="ltr" class="form-control" name="email" value="{{ old('email',(isset($email))?$email:'') }}" required>--}}

{{--@if ($errors->has('email'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('email') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}


{{--<div class="form-group{{ $errors->has('user_roles') ? ' has-error' : '' }}">--}}
{{--<label for="user_roles" class="col-md-4 control-label">{{trans('user.user_roles')}}</label>--}}

{{--<div class="col-md-6">--}}
{{--@foreach($roles as $role)--}}
{{--<div class="checkbox-inline">--}}
{{--<label>--}}
{{--<input value="{{ $role->id }}" name="user_roles[{{ $role->id }}]"  type="checkbox" class="ace"  @if(isset($user_roles) && is_array($user_roles) && in_array($role->id ,$user_roles)) checked="checked" @endif/>--}}
{{--<span class="lbl"> {{ trans('user.'.$role['slug']) }}</span>--}}
{{--</label>--}}
{{--</div>--}}
{{--@endforeach--}}

{{--@if ($errors->has('user_roles'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('user_roles') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--<label for="password" class="col-md-4 control-label">{{trans('user.password')}}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="password" type="password" class="form-control" name="password"  @if(empty($id)) required @endif>--}}

{{--@if ($errors->has('password'))--}}
{{--<span class="help-block">--}}
{{--<strong>{{ $errors->first('password') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--<label for="password-confirm" class="col-md-4 control-label">{{trans('user.password_confirm')}}</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" @if(empty($id)) required @endif>--}}
{{--</div>--}}
{{--</div>--}}


{{--<div class="form-group">--}}
{{--<div class="col-md-6 col-md-offset-4">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--<i class="fa fa-save"></i>--}}
{{--{{trans('global.save')}}--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
@endsection


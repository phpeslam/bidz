@foreach($roles as $role)
    <span class="badge
@if($role['slug'] == 'masjed_haram')
            badge-purple
            @elseif($role['slug'] == 'masjed_nabawy')
            badge-pink
            @elseif($role['slug'] == 'admin')
            badge-info
            @else
            badge-danger
            @endif
">{{ trans('user.'.$role['slug']) }}</span>
    @endforeach
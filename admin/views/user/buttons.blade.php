<a href="{{route('user-edit',['id'=>$id])}}" class="btn btn-success btn-sm" title="{{trans('global.edit')}}"><i class="fa fa-edit"></i>  </a>
<a href="{{route('user-delete',['id'=>$id])}}" class="btn btn-danger delete_row btn-sm " title="{{trans('global.delete')}}"><i class="fa fa-trash"></i>  </a>

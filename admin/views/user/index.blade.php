@extends('layouts.master')
@section('page_title','users')

@section('content')

    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     Users List
                    </h3>
                </div>
            </div>

        </div>
        <div class="m-portlet__body">
            <!--begin: Search Form -->
            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                <div class="row align-items-center">
                    <div class="col-xl-8 order-2 order-xl-1">
                        <div class="form-group m-form__group row align-items-center">
                            <div class="col-md-4">
                                <div class="m-form__group m-form__group--inline">
                                    <div class="m-form__label">
                                        <label>
                                            Roles:
                                        </label>
                                    </div>
                                    <div class="m-form__control">
                                        <select class="form-control m-bootstrap-select" id="fl_role">
                                            <option value="">{{ trans('user.all') }}</option>
                                            @foreach($roles as $role)
                                                 <option value="{{ $role->slug }}">{{ trans('user.'.$role->slug) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="d-md-none m--margin-bottom-10"></div>
                            </div>
                            <div class="col-md-4">
                                <div class="m-input-icon m-input-icon--left">
                                    <input type="text" class="form-control m-input" placeholder="Search..."
                                           id="generalSearch">
                                    <span class="m-input-icon__icon m-input-icon__icon--left">
															<span>
																<i class="la la-search"></i>
															</span>
														</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                        <a href="{{route('user-create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-user"></i>
													<span>
														create new
													</span>
												</span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>
                </div>
            </div>
            <!--end: Search Form -->
            <!--begin: Datatable -->
            <div class="m_datatable" id="ajax_data"> </div>
            <!--end: Datatable -->
        </div>
    </div>

    {{--<div class="row">--}}
    {{--<div class="col-xs-12">--}}
    {{--<h3 class="header smaller lighter blue">--}}
    {{--{{trans('user.module_title')}}--}}

    {{--<a href="{{route('user-create')}}" class="@if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif btn btn-mini btn-warning"><i class="fa fa-plus"></i> {{trans('global.create')}}</a>--}}
    {{--</h3>--}}

    {{--<div>--}}
    {{--@include('parts.notification')--}}
    {{--<select id="fl_role" class="filter_field  col-sm-4 @if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif">--}}
    {{--<option value="">{{ trans('user.all') }}</option>--}}
    {{--@foreach($roles as $role)--}}
    {{--<option value="{{ $role->slug }}">{{ trans('user.'.$role->slug) }}</option>--}}
    {{--@endforeach--}}
    {{--</select>--}}
    {{--<br />--}}
    {{--<br />--}}
    {{--<table id="users-table" class="table table-bordered">--}}
    {{--<thead>--}}
    {{--<tr>--}}
    {{--<th>#</th>--}}

    {{--<th>{{trans('user.name')}}</th>--}}
    {{--<th>{{trans('user.email')}}</th>--}}
    {{--<th>{{trans('user.user_roles')}}</th>--}}
    {{--<th>{{trans('global.created_at')}}</th>--}}

    {{--<th>{{trans('global.action')}}</th>--}}
    {{--</tr>--}}
    {{--</thead>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

@endsection

@push('page_footer')
    <script type="text/javascript">
        var user_show = '{{route('user-show')}}';
    </script>
    <script type="text/javascript" src="{{asset('js/admin/user/index.js')}}"></script>
@endpush

@extends('layouts.inner')
@section('page_title',trans('category.module_title'))

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <h3 class="header smaller lighter blue">
                {{ trans('category.module_title') }}
                <a href="{{ route('category-create') }}" class="@if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif btn btn-mini btn-warning"><i class="fa fa-plus"></i> {{ trans('global.create') }}</a>
            </h3>

            <div>
                @include('parts.notification')
                <table id="categories-table" class="table table-bordered">
                    <thead>

                    <tr>

                        <th style="width: 20px;">#</th>

                        <th>{{trans('category.title')}}</th>
                        <th style="width: 100px;">{{trans('category.projects_count')}}</th>



                        <th style="width: 200px;">{{trans('global.action')}}</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

@endsection

@push('page_footer')
<script type="text/javascript">
    var category_show = '{{route('category-show')}}';
</script>
<script type="text/javascript" src="{{asset('js/admin/category/index.js')}}"></script>
@endpush

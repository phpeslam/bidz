@extends('layouts.inner')
@section('page_title',$page_title)

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h3 class="header smaller lighter blue">
                {{ $page_title }}
                <a href="{{route('category-index')}}" class="@if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif btn btn-mini btn-warning"><i class="fa fa-repeat"></i> {{trans('global.back')}} </a>
            </h3>

            <div>
                @include('parts.notification')
                <form class="form-horizontal" role="form" method="POST" action="{{ route('category-store') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" id="id" value="{{$id or ''}}"/>
                    <div id="tabs">
                        <ul>
                            @foreach(getLanguages() as $code=>$lang)
                                <li>
                                    <a href="#t{{$lang['id']}}">{{ $lang['text'] }}</a>
                                </li>
                            @endforeach
                        </ul>
                        @foreach(getLanguages() as $k=>$lang)
                            <div id="t{{$lang['id']}}">
                                <p>

                                <div class="form-group {{ $errors->has('title.'.$lang['id']) ? ' has-error' : '' }}">
                                    <label class="col-sm-2 control-label no-padding-right" for="title{{$lang['id']}}">
                                        {{trans('category.title')}}
                                    </label>

                                    <div class="col-sm-10">
                                        <input type="text" dir="{{ $lang['direction'] }}" id="title{{$lang['id']}}"
                                               placeholder="{{trans('category.title')}}"  name="title[{{$lang['id']}}]"
                                               value="{{ old('title.'.$lang['id'],((isset($title[$lang['id']]))?$title[$lang['id']]:'')) }}"
                                               class="col-xs-12 col-sm-9" />
                                        @if ($errors->has('title.'.$lang['id']))
                                            <div class="help-block">
                                        <strong>{{ $errors->first('title.'.$lang['id']) }}</strong>
                                    </div>
                                        @endif
                                    </div>

                                </div>




                                </p>
                            </div>
                        @endforeach
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-save"></i>
                                {{trans('global.save')}}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
@push('page_header')
    <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.min.css')}}" />
@endpush
@push('page_footer')
    <!-- page specific plugin scripts -->
    <script src="{{asset('assets/js/jquery-ui.min.js')}}"></script>

    <script type="text/javascript">


        $( "#tabs" ).tabs({
            active: 't{{ getCurrentLang('id') }}'
        });

    </script>
@endpush

<a href="{{route('category-edit',['id'=>$id])}}" class="btn btn-success btn-sm btn-mini" title="{{trans('global.edit')}}"><i class="fa fa-edit"></i> </a>
<a href="{{route('category-delete',['id'=>$id])}}" class="btn btn-danger delete_row btn-sm btn-mini " title="{{trans('global.delete')}}"><i class="fa fa-trash"></i>  </a>


@if($is_active==1)
    <a href="{{route('category-status',['id'=>$id,'status'=>2])}}" class="btn btn-mini btn-info status_row btn-sm " title="{{trans('global.is_active_1')}}"><i class="fa fa-eye"></i>  </a>
@elseif($is_active==2)
    <a href="{{route('category-status',['id'=>$id,'status'=>1])}}" class="btn btn-mini btn-warning  status_row btn-sm " title="{{trans('global.is_active_0')}}"><i class="fa fa-eye-slash"></i>  </a>
@else
    <a href="{{route('category-status',['id'=>$id,'status'=>1])}}" class="btn btn-mini btn-info status_row btn-sm " title="{{trans('global.is_active_1')}}"><i class="fa fa-eye"></i>  </a>
    <a href="{{route('category-status',['id'=>$id,'status'=>2])}}" class="btn btn-mini btn-warning status_row btn-sm " title="{{trans('global.is_active_0')}}"><i class="fa fa-eye-slash"></i>  </a>
@endif
<!-- BEGIN: Aside Menu -->
<div
    id="m_ver_menu"
    class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
    data-menu-vertical="true"
    data-menu-scrollable="false" data-menu-dropdown-timeout="500"
    >
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item  @if(Route::currentRouteName()=='admin-home') m-menu__item--active @endif"
            aria-haspopup="true">
            <a href="{{ route('admin-home') }}" class="m-menu__link ">
                <i class="m-menu__link-icon  flaticon-dashboard"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Dashboard
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item m-menu__item--submenu @if(in_array(Route::currentRouteName(),[
            'auctions-active',
            'auctions-pending',
            'auctions-closed',
            'auction-view'
            ])) m-menu__item--expanded  m-menu__item--open @endif" aria-haspopup="true" data-menu-submenu-toggle="hover">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-analytics"></i>
                <span class="m-menu__link-text">
                    Auctions
                </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item @if(Route::currentRouteName()=='auctions-active') m-menu__item--active @endif"
                        aria-haspopup="true">
                        <a href="{{ route('auctions-active') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Active
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item @if(Route::currentRouteName()=='auctions-pending') m-menu__item--active @endif"
                        aria-haspopup="true">
                        <a href="{{ route('auctions-pending') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Pending
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item @if(Route::currentRouteName()=='auctions-closed') m-menu__item--active @endif"
                        aria-haspopup="true">
                        <a href="{{ route('auctions-closed') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Closed
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item @if(in_array(Route::currentRouteName(),[
            'client-index',
            'client-show',
            'client-view',
            'client-create',
            'client-edit',
            'client-delete',
            'client-store',
            ])) m-menu__item--active  @endif" aria-haspopup="true">
            <a href="{{ route('client-index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-users"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Clients
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item  @if(Route::currentRouteName()=='BankTransaction') m-menu__item--active @endif"
            aria-haspopup="true">
            <a href="{{ route('BankTransaction') }}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-coins"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Bank Transfer
                        </span>
                    </span>
                </span>
            </a>
        </li>

        <li class="m-menu__item m-menu__item--submenu  @if(in_array(Route::currentRouteName(),[
            'ads-index',
            'ads-show ',
            'ads-create',
            'ads-edit',
            'ads-delete',
            'ads-store',
            'adsSlider-index',
            'adsSlider-show ',
            'adsSlider-create',
            'adsSlider-edit',
            'adsSlider-delete',
            'adsSlider-store',
            ]))m-menu__item--expanded m-menu__item--open @endif"
            aria-haspopup="true" data-menu-submenu-toggle="hover">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-puzzle"></i>
                <span class="m-menu__link-text">
                    Ads
                </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item @if(in_array(Route::currentRouteName(),[
                        'adsSlider-index',
                        'adsSlider-show ',
                        'adsSlider-create',
                        'adsSlider-edit',
                        'adsSlider-delete',
                        'adsSlider-store',
                        ])) m-menu__item--active @endif"
                        aria-haspopup="true">
                        <a href="{{ route('adsSlider-index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                slider
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item @if(in_array(Route::currentRouteName(),[
                        'ads-index',
                        'ads-show ',
                        'ads-create',
                        'ads-edit',
                        'ads-delete',
                        'ads-store',
                        ])) m-menu__item--active @endif"
                        aria-haspopup="true">
                        <a href="{{ route('ads-index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                static
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>


        <li class="m-menu__item @if(in_array(Route::currentRouteName(),[
            'faq-index',
            'faq-show ',
            'faq-create',
            'faq-edit',
            'faq-delete',
            'faq-store',
            ])) m-menu__item--active  @endif" aria-haspopup="true">
            <a href="{{ route('faq-index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-questions-circular-button"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            FAQ
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item @if(in_array(Route::currentRouteName(),[
            'page-index',
            'page-show ',
            'page-create',
            'page-edit',
            'page-delete',
            'page-store',
            ])) m-menu__item--active  @endif" aria-haspopup="true">
            <a href="{{ route('page-index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-imac"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Pages
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item  @if(Route::currentRouteName()=='contact-us') m-menu__item--active @endif"
            aria-haspopup="true">
            <a href="{{ route('contact-us') }}" class="m-menu__link ">
                <i class="m-menu__link-icon  flaticon-map-location"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Contact US
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item @if(in_array(Route::currentRouteName(),[
            'user-index',
            'user-show ',
            'user-create',
            'user-edit',
            'user-delete',
            'user-store',
            ])) m-menu__item--active  @endif" aria-haspopup="true">
            <a href="{{ route('user-index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-users"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Administrators
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item  @if(Route::currentRouteName()=='settings-index') m-menu__item--active @endif"
            aria-haspopup="true">
            <a href="{{ route('settings-index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon  flaticon-cogwheel"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Settings
                        </span>
                    </span>
                </span>
            </a>
        </li>
        <li class="m-menu__item  @if(Route::currentRouteName()=='partner-index') m-menu__item--active @endif"
            aria-haspopup="true">
            <a href="{{ route('partner-index') }}" class="m-menu__link ">
                <i class="m-menu__link-icon fa fa-users"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Partners
                        </span>
                    </span>
                </span>
            </a>
        </li>        
        <li class="m-menu__item " aria-haspopup="true">
            <a href="{{ route('logout') }}" class="m-menu__link " onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                <i class="m-menu__link-icon flaticon-logout"></i>
                <span class="m-menu__link-title">
                    <span class="m-menu__link-wrap">
                        <span class="m-menu__link-text">
                            Logout
                        </span>
                    </span>
                </span>
            </a>
        </li>
    </ul>
</div>
{{--<!-- END: Aside Menu -->--}}
{{--<div id="sidebar" class="sidebar                  responsive                    ace-save-state">--}}
{{--<script type="text/javascript">--}}
{{--try{ace.settings.loadState('sidebar')}catch(e){}--}}
{{--</script>--}}

{{--<div class="sidebar-shortcuts" id="sidebar-shortcuts">--}}
{{--<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">--}}
{{--<a href="{{ route('admin-home') }}" class="btn btn-success">--}}
{{--<i class="ace-icon fa fa-tachometer"></i>--}}
{{--</a>--}}

{{--<a class="btn btn-info">--}}
{{--<i class="ace-icon fa fa-pencil"></i>--}}
{{--</a>--}}

{{--<a  title="{{ trans('global.profile') }}" href="{{ route('admin-profile') }}" class="btn btn-warning">--}}
{{--<i class="ace-icon fa fa-user"></i>--}}
{{--</a>--}}

{{--<a title="{{ trans('global.logout') }}" href="{{ route('logout') }}"--}}
{{--onclick="event.preventDefault();--}}
{{--document.getElementById('logout-form').submit();" class="btn btn-danger">--}}
{{--<i class="ace-icon fa fa-power-off"></i>--}}
{{--</a>--}}
{{--</div>--}}

{{--<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">--}}
{{--<span class="btn btn-success"></span>--}}

{{--<span class="btn btn-info"></span>--}}

{{--<span class="btn btn-warning"></span>--}}

{{--<span class="btn btn-danger"></span>--}}
{{--</div>--}}
{{--</div><!-- /.sidebar-shortcuts -->--}}

{{--<ul class="nav nav-list">--}}
{{--<li class="@if(Route::currentRouteName()=='admin-home') active @endif">--}}
{{--<a href="{{ route('admin-home') }}">--}}
{{--<i class="menu-icon fa fa-tachometer"></i>--}}
{{--<span class="menu-text"> {{ trans('global.admin-home') }} </span>--}}
{{--</a>--}}

{{--<b class="arrow"></b>--}}
{{--</li>--}}

{{--<li class="@if(in_array(Route::currentRouteName(),[--}}
{{--'user-index',--}}
{{--'user-show ',--}}
{{--'user-create',--}}
{{--'user-edit',--}}
{{--'user-delete',--}}
{{--'user-store',--}}
{{--])) active  @endif">--}}
{{--<a href="{{ route('user-index') }}">--}}
{{--<i class="menu-icon fa fa-user"></i>--}}
{{--<span class="menu-text"> {{ trans('global.user-index') }} </span>--}}
{{--</a>--}}

{{--<b class="arrow"></b>--}}
{{--</li>--}}


{{--<li class="@if(in_array(Route::currentRouteName(),[--}}
{{--'faq-index',--}}
{{--'faq-show ',--}}
{{--'faq-create',--}}
{{--'faq-edit',--}}
{{--'faq-delete',--}}
{{--'faq-store',--}}
{{--])) active  @endif">--}}
{{--<a href="{{ route('faq-index') }}">--}}
{{--<i class="menu-icon fa fa-question"></i>--}}
{{--<span class="menu-text"> {{ trans('global.faq-index') }} </span>--}}
{{--</a>--}}

{{--<b class="arrow"></b>--}}
{{--</li>--}}

{{--<li class="@if(in_array(Route::currentRouteName(),[--}}
{{--'page-index',--}}
{{--'page-show ',--}}
{{--'page-create',--}}
{{--'page-edit',--}}
{{--'page-delete',--}}
{{--'page-store',--}}
{{--])) active  @endif">--}}
{{--<a href="{{ route('page-index') }}">--}}
{{--<i class="menu-icon fa fa-apple"></i>--}}
{{--<span class="menu-text"> {{ trans('global.page-index') }} </span>--}}
{{--</a>--}}

{{--<b class="arrow"></b>--}}
{{--</li>--}}


{{--<li class="@if(in_array(Route::currentRouteName(),[--}}
{{--'category-index',--}}
{{--'category-show ',--}}
{{--'category-create',--}}
{{--'category-edit',--}}
{{--'category-delete',--}}
{{--'category-store',--}}
{{--])) active  @endif">--}}
{{--<a href="{{ route('category-index') }}">--}}
{{--<i class="menu-icon fa fa-certificate"></i>--}}
{{--<span class="menu-text"> {{ trans('global.category-index') }} </span>--}}
{{--</a>--}}

{{--<b class="arrow"></b>--}}
{{--</li>--}}

{{--<li class="@if(in_array(Route::currentRouteName(),[--}}
{{--'project-index',--}}
{{--'project-show ',--}}
{{--'project-create',--}}
{{--'project-edit',--}}
{{--'project-delete',--}}
{{--'project-store',--}}
{{--])) active  @endif">--}}
{{--<a href="{{ route('project-index') }}">--}}
{{--<i class="menu-icon fa fa-sellsy"></i>--}}
{{--<span class="menu-text"> {{ trans('global.project-index') }} </span>--}}
{{--</a>--}}

{{--<b class="arrow"></b>--}}
{{--</li>--}}


{{--</ul><!-- /.nav-list -->--}}

{{--<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">--}}
{{--<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>--}}
{{--</div>--}}
{{--</div>--}}

@if(\Session::has('alert-success'))
<div class="alert alert-success">
    <strong>نجاح العميلة!</strong>  {{ Session::get('alert-success') }}
</div>
@endif

@if(\Session::has('alert-info'))
    <div class="alert alert-info">
        <strong>معلومات!</strong>  {{ Session::get('alert-info') }}
    </div>
@endif

@if(\Session::has('alert-warning'))
    <div class="alert alert-warning">
        <strong>تحذير!</strong>  {{ Session::get('alert-warning') }}
    </div>
@endif

@if(\Session::has('alert-danger'))
    <div class="alert alert-danger">
        <strong>خطا!</strong>  {{ Session::get('alert-danger') }}
    </div>
@endif



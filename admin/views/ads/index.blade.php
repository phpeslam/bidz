@extends('layouts.master')
@section('page_title',$page_title)

@section('content')
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        {{$page_title}}
                    </h3>
                </div>
            </div>

            <div class="" style="margin: 20px;float: right;">
                        <a href="{{route('ads-create')}}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
												<span>
													<i class="la la-user"></i>
													<span>
														create new
													</span>
												</span>
                        </a>
                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                    </div>

        </div>
        <div class="m-portlet__body">
            <!--begin: Datatable -->
            <div class="m_datatable" id="ajax_data"> </div>
            <!--end: Datatable -->
        </div>
    </div>
@endsection

@push('page_footer')
    <script type="text/javascript">
        var ads_show = '{{route('ads-show')}}';
    </script>
    <script type="text/javascript" src="{{asset('js/admin/ads/index.js')}}"></script>
@endpush
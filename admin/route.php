<?php

Route::get('/', 'HomeController@index')->name('admin-home');
Route::group(['prefix' => 'contact'], function () {
    Route::get('/', 'ContactController@index')->name('contact-us');
    Route::get('show', 'ContactController@show')->name('contact-show');
    Route::get('view', 'ContactController@view')->name('contact-view');
    Route::get('delete/{id}', 'ContactController@delete')->name('contact-delete');
});
Route::group(['prefix' => 'BankTransaction'], function () {
    Route::get('/', 'BankTransactionController@index')->name('BankTransaction');
    Route::get('show', 'BankTransactionController@show')->name('BankTransaction-show');
    Route::get('view/{id}', 'BankTransactionController@view')->name('BankTransaction-view');
    Route::get('delete/{id}', 'BankTransactionController@delete')->name('BankTransaction-delete');

    Route::get('active/{id}', 'BankTransactionController@active')->name('BankTransaction-active');

   // Route::get('create', 'BankTransactionController@create')->name('BankTransaction-create');
   // Route::post('store', 'BankTransactionController@store')->name('BankTransaction-store');
});
    

Route::get('/bankTransfer', 'ContactController@index')->name('bank-transfer');

Route::get('/profile', 'HomeController@profile')->name('admin-profile');
Route::post('/profile/save', 'HomeController@profileSave')->name('admin-profile-save');
Route::group(['prefix' => 'users'], function () {
    Route::get('/', 'UserController@index')->name('user-index');
    Route::get('show', 'UserController@show')->name('user-show');
    Route::get('create', 'UserController@create')->name('user-create');
    Route::get('edit/{id}', 'UserController@edit')->name('user-edit');
    Route::get('delete/{id}', 'UserController@delete')->name('user-delete');
    Route::post('store', 'UserController@store')->name('user-store');
});


Route::group(['prefix' => 'faq'], function () {
    Route::get('/', 'FaqController@index')->name('faq-index');
    Route::get('show', 'FaqController@show')->name('faq-show');
    Route::get('create', 'FaqController@create')->name('faq-create');
    Route::get('edit/{id}', 'FaqController@edit')->name('faq-edit');
    Route::get('delete/{id}', 'FaqController@delete')->name('faq-delete');
    Route::get('status/{id}/{status}', 'FaqController@status')->name('faq-status');
    Route::post('store', 'FaqController@store')->name('faq-store');
});
Route::group(['prefix' => 'ads'], function () {
    Route::get('/', 'AdsController@index')->name('ads-index');
    Route::get('show', 'AdsController@show')->name('ads-show');
    Route::get('create', 'AdsController@create')->name('ads-create');
    Route::get('edit/{id}', 'AdsController@edit')->name('ads-edit');
    Route::get('delete/{id}', 'AdsController@delete')->name('ads-delete');
    Route::get('status/{id}/{status}', 'AdsController@status')->name('ads-status');
    Route::post('store', 'AdsController@store')->name('ads-store');
});

Route::group(['prefix' => 'AdsSlider'], function () {
    Route::get('/', 'AdsSliderController@index')->name('adsSlider-index');
    Route::get('show', 'AdsSliderController@show')->name('adsSlider-show');
    Route::get('create/{id}', 'AdsSliderController@create')->name('adsSlider-create');
    Route::get('edit/{id}', 'AdsSliderController@edit')->name('adsSlider-edit');
    Route::get('delete/{id}', 'AdsSliderController@delete')->name('adsSlider-delete');
    Route::get('status/{id}/{status}', 'AdsSliderController@status')->name('adsSlider-status');
    Route::post('store', 'AdsSliderController@store')->name('adsSlider-store');
});


Route::group(['prefix' => 'page'], function () {
    Route::get('/', 'PageController@index')->name('page-index');
    Route::get('show', 'PageController@show')->name('page-show');
    Route::get('create', 'PageController@create')->name('page-create');
    Route::get('edit/{id}', 'PageController@edit')->name('page-edit');
    Route::get('delete/{id}', 'PageController@delete')->name('page-delete');
    Route::get('status/{id}/{status}', 'PageController@status')->name('page-status');
    Route::post('store', 'PageController@store')->name('page-store');
});


Route::group(['prefix' => 'articles'], function () {
    Route::get('/', 'ArticleController@index')->name('article-index');
    Route::get('show', 'ArticleController@show')->name('article-show');
    Route::get('create', 'ArticleController@create')->name('article-create');
    Route::get('edit/{id}', 'ArticleController@edit')->name('article-edit');
    Route::get('delete/{id}', 'ArticleController@delete')->name('article-delete');
    Route::get('status/{id}/{status}', 'ArticleController@status')->name('article-status');
    Route::post('store', 'ArticleController@store')->name('article-store');
});


Route::group(['prefix' => 'teams'], function () {
    Route::get('/', 'TeamController@index')->name('team-index');
    Route::get('show', 'TeamController@show')->name('team-show');
    Route::get('create', 'TeamController@create')->name('team-create');
    Route::get('edit/{id}', 'TeamController@edit')->name('team-edit');
    Route::get('delete/{id}', 'TeamController@delete')->name('team-delete');
    Route::get('status/{id}/{status}', 'TeamController@status')->name('team-status');
    Route::post('store', 'TeamController@store')->name('team-store');
});

Route::group(['prefix' => 'sliders'], function () {
    Route::get('/', 'SliderController@index')->name('slider-index');
    Route::get('show', 'SliderController@show')->name('slider-show');
    Route::get('create', 'SliderController@create')->name('slider-create');
    Route::get('edit/{id}', 'SliderController@edit')->name('slider-edit');
    Route::get('delete/{id}', 'SliderController@delete')->name('slider-delete');
    Route::get('status/{id}/{status}', 'SliderController@status')->name('slider-status');
    Route::post('store', 'SliderController@store')->name('slider-store');
});

Route::group(['prefix' => 'services'], function () {
    Route::get('/', 'ServiceController@index')->name('service-index');
    Route::get('show', 'ServiceController@show')->name('service-show');
    Route::get('create', 'ServiceController@create')->name('service-create');
    Route::get('edit/{id}', 'ServiceController@edit')->name('service-edit');
    Route::get('delete/{id}', 'ServiceController@delete')->name('service-delete');
    Route::get('status/{id}/{status}', 'ServiceController@status')->name('service-status');
    Route::post('store', 'ServiceController@store')->name('service-store');
});

Route::group(['prefix' => 'reviews'], function () {
    Route::get('/', 'ReviewController@index')->name('review-index');
    Route::get('show', 'ReviewController@show')->name('review-show');
    Route::get('create', 'ReviewController@create')->name('review-create');
    Route::get('edit/{id}', 'ReviewController@edit')->name('review-edit');
    Route::get('delete/{id}', 'ReviewController@delete')->name('review-delete');
    Route::get('status/{id}/{status}', 'ReviewController@status')->name('review-status');
    Route::post('store', 'ReviewController@store')->name('review-store');
});

Route::group(['prefix' => 'clients'], function () {
    Route::get('/', 'ClientController@index')->name('client-index');
    Route::get('show', 'ClientController@show')->name('client-show');
    Route::get('view/{id}', 'ClientController@view')->name('client-view');
    Route::get('create', 'ClientController@create')->name('client-create');
    Route::get('edit/{id}', 'ClientController@edit')->name('client-edit');
    Route::get('delete/{id}', 'ClientController@delete')->name('client-delete');
    Route::get('status/{id}/{status}', 'ClientController@status')->name('client-status');
    Route::post('store', 'ClientController@store')->name('client-add');
    Route::post('update', 'ClientController@updateinfo')->name('client-update');

    Route::get('addbalance/create/{id}', 'BankTransactionController@create')->name('BankTransaction-create');
    Route::post('addbalance/store', 'BankTransactionController@store')->name('BankTransaction-store');
    Route::get('active/client/{id}', 'ClientController@activate')->name('active-client');
});

Route::group(['prefix' => 'categories'], function () {
    Route::get('/', 'CategoryController@index')->name('category-index');
    Route::get('show', 'CategoryController@show')->name('category-show');
    Route::get('create', 'CategoryController@create')->name('category-create');
    Route::get('edit/{id}', 'CategoryController@edit')->name('category-edit');
    Route::get('delete/{id}', 'CategoryController@delete')->name('category-delete');
    Route::get('status/{id}/{status}', 'CategoryController@status')->name('category-status');
    Route::post('store', 'CategoryController@store')->name('category-store');
});
Route::group(['prefix' => 'projects'], function () {
    Route::get('/', 'ProjectController@index')->name('project-index');
    Route::get('show', 'ProjectController@show')->name('project-show');
    Route::get('create', 'ProjectController@create')->name('project-create');
    Route::get('edit/{id}', 'ProjectController@edit')->name('project-edit');
    Route::get('delete/{id}', 'ProjectController@delete')->name('project-delete');
    Route::get('status/{id}/{status}', 'ProjectController@status')->name('project-status');
    Route::post('store', 'ProjectController@store')->name('project-store');
    Route::group(['prefix' => 'sub/{project_id}'], function () {
        Route::get('/', 'ProjectController@showImages')->name('project-show-sub-sub');
        Route::get('delete/{id}', 'ProjectController@deleteImages')->name('project-delete-sub-sub');
        Route::post('store', 'ProjectController@storeImages')->name('project-store-sub-sub');
    });
});

Route::group(['prefix' => 'auctions'], function () {
    Route::get('active', 'AuctionsController@active')->name('auctions-active');
    Route::get('pending', 'AuctionsController@pending')->name('auctions-pending');
    Route::get('closed', 'AuctionsController@closed')->name('auctions-closed');
    Route::get('show/{active}/{pending}', 'AuctionsController@show')->name('auction-show');
    Route::get('view/{id}', 'AuctionsController@view')->name('auction-view');
    Route::get('create', 'AuctionsController@create')->name('auction-create');
    Route::get('edit/{id}', 'AuctionsController@edit')->name('auction-edit');
    Route::get('delete/{id}', 'AuctionsController@delete')->name('auction-delete');
    Route::get('status/{id}/{status}', 'AuctionsController@status')->name('auction-status');
    Route::post('store', 'AuctionsController@store')->name('auction-store');
});

Route::group(['prefix' => 'settings'], function () {
    Route::get('/', 'SettingsController@index')->name('settings-index');
    Route::get('show', 'SettingsController@show')->name('settings-show');
    Route::get('edit/{id}', 'SettingsController@edit')->name('settings-edit');
    Route::post('store', 'SettingsController@store')->name('settings-store');
});

Route::group(['prefix' => 'partners'], function () {
    Route::get('/', 'PartnersController@index')->name('partner-index');
    Route::get('show', 'PartnersController@show')->name('partner-show');
    Route::get('create', 'PartnersController@create')->name('partner-create');
    Route::post('store', 'PartnersController@store')->name('partner-store');
    Route::get('delete/{id}', 'PartnersController@delete')->name('partner-delete');
});

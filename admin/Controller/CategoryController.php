<?php

namespace Admin\Controller;

use App\Model\Category;
use App\Model\CategoryLanguage;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CategoryController extends AdminController
{



    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $data['datatables'] = true;

        return view('category.index',$data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables)
    {
        return $datatables->eloquent(Category::query()
            ->with('currentLanguage') ->withCount('projects') )

            ->addColumn('action', 'category.buttons')
            ->filter(function ($query){
                $search = app('request')->get('search',[]);
                if(array_key_exists('value',$search)){
                    $value = trim($search['value']);
                    if(!empty($value)) {
                        $query->whereHas('currentLanguage',function($query) use($value){
                            $query->where('title','like','%'.$value.'%') ;
                        })

                        ;
                    }
                }
            })
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = [];
        $data['page_title'] = trans('category.page_title_create');
        return view('category.form',$data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $row = Category::where('id',$id)->first();
        if($row){
            $data = $row->toArray();
            $data['page_title'] = trans('category.page_title_edit');
            foreach ($row->categoryLanguages as $categoryLanguage){
                $data['title'][$categoryLanguage->language_id] = $categoryLanguage->title;
            }

            return view('category.form',$data);
        }
        return redirect()->route('category-index')->with('alert-warning',trans('global.not_found_row'));
    }



    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $id = (int)$request->get('id',0);

        $rules = [];
        $messages = trans('category.validation_messages');

        foreach (getLanguages() as $key=>$v){
            $rules['title.'.$v['id']] = 'required|max:255';
        }
        if(!is_array($messages)){
            $messages = [];
        }
        $validator = \Validator::make($request->except(['_token']), $rules,$messages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->with('alert-warning', trans('global.complete_warning'))
                ->withInput();
        }


        $row = Category::findOrNew($id);




        \DB::beginTransaction();
        try{
            $row->save();
            CategoryLanguage::where('category_id',$row->id)->delete();
            $langs = [];
            foreach (getLanguages() as $item) {
                $langs[] = [
                    'category_id'=> $row->id,
                    'language_id'=> $item['id'],
                    'title'=>$request->get('title')[$item['id']]
            ];
            }

            CategoryLanguage::insert($langs);
            \DB::commit();

            return redirect()->route('category-index')->with('alert-success',trans('global.saved_successfully'));
        }catch (\PDOException $e){
            \DB::rollBack();
            return redirect()->back()->with('alert-danger',$e->getMessage())->withInput();
        }
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id,$status)
    {
        if(!in_array($status,[1,2])){
            return response()->json([true,trans('global.status_not_allowed')]);
        }
        $row = Category::where('id',$id)->first();
        if($row){
            $row->is_active = $status;
            try{
                $row->save();
                return response()->json([true,trans('global.status_successfully_'.$status)]);
            }catch (\PDOException $e){
                return response()->json([false,$e->getMessage()]);
            }
        }
        return response()->json([false,trans('global.not_found_row')]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $row = Category::where('id',$id)->first();
        if($row){
            try{
                $row->delete();
                return response()->json([true,trans('global.deleted_successfully')]);
            }catch (\PDOException $e){
                return response()->json([false,$e->getMessage()]);
            }
        }
        return response()->json([false,trans('global.not_found_row')]);
    }

}

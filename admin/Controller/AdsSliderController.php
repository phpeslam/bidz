<?php

namespace Admin\Controller;

use APP;
use App\Model\AdsSlider;
use App\Model\Auction;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Session;
use Carbon\Carbon;

class AdsSliderController extends AdminController {

    public function index() {
        $data = [];
        $data['datatables'] = true;
        $data['page_title'] = 'Ads Slider';
        return view('AdsSlider.index', $data);
    }

    public function show(Datatables $datatables) {
        return $datatables->eloquent(AdsSlider::query()->orderBy('id', 'desc'))
                        ->addColumn('action', 'AdsSlider.buttons')
                        ->filter(function ($query) {
                            $search = app('request')->get('search', []);
                            if (array_key_exists('value', $search)) {
                                $value = trim($search['value']);
                                if (!empty($value)) {
                                    $query->where('id', 'like', '%' . $value . '%');
                                }
                            }
                        })
                        ->make(true);
    }

    public function create($id) {
        $adsid = AdsSlider::select('id')->where('auctionID', $id)->first();
        if ($adsid) {
            $retrunID = $adsid;
        } else {
            $ads = new AdsSlider();
            $ads->auctionID = $id;
            $ads->created_at = date('Y-m-d H:i:s');
            $ads->save();
            $adsid = $ads->id;
        }
        return redirect()->route('adsSlider-edit', ['id' => $adsid]);
    }

    public function edit($id) {
        $data = [];
        $data['page_title'] = 'edit slider Ads';
        $data['info'] = AdsSlider::find($id);
        return view('AdsSlider.info', $data);
    }

    public function store(Request $request) {
        $request->validate([
            'id' => 'required|numeric',
            'start' => 'required|date',
            'end' => 'required|date',
            'switchActive' => 'Boolean',
        ]);
        $AdsSlider = AdsSlider::find($request->id);
        $auc = Auction::find($AdsSlider->auctionID);
        if(!$auc)
        {
            return redirect()->back()->with('alert-danger','an error')->withInput();
        }


        $endTime   = new Carbon($request->end);
        $test =$auc->start_at;
        $new = Carbon::parse($auc->start_at)->addDays($auc->bid_days);
        $after = Carbon::parse($test)->subDays(1);
       // dd($endTime,$new,$after);
        if($new <$endTime)
        {
            return redirect()->back()->with('alert-danger','an error on end time')->withInput();
        }
        if ($request->hasFile('image')) {
            $img = Input::file('image');
            $ext = $img->getClientOriginalExtension();
            $path = public_path() . '/uploads/slider';
            $fullename = time() . '.' . $ext;
            $img->move($path, $fullename);
            $request->merge(['image' => $fullename]);
        }
        DB::beginTransaction();
        try {
            DB::table('ads_slider')
                    ->where('id', $request->id)
                    ->update([
                        'start_at' => $request->start,
                        'end_at' => $request->end,
                        'is_active' => $request->switchActive,
                        'image' => $fullename
            ]);

            DB::commit();
            return redirect()->route('adsSlider-index')->with('alert-success', trans('global.saved_successfully'));
        } catch (\PDOException $e) {
            DB::rollBack();
            return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
        }
    }

    public function status($id, $status) {
        if (!in_array($status, [1, 2])) {
            return response()->json([true, trans('global.status_not_allowed')]);
        }
        $row = AdsSlider::where('id', $id)->first();
        if ($row) {
            $row->is_active = $status;
            try {
                $row->save();
                return response()->json([true, trans('global.status_successfully_' . $status)]);
            } catch (\PDOException $e) {
                return response()->json([false, $e->getMessage()]);
            }
        }
        return response()->json([false, trans('global.not_found_row')]);
    }


    public function delete($id)
    {
        $row = AdsSlider::where('id',$id)->first();
        if($row){
            try{
                $row->delete();
                Session::flash('message', trans('global.deleted_successfully'));
                return back();
            }catch (\PDOException $e){

                Session::flash('message', $e->getMessage());
                return back();
            }
        }
        Session::flash('message', trans('global.not_found_row'));
        return back();
    }

}

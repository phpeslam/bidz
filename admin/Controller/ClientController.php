<?php

namespace Admin\Controller;

use App\Model\Auction;
use App\Model\Clients;
use App\Model\City;
use App\Model\Country;
use App\Model\FollowAuction;
use App\Model\BankTransaction;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use App\Custum\Sms;
use App\Mail\RegisterEmail;
use App\Model\Member;
use Session;
class ClientController extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $data['datatables'] = true;

        return view('clients.index', $data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables)
    {
        $qq = Clients::query()->select('id', 'first_name', 'last_name', 'email', 'membership_type', 'created_at')->orderBy('id','desc');
        return $datatables->eloquent($qq)
            ->addColumn('action', 'clients.buttons')
            ->make(true);
    }
    public function activate($id)
    {
        $qq = Clients::find($id);
        if($qq->is_active == 0)
        {
            $qq->is_active = 1 ;
            $qq->save();
            Session::flash('message', 'User Activated');
            return redirect()->route('client-index');

        }
        else
        {
            $qq->is_active = 0 ;
            $qq->save();
            Session::flash('message', 'User Blocked');
            return redirect()->route('client-index');
        }
        
        
        return redirect()->back();
    }

    public function view($id)
    {
        $data = [];
        $info = Clients::find($id);
        $data['page_title'] =$info->first_name . ' '. $info->last_name;
        $data['info'] = $info;
        $data['activeAuctions'] = Auction::where(['member_id' => $id,'is_active'=>1])->with(['CarType'])->get();
        $data['closedAuctions'] = Auction::where(['member_id' => $id,'is_active'=>0])->get();
        $data['countUserAuctions'] = Auction::where(['member_id' => $id])->get()->count();


        
        $blances = BankTransaction::where('userID',$id)->whereNotIn('type', [3])->get();
            $total = 0;
            foreach($blances as $blance)
            {
                if($blance->case_balance == 'deposited')
                {
                    $total+=$blance->value;
                }
                elseif( $blance->case_balance == 'add_balance')
                {
                    $total= $total;
                }
                else
                {
                    $total-=$blance->value;
                }
            }
        $data['balance'] = $total;
        $find = FollowAuction::where(['user_id' => $id])->get()->count();
        $data['countFollowAuctions'] = $find;
        $data['market_image'] = $info->market_image;
        $data['membership_type'] = $info->membership_type;
        return view('clients.profile', $data);
    }
    public function create()
    {
        $data = [];
        $data['page_title'] = 'create new client';
        $data['countries'] = Country::where('is_active', 1)->with('currentLanguage')->get();
        if (!empty(old('country_id'))) {
            $cities = City::where('is_active', 1)->where('country_id', old('country_id'))->with('currentLanguage')->get();
            $data['cities'] = [];
            foreach ($cities as $city) {
                $data['cities'][] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
            }
        }
        return view('clients.form', $data);
    }

    public function store(request $request){

        $rules = [
            'membership_type' => ['required', Rule::in([1, 2])],
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address' => 'required|max:255',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City())->getTable() . ',id',
            'phone_number' => ['required', 'unique:' . (new Member())->getTable() . ',phone_number'],
            'password' => 'required|min:5|max:20|confirmed',
            'email' => ['required', 'email', 'confirmed', 'unique:' . (new Member())->getTable() . ',email'],
        ];

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $row = new Member();
        $row->first_name = $request->get('first_name');
        $row->last_name = $request->get('last_name');
        $row->email = $request->get('email');
        $row->password = \Hash::make($request->get('password'));
        $row->phone_number = $request->get('phone_number');
        $row->membership_type = $request->get('membership_type');
        $row->city_id = $request->get('city_id');
        $row->country_id = $request->get('country_id');
        $row->address = $request->get('address');

        $row->is_active_phone = 0;
        $row->is_active_email = 0;
        $row->email_act_code = str_random(30);
        $row->phone_act_code = rand(1000, 9999);

        $row->is_active = 0;

        try {
            $row->save();
            \Mail::to($row->email)->send(new RegisterEmail($row));
            Sms::send($row->phone_number, trans('alerts.your_activation_code').$row->phone_act_code );
            \Auth::guard('members')->login($row, true);
            Session::flash('message', trans('alerts.registered'));
            return redirect()->route('client-index');
        } catch (\PDOException $e) {

            Session::flash('message', $e->getMessage());
            return redirect()->back()->withInput();
        }
    }

    public function edit($id){
        $id = intval($id);
        $data = [];
        $data['page_title'] = 'edit client';
       $Member = Member::find($id);
        $data['countries'] = Country::where('is_active', 1)->with('currentLanguage')->get();
       $country_id =  old('country_id') ?? $Member->country_id;
        if ($country_id) {
            $cities = City::where('is_active', 1)->where('country_id', $country_id)->with('currentLanguage')->get();
            $data['cities'] = [];
            foreach ($cities as $city) {
                $data['cities'][] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
            }
        }
          $data['Member'] = $Member;
        return view('clients.edit', $data);
    }


    public function updateinfo(Request $request){
        $rules = [
            'id' => 'required',
            'membership_type' => ['required', Rule::in([1, 2])],
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address' => 'required|max:255',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City())->getTable() . ',id',
            'phone_number' => ['required'],
            'email' => ['required', 'email'],
        ];

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        \DB::table('members')
            ->where('id', $request->id)
            ->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
            ]);
        Session::flash('message', trans('alerts.registered'));
        return redirect()->route('client-index');
    }
    public function delete($id)
    {
        $row = Member::where('id',$id)->first();
        if($row){
            try{
                $row->delete();
                Session::flash('message', trans('global.deleted_successfully'));
                return back();
            }catch (\PDOException $e){

                Session::flash('message', $e->getMessage());
                return back();
            }
        }
        Session::flash('message', trans('global.not_found_row'));
        return back();
    }

}
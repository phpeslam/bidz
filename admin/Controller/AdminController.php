<?php

namespace Admin\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

abstract class AdminController extends Controller
{
    public function __construct()
    {
        \App::setLocale('en');
    }


}

<?php

namespace Admin\Controller;

use App\User;
use HttpOz\Roles\Models\Role;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Session;

class UserController extends AdminController
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $data['datatables'] = true;
        $data['bootbox'] = true;
        $data['roles'] = Role::all(['id','slug']);
        return view('user.index',$data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables)
    {
        $fl_role = $datatables->getRequest()->get('fl_role');
        $qq = User::query()->with('roles')
            ->where('id','!=',\Auth::user()->id);
        if(!empty($fl_role)){

            $qq = $qq->whereHas('roles',function($q) use($fl_role){
                $q->where('slug',$fl_role);
            });
        }
        return $datatables->eloquent($qq)
            ->addColumn('role', 'user.roles')
            ->addColumn('action', 'user.buttons')
            ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $data = [];
        $data['page_title'] = trans('user.page_title_create');
        $data['user_roles'] = old('user_roles',[]);
        $data['roles'] = Role::all(['id','slug']);
        return view('user.form',$data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $row = User::where('id',$id)->first();
        if($row){
            $data = $row->toArray();
            $data['user_roles'] = [];
            foreach ($row->roles as $role){
                $data['user_roles'][] = $role->id;
            }

            $data['roles'] = Role::all(['id','slug']);
            $data['page_title'] = trans('user.page_title_edit');
            return view('user.form',$data);
        }
        return redirect()->route('user-index')->with('alert-warning',trans('global.not_found_row'));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'user_roles' => 'required|array',
            'password' => 'required|min:6|confirmed'
        ];
        $id = (int)$request->get('id',0);

        if(!empty($id)){
            $rules['email'] = 'required|email|max:255|unique:users,email,'.$id;

            unset($rules['password']);
        }
        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }


        $row = User::findOrNew($id);
        $row->name = $request->get('name');
        $row->email = $request->get('email');

        if(!empty($request->get('password'))){
            $row->password = \Hash::make($request->get('password'));
        }

        try{
            $row->save();
            $row->syncRoles($request->get('user_roles'));
            return redirect()->route('user-index')->with('alert-success',trans('global.saved_successfully'))->withInput();
        }catch (\PDOException $e){
            return redirect()->back()->with('alert-danger',$e->getMessage())->withInput();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $row = User::find($id);
        if($row && $id != 1 ){
            try{
                $row->delete();
                Session::flash('message', trans('global.deleted_successfully'));
                return back();
            }catch (\PDOException $e){
                $return  = response()->json([false,$e->getMessage()]);
                Session::flash('message', $return);
                return back();
            }
        }
        Session::flash('message', trans('global.not_found_row'));
        return back();
        return response()->json([false,trans('global.')]);
    }
}

<?php

namespace Admin\Controller;

use App\Model\Auction;
use App\Model\AuctionOtherSpecification;
use App\Model\AuctionImage;
use App\Model\AuctionFile;
use Session;
use Illuminate\Support\Facades\DB;
use App\Model\Clients;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AuctionsController extends AdminController
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $data['datatables'] = true;
        return view('clients.index', $data);
    }

    // Auctions Active
    public function active()
    {
        $data = [];
        $data['page_title'] = 'Auctions Active';
        $data['datatables'] = true;
        $data['Auctions_Active'] = 1;
        $data['pending'] = 0;
        return view('auctions.index', $data);
    }
    // Auctions Finished
    public function pending()
    {
        $data = [];
        $data['page_title'] = 'Auctions pending';
        $data['datatables'] = true;
        $data['Auctions_Active'] = 2;
        $data['pending'] = 1;
        return view('auctions.index', $data);
    }
    // Auctions Finished
    public function closed()
    {
        $data = [];
        $data['page_title'] = 'Auctions closed';
        $data['datatables'] = true;
        $data['Auctions_Active'] = 2;
        $data['pending'] = 0;
        return view('auctions.index', $data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($active,$pending = 0 ,  Datatables $datatables)
    {
        $active = $active == 1 ? $active : 0;
        $pending = $pending == 1?  0 : 1;
        $qq = Auction::query()->select('id', 'title', 'description', 'member_id', 'provider_id','minimum_bid', 'bid_type','total_bids', 'created_at')->where(['isWork' =>  $active , 'provider_accept' => $pending])->orderBy('id','desc')->with('Member');
        return $datatables->eloquent($qq)
            ->addColumn('action', 'auctions.buttons')
            ->make(true);
    }

    public function view($id)
    {
        $data = [];
        $info = Auction::find($id);
        $data['page_title'] = $info->title;
        $data['info'] = $info;
        $OtherSpecification = AuctionOtherSpecification::select('other_specification_id')->where(['auction_other_specifications.auction_id' => $id])->get();
        $data['OtherSpecification']  = $OtherSpecification->toArray();
        $images = AuctionImage::select('image')->where(['auction_id' => $id])->get();
        $data['images'] = $images->toArray();
        return view('auctions.info', $data);
    }

    public function delete($id){

        $files = AuctionFile::where('auction_id',$id)->get();
        $images = AuctionImage::where('auction_id',$id)->get();
        $row = Auction::where('id',$id)->first();

        if($row){
            try{
                if($files){
                    \DB::table('auction_files')->where('auction_id', $id)->delete();
                }
                if($images){
                    \DB::table('auction_images')->where('auction_id', $id)->delete();
                }
                $row->delete();

                Session::flash('message', trans('global.deleted_successfully'));
                return back();
            }catch (\PDOException $e){
                $return = response()->json([false,$e->getMessage()]);
                Session::flash('message', $return);
                return back();
            }
        }
        $return =  response()->json([false,]);
        Session::flash('message', trans('global.not_found_row'));
        return back();
    }
}
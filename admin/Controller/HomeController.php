<?php

namespace Admin\Controller;

use App\Model\Auction;
use App\User;
use App\Model\Member;
use App\Model\Partner;
use App\Model\AdsSlider;
use App\Model\BankTransaction;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class HomeController extends AdminController {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $auctions = Auction::count();
        $closed_auctions = Auction::where(['is_active' => 0])->count();
        $opened_auctions = Auction::where(['is_active' => 1])->count();
        $pending_auctions = Auction::where(['provider_accept' => 0])->count();

        $admins = User::count();
        $partners = Partner::count();
        $members = Member::where(['membership_type' => 1])->count();
        $markets = Member::where(['membership_type' => 2])->count();

        $ads = AdsSlider::where(['is_active' => 1])->count();
        $bank = BankTransaction::count();

        $data['auctions'] = $auctions;
        $data['closed_auctions'] = $closed_auctions;
        $data['opened_auctions'] = $opened_auctions;
        $data['pending_auctions'] = $pending_auctions;

        $data['admins'] = $admins;
        $data['partners'] = $partners;
        $data['members'] = $partners;
        $data['markets'] = $markets;
        $data['total_members'] = $admins + $partners + $partners + $markets;

        $data['ads'] = $ads;
        $data['bank'] = $bank;
        $data['total'] = $ads + $bank;
        return view('home.index', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile() {
        $data = [];
        $data['page_title'] = trans('global.profile');
        return view('home.profile', $data);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function profileSave(Request $request) {

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,' . \Auth::user()->id,
        ];

        if (!empty($request->get('password'))) {
            $rules['password'] = 'min:6|confirmed';
            $rules['current_password'] = 'required|min:6';
        }

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
            ;
        }
        if (!empty($request->get('password')) && !\Hash::check($request->get('current_password'), \Auth::user()->password)) {
            return redirect()->back()->with('alert-danger', trans('user.current_password_msg'));
        }

        $row = \Auth::user();
        $row->name = $request->get('name');
        $row->email = $request->get('email');

        if (!empty($request->get('password'))) {
            $row->password = \Hash::make($request->get('password'));
        }

        try {
            $row->save();
            return redirect()->back()->with('alert-success', trans('global.saved_successfully'));
        } catch (\PDOException $e) {
            return redirect()->back()->with('alert-danger', $e->getMessage());
        }
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables) {
        return $datatables->eloquent(MemberAge::query()
                                ->with('currentLanguage')->withCount('members'))
                        ->addColumn('action', 'age.buttons')
                        ->filter(function ($query) {
                            $search = app('request')->get('search', []);
                            if (array_key_exists('value', $search)) {
                                $value = trim($search['value']);
                                if (!empty($value)) {
                                    $query->whereHas('currentLanguage', function($query) use($value) {
                                        $query->where('text', 'like', '%' . $value . '%')
                                        ;
                                    })

                                    ;
                                }
                            }
                        })
                        ->make(true);
    }

}

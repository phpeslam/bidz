<?php

namespace Admin\Controller;

use App\Model\Auction;
use App\Model\Setting;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SettingsController extends AdminController {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $data = [];
        $data['page_title'] = 'Settings';
        $data['datatables'] = true;

        return view('settings.index', $data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables) {
        $qq = Setting::query()->orderBy('id', 'asc');
        return $datatables->eloquent($qq)
                        ->addColumn('action', 'settings.buttons')
                        ->make(true);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id) {
        $row = Setting::where('id', $id)->first();
        if ($row) {
            $data['row'] = $row;
            $data['page_title'] = trans('settings.page_title_edit');
            return view('settings.form', $data);
        }
        return redirect()->route('settings-index')->with('alert-warning', trans('global.not_found_row'));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request) {
        $id = (int) $request->get('id', 0);

        $row = Setting::findOrNew($id);
        \DB::beginTransaction();
        try {
            if (!empty($request->input('value'))) {
                Setting::where('id', $id)->update(['value' => $request->input('value')]);
            } else {
                Setting::where('id', $id)->update(['value' => '']);
            }
            \DB::commit();

            return redirect()->route('settings-index')->with('alert-success', trans('global.saved_successfully'));
        } catch (\PDOException $e) {
            \DB::rollBack();
            return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
        }
    }

}

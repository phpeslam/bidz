<?php

namespace Admin\Controller;
use APP;
use App\Model\Ads;
use App\Model\AdsLanguage;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Session;

class AdsController  extends AdminController
{
    public function index(){
        $data = [];
        $data['datatables'] = true;
        $data['page_title'] = 'Static Ads';
        return view('ads.index', $data);
    }


    public function show(Datatables $datatables)
    {
        return $datatables->eloquent(Ads::query()
            ->with('currentLanguage') )
            ->addColumn('action', 'ads.buttons')
            ->make(true);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id)
    {
        $row = Ads::where('id',$id)->first();
        if($row){
            $data = $row->toArray();
            $data['page_title'] = trans('global.edit');
            foreach ($row->adsLanguages as $adsLanguage){
                $data['title'][$adsLanguage->language_id] = $adsLanguage->title;
                $data['link'][$adsLanguage->language_id] = $adsLanguage->link;
                $data['image'][$adsLanguage->language_id] = $adsLanguage->link;
            }

            return view('ads.form',$data);
        }
        return redirect()->route('ads-index')->with('alert-warning',trans('global.not_found_row'));
    }

    public function create()
    {
        $page_title= 'add ads';
        return view('ads.form',compact('page_title'));
    }



    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request)
    {
        $id = (int)$request->get('id',0);

        $rules = [];
        $messages = trans('faq.validation_messages');
//dd(getLanguages());
        foreach (getLanguages() as $key=>$v){
            $rules['title.'.$v['id']] = 'required|max:255';
            $rules['link.'.$v['id']] = 'required';
            $rules['image.'.$v['id']] = 'image|mimes:jpeg,png,jpg,gif,svg|max:6048';

            
        }
        if(!is_array($messages)){
            $messages = [];
        }
        $validator = \Validator::make($request->except(['_token']), $rules,$messages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->with('alert-warning', trans('global.complete_warning'))
                ->withInput();
        }
     //   front/'.getCurrentLang('direction').'/img/banner.png


        $row = Ads::findOrNew($id);
        $imgI= [];//dd($request->all());
        foreach (getLanguages() as $key=>$v){
            $img = $request->image[$v['id']];
            $ext = $img->getClientOriginalExtension();
            $path = public_path() . '/front/'.$key.'/img';
            $fullename = time() . '.' . $ext;
            $imgI[]=$fullename;
            $img->move($path, $fullename);
        
    }
        
//dd($imgI);
//        if($request->get('image')){
//            $imageName = time().'.'.$request->get('image')->getClientOriginalExtension();
//            $request->get('image')->storeAs(AdsLanguage::FILE_PATH, $imageName);
//        }

        \DB::beginTransaction();
        try{
            $row->save();
            AdsLanguage::where('ads_id',$row->id)->delete();
            $langs = [];
            foreach (getLanguages() as $item) {


                $langs[] = [
                    'ads_id'=> $row->id,
                    'language_id'=> $item['id'],
                    'title'=>$request->get('title')[$item['id']],
                    'link'=>$request->get('link')[$item['id']],
                    'image'=>$imgI[$item['id']-1],
                ];
            }

            AdsLanguage::insert($langs);
            \DB::commit();

            return redirect()->route('ads-index')->with('alert-success',trans('global.saved_successfully'));
        }catch (\PDOException $e){
            \DB::rollBack();
            return redirect()->back()->with('alert-danger',$e->getMessage())->withInput();
        }
    }


    public function status($id,$status)
    {
        if(!in_array($status,[1,2])){
            return response()->json([true,trans('global.status_not_allowed')]);
        }
        $row = Ads::where('id',$id)->first();
        if($row){
            $row->is_active = $status;
            try{
                $row->save();
                return redirect()->route('ads-index')->with('alert-success',trans('global.saved_successfully'));
                return response()->json([true,trans('global.status_successfully_'.$status)]);
            }catch (\PDOException $e){
                return redirect()->back()->with('alert-danger',$e->getMessage())->withInput();
                return response()->json([false,$e->getMessage()]);
            }
        }
        return redirect()->back()->with('alert-danger','an error')->withInput();
        return response()->json([false,trans('global.not_found_row')]);
    }
}
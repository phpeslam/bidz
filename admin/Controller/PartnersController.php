<?php

namespace Admin\Controller;

use App\Model\Partner;
use App\Model\PageLanguage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

class PartnersController extends AdminController {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $data = [];
        $data['page_title'] = 'Partners';
        $data['datatables'] = true;

        return view('partners.index', $data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables) {
        $qq = Partner::query()->orderBy('id', 'asc');
        return $datatables->eloquent($qq)
                        ->addColumn('action', 'partners.buttons')
                        ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $data = [];
        $data['page_title'] = 'Add partner';
        return view('partners.form', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id) {
        $row = Page::where('id', $id)->first();
        if ($row) {
            $data = $row->toArray();
            $data['page_title'] = trans('page.page_title_edit');
            foreach ($row->pageLanguages as $pageLanguage) {
                $data['title'][$pageLanguage->language_id] = $pageLanguage->title;
                $data['desc'][$pageLanguage->language_id] = $pageLanguage->desc;
            }

            return view('page.form', $data);
        }
        return redirect()->route('page-index')->with('alert-warning', trans('global.not_found_row'));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request) {
        $id = (int) $request->get('id', 0);

        $rules = [];
        $messages = trans('page.validation_messages');


        $validator = \Validator::make($request->except(['_token']), $rules, $messages);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->with('alert-warning', trans('global.complete_warning'))
                            ->withInput();
        }
       
            $row = new Partner();
            if ($request->hasFile('image')) {
                $img = Input::file('image');
                $ext = $img->getClientOriginalExtension();
                $path = public_path() . '/uploads/partners';
                $fullename = time() . '.' . $ext;
                $img->move($path, $fullename);
                $request->merge(['image' => $fullename]);
                $row->image = $fullename;
            }
            $row->name = $request->input('name');
            $row->save();
       
        return redirect()->route('partner-index')->with('alert-success', trans('global.saved_successfully'));
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, $status) {
        if (!in_array($status, [1, 2])) {
            return response()->json([true, trans('global.status_not_allowed')]);
        }
        $row = Page::where('id', $id)->first();
        if ($row) {
            $row->is_active = $status;
            try {
                $row->save();
                return response()->json([true, trans('global.status_successfully_' . $status)]);
            } catch (\PDOException $e) {
                return response()->json([false, $e->getMessage()]);
            }
        }
        return response()->json([false, trans('global.not_found_row')]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        $row = Partner::where('id', $id)->first();
        if ($row) {
            try {
                $row->delete();
                //return response()->json([true, trans('global.deleted_successfully')]);
                return redirect()->route('partner-index');
            } catch (\PDOException $e) {
                return response()->json([false, $e->getMessage()]);
            }
        }
        return response()->json([false, trans('global.not_found_row')]);
    }

}

<?php

namespace Admin\Controller;

use App\Model\Page;
use App\Model\PageLanguage;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class PageController extends AdminController {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        $data = [];
        $data['datatables'] = true;

        return view('page.index', $data);
    }

    /**
     * @param Datatables $datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Datatables $datatables) {
        return $datatables->eloquent(Page::query()
                                ->with('currentLanguage'))
                        ->addColumn('action', 'page.buttons')
                        ->filter(function ($query) {
                            $search = app('request')->get('search', []);
                            if (array_key_exists('value', $search)) {
                                $value = trim($search['value']);
                                if (!empty($value)) {
                                    $query->whereHas('currentLanguage', function($query) use($value) {
                                        $query->where('title', 'like', '%' . $value . '%')
                                        ->orWhere('desc', 'like', '%' . $value . '%');
                                    })

                                    ;
                                }
                            }
                        })
                        ->make(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {
        $data = [];
        $data['page_title'] = trans('page.page_title_create');
        return view('page.form', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id) {
        $row = Page::where('id', $id)->first();
        if ($row) {
            $data = $row->toArray();
            $data['page_title'] = trans('page.page_title_edit');
            foreach ($row->pageLanguages as $pageLanguage) {
                $data['title'][$pageLanguage->language_id] = $pageLanguage->title;
                $data['desc'][$pageLanguage->language_id] = $pageLanguage->desc;
            }

            return view('page.form', $data);
        }
        return redirect()->route('page-index')->with('alert-warning', trans('global.not_found_row'));
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function store(Request $request) {
        $id = (int) $request->get('id', 0);

        $rules = [];
        $messages = trans('page.validation_messages');

        foreach (getLanguages() as $key => $v) {
            $rules['title.' . $v['id']] = 'required|max:255';
            $rules['desc.' . $v['id']] = 'required';
        }
        if (!is_array($messages)) {
            $messages = [];
        }
        $validator = \Validator::make($request->except(['_token']), $rules, $messages);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->with('alert-warning', trans('global.complete_warning'))
                            ->withInput();
        }


        $row = Page::findOrNew($id);




        \DB::beginTransaction();
        try {
            $row->save();
            PageLanguage::where('page_id', $row->id)->delete();
            $langs = [];
            foreach (getLanguages() as $item) {
                $langs[] = [
                    'page_id' => $row->id,
                    'language_id' => $item['id'],
                    'title' => $request->get('title')[$item['id']],
                    'desc' => $request->get('desc')[$item['id']]
                ];
            }

            PageLanguage::insert($langs);
            \DB::commit();

            return redirect()->route('page-index')->with('alert-success', trans('global.saved_successfully'));
        } catch (\PDOException $e) {
            \DB::rollBack();
            return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
        }
    }

    /**
     * @param $id
     * @param $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, $status) {
        if (!in_array($status, [1, 2])) {
            return response()->json([true, trans('global.status_not_allowed')]);
        }
        $row = Page::where('id', $id)->first();
        if ($row) {
            $row->is_active = $status;
            try {
                $row->save();
                return response()->json([true, trans('global.status_successfully_' . $status)]);
            } catch (\PDOException $e) {
                return response()->json([false, $e->getMessage()]);
            }
        }
        return response()->json([false, trans('global.not_found_row')]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id) {
        $row = Page::where('id', $id)->first();
        if ($row) {
            try {
                $row->delete();
                return response()->json([true, trans('global.deleted_successfully')]);
            } catch (\PDOException $e) {
                return response()->json([false, $e->getMessage()]);
            }
        }
        return response()->json([false, trans('global.not_found_row')]);
    }

}

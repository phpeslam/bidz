<?php


namespace Admin\Controller;

use App\Model\BankTransaction;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
class BankTransactionController extends AdminController
{
    public function index()
    {
        $data = [];
        $data['page_title'] = 'Bank Transactions';
        return view('BankTransaction.index',$data);
    }

    public function show(Datatables $datatables)
    {
        $qq =  BankTransaction::query()->orderBy('id','desc')->whereNotNull('image');
        return $datatables->eloquent($qq)
            ->addColumn('action', 'BankTransaction.buttons')
            ->editColumn('userID', function(BankTransaction $client) {
                return $client->member->first_name.' '.$client->member->last_name;
        })
            ->make(true);

    }



    public function view($id)
    {
        $data = [];
        $data['page_title'] = trans('transfers.BankTransfers');
        $data['info'] =  BankTransaction::find($id);
        $data['id'] = $id;
        return view('BankTransaction.info', $data);
    }

    public function active($id)
    {
        $bala =  BankTransaction::find($id);
        $bala->case_balance = 'deposited';
        $bala->save();
        return redirect()->route('BankTransaction')->with('alert-success',trans('global.saved_successfully'));
    }

    public function create($id)
    {
        $page_title= 'Add Balance';
        return view('BankTransaction.form',compact('page_title','id'));
    }
    public function store(Request $request )
    {
        $rules = [];
        $messages = trans('faq.validation_messages');

        foreach (getLanguages() as $key=>$v){
          //  $rules['user_id'] = 'required|exists:members,id';
            $rules['id'] = 'required|exists:members,id';
            $rules['price'] = 'required|numeric';
        }
        if(!is_array($messages)){
            $messages = [];
        }
        $validator = \Validator::make($request->except(['_token']), $rules,$messages);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->with('alert-warning', trans('global.complete_warning'))
                ->withInput();
        }
    try{
            $bala = new BankTransaction;
            $bala->date = date('Y-m-d h:i:s');
            $bala->value = $request->price;
            $bala->userID = $request->id;
            $bala->type_balance = 'Add Balance';
            $bala->case_balance = 'deposited';
            $bala->type = 1;
            $bala->save();
            return redirect()->route('client-index')->with('alert-success',trans('global.saved_successfully'));
    }
    catch (\PDOException $e){
                \DB::rollBack();
                return redirect()->back()->with('alert-danger',$e->getMessage())->withInput();
            }
    }


}
<?php

namespace Admin\Controller;

use App\Model\ContactUS;
use Session;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
class ContactController  extends AdminController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];
        $data['page_title'] = 'Contact-us';
        return view('contact.index',$data);
    }
    public function show(Datatables $datatables)
    {
        $qq =  ContactUS::query()->orderBy('id','desc');
        return $datatables->eloquent($qq)
            ->addColumn('action', 'contact.buttons')
            ->make(true);

    }
    public function delete($id){
        $row = ContactUS::where('id',$id)->first();
        if($row){
            try{
                $row->delete();
                $return = response()->json([true,trans('global.deleted_successfully')]);
                Session::flash('message', $return);
                return back();
            }catch (\PDOException $e){
                $return = response()->json([false,$e->getMessage()]);
                Session::flash('message', $return);
                return back();
            }
        }
        $return =  response()->json([false,trans('global.not_found_row')]);
        Session::flash('message', $return);
        return back();
    }
}
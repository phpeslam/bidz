<?php

return [
    'page_title' => 'Notifications',
    'latest' => 'Latest  Notifications',
    'none' => 'No Notifications Found',
    'explore' => 'Check out bid details now ',
    'more_details' => 'Bidd Details ',
    'congratulation' => 'Congratulation ! ',
    'approve' => 'Approve',
    'reject' => 'Reject',
    'by' => 'by',
    'no_aucation' => 'There is no Bid to do this',
    'car_request' => 'Request for Approval of the sale of a %s  in your Agancy',
    'car_approveed' => '%s  Car Accepted ',
    'car_reject' => '%s Car Rejected',

];

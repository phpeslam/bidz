<?php

return [
    'BankTransfers' => ' تحويل بنكى ',
    'add_balance' => ' إضافة رصيد ',
    'Insur_balance' => '  مبلغ التأمين ',
    'Special_Ads' => '  إعلان مميز  ',
    'type' => ' نوع الإيداع  ',
    'image' => ' صورة الايداع ',
    'date' => 'تاريخ التحويل',
    'value' => ' قيمة التحويل ',
    'send' => ' إرسال ',
    'msg_success' => ' تم إرسال تحويلاتك بنجاح',
    'error-trans'=> "خطأ ، حاول إرسال تحويلاتك مرة أخرى",
    'ElctonPayment' => 'دفع الكتروني',
];
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'login_form' =>'Please Enter Your Information',
    'forget-password'=>'I forgot my password',
    'remember-password'=>'Remember Me',
    'login-btn'=>'Login',
    'enter-password'=>'Enter password',
    'enter-email'=>'Enter email address',
    'send-me'=>'Send Me!',
    'back-to-login'=>'Back to login',
    'enter-instructions'=>' Enter your email and to receive instructions ',
    'retrieve-password'=>'Retrieve Password',

];

<?php
return
    [
        'profile'=>'profile ',
        'upload-error'=>'upload error ',
        'mail-sent'=>'Email sent successfully',
        'your_activation_code'=>'Your activation code is',
        'your_activation_code_sent'=>'Your activation code has been sent to the mobile',
        'undefined'=>'undefined',
        'type-mobile-activation-code'=>'Please type your mobile activation code',
        'type-email-activation-code'=>'Please enter your activation code',
        'mobile-activated'=>'Mobile activated',
        'email-activated'=>'Email has been activated ',
        'Unactivated-activation'=>'Unactivated activation system ',
        'registered'=>'successfully registered ',
        'email-link-sent'=>'link sent to your email',
        'Email-incorrect'=>'Email is incorrect',
        'Password-required'=>'Password is required ',
        'Password-email-required'=>'Your  e-mail or Password is incorrect ',
        'bid-add'=>'Bid added successfully ',
        'wait-approve'=> 'The bid has been successfully added and is awaiting the approval of the show',
        'your-bidder'=> 'your ,bidder sent successfully',
        'error-bidding'=> 'error , try to send your bidder again',
        'error-added'=> 'error , added-before',
        'added-follow'=> 'added to your follow successfully',
        'success-delete'=> 'successfully deleted',
        'success-message'=> 'your ,message sent successfully',
        'error-message'=> 'error , try to send your message again',

    ];
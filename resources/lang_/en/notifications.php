<?php

return [
    'page_title' => 'Notifications',
    'latest' => 'Latest  Notifications',
    'none' => 'None Notifications Found',
    'explore' => 'Find out the bidding details now ',
    'more_details' => 'Bidding Details ',
    'congratulation' => 'congratulation ',
    'approve' => 'Approve',
    'reject' => 'Reject',
    'by' => 'by',
    'no_aucation' => 'There is no bid to do this',
    'car_request' => 'Request for approval of the sale of a %s  in your Agancy',
    'car_approveed' => '%s  Car Accepted ',
    'car_reject' => '%s Car Reject',

];

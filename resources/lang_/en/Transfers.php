<?php

return [
    'BankTransfers' => ' Bank Transfers ',
    'add_balance' => ' Add balance ',
    'Insur_balance' => '  Insurance ',
    'Special_Ads' => ' Special Ads ',
    'type' => ' Type  ',
    'image' => ' Transaction images',
    'date' => 'Transaction date',
    'value' => 'Transaction Value ',
    'send' => ' Send ',
    'msg_success' => ' Bank Transfers send succesfuly',
    'error-trans'=>   'error , try to send your Transfers again',
    'ElctonPayment' => 'Electronic payment',
];
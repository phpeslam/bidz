<?php

return [
    'BankTransfers' => ' تحويل بنكى ',
    'add_balance' => ' إضافة رصيد ',
    'Insur_balance' => '  مبلغ التأمين ',
    'Special_Ads' => '  اعلان مميز  ',
    'type' => ' نوع الإيداع  ',
    'image' => ' صورة الايداع ',
    'date' => 'تاريخ التحويل',
    'value' => ' قيمة التحويل ',
    'send' => ' ارسال ',
    'error-trans'=> "خطأ ، حاول إرسال تحويلاتك مرة أخرى",
    'ElctonPayment' => 'دفع الكتروني',
];
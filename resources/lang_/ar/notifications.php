<?php

return [
    'page_title' => 'الاشعارات',
    'latest' => 'آخر الاشعارات',
    'none' => 'لا يوجد اشعارات ',
    'explore' => 'إستكشف تفاصيل المزايدة الآن ',
    'more_details' => 'تفاصيل المزايدة ',
    'congratulation' => 'تهانينا ',
    'approve' => 'موافقة',
    'reject' => 'رفض',
    'by' => 'بواسطة',
    'no_aucation' => 'لا يوجد مزايدة للقيام بهذا الامر',
    'car_request' => 'طلب موافقة بيع %s من خلال المعرض',
    'car_approveed' => 'تم قبول السيارة %s',
    'car_reject' => 'تم رفض السيارة %s',
];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'enter-email'=>'ادخل البريد الالكترونى',
    'enter-password'=>'كلمة المرور',
    'remember-password'=>'تذكرنى',
    'login-btn'=>'تسجيل ',
    'send-me'=>'ارسل بريد ',
    'retrieve-password'=>' اعادة تعيين كلمة المرور   ',
    'enter-instructions'=>' من فضلك قم بادخال البريد الالكترونى   ',
    'back-to-login'=>'   عودة لتسجيل الدخول',
    'forget-password'=>'نسيت كلمة المرور',
    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',

];

@extends('layouts.app')

@section('content')
    <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="form-group m-form__group">
            <span class="block input-icon input-icon-right {{ $errors->has('email') ? ' has-error' : '' }}">
            <input class="form-control m-input" type="text" placeholder="Email" name="email" value="{{ old('email') }}"
                   autocomplete="off" required>
            @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group m-form__group">
            <input class="form-control m-input m-login__form-input--last" type="password" placeholder="Password"
                   name="password">
            @if ($errors->has('password'))
                <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif

        </div>
        <div class="row m-login__form-sub">
            <div class="col m--align-left">
                <label class="m-checkbox m-checkbox--focus">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                    Remember me 
                    <span></span>
                </label>
            </div>
            <div class="col m--align-right">
                <a href="{{ route('password.request') }}" id="m_login_forget_password" class="m-link">
                    Forget Password ?
                </a>
            </div>
        </div>
        <div class="m-login__form-action">
            <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                Sign In
            </button>
        </div>
    </form>
    {{--<div id="login-box" class="login-box visible widget-box no-border">--}}
        {{--<div class="widget-body">--}}
            {{--<div class="widget-main">--}}
                {{--<h4 class="header blue lighter bigger">--}}
                    {{--<i class="ace-icon fa fa-coffee green"></i>--}}
                    {{--Please Enter Your Information--}}
                {{--</h4>--}}

                {{--<div class="space-6"></div>--}}

                {{--<form method="POST" action="{{ route('login') }}">--}}
                    {{--{{ csrf_field() }}--}}
                    {{--<fieldset>--}}
                        {{--<label class="block clearfix">--}}
														{{--<span class="block input-icon input-icon-right {{ $errors->has('email') ? ' has-error' : '' }}">--}}

                                                                 {{--<input id="email" dir="ltr" type="email"--}}
                                                                        {{--class="form-control" name="email"--}}
                                                                        {{--value="{{ old('email') }}"--}}
                                                                        {{--placeholder="{{ trans('auth.enter-email') }}"--}}
                                                                        {{--required autofocus>--}}
{{--<i class="ace-icon fa fa-user"></i>--}}
                                                            {{--@if ($errors->has('email'))--}}
                                                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                                            {{--@endif--}}

														{{--</span>--}}
                        {{--</label>--}}

                        {{--<label class="block clearfix">--}}
														{{--<span class="block input-icon input-icon-right">--}}

                                                                  {{--<input id="password" type="password"--}}
                                                                         {{--class="form-control" name="password"--}}
                                                                         {{--placeholder="{{ trans('auth.enter-password') }}"--}}
                                                                         {{--required>--}}
                                                            {{--<i class="ace-icon fa fa-lock"></i>--}}

                                                            {{--@if ($errors->has('password'))--}}
                                                                {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                                            {{--@endif--}}

														{{--</span>--}}
                        {{--</label>--}}

                        {{--<div class="space"></div>--}}

                        {{--<div class="clearfix">--}}
                            {{--<label class="inline">--}}
                                {{--<input type="checkbox" name="remember"--}}
                                       {{--class="ace" {{ old('remember') ? 'checked' : '' }}/>--}}

                                {{--<span class="lbl"> {{ trans('auth.remember-password') }}</span>--}}
                            {{--</label>--}}

                            {{--<button type="submit"--}}
                                    {{--class="width-35 @if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif btn btn-sm btn-primary">--}}
                                {{--<i class="ace-icon fa fa-key"></i>--}}
                                {{--<span class="bigger-110">{{ trans('auth.login-btn') }}</span>--}}
                            {{--</button>--}}
                        {{--</div>--}}

                        {{--<div class="space-4"></div>--}}
                    {{--</fieldset>--}}
                {{--</form>--}}

            {{--</div><!-- /.widget-main -->--}}

            {{--<div class="toolbar clearfix">--}}
                {{--<div>--}}
                    {{--<a href="{{ route('password.request') }}" data-target="#forgot-box" class="forgot-password-link">--}}
                        {{--<i class="ace-icon fa fa-arrow-left"></i>--}}
                        {{--{{ trans('auth.forget-password') }}--}}
                    {{--</a>--}}
                {{--</div>--}}


            {{--</div>--}}
        {{--</div><!-- /.widget-body -->--}}
    {{--</div><!-- /.login-box -->--}}

@endsection

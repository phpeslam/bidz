@extends('layouts.app')

@section('content')

    <div id="forgot-box" class="forgot-box visible widget-box no-border">
        <div class="widget-body">
            <div class="widget-main">
                <h4 class="header red lighter bigger">
                    <i class="ace-icon fa fa-key"></i>
                    {{ trans('auth.retrieve-password') }}
                </h4>

                <div class="space-6"></div>
                <p>
                    {{ trans('auth.enter-instructions') }}

                </p>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form  method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <fieldset>
                        <label class="block clearfix">
														<span class="block input-icon input-icon-right {{ $errors->has('email') ? ' has-error' : '' }}">
															          <input id="email" type="email" class="form-control" name="email"  placeholder="{{ trans('auth.enter-email') }}" value="{{ old('email') }}" required>
<i class="ace-icon fa fa-envelope"></i>
                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                            @endif

														</span>
                        </label>

                        <div class="clearfix">
                            <button type="submit" class="width-35 pull-right btn btn-sm btn-danger">
                                <i class="ace-icon fa fa-lightbulb-o"></i>
                                <span class="bigger-110"> {{ trans('auth.send-me') }}</span>
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div><!-- /.widget-main -->

            <div class="toolbar center">
                <a href="{{ route('login') }}" data-target="#login-box" class="back-to-login-link">
                    {{ trans('auth.back-to-login') }}
                    <i class="ace-icon fa fa-arrow-right"></i>
                </a>
            </div>
        </div><!-- /.widget-body -->
    </div><!-- /.forgot-box -->

@endsection

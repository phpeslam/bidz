@extends('layouts.members')
@section('page_content')
<!-- Bidz Head -->
@if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
            {{ Session::get('message') }}
        </p>
    @endif
<div class="bidz-head">
    <div class="bid-cover">
        <a href="#" data-src="{{ $aut_detail->image }}" class="image"></a>
    </div>
    <div class="info">
        <div class="main-info">
            <h3>{{ $aut_detail->title }}</h3>
            <h5 class="auther"> {{ $aut_detail->member->first_name }} {{ $aut_detail->member->last_name }}</h5>
            <ul>
                <li class="danger-color">الحد الادني للمزايدة :  {{ $aut_detail->minimum_bid }}  ريال سعودى</li>
            </ul>
            <ul class="wide">
                <li>مشاهدات : <span>{{ (int)$aut_detail->views }}</span></li>
                <li>تسلسل : <span>{{ $aut_detail->id }}</span></li>
                <li> النوع : <span>
                        {{ trans('bid.select_bid_type_'.$aut_detail->bid_type) }}
                    </span></li>
            </ul>
            <span class="time-end">وقت الانتهاء  :  @if($aut_detail){{ $aut_detail->start_at->addDays($aut_detail->bid_days)->format('D, d/m/Y h:i') }} @endif</span>
        </div>
        <div class="price-info">
            <h3 class="price">
                السعر الحالي
                @if($aut_detail->current_price != 0 || $aut_detail->current_price != null)
                <span>{{$aut_detail->current_price}} ريال سعودى</span>
                @else
                <span>{{$aut_detail->car_price}} ريال سعودى</span>
                @endif
            </h3>
            @if($aut_detail->start_at && $aut_detail->start_at->addDays($aut_detail->bid_days)->gt(\Carbon\Carbon::now()))
            <span class="time">الوقت المتبقي</span>
            <div class="timer-block" data-year="{{ $aut_detail->start_at->addDays($aut_detail->bid_days)->year }}" data-month="{{ $aut_detail->start_at->addDays($aut_detail->bid_days)->month }}" data-day="{{ $aut_detail->start_at->addDays($aut_detail->bid_days)->day }}" data-hour="{{ $aut_detail->start_at->addDays($aut_detail->bid_days)->hour }}" data-minute="{{ $aut_detail->start_at->addDays($aut_detail->bid_days)->minute }}"></div>
            @endif
        </div>
        @if(isset(\Auth::guard('members')->user()->id))
        @if($aut_detail->isWork == 1 && $aut_detail->member_id != \Auth::guard('members')->user()->id && $aut_detail->provider_id != \Auth::guard('members')->user()->id && \Auth::guard('members')->user()->membership_type != 2 )
        <a href="{{ route('member-addbidding',encrypt([$aut_detail->id,$aut_detail->title,$aut_detail->minimum_bid])) }}" class="btn primary rounded">مزايدة</a>
        <a href="{{route('member-follow',$aut_detail->id)}}" class="btn secondary rounded">اضافه لقائمة متابعاتي</a>
        @endif
        @endif
    </div>
</div>
<!-- // Bidz Head -->

<!-- Warning -->
@if($balance == 0 && \Auth::guard('members')->check() == true)
<div class="alert warning cta-alert">
    <p>يرجي العلم انه يتوجب عليك ايداع تأمين لتتمكن من القيام بعملية المزايدة على المركبات , برجي الضغط على الرابط التالي لتتمكن من ايداع تأمين ومواصلة عملية المزايدة</p>
    <a href="{{ route('BankTransfers')}}"  class="btn dark">إيداع مبلغ التأمين</a>
</div>
@endif
<!-- // Warning -->

<!-- Add Money Modal -->
<div class="modal-box tornado-ui" id="add-money">
    <div class="modal-content">
        <div class="modal-head">
            ايداع مبلغ المزايدة
            <span class="close-modal ti-clear"></span>
        </div>

        <form class="modal-body form-ui">
            <label class="checkbox"> <input type="checkbox" checked> <span class="ti-checkmark">اقر بفحص المركبه واقبل بحالتها كما هي</span> </label>
            <input type="number" placeholder="المبلغ">
            <input type="submit" value="مزايدة" class="btn primary">
        </form>
    </div>
</div>

<!-- Full Details -->
<div class="full-details">
    <div class="row">
        <!-- Slider -->
        <div class="col-12 col-l-5">
            <div class="photo-slider">
                @foreach($aut_detail->auctionImages as $im)
                <div class="item"><a href="#" data-src="{{ $im->image }}"></a></div>
                @endforeach
            </div>

            <div class="thumbs-slider row gutter-small">

                @foreach($aut_detail->auctionImages as $im)
                <div class="item"><a href="javascript:void(0)" data-src="{{ $im->image }}"></a></div>

                @endforeach

            </div>
        </div>
        <!-- Details -->
        <div class="col-12 col-l-7">
            <h3>الوصف</h3>
            <p>{{ $aut_detail->description }}</p>
            <h3>التفاصيل</h3>
            <ul>

                <li><span> {{ trans('bid.category_id') }} :   </span>
                    {{ $aut_detail->category->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.sub_category_id') }} :   </span>
                    {{ $aut_detail->subCategory->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.car_color_id') }} :   </span>
                    {{ $aut_detail->carColor->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.car_type_id') }} :   </span>
                    {{ $aut_detail->carType->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.engine_type_id') }} :   </span>
                    {{ $aut_detail->engineType->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.fuel_id') }} :   </span>
                    {{ $aut_detail->fuel->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.import_id') }} :   </span>
                    {{ $aut_detail->import->currentLanguage->title }}
                </li> <li><span> {{ trans('bid.motion_vector_id') }} :   </span>
                    {{ $aut_detail->motionVector->currentLanguage->title }}
                </li> <li><span> {{ trans('bid.specification_id') }} :   </span>
                    {{ $aut_detail->specification->currentLanguage->title }}
                </li>
                <li><span> {{ trans('bid.manufacturing_year') }}  :  </span>
                    {{ $aut_detail->manufacturing_year }}
                </li>   <li><span> {{ trans('bid.engine_size') }}  :  </span>
                    {{ $aut_detail->engine_size }}
                </li>   <li><span> {{ trans('bid.vaild_form') }} :  </span>

                    @if($aut_detail->vaild_form==1) نعم @else لا  @endif
                </li><li><span> {{ trans('bid.form_years') }}  :  </span>
                    {{ $aut_detail->form_years }}
                </li>
                <li><span> {{ trans('bid.is_periodic_inspection') }} :   </span>
                    @if($aut_detail->is_periodic_inspection==1) نعم @else لا  @endif
                </li><li><span> {{ trans('bid.mileage') }} :  </span>
                    {{ $aut_detail->mileage }}
                </li><li><span> {{ trans('bid.chassis_no') }} :   </span>
                    {{ $aut_detail->chassis_no }}
                </li>

            </ul>
           @php($Af =  \App\Model\AuctionFile::where('auction_id',$aut_detail->id)->first())
           <a href="{{ asset('storage/auction_file/'.$Af->file ) }}" target="_blank" download="car" class="btn primary">تحميل تقرير الفحص</a>
            
        </div>
    </div>
</div>
<!-- // Full Details -->
<?php $agancy = getMemperName($aut_detail->provider_id); ?>
<!-- Contact Auther -->

<div class="contact-auther">
    <h2>مواصفات أخرى  </h2>
    <div class="row gutter-small">
    
    @foreach($aut_detail->otherSpecifications as $other)
        <div class="contact-block box-3x1">
            <a href="#" class="content-box"> {{$other->currentLanguage->title }} </a>
        </div>
    @endforeach    
    </div>
</div>

<div class="contact-auther">
    <h2>معلومات الاتصال بالمعرض</h2>
    <div class="row gutter-small">
        <div class="contact-block box-2x1">
            <a href="#" class="content-box">  الأسم : {{$agancy->first_name .' '. $agancy->last_name}} </a>
        </div>
        <div class="contact-block box-2x1">
            <a href="#" class="content-box">الهاتف  :  {{$agancy->phone_number}}</a>
        </div>
        <div class="contact-block box-2x1">
            <a href="#" class="content-box">بريد :  {{$agancy->email}}</a>
        </div>
        <div class="contact-block box-2x1">
            <a href="#" class="content-box">العنوان :  {{$agancy->address}}</a>
        </div>
        {{--<div class="contact-block box-5x1">--}}
        {{--<a href="#" class="content-box">عنوان خرائط جوجل</a>--}}
        {{--</div>--}}
    </div>
</div>


<!-- // Contact Auther -->

<!-- Other -->
<div class="other-bidz">
    <h2>مزادات مضافة حديثاً</h2>
    <div class="row">
        @foreach($addedNew as $item)
            <div class="bidz-block col-12 col-m-6 col-l-4">
                <div class="content-box">
                    <div class="image">
                        <a href="{{ route('front-bid-detail',$item->id) }}" data-src="{{ $item->image }}"></a>
                        <a href="{{ route('front-bid-detail',$item->id) }}"
                           class="ti-eye views">{{ (int)$item->views }}</a>
                        <a href="{{ route('front-bid-detail',$item->id) }}" class="bidz-id">#{{ $item->id }}</a>
                        @if($item->provider_accept==0)
                            <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip"> فى انتظار تاكيد
                                المالك </a>
                        @elseif($item->start_at && $item->start_at->addDays($item->bid_days)->gt(\Carbon\Carbon::now()))
                            <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip"> جارى المزايدة</a>
                        @else
                            @if($item->bid_type==1)
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip"> بيعت لاعلى
                                    سعر</a>
                            @else
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip">بيع بموافقه
                                    المالك</a>
                            @endif
                        @endif
                    </div>
                    <div class="info">
                        <a href="{{ route('front-bid-detail',$item->id) }}"><h3>{{ $item->title }}</h3></a>
                        <h4 class="price">السعر الحالي :{{ $item->car_price }}ر.س</h4>
                        <ul>
                            <li>الحد الادني للمزايدة {{ $item->minimum_bid }} ريال</li>
                            <li>عدد المزايدات حتي الان : {{ $item->total_bids }} مزايدة</li>
                        </ul>
                        <p>{{ str_limit($item->description,100) }}</p>
                    </div>
                    <div class="action">
                        <!-- Tiemr -->
                        @if($item->start_at && $item->start_at->addDays($item->bid_days)->gt(\Carbon\Carbon::now()))
                            <div class="timer-block"
                                 data-year="{{ $item->start_at->addDays($item->bid_days)->year }}"
                                 data-month="{{ $item->start_at->addDays($item->bid_days)->month }}"
                                 data-day="{{ $item->start_at->addDays($item->bid_days)->day }}"
                                 data-hour="{{ $item->start_at->addDays($item->bid_days)->hour }}"
                                 data-minute="{{ $item->start_at->addDays($item->bid_days)->minute }}"></div>
                        @else      
                        <div class="timer-block" data-year="{{ $item->start_at->addDays($item->bid_days)->year }}" data-month="{{ $item->start_at->addDays($item->bid_days)->month }}" data-day="{{ $item->start_at->addDays($item->bid_days)->day }}" data-hour="{{ $item->start_at->addDays($item->bid_days)->hour }}" data-minute="{{ $item->start_at->addDays($item->bid_days)->minute }}"></div>   
                        @endif
                        <a href="{{ route('front-bid-detail',$item->id) }}" class="btn secondary more">التفاصيل</a>
                        <a href="{{ route('member-follow',$item->id) }}" title="اضف للمفضله" class="btn warning ti-bookmark"></a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<!-- // Other -->
@endsection
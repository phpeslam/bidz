@extends('layouts.front')
@section('page_content')
<!-- Home Slider -->
<div class="home-slider">
    <!-- item -->
    @foreach($sliders as $slider)
    @foreach($auctions2 as $auction)
    @if($slider->auctionID == $auction->id)
    <div class="item">
        <div class="container">
            <div class="row align-center-z">
                <div class="col-12 col-m-7">
                    <img src="{{url('/')}}/uploads/slider/{{$slider->image}}" alt="">
                    <span class="price">{{trans('home.current_price')}}<i>{{number_format($auction->car_price)}}</i></span>
                </div>
                <div class="col-12 col-m-5">
                    <a href="{{ route('front-bid-detail',$auction->id) }}"><h3>{{$auction->title}} </h3></a>
                    <!-- Tiemr -->
                    <div class="timer-block" data-year="{{ $auction->start_at->addDays($auction->bid_days)->year }}" data-month="{{ $auction->start_at->addDays($auction->bid_days)->month }}" data-day="{{ $auction->start_at->addDays($auction->bid_days)->day }}" data-hour="{{ $auction->start_at->addDays($auction->bid_days)->hour }}" data-minute="{{ $auction->start_at->addDays($auction->bid_days)->minute }}"></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endforeach
    @endforeach
    <!-- // item -->
</div>
<!-- // Home Slider -->

<!-- Page Content -->
<div class="container page-content">
    <!-- Banners -->
    <div class="row">
    @if(count($ads)>0)
    @foreach($ads as $ad)
   
        <a href="{{$ad->currentLanguage->link}}" class="banner col-12 col-m-6"><img src="{{ asset('front/ar/img/'.$ad->currentLanguage->image) }}" alt="" style="width:570px;height:208px;"></a>
    @endforeach  
    @else  
    <a href="#" class="banner col-12 col-m-6"><img src="{{ asset('front/'.getCurrentLang('direction').'/img/banner.png') }}" alt=""></a>
    <a href="#" class="banner col-12 col-m-6"><img src="{{ asset('front/'.getCurrentLang('direction').'/img/banner-2.png') }}" alt=""></a>
    @endif
    </div>

    <!-- // Banners -->

    <!-- Bidz -->
    <div class="tabs bidz-tabs">
        <!-- Options and Tabs -->
        <div class="head">
            

            <form class="form-ui"  method="get" action="{{route('search')}}">
                {{ csrf_field() }}
                <ul class="tabs-menu" id ="myid">
                    <li data-tab="tab-1" class="ti-launch-rocket" id='1'>المزادات الحالية</li>
                    <li data-tab="tab-2" class="ti-check-circular-button" id='0'>مزادات مغلقة</li>
                 </ul>
                <div class="col-12">
                    <label>عرض حسب</label>
                    <!-- <input type="hidden" name='action_type' id='action_typeII' value='1' > -->
                    <label class="modern-check">
                        <select class="" name="action_type" id="action_type">
                            <option value="1">المزادات الحالية</option>
                            <option value="0">مزادات مغلقة</option>
                            <option value="-1"> الكل</option>

                        </select>
                    </label>

                    <label class="modern-check">
                        <select class="" name="bid_type" id="bid_type">
                            <option value="">{{ trans('bid.select_bid_type') }}</option>
                            <option value="1">{{ trans('bid.select_bid_type_1') }}</option>
                            <option value="2">{{ trans('bid.select_bid_type_2') }}</option>

                        </select>
                    </label>
                    <label class="modern-check">
                        <select class="" name="category_id" id="category_id">
                            <option value="">{{ trans('bid.select_category_id') }}</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->currentLanguage->title }}
                            </option>
                            @endforeach
                        </select>
                    </label>
                    <label class="modern-check">
                        <select class="" name="minimum_bid" id="minimum_bid">
                            <option value="">{{ trans('bid.minimum_bid') }}</option>
                            @for($i=100;$i<=3000;$i=$i+100)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </label>
                    <label class="modern-check">
                        <select class="" name="max_bid" id="max_bid">
                            <option value="">{{ trans('bid.select_max_bid') }}</option>
                            @for($i=100;$i<=3000;$i=$i+100)
                            <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                    </label>
                    <label class="modern-check">
                        <select class="" name="sort" id="bid_sort">
                            <option value="">{{ trans('bid.bid_sort') }}</option>
                            <option value="desc">{{ trans('bid.desc_asc') }}</option>
                            <option value="asc">{{ trans('bid.asc_desc') }}</option>
                        </select>
                    </label>
                    <label class="modern-check">
                        <select class="" name="manufacturing_year_from" id="manufacturing_year_from">
                            <option value="">{{ trans('bid.manufacturing_year_from') }}</option>
                            @for($i=date('Y');$i>=1980;--$i)
                            <option value="{{ $i }}"
                                    @if(old('manufacturing_year')==$i) selected="selected" @endif
                                    >{{ $i }}</option>
                            @endfor
                        </select>
                    </label>
                    <label class="modern-check">
                        <select class="" name="manufacturing_year_to" id="manufacturing_year_to">
                            <option value="">{{ trans('bid.manufacturing_year_to') }}</option>
                            @for($i=date('Y');$i>=1980;--$i)
                            <option value="{{ $i }}"
                                    @if(old('manufacturing_year')==$i) selected="selected" @endif
                                    >{{ $i }}</option>
                            @endfor
                        </select>
                    </label>
                </div>
                <div class="col-12">
                    <!--<label>بحث سريع</label>-->
                    <div class="search-box">
                        <!--<input type="text" placeholder="كلمات البحث تكتب هنا">
                        <button class="ti-search search-btn"></button>-->
                    </div>
                    <button type="submit" class="btn secondary">بحث متقدم</button>
                </div>
            </form>
        </div>
        <!-- // Options and Tabs -->

        <div class="tabs-wraper">
            <!-- Tab Content -->
            <div class="tab-content" id="tab-1">
                <div class="row">
                    <!-- Bidz Block -->
                    @foreach($items as $item)
                    @if($item->start_at && $item->start_at->addDays($item->bid_days)->gt(\Carbon\Carbon::now()))
                    <div class="bidz-block col-12 col-m-6 col-l-4">
                        <div class="content-box">
                            <div class="image">
                                <a href="{{ route('front-bid-detail',$item->id) }}" data-src="{{ $item->image }}"></a>
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="ti-eye views">{{ (int)$item->views }}</a>
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="bidz-id">#{{ $item->id }}</a>
                                @if($item->bid_type == 1)
                                <a href="#" class="sale-tip">{{ trans('bid.select_bid_type_1') }}</a>
                                @else
                                <a href="#" class="sale-tip">{{ trans('bid.select_bid_type_2') }}</a>
                                @endif
                            </div>
                            <div class="info">
                                <a href="{{ route('front-bid-detail',$item->id) }}"><h3>{{$item->title}}</h3></a>
                                <h5 class="auther">{{$item->member->first_name}} {{$item->member->last_name}}</h5>
                                <h4 class="price">{{ trans('home.current_price') }} : {{$item->car_price}} {{ trans('home.sar') }}</h4>
                                <ul>
                                    <li>{{ trans('home.minimum_bid') }} {{$item->minimum_bid}} {{ trans('home.sar') }}</li>
                                    <li>{{ trans('home.number_bid') }} {{$item->total_bids}} {{ trans('home.bid') }}</li>
                                </ul>
                                <p>{{ str_limit($item->description,100) }}</p>
                            </div>
                            <div class="action">
                                <!-- Tiemr -->
                                <div class="timer-block" data-year="{{ $item->start_at->addDays($item->bid_days)->year }}" data-month="{{ $item->start_at->addDays($item->bid_days)->month }}" data-day="{{ $item->start_at->addDays($item->bid_days)->day }}" data-hour="{{ $item->start_at->addDays($item->bid_days)->hour }}" data-minute="{{ $item->start_at->addDays($item->bid_days)->minute }}"></div>
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="btn secondary more">{{ trans('home.details') }}</a>
                                <a href="{{ route('member-follow',$item->id) }}" title="اضف للمفضله" class="btn warning ti-bookmark"></a>

                                <a href="{{ route('member-addbidding',encrypt([$item->id,$item->title,$item->minimum_bid])) }}" class="btn primary add-bid">{{ trans('home.bid') }}</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    <!-- // Bidz Block -->
                </div>
            </div>

            <!-- Tab Content -->
            <div class="tab-content" id="tab-2">
                <div class="row">
                    <!-- Bidz Block -->
                    @foreach($items as $item)
                    @if($item->start_at && $item->start_at->addDays($item->bid_days)->lt(\Carbon\Carbon::now()))
                    <div class="bidz-block col-12 col-m-6 col-l-4">
                        <div class="content-box">
                            <div class="image">
                                <a href="{{ route('front-bid-detail',$item->id) }}" data-src="{{ $item->image }}"></a>
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="ti-eye views">{{ (int)$item->views }}</a>
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="bidz-id">#{{ $item->id }}</a>
                                @if($item->bid_type == 1)
                                <a href="#" class="sale-tip">{{ trans('bid.select_bid_type_1') }}</a>
                                @else
                                <a href="#" class="sale-tip">{{ trans('bid.select_bid_type_2') }}</a>
                                @endif
                            </div>
                            <div class="info">
                                <a href="#"><h3>{{$item->title}}</h3></a>
                                <h5 class="auther">{{$item->member->first_name}} {{$item->member->last_name}}</h5>
                                <h4 class="price">{{ trans('home.current_price') }} : {{ $item->current_price == null ? $item->car_price : $item->current_price}} {{ trans('home.sar') }}</h4>
                                <ul>
                                    <li>{{ trans('home.minimum_bid') }} {{$item->minimum_bid}} {{ trans('home.sar') }}</li>
                                    <li>{{ trans('home.number_bid') }} {{$item->total_bids}} {{ trans('home.bid') }}</li>
                                </ul>
                                <p>{{ str_limit($item->description,100) }}</p>
                            </div>
                            <div class="action">
                                <!-- Tiemr -->
                                <div class="timer-block" data-year="{{ $item->start_at->addDays($item->bid_days)->year }}" data-month="{{ $item->start_at->addDays($item->bid_days)->month }}" data-day="{{ $item->start_at->addDays($item->bid_days)->day }}" data-hour="{{ $item->start_at->addDays($item->bid_days)->hour }}" data-minute="{{ $item->start_at->addDays($item->bid_days)->minute }}"></div>
                                <a href="{{ route('front-bid-detail',$item->id) }}" class="btn secondary more">{{ trans('home.details') }}</a>
                                <a href="{{ route('member-follow',$item->id) }}" title="اضف للمفضله" class="btn warning ti-bookmark"></a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    <!-- // Bidz Block -->
                </div>
            </div>
        </div>
    </div>
    <!-- // Bidz -->

    <!--<a href="#" class="big-more">عرض المزيد من المزايدات</a>-->
</div>
<!-- // Page Content -->

<!-- Partners -->
<div class="partners-section">
    <div class="container">
        <!-- Partners Slider -->
        <div class="partners">
            <h3>{{ trans('home.partners') }}</h3>
            <div class="partners-slider row">
                <!-- item -->
                @foreach($partners as $partner)
                <div class="col-6 item"><a href="#"><img src="{{ asset('uploads/partners/'.$partner->image) }}" alt=""></a></div>
                @endforeach
            </div>
        </div>
        <!-- // Partners Slider -->
    </div>
</div>
<!-- // Partners -->

@endsection


@push('page_footer')
<script type="text/javascript">


jQuery(document).ready(function($) {
    $("#myid li").click(function() {
    $("#action_typeII").val() = this.id;

    console.log(this.id);
});
});
</script>

@endpush
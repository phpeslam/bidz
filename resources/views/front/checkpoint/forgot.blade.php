@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>نسيت كلمة المرور</h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="{{ url()->current() }}">نسيت كلمة المرور</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
        <div class="row">
            <!-- Login -->
            <div class="col-8 primary-box">
                <div class="content-box">
                    <h3 class="head"> نسيت كلمة المرور</h3>
                    @include('wedgets.notification')
                    <form class="form-ui" method="post" action="{{ route('forgot-pass') }}" id="login_frm" name="login_frm">
                        {!! csrf_field() !!}
                        <label>      البريد الالكتروني</label>
                        <input dir="ltr" name="email" value="{{ old('email') }}" id="email" type="email" placeholder="البريد الالكتروني">
                        @if ($errors->has('email'))
                            <div class="error">
                                {{ $errors->first('email') }}
                            </div>
                        @endif

                        
                        <input type="submit" value="إرسال " class="btn secondary round">
                    </form>
                </div>
            </div>
            <!-- Signup -->

        </div>
    </div>
    <!-- // Page Content -->

@endsection


@push('footer')

@endpush




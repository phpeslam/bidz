@extends('layouts.inner')
@section('page_title','     تفعيل العضويه ')
@section('page_content')
    <section class="inside-main-page">
        <span class="custom-space"></span>




        <section class="inside-main-page">
            <span class="custom-space"></span>

            <div class="container">
                <div class="col-md-6 col-md-offset-3">
                    @include('wedgets.notification')
                    @if(\Auth::user()->mobile_verified != 1 )
                <div class="row">
                    <form method="post" name="form_active_sms" id="form_active_sms" action="{{ route('checkpoint-do-active') }}">
                        {!! csrf_field() !!}
         <input type="hidden" name="active_type" id="active_type_sms" value="sms" />
                        <span class="custom-space-xs"></span>
                        <p class="txt-note txt-center">ستصلك رساله <b class="red-color">SMS</b> على جوالك ({{ \Auth::user()->mobile_number }}) تحتوى على كود التفعيل</p>
                        <span class="custom-space-sm"></span>
                        <input type="text" placeholder="كود التفعيل" name="mobile_verify_code" id="mobile_verify_code"   dir="ltr" class="custom-input gary-input">
                        <span class="custom-space-sm"></span>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <input type="submit" value="تفعيل الجوال"  class="big-btn r-40 red-bg red-hover">
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="alert-warning ">

                            اذا لم تصلك رسالة التفعيل من فضلك
                            <a href="javascript:void(0);" rel="m" class="resend">اضغط هنا </a>

                        </div>
                    </form>


                </div><!--End search-->
                    <span class="custom-space"></span>
                    @endif
                    @if(\Auth::user()->email_verified != 1 )
                <div class="row">
                    <form method="post" name="form_active_email" id="form_active_email" action="{{ route('checkpoint-do-active') }}">
                        {!! csrf_field() !!}
                        <input type="hidden" name="active_type" id="active_type_sms" value="email" />
                        <span class="custom-space-xs"></span>
                        <p class="txt-note txt-center">ستصلك رساله <b class="red-color">Email</b> على بريديك ({{ \Auth::user()->email }}) تحتوى على كود التفعيل</p>
                        <span class="custom-space-sm"></span>
                        <input type="text" placeholder="كود التفعيل" dir="ltr" name="email_verify_code" id="email_verify_code" class="custom-input gary-input">
                        <span class="custom-space-sm"></span>
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                <input type="submit" value="تفعيل البريد" class="big-btn r-40 red-bg red-hover">
                            </div>
                        </div>
                        <br />
                        <br />
                        <div class="alert-warning ">

                          من فضلك قم بمراجعة البريد الالكترونى وممكن ان تكون الرسالة فى الرسائل المهملة junk mail  اذا لم تصل الرسالة من فضلك
                            <a href="javascript:void(0);" rel="e" class="resend">اضغط هنا </a>

                        </div>
                    </form>

                </div><!--End search-->
                    <span class="custom-space"></span>
                    @endif

                    @if(\Auth::user()->is_active==2)
                        <div class="alert alert-warning">
                            <strong>تحذير!</strong>

                            تم اغلاق الحساب الخاص بكم  واذا واجهت اى مشكلة من فضلك قبم بالتواصل معنا عبر
                            <a href="{{ route('contactUs') }}" >اتصل بنا </a>
                        </div>
                    @endif

                    @if(\Auth::user()->mobile_verified == 1 && \Auth::user()->email_verified ==1 && \Auth::user()->is_active ==0)


                            <div class="alert alert-info">
                                <strong>معلومات!</strong>
                                يرجى الانتظار حتى يتم تفعيل حسابكم من إدارة الموقع  واذا واجهت اى مشكلة يرجى مرسالتنا من خلال صفحة
                                <a href="{{ route('contactUs') }}" >اتصل بنا </a>
                            </div>





                        @endif
            </div>
            </div>

        </section>







    </section><!--End inside-main-page-->

    @endsection


@push('page_footer')
<script type="text/javascript">

$('a.resend').click(function(){
    var current_i = $(this).attr('rel');
    var current_row = $(this).closest('div.row');
    current_row.LoadingOverlay("show");
    $.post('{{ route('checkpoint-do-resend') }}',{
        action:current_i,
        _token: '{{ csrf_token() }}'
    },function (result) {
        current_row.LoadingOverlay("hide");
        if(result[0]){
            toastr.success(result[1]);
        }else{
            toastr.warning(result[1]);
        }
    },'json');
})  ;

</script>

@endpush




@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>نسيت كلمة المرور</h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="{{ url()->current() }}">نسيت كلمة المرور</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
        <div class="row">
            <!-- Login -->
            <div class="col-12 col-m-5 primary-box">
                <div class="content-box">
                    <h3 class="head"> نسيت كلمة المرور</h3>
                    @include('wedgets.notification')
                    <form class="form-ui" method="post" action="{{ route('reset-pass') }}" id="login_frm" name="login_frm">
                        {!! csrf_field() !!}
                        <label>      كود التحقق</label>
                        <input dir="ltr" name="code" id="email" type="code" placeholder="كود التحقق">
                        @if ($errors->has('code'))
                            <div class="error">
                                {{ $errors->first('code') }}
                            </div>
                        @endif
                        <label>      كلمة السر الجديد</label>
                        <input dir="ltr" name="password" type="password" placeholder="كلمة السر الجديد">
                        @if ($errors->has('password'))
                            <div class="error">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                        <label>       اعد كتابة كلمة السر الجديد</label>
                        <input dir="ltr" name="password_confirmation"  type="password" placeholder="كلمة السر الجديد">
                        @if ($errors->has('password_confirmation'))
                            <div class="error">
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        @endif

                        
                        <input type="submit" value="تسجيل الدخول" class="btn secondary round">
                    </form>
                </div>
            </div>
            <!-- Signup -->

        </div>
    </div>
    <!-- // Page Content -->

@endsection


@push('footer')

@endpush





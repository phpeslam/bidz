@extends('layouts.inner')
@section('page_title','        ايقاف الصالون ')
@section('page_content')
    <section class="inside-main-page">
        <span class="custom-space"></span>




        <section class="inside-main-page">
            <span class="custom-space"></span>

            <div class="container">
                <div class="col-md-6 col-md-offset-3">
                    @include('wedgets.notification')

                        <div class="alert alert-warning">
                            <strong>تحذير!</strong>

                           تم ايقاف الصالون الخاص بكم  اذا كنت تواجه اى مشكلة لا تردد بالاتصال بنا عبر صفحة
                            <a href="{{ route('home-contact-us') }}" >اتصل بنا </a>
                        </div>


            </div>
            </div>

        </section>







    </section><!--End inside-main-page-->

    @endsection


@push('page_footer')
<script type="text/javascript">
    var get_district = '{{ route('globals-district') }}';
</script>
<script  src="{{ asset('front/model/js/register/index.js') }}" ></script>
@endpush


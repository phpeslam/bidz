@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>تسجيل الدخول</h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="{{ url()->current() }}">تسجيل الدخول</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
        <div class="row">
            <!-- Login -->
            <div class="col-12 col-m-5 primary-box">
                <div class="content-box">
                    <h3 class="head">تسجيل الدخول</h3>
                    @include('wedgets.notification')
                    <form class="form-ui" method="post" action="{{ route('front-login-sub') }}" id="login_frm" name="login_frm">
                        {!! csrf_field() !!}
                        <label>      البريد الالكتروني</label>
                        <input dir="ltr" name="email" value="{{ old('email') }}" id="email" type="email" placeholder="البريد الالكتروني">
                        <label>كلمة المرور</label>
                        <input type="password" name="password" id="password" placeholder="يرجي ادخال كلمة المرور">
                        <label class="checkbox">
                            <input type="checkbox" value="1" name="remember_me" id="remember_me">
                            <span class="ti-checkmark">تذكر حسابي</span>

                            
                        </label>

                        <a href="{{ route('forgot') }}" data-target="#forgot-box" class="forgot-password-link">
                        <i class="ace-icon fa fa-arrow-left"></i>
                        {{ trans('auth.forget-password') }}
                            </a>
                        <input type="submit" value="تسجيل الدخول" class="btn secondary round">
                    </form>
                </div>
            </div>
            <!-- Signup -->
            <div class="col-12 col-m-7 primary-box">
                <div class="content-box">
                    <h3 class="head">انشاء حساب جديد</h3>

                    <div class="signup-notfi">
                        <h2>ليس لديك حساب على منصتنا يمكنك انشاء حساب الان بكل سهوله</h2>
                        <a href="{{ route('front-register') }}" class="btn secondary round block-lvl">أنشاء حساب جديد</a>
                        <div class="social-login row">
                            <div class="col-12 col-m-5"><a href="{{ route('front-register') }}" class="btn dark round block-lvl">تسجيل حساب عادي</a></div>
                            <div class="col-12 col-m-2"><h3>أو</h3> </div>
                            <div class="col-12 col-m-5"><a href="{{ route('front-register') }}" class="btn primary round block-lvl">التسجيل كمعرض</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Content -->

@endsection


@push('footer')

@endpush


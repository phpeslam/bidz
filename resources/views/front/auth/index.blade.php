@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>{{ trans('register.create_new_account') }}</h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="{{ route('front-register') }}">{{ trans('register.create_new_account') }}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
        <!-- Global Form -->
        <div class="primary-box">
            <div class="content-box">
                <h3 class="head">{{ trans('register.create_new_account') }}</h3>
                <div class="social-login row align-center-z" id="change_type">
                    <div class="col-12 col-m-3"><a href="javascript:void(0);" rel="1" class="btn @if(old('membership_type',1)==1) primary @else dark @endif round block-lvl" onclick="hide_form();"> {{ trans('register.create_member') }}    </a></div>
                    <div class="col-12 col-m-1"><h3>{{ trans('register.or') }}</h3> </div>
                    <div class="col-12 col-m-3"><a href="javascript:void(0);" rel="2" class="btn @if(old('membership_type',1)==2) primary @else dark @endif  round block-lvl" onclick="show_form();">   {{ trans('register.create_market') }}</a></div>
                </div>
                <form class="form-ui row" action="{{ route('front-register-save') }}" method="post" name="form_reg" id="form_reg" enctype="multipart/form-data">
                    {!! csrf_field() !!}
                     <input type="hidden" value="{{ old('membership_type',1) }}" id="membership_type" name="membership_type" />
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('first_name') ? ' error' : '' }}">{{ trans('register.first_name') }}</label>
                        <input class="{{ $errors->has('first_name') ? ' error' : '' }}" type="text" placeholder="{{ trans('register.first_name') }}" value="{{ old('first_name') }}" name="first_name" id="first_name" />
                        @if ($errors->has('first_name'))
                            <div class="error">
                                 {{ $errors->first('first_name') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('last_name') ? ' error' : '' }}">  {{ trans('register.last_name') }}</label>
                        <input class="{{ $errors->has('last_name') ? ' error' : '' }}" type="text" placeholder="{{ trans('register.last_name') }}" value="{{ old('last_name') }}"  name="last_name" id="last_name" />
                        @if ($errors->has('last_name'))
                            <div class="error">
                                {{ $errors->first('last_name') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('email') ? ' error' : '' }}"> {{ trans('register.email') }}  </label>
                        <input class="{{ $errors->has('email') ? ' error' : '' }}" type="email" placeholder="{{ trans('register.email') }}" dir="ltr" value="{{ old('email') }}"  name="email" id="email" />
                        @if ($errors->has('email'))
                            <div class="error">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('email_confirmation') ? ' error' : '' }}">     {{ trans('register.email_confirmation') }}</label>
                        <input class="{{ $errors->has('email_confirmation') ? ' error' : '' }}" type="email" placeholder="{{ trans('register.email_confirmation') }}" value="{{ old('email_confirmation') }}"  dir="ltr" name="email_confirmation" id="email_confirmation"  />
                        @if ($errors->has('email_confirmation'))
                            <div class="error">
                                {{ $errors->first('email_confirmation') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('password') ? ' error' : '' }}">   {{ trans('register.password') }}</label>
                        <input class="{{ $errors->has('password') ? ' error' : '' }}" type="password" placeholder="{{ trans('register.password') }}" name="password" id="password" />
                        @if ($errors->has('password'))
                            <div class="error">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('password_confirmation') ? ' error' : '' }}">     {{ trans('register.password_confirmation') }}</label>
                        <input class="{{ $errors->has('password_confirmation') ? ' error' : '' }}" type="password" placeholder="{{ trans('register.password_confirmation') }}    " name="password_confirmation" id="password_confirmation" />
                        @if ($errors->has('password_confirmation'))
                            <div class="error">
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="  col-12 col-l-6">
                        <label class="{{ $errors->has('country_id') ? ' error' : '' }}">{{ trans('register.country_id') }}</label>
                        <select class="{{ $errors->has('country_id') ? ' error' : '' }}" name="country_id" id="country_id" >
                            <option value="">{{ trans('register.country_id_select') }}</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @if(old('country_id') == $country->id) selected="selected" @endif>{{ $country->currentLanguage->title }}</option>
                                @endforeach
                        </select>
                        @if ($errors->has('country_id'))
                            <div class="error">
                                {{ $errors->first('country_id') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('city_id') ? ' error' : '' }}">{{ trans('register.city_id') }}</label>
                        <select class="{{ $errors->has('city_id') ? ' error' : '' }}" name="city_id" id="city_id" @if(isset($cities) && count($cities)>0)  @else disabled="disabled"  @endif>
                            <option value="">{{ trans('register.city_id_select') }}</option>
                            @if(isset($cities) && count($cities)>0)
                                @foreach($cities as $city)
                                    <option value="{{ $city['id'] }}" @if(old('city_id') == $city['id']) selected="selected" @endif>{{ $city['title'] }}</option>
                                    @endforeach
                                @endif
                        </select>
                        @if ($errors->has('city_id'))
                            <div class="error">
                                {{ $errors->first('city_id') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('phone_number') ? ' error' : '' }}">{{ trans('register.phone_number') }}</label>
                        <input class="{{ $errors->has('phone_number') ? ' error' : '' }}" type="tel" dir="ltr" placeholder="{{ trans('register.phone_number') }}" value="{{ old('phone_number') }}"  name="phone_number" id="phone_number" >
                        @if ($errors->has('phone_number'))
                            <div class="error">
                                {{ $errors->first('phone_number') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('address') ? ' error' : '' }}">{{ trans('register.address') }}</label>
                        <input class="{{ $errors->has('address') ? ' error' : '' }}" type="text" placeholder="{{ trans('register.address') }}" value="{{ old('address') }}"  name="address" id="address" >
                        @if ($errors->has('address'))
                            <div class="error">
                                {{ $errors->first('address') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <!-- Control -->
                    <div class="col-12 col-l-6" id="market_image" style="display: none">
                        <label class="">{{ trans('register.image') }}</label>
                        <input class="" type="file" placeholder="{{ trans('register.address') }}" value="{{ old('address') }}"  name="image" id="address" >
                        @if ($errors->has('image'))
                            <div class="error">
                                {{ $errors->first('image') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12">
                        <label > {{ trans('register.terms_cond') }}  </label>
                        <div class="faqs-signup">
                            <p>هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق  هذا النص يمكن أن يتم تركيبه على أي تصميم دون مشكلة فلن يبدو وكأنه نص منسوخ، غير منظم، غير منسق، أو حتى غير مفهوم. لأنه مازال نصاً بديلاً ومؤقتاً.</p>
                            <p>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</p>
                            <p>ومن هنا وجب على المصمم أن يضع نصوصا مؤقتة على التصميم ليظهر للعميل الشكل كاملاً،دور مولد النص العربى أن يوفر على المصمم عناء البحث عن نص بديل لا علاقة له بالموضوع الذى يتحدث عنه التصميم فيظهر بشكل لا يليق.</p>
                        </div>
                        <label class="checkbox {{ $errors->has('terms_cond_accept') ? ' error' : '' }}">
                            <input class="{{ $errors->has('terms_cond_accept') ? ' error' : '' }}" type="checkbox" name="terms_cond_accept" id="terms_cond_accept">
                            <span class="ti-checkmark"> {{ trans('register.terms_cond_accept') }}      </span>
                            @if ($errors->has('terms_cond_accept'))
                                <div class="error">
                                    {{ $errors->first('terms_cond_accept') }}
                                </div>
                            @endif
                        </label>
                        <input type="submit" value=" {{ trans('register.create_new_account_btn') }}  " class="btn primary">
                    </div>
                </form>
            </div>
        </div>
        <!-- // Global Form -->
    </div>
    <!-- // Page Content -->

@endsection


@push('footer')
<script type="text/javascript">
$(function(){
   $('div#change_type a').click(function(){
       if(!$(this).hasClass('primary')){
           $('div#change_type a').removeClass('primary').removeClass('dark').addClass('dark');
           $(this).removeClass('dark').addClass('primary');
           $('input#membership_type').val($(this).attr('rel'));
       }
   }) ;
   $('select#country_id').change(function(){
       $('select#city_id').html('').attr('disabled',true);
       var country_id = $(this).val();

       if(country_id != '') {
           $.get('{{ route('front-register-cities') }}', {
               country_id:country_id
           },function(result){
               if(result.length>0){
                   $('select#city_id').html('<option value="">{{ trans('register.city_id_select') }}</option>');
                   $.each(result,function (x,item) {
                       $('select#city_id').append('<option value="'+item.id+'">'+item.title+'</option>');
                   });
                   $('select#city_id').removeAttr('disabled');
               }
           },'json');
       }
   });
});

function show_form(){
    $('#market_image').css('display','block');
}
function hide_form(){
    $('#market_image').css('display','none');
}
</script>
@endpush


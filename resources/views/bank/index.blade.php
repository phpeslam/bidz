@extends('layouts.members')
@section('page_content')
    @include('wedgets.notification')
    <div class="container page-content tabs tabing-bnk">
        <ul class="tabs-menu">
            <li data-tab="tab-1">{{trans('Transfers.BankTransfers')}}</li>
            <li data-tab="tab-2">{{trans('Transfers.ElctonPayment')}}</li>
        </ul>
        <!-- Global Form -->
        <div class="tabs-wraper">
            <div class="tab-content" id="tab-1">
                <div class="row">
                <div class="col-12 col-m-12">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                                {{ Session::get('message') }}
                            </p>
                        @endif
                    </div>
                    <div class="col-12 col-m-6 bank">
                        <h3>{{$bank_name->value}}</h3>
                        <img src="{{ asset('front/'.getCurrentLang('direction').'/img/bank-1.png') }}">
                         <ul>
                            <li>{{$bank_account->value}}</li>
                            <!-- <li>بيانات البنك تكتب هنا</li>
                            <li>بيانات البنك تكتب هنا</li>
                            <li>بيانات البنك تكتب هنا</li>
                            <li>بيانات البنك تكتب هنا</li> -->
                        </ul> 
                    </div>

                    <div class="col-12 col-m-6 bank">
                        <h3>{{$bank_name->value}}</h3>
                        <img src="{{ asset('front/'.getCurrentLang('direction').'/img/bank-2.png') }}">
                        <ul>
                            <li>{{$bank_account->value}}</li>
                            <!-- <li>بيانات البنك تكتب هنا</li>
                            <li>بيانات البنك تكتب هنا</li>
                            <li>بيانات البنك تكتب هنا</li>
                            <li>بيانات البنك تكتب هنا</li> -->
                        </ul>
                    </div>
                    

                    <form class="col-12 col-m-12 form-ui" method="post" action="{{url('Transfer')}}"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <label>{{trans('Transfers.type')}}</label>
                        <select name="type" id="" required>
                            <option value="1">{{trans('Transfers.add_balance')}}</option>
                            <option value="2">{{trans('Transfers.Insur_balance')}}</option>
                            <option value="3">{{trans('Transfers.Special_Ads')}}</option>
                        </select>

                        <label>{{trans('Transfers.image')}}</label>
                        <input type="file" name="image" required>

                        <!-- <label>{{trans('Transfers.date')}}</label>
                        <input type="datetime-local" name="date" required> -->

                        <label>{{trans('Transfers.value')}}</label>
                        <input type="number" name="value" required>

                        <input type="submit" value="{{trans('Transfers.send')}}" class="btn primary">
                    </form>

                </div>
            </div>

            <div class="tab-content" id="tab-2">
                <div class="row">
                <div class="col-12 col-m-6">
                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                                {{ Session::get('message') }}
                            </p>
                        @endif
                    </div>

                    <form class="col-12 col-m-6 form-ui" method="post" action="{{route('paytabs')}}"
                          enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        
                        <label>{{trans('Transfers.value')}}</label>
                        <input type="text" name="value" required>

                        <input type="submit" value="{{trans('Transfers.send')}}" class="btn primary">
                    </form>


                    
                </div>
            </div>
        </div>
        <!-- // Global Form -->
    </div>
@endsection
@extends('layouts.members')
@section('page_content')
    @include('wedgets.notification')
    <!-- Contact US -->
    <div class="row">
        <div class="col-12 col-m-4 contact-info">
            <!-- Main Contact -->
            @php
            $phones = \App\Model\Setting::where('key','phone')->first();
            $emails = \App\Model\Setting::where('key','email')->first();
            $addressAR = \App\Model\Setting::where('key','address_ar')->first();
            $phones = explode(',',$phones->value);
            $emails = explode(',',$emails->value);

            $facebook = \App\Model\Setting::where('key','facebook')->first();
            $twitter = \App\Model\Setting::where('key','twitter')->first();
            $google_plus = \App\Model\Setting::where('key','google_plus')->first();
            $instagram = \App\Model\Setting::where('key','instagram')->first();
            @endphp
            <div class="main-contact ti-phone">
            <p>
            @foreach($phones as $phone)
                 {{$phone}}<br> 
            @endforeach
            </p>
            </div>
            <!-- Main Contact -->
            <div class="main-contact ti-mail">
                <p>
                @foreach($emails as $phone)
                    {{$phone}}<br> 
                @endforeach
                </p>
            </div>
            <!-- Main Contact -->
            <div class="main-contact ti-map-marker-multiple">
                <p>{{$addressAR->value}}</p>
            </div>
            <!-- Social -->
            <div class="social-contact">
                <h4>تابعونا على موقع التواصل الاجتماعي</h4>
                <a href="{{$facebook->value}}" class="ti-facebook"></a>
                <a href="{{$twitter->value}}" class="ti-twitter"></a>
                <a href="{{$google_plus->value}}" class="ti-google-plus"></a>
                <a href="{{$instagram->value}}" class="ti-instagram"></a>
            </div>
        </div>
        <div class="col-12 col-m-8 primary-box">
            <div class="content-box">
                <h3 class="head">ارسل لنا رساله</h3>
                @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                        {{ Session::get('message') }}
                    </p>
                    @endif
                <form class="form-ui row" method="post" action="{{url('sendmessage')}}">
                    {!! csrf_field() !!}
                    <div class="col-12 col-m-6">
                        <label>الاسم كامل</label>
                        <input type="text" placeholder="اكتب اسمك هنا" name="name" required>
                    </div>
                    <div class="col-12 col-m-6">
                        <label>البريد الالكتروني</label>
                        <input type="email" placeholder="اكتب بريدك الالكتروني" name="email" required>
                    </div>
                    <div class="col-12">
                        <label>الرساله</label>
                        <textarea placeholder="اكتب رسالتك هنا" name="message" required> </textarea>
                        <input type="submit" value="ارسال" class="btn secondary prim">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Map -->
    <div class="map">
        <img src="img/map.png" alt="">
    </div>
@endsection
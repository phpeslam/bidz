@extends('layouts.members')
@section('page_content')
@include('wedgets.notification')

<br/>

<div class="container page-content">
    <!-- About US -->
    <div class="about-us row row-reverse align-center-y">
        <div class="col-12 col-m-5 col-l-4">
            <img src="{{ asset('front/'.getCurrentLang('direction').'/img/about.png') }}" alt="" class="fluid">
        </div>
        <div class="col-12 col-m-7 col-l-8">
            <h2>{{$page->currentLanguage->title}}</h2>
            @php echo html_entity_decode($page->currentLanguage->desc); @endphp
        </div>
    </div>
    <!-- // About US -->
</div>
@endsection
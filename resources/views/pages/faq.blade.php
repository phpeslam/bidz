@extends('layouts.members')
@section('page_content')
    @include('wedgets.notification')
    <!-- Accordion Wraper -->
    <div class="accordion tornado-ui">
        <?php $x = 1 ?>
        @foreach($Faqs as $Faq)
            <div class="accordion-item">
                <div class="accordion-title ti-arrow-down-chevron">{{$x . '-'. $Faq->currentLanguage->title}}</div>
                <div class="accordion-content">
                    <p>{{$Faq->currentLanguage->desc}}</p>
                </div>
            </div>
            <?php $x++; ?>
        @endforeach
    </div>
    <!-- // End Accordion Wraper -->
@endsection
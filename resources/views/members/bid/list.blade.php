@extends('layouts.members')
@section('page_content')
    @include('wedgets.notification')
    <!-- Big User Area -->
    <div class="bua">
        <div class="info">
            @if(\Auth::guard('members')->user()->membership_type ==1)
            <a href="{{ route('member-bid-list') }}" class="btn primary round">مناقصاتي كبائع</a>
            <a href="#" class="btn secondary round">مناقصاتي كمشتري</a>
            @endif
        </div>
        <a href="{{url('BankTransfers')}}" class="btn primary big round">إضافة رصيد أخري الي حسابي</a>
    </div>

    <!-- Bidz -->
    <div class="container page-content">
    @if(count($items) > 0)
        @foreach($items as $item)

        <div class="bidz-block wide-theme col-12 col-m-6 col-l-4">
            <div class="content-box">
                <div class="image">
                    <a href="{{ route('front-bid-detail',$item->id) }}" data-src="{{ $item->image }}"></a>
                    <a href="{{ route('front-bid-detail',$item->id) }}" class="ti-eye views">{{ (int)$item->views }}</a>
                    <a href="{{ route('front-bid-detail',$item->id) }}" class="bidz-id">#{{ $item->id }}</a>
                    @if($item->provider_accept==0)
                        <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip"> فى انتظار تاكيد المعرض </a>
                        @elseif($item->start_at && $item->start_at->addDays($item->bid_days)->gt(\Carbon\Carbon::now()))
                    <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip">    جارى المزايدة</a>
                        @else
                        @if($item->bid_type==1)
                        <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip">     بيعت لاعلى سعر</a>
                            @else
                            <a href="{{ route('front-bid-detail',$item->id) }}" class="sale-tip">بيع بموافقه المالك</a>
                            @endif
                    @endif
                </div>
                <div class="info">
                    <a href="{{ route('front-bid-detail',$item->id) }}"><h3>{{ $item->title }}</h3></a>
                    <h4 class="price">السعر الحالي :{{ $item->car_price }}ر.س</h4>
                    <ul>
                        <li>الحد الادني للمزايدة {{ $item->minimum_bid }} ريال</li>
                        <li>عدد المزايدات حتي الان : {{ $item->total_bids }} مزايدة</li>
                    </ul>
                    <p>{{ str_limit($item->description,100) }}</p>
                </div>
                <div class="action">
                    <!-- Tiemr -->
                    @if($item->start_at && $item->start_at->addDays($item->bid_days)->gt(\Carbon\Carbon::now()))
                    <div class="timer-block" data-year="{{ $item->start_at->addDays($item->bid_days)->year }}" data-month="{{ $item->start_at->addDays($item->bid_days)->month }}" data-day="{{ $item->start_at->addDays($item->bid_days)->day }}" data-hour="{{ $item->start_at->addDays($item->bid_days)->hour }}" data-minute="{{ $item->start_at->addDays($item->bid_days)->minute }}"></div>
                    @endif
                        <a href="{{ route('front-bid-detail',$item->id) }}" class="btn secondary more">التفاصيل</a>
                </div>
            </div>
        </div>
@endforeach
@else
                <div class="alert success cta-alert">لا توجد مناقصات </div>
            @endif

    </div>

    {{ $items->links() }}

    @endsection

@push('footer')
@endpush
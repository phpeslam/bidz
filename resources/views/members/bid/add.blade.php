@extends('layouts.members')
@section('page_content')

    @include('wedgets.notification')
    <!-- Global Form -->
    <div class="primary-box">
        <div class="content-box">
            <h3 class="head">فتح مزايدة</h3>
            <form class="form-ui row" method="post" enctype="multipart/form-data" name="member-bid-save" id="member-bid-save" action="{{ route('member-bid-save') }}">

               {!! csrf_field() !!}
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('bid_type') ? ' error' : '' }}"> {{ trans('bid.bid_type') }}  </label>
                    <select class="{{ $errors->has('bid_type') ? ' error' : '' }}" name="bid_type" id="bid_type">
                        <option value="">{{ trans('bid.select_bid_type') }}</option>
                        <option value="1" @if(old('bid_type')==1) selected="selected" @endif>{{ trans('bid.select_bid_type_1') }}</option>
                        <option value="2" @if(old('bid_type')==2) selected="selected" @endif>{{ trans('bid.select_bid_type_2') }}</option>

                    </select>
                    @if ($errors->has('bid_type'))
                        <div class="error">
                            {{ $errors->first('bid_type') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('minimum_bid') ? ' error' : '' }}"> {{ trans('bid.minimum_bid') }}  </label>
                    <select class="{{ $errors->has('minimum_bid') ? ' error' : '' }}" name="minimum_bid" id="minimum_bid">
                        <option value="">{{ trans('bid.select_minimum_bid') }}</option>
                        @for($i=100;$i<=3000;$i=$i+100)
                            <option value="{{ $i }}" @if(old('minimum_bid')==$i) selected="selected" @endif>{{ $i }}</option>
                            @endfor
                    </select>
                    @if ($errors->has('minimum_bid'))
                        <div class="error">
                            {{ $errors->first('minimum_bid') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->

                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('bid_days') ? ' error' : '' }}"> {{ trans('bid.bid_days') }}  </label>
                    <select class="{{ $errors->has('bid_days') ? ' error' : '' }}" name="bid_days" id="bid_days">
                        <option value="">{{ trans('bid.select_bid_days') }}</option>
                        @for($i=1;$i<=15;$i++)
                            <option value="{{ $i }}" @if(old('bid_days')==$i) selected="selected" @endif>{{ $i }}</option>
                            @endfor
                    </select>
                    @if ($errors->has('bid_days'))
                        <div class="error">
                            {{ $errors->first('bid_days') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('car_price') ? ' error' : '' }}"> سعر السيارة </label>
                    <input class="{{ $errors->has('car_price') ? ' error' : '' }}" type="number" name="car_price" id="car_price" value="{{ old('car_price') }}" />
                    @if ($errors->has('car_price'))
                        <div class="error">
                            {{ $errors->first('car_price') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('category_id') ? ' error' : '' }}"> {{ trans('bid.category_id') }}  </label>
                    <select class="{{ $errors->has('category_id') ? ' error' : '' }}" name="category_id" id="category_id">
                        <option value="">{{ trans('bid.select_category_id') }}</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}"
                            @if(old('category_id')==$category->id) selected="selected" @endif
                            >{{ $category->currentLanguage->title }}
                            </option>
                            @endforeach
                    </select>
                    @if ($errors->has('category_id'))
                        <div class="error">
                            {{ $errors->first('category_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('sub_category_id') ? ' error' : '' }}"> {{ trans('bid.sub_category_id') }}  </label>
                    <select class="{{ $errors->has('sub_category_id') ? ' error' : '' }}" name="sub_category_id" id="sub_category_id" @if(isset($sub_categories) && count($sub_categories)>0)  @else disabled="disabled"  @endif>
                        <option value="">{{ trans('bid.select_sub_category_id') }}</option>
                        @if(isset($sub_categories) && count($sub_categories)>0)
                            @foreach($sub_categories as $sub_category)
                                <option value="{{ $sub_category['id'] }}" @if(old('sub_category_id') == $sub_category['id']) selected="selected" @endif>{{ $sub_category['title'] }}</option>
                            @endforeach
                        @endif
                    </select>
                    @if ($errors->has('sub_category_id'))
                        <div class="error">
                            {{ $errors->first('sub_category_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('car_color_id') ? ' error' : '' }}"> {{ trans('bid.car_color_id') }}  </label>
                    <select class="{{ $errors->has('car_color_id') ? ' error' : '' }}" name="car_color_id" id="car_color_id">
                        <option value="">{{ trans('bid.select_car_color_id') }}</option>
                        @foreach($car_colors as $car_color)
                            <option value="{{ $car_color->id }}"
                                    @if(old('car_color_id')==$car_color->id) selected="selected" @endif
                            >{{ $car_color->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('car_color_id'))
                        <div class="error">
                            {{ $errors->first('car_color_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('car_type_id') ? ' error' : '' }}"> {{ trans('bid.car_type_id') }}  </label>
                    <select class="{{ $errors->has('car_type_id') ? ' error' : '' }}" name="car_type_id" id="car_type_id">
                        <option value="">{{ trans('bid.select_car_type_id') }}</option>
                        @foreach($car_types as $car_type)
                            <option value="{{ $car_type->id }}"
                                    @if(old('car_type_id')==$car_type->id) selected="selected" @endif
                            >{{ $car_type->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('car_type_id'))
                        <div class="error">
                            {{ $errors->first('car_type_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('engine_type_id') ? ' error' : '' }}"> {{ trans('bid.engine_type_id') }}  </label>
                    <select class="{{ $errors->has('engine_type_id') ? ' error' : '' }}" name="engine_type_id" id="engine_type_id">
                        <option value="">{{ trans('bid.select_engine_type_id') }}</option>
                        @foreach($engine_types as $engine_type)
                            <option value="{{ $engine_type->id }}"
                                    @if(old('engine_type_id')==$engine_type->id) selected="selected" @endif
                            >{{ $engine_type->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('engine_type_id'))
                        <div class="error">
                            {{ $errors->first('engine_type_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('fuel_id') ? ' error' : '' }}"> {{ trans('bid.fuel_id') }}  </label>
                    <select class="{{ $errors->has('fuel_id') ? ' error' : '' }}" name="fuel_id" id="fuel_id">
                        <option value="">{{ trans('bid.select_fuel_id') }}</option>
                        @foreach($fuels as $fuel)
                            <option value="{{ $fuel->id }}"
                                    @if(old('fuel_id')==$fuel->id) selected="selected" @endif
                            >{{ $fuel->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('fuel_id'))
                        <div class="error">
                            {{ $errors->first('fuel_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('import_id') ? ' error' : '' }}"> {{ trans('bid.import_id') }}  </label>
                    <select class="{{ $errors->has('import_id') ? ' error' : '' }}" name="import_id" id="import_id">
                        <option value="">{{ trans('bid.select_import_id') }}</option>
                        @foreach($imports as $import)
                            <option value="{{ $import->id }}"
                                    @if(old('import_id')==$import->id) selected="selected" @endif
                            >{{ $import->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('import_id'))
                        <div class="error">
                            {{ $errors->first('import_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('motion_vector_id') ? ' error' : '' }}"> {{ trans('bid.motion_vector_id') }}  </label>
                    <select class="{{ $errors->has('motion_vector_id') ? ' error' : '' }}" name="motion_vector_id" id="motion_vector_id">
                        <option value="">{{ trans('bid.select_motion_vector_id') }}</option>
                        @foreach($motion_vectors as $motion_vector)
                            <option value="{{ $motion_vector->id }}"
                                    @if(old('motion_vector_id')==$motion_vector->id) selected="selected" @endif
                            >{{ $motion_vector->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('motion_vector_id'))
                        <div class="error">
                            {{ $errors->first('motion_vector_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('specification_id') ? ' error' : '' }}"> {{ trans('bid.specification_id') }}  </label>
                    <select class="{{ $errors->has('specification_id') ? ' error' : '' }}" name="specification_id" id="specification_id">
                        <option value="">{{ trans('bid.select_specification_id') }}</option>
                        @foreach($specifications as $specification)
                            <option value="{{ $specification->id }}"
                                    @if(old('specification_id')==$specification->id) selected="selected" @endif
                            >{{ $specification->currentLanguage->title }}
                            </option>
                        @endforeach
                    </select>
                    @if ($errors->has('specification_id'))
                        <div class="error">
                            {{ $errors->first('specification_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('manufacturing_year') ? ' error' : '' }}"> {{ trans('bid.manufacturing_year') }}  </label>
                    <select class="{{ $errors->has('manufacturing_year') ? ' error' : '' }}" name="manufacturing_year" id="manufacturing_year">
                        <option value="">{{ trans('bid.select_manufacturing_year') }}</option>
                        @for($i=date('Y');$i>=1980;--$i)
                            <option value="{{ $i }}"
                            @if(old('manufacturing_year')==$i) selected="selected" @endif
                            >{{ $i }}</option>
                            @endfor
                    </select>
                    @if ($errors->has('manufacturing_year'))
                        <div class="error">
                            {{ $errors->first('manufacturing_year') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('mileage') ? ' error' : '' }}"> {{ trans('bid.mileage') }}  </label>
                    <input class="{{ $errors->has('mileage') ? ' error' : '' }}" type="number" name="mileage" id="mileage" value="{{ old('mileage') }}" />
                    @if ($errors->has('mileage'))
                        <div class="error">
                            {{ $errors->first('mileage') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->

                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('chassis_no') ? ' error' : '' }}"> {{ trans('bid.chassis_no') }}  </label>
                    <input class="{{ $errors->has('chassis_no') ? ' error' : '' }}" type="number" name="chassis_no" id="chassis_no" value="{{ old('chassis_no') }}" />
                    @if ($errors->has('chassis_no'))
                        <div class="error">
                            {{ $errors->first('chassis_no') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->

                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('engine_size') ? ' error' : '' }}"> {{ trans('bid.engine_size') }}  </label>
                    <input class="{{ $errors->has('engine_size') ? ' error' : '' }}" type="number" name="engine_size" id="engine_size" value="{{ old('engine_size') }}" />
                    @if ($errors->has('engine_size'))
                        <div class="error">
                            {{ $errors->first('engine_size') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->



                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('title') ? ' error' : '' }}">  {{ trans('bid.title') }}     </label>
                    <input class="{{ $errors->has('title') ? ' error' : '' }}" type="text" value="{{ old('title') }}" name="title" id="title" placeholder=" {{ trans('bid.title') }} ">
                    @if ($errors->has('title'))
                        <div class="error">
                            {{ $errors->first('title') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
@if(\Auth::guard('members')->user()->membership_type == 1)
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('provider_id') ? ' error' : '' }}">         {{ trans('bid.provider_id') }}  </label>
                    <select class="{{ $errors->has('provider_id') ? ' error' : '' }}" name="provider_id" id="provider_id">
                        <option> {{ trans('bid.provider_id_select') }}</option>
                        @foreach($providers as $provider)
                            <option value="{{ $provider->id }}"
                            @if(old('provider_id')==$provider->id) selected="selected" @endif
                            >{{ $provider->first_name }} {{ $provider->last_name }}</option>
                            @endforeach
                    </select>
                    @if ($errors->has('provider_id'))
                        <div class="error">
                            {{ $errors->first('provider_id') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
@endif
                <!-- Control -->
                <div class="col-12">
                    <label class="{{ $errors->has('description') ? ' error' : '' }}">{{ trans('bid.description') }}</label>
                    <textarea class="{{ $errors->has('description') ? ' error' : '' }}" name="description" id="description" placeholder="{{ trans('bid.description') }}">{{ old('description') }}</textarea>
                    <!-- Form Clone -->
                    @if ($errors->has('description'))
                        <div class="error">
                            {{ $errors->first('description') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <div class="col-12 col-l-6">
                    <label class="{{ $errors->has('is_periodic_inspection') ? ' error' : '' }}"> <input type="checkbox" name="is_periodic_inspection" id="is_periodic_inspection" value="1" @if(old('is_periodic_inspection')==1) checked="checked" @endif /> {{ trans('bid.is_periodic_inspection') }} </label>
                    <label class="{{ $errors->has('is_periodic_inspection') ? ' error' : '' }}"> <input type="checkbox" name="vaild_form" id="vaild_form" onclick="show_form()" value="1"@if(old('vaild_form')==1) checked="checked" @endif /> {{ trans('bid.vaild_form') }} </label>
                    @if ($errors->has('is_periodic_inspection'))
                        <div class="error">
                            {{ $errors->first('is_periodic_inspection') }}
                        </div>
                    @endif
                    @if ($errors->has('vaild_form'))
                        <div class="error">
                            {{ $errors->first('vaild_form') }}
                        </div>
                    @endif
                </div>
                
                                               <!-- Control -->
                    <div class="col-12 col-l-6" id="num_of_years" style="display:none">
                    <label class="{{ $errors->has('form_years') ? ' error' : '' }}"> {{ trans('bid.form_years') }}  </label>
                    <select class="{{ $errors->has('form_years') ? ' error' : '' }}" name="form_years" id="form_years">
                        <option value="">{{ trans('bid.select_form_years') }}</option>
                        @for($i=1;$i<=3;$i++)
                            <option value="{{ $i }}"
                                    @if(old('form_years')==$i) selected="selected" @endif
                            >{{ $i }}</option>
                        @endfor
                    </select>
                    @if ($errors->has('form_years'))
                        <div class="error">
                            {{ $errors->first('form_years') }}
                        </div>
                    @endif
                </div>
                <!-- Control -->
                <!-- Control -->
                <div class="col-12">
                    <label class="{{ $errors->has('images') ? ' error' : '' }}">صور (كل امتدادات الصور)</label>
                    <div class="form-clone">
                        <div class="clone">
                            <div class="file-input">
                                <input type="file" name="images[]" class="file">
                                <span class="btn dark">{{trans('bid.browse')}}</span>
                                <input class="file-path" placeholder="File Upload">

                            </div>
                        </div>
                        <button class="btn primary ti-plus clone-btn" data-type="image"></button>
                    </div>
                    @if ($errors->has('images'))
                        <div class="error">
                            {{ $errors->first('images') }}
                        </div>
                    @endif
                </div>
<div class="row">
    @foreach($other_specifications as $other_specification)
        <div class="col-12 col-l-3">
            <label> <input type="checkbox" class="{{ $errors->has('other_specifications') ? ' error' : '' }}" name="other_specifications[]" id="other_specifications_{{ $other_specification->id }}" value="{{ $other_specification->id }}" @if(is_array(old('other_specifications')) && in_array($other_specification->id,old('other_specifications'))) checked="checked" @endif /> {{ $other_specification->currentLanguage->title }} </label>
        </div>
        @endforeach
        @if ($errors->has('other_specifications'))
            <div class="error">
                {{ $errors->first('other_specifications') }}
            </div>
        @endif
</div>
                <div class="col-12">
                    <label class="{{ $errors->has('files') ? ' error' : '' }}"> تقرير فنى (jpeg,pdf,rar,zip,bmp,png,jpg)  </label>
                    <div class="form-clone">
                        <div class="clone">
                            <div class="file-input">
                                <input type="file" name="files[]" class="file">
                                <span class="btn dark">{{trans('bid.browse')}}</span>
                                <input class="file-path" placeholder="File Upload">

                            </div>
                        </div>
                        <!-- <button class="btn primary ti-plus clone-btn" data-type="files"></button> -->
                    </div>
                    @if ($errors->has('files'))
                        <div class="error">
                            {{ $errors->first('files') }}
                        </div>
                    @endif
                </div>

                <div class="col-12">
                    <input type="submit" value="نشر المزايدة" class="btn primary round pro">
                </div>
            </form>
        </div>
    </div>
    <!-- // Global Form -->
    @endsection

@push('footer')
    <script type="text/javascript">
        $(function(){
            $('select#category_id').change(function(){
                $('select#sub_category_id').html('').attr('disabled',true);
                var category_id = $(this).val();

                if(category_id != '') {
                    $.get('{{ route('front-sub-categories') }}', {
                        category_id:category_id
                    },function(result){
                        if(result.length>0){
                            $('select#sub_category_id').html('<option value="">{{ trans('bid.select_sub_category_id') }}</option>');
                            $.each(result,function (x,item) {
                                $('select#sub_category_id').append('<option value="'+item.id+'">'+item.title+'</option>');
                            });
                            $('select#sub_category_id').removeAttr('disabled');
                        }
                    },'json');
                }
            });

            $(".clone-btn").click(function(e){
                e.preventDefault();
                var type = $(this).attr('data-type');
                var html = ' <div class="form-clone"><div class="clone"><div class="file-input"><input type="file" name="'+type+'[]" class="file"><span class="btn dark">{{trans("bid.browse")}}</span><input class="file-path" placeholder="File Upload"></div></div><button class="btn primary remove-clone ti-minus"></button></div>'
                $(this).parent().after(html);

            });
        });
        
        function show_form(){
    $('#num_of_years').css('display','block');
}
    </script>
@endpush
@extends('layouts.members')
@section('page_content')
<!-- Warning -->
<!-- <div class="alert warning cta-alert"> -->
    <!-- <p>يرجي العلم انه يتوجب عليك ايداع تأمين لتتمكن من القيام بعملية المزايدة على المركبات</p> -->
    <!-- <a href="#" data-modal="add-money" class="btn dark">إيداع مبلغ التأمين</a> -->
<!-- </div> -->
<!-- // Warning -->
<div class="col-md-8 col-md-offset-2 primary-box">
    <div class="content-box">
        <h3 class="head">{{$page_title}}</h3>
        @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
            {{ Session::get('message') }}
        </p>
        @endif
        <form class="form-ui row" method="post" action="{{route('member-dobidding')}}">
            <!-- {!! csrf_field() !!} -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <!-- <input type="hidden" name="id" value="encrypt({{$acuid}})"> -->
            <input type="hidden" name="id" value="{{$acuid}}">
            <div class="col-4"> عنوان المزايدة :{{$title}} </div>
            <div class="col-4">الحد الأدنى للمزايدة : {{$min}}  </div>
            <div class="col-12">
                <label>مبلغ المزايدة</label>
                <input type="number" placeholder="مبلغ المزايدة" min="{{$lastBid}}" step="{{$min}}" value="{{$myBid}}" name="price" required>
                <input type="submit" value="ارسال" class="btn secondary prim">
            </div>
        </form>
    </div>
</div>
@endsection
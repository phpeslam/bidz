@extends('layouts.members')
@section('page_content')
    <!-- Page Content -->
    <div class="container page-content">
        <!-- Notfications -->
        <div class="other-bidz">
            <h2>{{trans('notifications.latest')}}</h2>

            @if(Session::has('message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
                    {{ Session::get('message') }}
                </p>
            @endif
            @if(count($notifications) > 0)
                @foreach($notifications as $notification)
                    @if($notification->type == 1)
                        <div class="alert cta-alert">
                            <p>{{sprintf(trans('notifications.car_request'), $notification->auction->title)}}
                                <br> {{mb_substr($notification->auction->description,0,200,'utf-8')}}
                                <br>
                                @if($notification->action !=null)
                                    
                                    <span style="color:rgb(210,0,128)"><small>{{sprintf(trans('notifications.'.$notification->action),$notification->auction->title)}} {{$notification->updated_at}}</small></span>
                                @endif
                            </p>
                            @if($notification->auction->provider_accept)
                                <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                                   class="btn info">{{trans('notifications.more_details')}}</a>
                            @elseif($notification->action == '')
                                <a href="{{ route('member-reject',$notification->auctionID) }}"
                                   class="btn danger">{{trans('notifications.reject')}}</a>
                                <a href="{{ route('member-approve',$notification->auctionID) }}"
                                   class="btn success">{{trans('notifications.approve')}}</a>
                                <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                                   class="btn info">{{trans('notifications.more_details')}}</a>   
                            @endif
                        </div>
                    @endif

                    @if($notification->type == 2)
                        <div class="alert success cta-alert">
                            <p>{{trans('notifications.congratulation')}}<br>
                                {{sprintf(trans('notifications.car_approveed'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 3)
                        <div class="alert danger cta-alert">
                            <p>{{trans('notifications.reject')}}<br>
                                {{sprintf(trans('notifications.car_reject'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 4)
                        <div class="alert success cta-alert">
                            <p>{{ $notification->message }}<br>
                                {{sprintf(trans('notifications.car_reject'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 5)
                        <div class="alert success cta-alert">
                            <p>{{ $notification->message }}<br>
                                {{sprintf(trans('notifications.car_reject'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 6)
                        <div class="alert success cta-alert">
                            <p>{{ $notification->message }}<br>
                                {{sprintf(trans('notifications.car_reject'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 7)
                        <div class="alert success cta-alert">
                            <p>{{ $notification->message }}<br>
                                {{sprintf(trans('notifications.car_reject'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 8)
                        <div class="alert success cta-alert">
                            <p>{{ $notification->message }}<br>
                                {{sprintf(trans('notifications.car_reject'),$notification->auction->title)}} {{trans('notifications.by')}}
                                <?php
                                $_provider = getMemperName($notification->auction->provider_id);
                                echo $_provider->first_name .' '. $_provider->last_name;
                                ?>
                                <br>
                                {{$notification->created_at}}
                            </p>
                            <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                               class="btn dark">{{trans('notifications.more_details')}}</a>
                        </div>
                    @endif

                    @if($notification->type == 9)
                        <div class="alert cta-alert">
                            <p>{{sprintf(trans('notifications.car_request'), $notification->auction->title)}}
                                <br> {{mb_substr($notification->auction->description,0,200,'utf-8')}}
                                <br>
                                @if($notification->action !=null)
                                    
                                    <span style="color:rgb(210,0,128)"><small>{{sprintf(trans('notifications.'.$notification->action),$notification->auction->title)}} {{$notification->updated_at}}</small></span>
                                @endif
                            </p>
                            @if($notification->action != null)
                                <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                                   class="btn info">{{trans('notifications.more_details')}}</a>
                            @elseif($notification->action == '')
                                <a href="{{ route('owner-reject',$notification->auctionID) }}"
                                   class="btn danger">{{trans('notifications.reject')}}</a>
                                <a href="{{ route('owner-approve',$notification->auctionID) }}"
                                   class="btn success">{{trans('notifications.approve')}}</a>
                                <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                                   class="btn info">{{trans('notifications.more_details')}}</a>   
                            @endif
                        </div>
                    @endif
                    @if($notification->type == 10)
                        <div class="alert cta-alert">
                            <p>{{sprintf(trans('notifications.car_request'), $notification->auction->title)}}
                                <br> {{mb_substr($notification->auction->description,0,200,'utf-8')}}
                                <br>
                                @if($notification->action !=null)
                                    
                                    <span style="color:rgb(210,0,128)"><small>{{sprintf(trans('notifications.'.$notification->action),$notification->auction->title)}} {{$notification->updated_at}}</small></span>
                                @endif
                            </p>
                            @if($notification->action != null)
                                <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                                   class="btn info">{{trans('notifications.more_details')}}</a>
                            @elseif($notification->action == '')
                                <a href="{{ route('owner-republish',$notification->auctionID) }}"
                                   class="btn danger">لاعادة النشر</a>
                                <a href="{{ route('front-bid-detail',$notification->auctionID) }}"
                                   class="btn info">{{trans('notifications.more_details')}}</a>   
                            @endif
                        </div>
                    @endif
                @endforeach
            @else
                <div class="alert success cta-alert">{{trans('notifications.none')}}</div>
            @endif

            {{--<!-- Success -->--}}
            {{--<div class="alert success cta-alert">--}}
            {{--<p>تهانينا :) <br> -  لقد فزت بالمزايدة رقم #545454  بسعر 150.000 ر.س سيارة مريسدس ماس 2018 <br> يتوجب عليك الآن التوجه إلي صفحة حساباتنا البنكية و تحويل المبلغ خلال 48 ساعة لإدارة الموقع  ليتم إتمام البيعه وإستلام سيارتك من المعرض مباشرة</p>--}}
            {{--<a href="#" class="btn dark">تحويل المبلغ</a>--}}
            {{--</div>--}}

            {{--<!-- Normal -->--}}
            {{--<div class="alert cta-alert">--}}
            {{--<p>أخبار سعيدة :)) <br> تم وصول عدد المساومين في مزيادتك رقم #5454 - سيارة بورش 2016 ووصول السيارة لأعلي سعر 156.000 ريال سعودي <br> إستكشف تفاصيل المزايدة لآن</p>--}}
            {{--<a href="#" class="btn dark">تفاصيل المزايدة</a>--}}
            {{--</div>--}}

            {{--<!-- Urgent -->--}}
            {{--<div class="alert danger cta-alert">--}}
            {{--<p>مزايدتك راح تروح عليك ..؟!! <br>  لإستكمال مزايدتك رقم #323232 - سيارة بورش 2015 - وضرورة تحويل قيمة المزايدة علي حساباتنا البنكية قبل مرور 24 ساعة لعدم خسارة المزايدة <br> إستكشف تفاصيل المزايدة لآن</p>--}}
            {{--<a href="#" class="btn dark">تفاصيل المزايدة</a>--}}
            {{--</div>--}}
        </div>
    </div>
    <!-- // Page Content -->
@endsection
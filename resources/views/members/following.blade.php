@extends('layouts.members')
@section('page_content')
    @include('wedgets.notification')

    <!-- Bidz -->
	@if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
            {{ Session::get('message') }}
        </p>
    @endif
    <div class="container page-content">
    @if(count($auctions)>0)
		@foreach($auctions as $item)
        <div class="bidz-block wide-theme col-12 col-m-6 col-l-4">
            <div class="content-box">
                <div class="image">
                    <a href="{{ route('front-bid-detail',$item->id) }}" data-src="{{ $item->image }}"></a>
                    <a href="{{ route('front-bid-detail',$item->id) }}" class="ti-eye views">{{ (int)$item->views }}</a>
                    <a href="{{ route('front-bid-detail',$item->id) }}" class="bidz-id">#{{ $item->id }}</a>
                                @if($item->bid_type == 1)
                                <a href="#" class="sale-tip">{{ trans('bid.select_bid_type_1') }}</a>
                                @else
                                <a href="#" class="sale-tip">{{ trans('bid.select_bid_type_2') }}</a>
                                @endif
                </div>
                <div class="info">
                    <a href="{{ route('front-bid-detail',$item->id) }}"><h3>{{ $item->title }}</h3></a>
                    <h4 class="price">السعر الحالي : 5060ر.س</h4>
                    <ul>
                        <li>الحد الادني للمزايدة {{ $item->minimum_bid }} ريال</li>
                        <li>عدد المزايدات حتي الان : {{ $item->total_bids }} مزايدة</li>
                    </ul>
                    <p>{{ str_limit($item->description,100) }}</p>
                </div>
                <div class="action">
                    <!-- Tiemr -->
                    @if($item->start_at && $item->start_at->addDays($item->bid_days)->gt(\Carbon\Carbon::now()))
                    <div class="timer-block" data-year="{{ $item->start_at->addDays($item->bid_days)->year }}" data-month="{{ $item->start_at->addDays($item->bid_days)->month }}" data-day="{{ $item->start_at->addDays($item->bid_days)->day }}" data-hour="{{ $item->start_at->addDays($item->bid_days)->hour }}" data-minute="{{ $item->start_at->addDays($item->bid_days)->minute }}"></div>
                    @endif
                        <a href="{{ route('front-bid-detail',$item->id) }}" class="btn secondary more">{{ trans('home.details') }}</a>
						<a href="{{ route('delete-follow',$item->id) }}" class="btn warning">{{ trans('home.delete_fav') }}</a>
                </div>
            </div>
        </div>

@endforeach
@else

<div class="alert success cta-alert">لا يوجد متابعات</div>
@endif  


    </div>

    @endsection

@push('footer')
@endpush
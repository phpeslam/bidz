@extends('layouts.members')
@section('page_content')
    <!-- Big User Area -->
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">
            {{ Session::get('message') }}
        </p>
    @endif
 
    <div class="bua">
        <div class="info">
            <h2 class="price">رصيدي {{$balance}} ر.س</h2>
        </div>
        <a href="{{url('BankTransfers')}}" class="btn primary big round">إضافة رصيد أخري الي حسابي</a>
    </div>

    <!-- Profile -->
    <div class="primary-box">
        <div class="content-box">
            <h3 class="head">البيانات الشخصيه <a href="{{route('member-profile-edit')}}" class="edit">تحديث الملف الشخصي</a> </h3>
            <!-- Profile Details -->
            <div class="row user-info">
                <div class="col-12 col-m-4 col-l-3">

                    <h3>   {{ \Auth::guard('members')->user()->first_name }} {{ \Auth::guard('members')->user()->last_name }}</h3>
                    <h4>نوع الحساب  : @if(\Auth::guard('members')->user()->membership_type ==2)معرض@else شخص@endif</h4>
                </div>
                <div class="col-12 col-m-8 col-l-9">
                    <div class="row">
                        <ul class="col-12 col-m-6">
                            <li>الإسم الإول :  {{ \Auth::guard('members')->user()->first_name }}</li>
                            <li>الإسم الأخير :    {{ \Auth::guard('members')->user()->last_name }}</li>
                            <li>الجوال : {{ \Auth::guard('members')->user()->phone_number }}</li>
                            <li>البريد الإلكتروني : {{ \Auth::guard('members')->user()->email }}</li>
                            <li> الدولة :  {{ getCountryName(\Auth::guard('members')->user()->country_id) }}</li>
                            <li>المدينة : {{ getCityName(\Auth::guard('members')->user()->city_id) }}  </li>
                            <li>العنوان كاملا ً :  {{ \Auth::guard('members')->user()->address }}</li>
                        </ul>
                        <ul class="col-12 col-m-6">
                        @if(\Auth::guard('members')->user()->image != null)
                        <img src="{{ asset(\Auth::guard('members')->user()->image)}}" alt="" class="fluid">
                        @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // Profile -->
    @endsection
@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>{{ trans('register.update_profile') }}</h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="{{ route('front-register') }}">{{ trans('register.update_profile') }}</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
        <!-- Global Form -->
        <div class="primary-box">
            <div class="content-box">
                <h3 class="head">{{ trans('register.update_profile') }}</h3>
                <form class="form-ui row" action="{{ route('member-update-profie') }}" method="post" name="form_reg" id="form_reg" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('first_name') ? ' error' : '' }}">{{ trans('register.first_name') }}</label>
                        <input class="{{ $errors->has('first_name') ? ' error' : '' }}" type="text" placeholder="{{ trans('register.first_name') }}" value="{{$user->first_name ?? old('first_name')}}" name="first_name" id="first_name" />
                        @if ($errors->has('first_name'))
                            <div class="error">
                                {{ $errors->first('first_name') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('last_name') ? ' error' : '' }}">  {{ trans('register.last_name') }}</label>
                        <input class="{{ $errors->has('last_name') ? ' error' : '' }}" type="text" placeholder="{{ trans('register.last_name') }}" value="{{$user->last_name ?? old('last_name') }}"  name="last_name" id="last_name" />
                        @if ($errors->has('last_name'))
                            <div class="error">
                                {{ $errors->first('last_name') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('email') ? ' error' : '' }}"> {{ trans('register.email') }}  </label>
                        <input class="{{ $errors->has('email') ? ' error' : '' }}" type="email" placeholder="{{ trans('register.email') }}" dir="ltr" value="{{$user->email ?? old('email') }}"  name="email" id="email" />
                        @if ($errors->has('email'))
                            <div class="error">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('phone_number') ? ' error' : '' }}">{{ trans('register.phone_number') }}</label>
                        <input class="{{ $errors->has('phone_number') ? ' error' : '' }}" type="tel" dir="ltr" placeholder="{{ trans('register.phone_number') }}" value="{{$user->phone_number ?? old('phone_number') }}"  name="phone_number" id="phone_number" >
                        @if ($errors->has('phone_number'))
                            <div class="error">
                                {{ $errors->first('phone_number') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('password') ? ' error' : '' }}">   {{ trans('register.password') }}</label>
                        <input class="{{ $errors->has('password') ? ' error' : '' }}" type="password" placeholder="{{ trans('register.password') }}" name="password" id="password" />
                        @if ($errors->has('password'))
                            <div class="error">
                                {{ $errors->first('password') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('password_confirmation') ? ' error' : '' }}">     {{ trans('register.password_confirmation') }}</label>
                        <input class="{{ $errors->has('password_confirmation') ? ' error' : '' }}" type="password" placeholder="{{ trans('register.password_confirmation') }}    " name="password_confirmation" id="password_confirmation" />
                        @if ($errors->has('password_confirmation'))
                            <div class="error">
                                {{ $errors->first('password_confirmation') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="  col-12 col-l-6">
                        <label class="{{ $errors->has('country_id') ? ' error' : '' }}">{{ trans('register.country_id') }}</label>
                        <select class="{{ $errors->has('country_id') ? ' error' : '' }}" name="country_id" id="country_id" >
                            <option value="">{{ trans('register.country_id_select') }}</option>
                            <?php $country_id =$user->country_id ?? old('country_id') ;?>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}" @if($country_id == $country->id) selected="selected" @endif>{{ $country->currentLanguage->title }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('country_id'))
                            <div class="error">
                                {{ $errors->first('country_id') }}
                            </div>
                        @endif
                    </div>
                    <!-- Control -->
                    <div class="col-12 col-l-6">
                        <label class="{{ $errors->has('city_id') ? ' error' : '' }}">{{ trans('register.city_id') }}</label>
                        <select class="{{ $errors->has('city_id') ? ' error' : '' }}" name="city_id" id="city_id" @if(isset($cities) && count($cities)>0)  @else disabled="disabled"  @endif>
                            <option value="">{{ trans('register.city_id_select') }}</option>
                            <?php $city_id = $user->city_id ?? old('city_id') ;?>
                            @if(isset($cities) && count($cities)>0)
                                @foreach($cities as $city)
                                    <option value="{{ $city['id'] }}" @if($city_id == $city['id']) selected="selected" @endif>{{ $city['title'] }}</option>
                                @endforeach
                            @endif
                        </select>
                        @if ($errors->has('city_id'))
                            <div class="error">
                                {{ $errors->first('city_id') }}
                            </div>
                        @endif
                    </div>

                    <!-- Control -->
                    <div class="col-12 col-l-12">
                        <label class="{{ $errors->has('address') ? ' error' : '' }}">{{ trans('register.address') }}</label>
                        <input class="{{ $errors->has('address') ? ' error' : '' }}" type="text" placeholder="{{ trans('register.address') }}" value="{{$user->address ?? old('address') }}"  name="address" id="address" >
                        @if ($errors->has('address'))
                            <div class="error">
                                {{ $errors->first('address') }}
                            </div>
                        @endif
                    </div>

                    <div class="col-12 col-l-12">
                        <label class="{{ $errors->has('image') ? ' error' : '' }}">{{\App::isLocale('en')?'image profile':'الصورة الشخصية'}}</label>
                        <input class="{{ $errors->has('image') ? ' error' : '' }}" type="file" name="image"  >
                        <img src="{{ asset($user->image)}}"  style="width: 80px;height: 80px;">

                        @if ($errors->has('image'))
                            <div class="error">
                                {{ $errors->first('image') }}
                            </div>
                        @endif
                    </div>


                    <!-- Control -->
                    <div class="col-12">
                        <input type="submit" value=" {{ trans('global.save') }}  " class="btn primary">
                    </div>
                </form>
            </div>
        </div>
        <!-- // Global Form -->
    </div>
    <!-- // Page Content -->

@endsection


@push('footer')
    <script type="text/javascript">
        $(function(){
            $('div#change_type a').click(function(){
                if(!$(this).hasClass('primary')){
                    $('div#change_type a').removeClass('primary').removeClass('dark').addClass('dark');
                    $(this).removeClass('dark').addClass('primary');
                    $('input#membership_type').val($(this).attr('rel'));
                }
            }) ;
            $('select#country_id').change(function(){
                $('select#city_id').html('').attr('disabled',true);
                var country_id = $(this).val();

                if(country_id != '') {
                    $.get('{{ route('front-register-cities') }}', {
                        country_id:country_id
                    },function(result){
                        if(result.length>0){
                            $('select#city_id').html('<option value="">{{ trans('register.city_id_select') }}</option>');
                            $.each(result,function (x,item) {
                                $('select#city_id').append('<option value="'+item.id+'">'+item.title+'</option>');
                            });
                            $('select#city_id').removeAttr('disabled');
                        }
                    },'json');
                }
            });
        });

        function show_form(){
            $('#market_image').css('display','block');
        }
        function hide_form(){
            $('#market_image').css('display','none');
        }
    </script>
@endpush


@extends('layouts.members')
@section('page_content')
    <!-- Big User Area -->
    <style>
table {
    width:100%;
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 15px;
    text-align: left;
}
table#t01 tr:nth-child(even) {
    background-color: #eee;
}
table#t01 tr:nth-child(odd) {
   background-color: #fff;
}
table#t01 th {
    background-color: #ffffff;
    color: black;
}
</style>
    <div class="bua">
        <div class="info">
            <h2 class="price">رصيدي {{$balance}} ر.س</h2>
        </div>
        <a href="{{url('BankTransfers')}}" class="btn primary big round">إضافة رصيد أخري الي حسابي</a>
    </div>

    <!-- Profile -->
    <div class="primary-box">
        <div class="content-box">
            <h3 class="head">تفاصيل الرصيد </h3>
            <!-- Profile Details -->
            <div class="row user-info">
                <div class="col-12 col-m-12">
                    <div class="row">
                    <table id="t01">
                    <thead>
                        <tr>
                            <th> التاريخ</th>
                            <th> القيمة</th>
                            <th> المزايدة  </th>
                            <th> الحالة </th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($blances)>0)
                        @foreach($blances as $user)
                        <tr>
                            <td> {{$user->date}} </td>
                            <td> {{$user->value}} </td>
                            @if($user->Auction != null)
                            <td> {{$user->Auction->title}} </td>
                            @else
                            <td> لا يوجد </td>
                            @endif
                            <td>
                            @if($user->case_balance == 'deposited')
                            ايداع رصيد
                            @elseif($user->case_balance == 'withdraw')
                            سحب رصيد
                            @elseif($user->case_balance == 'freezing')
                            تجميد رصيد
                            @elseif($user->case_balance == 'add_balance')
                            اضافة رصيد
                            @endif
                             </td>
                        </tr>
                        @endforeach
                      
                    @else

                    <div class="alert success cta-alert">لا يوجد رصيد</div>
                    @endif  
                </tbody>
                </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- // Profile -->
    @endsection
<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'x') }}</title>
</head>
<body>
<div style="background:#e7e5ea;">
    <div style="width:80%;margin:auto;">

    
        <div style="direction: rtl;font-family: 'Noto Kufi Arabic', Tahoma, Verdana;padding-top: 50px;padding-bottom: 80px;background: #fff;">
            <div style="width: 90%;margin: auto;">
                <p style="line-height: 3;text-align: center;font-size: 18px;color: #000;">مرحبا بك  <span style="color: #fe4b37;">{{$email->name}}</span>,</p>
                <p style="line-height: 3;text-align: center;font-size: 18px;color: #000;">شكرا لك لمراسلتك لنا</p>
                <br>
                <p style="line-height: 3;text-align: center;font-size: 18px;color: #000;"> 
                سنرد عليك في أقرب .. فريق موقع مزايدات السعودية
                </p>
                <p style="line-height: 3;text-align: center;font-size: 18px;color: #000;">   </p>
            </div>
        </div>

    </div>
</div>
</body></html>
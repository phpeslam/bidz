<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'x') }}</title>
        
</head>
<body>
    
<div style="background:#e7e5ea;">
    <div style="width:80%;margin:auto;">

        <div style="direction: rtl;font-family: Tahoma, Verdana;padding: 50px 0;font-size: 18px;">
            <div style="width:90%;margin:auto;">
                <div>عزيزي مدير الموقع<br>
                    <br>
                    تم ارسال رسالة جديدة.<br>
                    <br>
                    <strong>محتوى الرسالة </strong><br>
                    <ul>
                        <li><strong>اسم المرسل:</strong> {{$email->name}}</li>
                        <li><strong>بريده الاكتروني:</strong> {{$email->email}}</li>
                        <li><strong>محتوى الرسالة:</strong> {{$email->description}}</li>
                    </ul>
    
                    <br>
    
                    بالتوفيق,<br>
    
                    <br>
    
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>
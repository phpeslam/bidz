<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', '') }}</title>
        
    
</head>
<body>

<div style="width: 90%;margin: auto;">
    <p style="line-height: 3;color: #000;"> أهلا بك في موقع مزايدات السعودية
        <br>
        <span style="color: #fe4b37;">{{$user->name}}</span>  أنت الآن مشترك تتمتع بالأولية و تحظي بأهتمامنا <br>
        و حرصاً منا على الوصول الى بريدكم الصحيح</p>
    كود التفعيل لديكم    {{$user->email_act_code}}    
    <div class="btn" style="text-align: center;">
        <a href="{{ route('home-act-email') }}?act={{$user->email_act_code}}&email={{ $user->email }}">
            <button style="background: #fe4b37;border: 0;text-decoration: none;color: #fff;padding: 20px 30px;font-size: 24px;font-family: Tahoma, Verdana;">اضغط هنا للتأكيد</button>
        </a>
    </div>
</div>
    
</body>
</html>
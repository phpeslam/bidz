@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>   رفع ملف </h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="#">تفعيل العضوية</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
    @include('wedgets.notification')
        <!-- Global Form -->

        <div class="primary-box">
            <div class="content-box">
                <h3 class="head">     ملف السجل التجارى  </h3>
                    <form class="form-ui row" enctype="multipart/form-data" method="post" name="form_active_upload" id="form_active_upload" action="{{ route('checkpoint-do-upload') }}">

                        {!! csrf_field() !!}

                        <!-- Control -->
                        <div class="col-12 col-l-6">
                            <label >   الملف  </label>
                            <input  type="file"   name="file_upload" id="file_upload"   />
                            <span>برجاء رفع ملف jpeg,pdf,rar,zip,bmp,png,jpg</span>
                            <input type="submit" value="  رفع الملف" class="btn primary">
                        </div>

                    </form>



            </div>
        </div>

    </div>
    <!-- // Page Content -->


    @endsection


@push('header')

    @endpush
@push('footer')
<script type="text/javascript">

$('a.resend').click(function(){
    var current_i = $(this).attr('rel');
    var current_row = $(this).closest('div.row');

    $.post('{{ route('checkpoint-do-resend') }}',{
        action:current_i,
        _token: '{{ csrf_token() }}'
    },function (result) {

        if(result[0]){
            toastr.success(result[1]);
        }else{
            toastr.warning(result[1]);
        }
    },'json');
})  ;

</script>

@endpush


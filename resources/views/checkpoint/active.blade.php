@extends('layouts.front')

@section('page_content')
    <!-- Page Head -->
    <div class="page-head">
        <div class="container">
            <div class="head-info">
                <h1>تفعيل العضوية</h1>
                <div class="breadcrumb">
                    <a href="{{ route('front-home') }}">{{ trans('global.main') }}</a>
                    <a href="#">تفعيل العضوية</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Page Head -->

    <!-- Page Content -->
    <div class="container page-content">
    @include('wedgets.notification')
        <!-- Global Form -->
        @if(\Auth::guard('members')->user()->is_active_phone != 1 )
        <div class="primary-box">
            <div class="content-box">
                <h3 class="head">تفعيل العضوية جوال  </h3>


                    <p class="txt-note txt-center">ستصلك رساله <b class="red-color">SMS</b> على جوالك ({{ \Auth::guard('members')->user()->phone_number }}) تحتوى على كود التفعيل</p>
                    <form class="form-ui row" method="post" name="form_active_sms" id="form_active_sms" action="{{ route('checkpoint-do-active') }}">

                        {!! csrf_field() !!}
                        <input type="hidden" name="active_type" id="active_type_sms" value="sms" />
                        <!-- Control -->
                        <div class="col-12 col-l-6">
                            <label >كود التفعيل </label>
                            <input  type="text"  placeholder="كود التفعيل" name="mobile_verify_code" id="mobile_verify_code"   dir="ltr" />
                            <input type="submit" value="تفعيل الجوال" class="btn primary">
                        </div>

                    </form>
                    <div class="alert-warning ">

                        اذا لم تصلك رسالة التفعيل من فضلك
                        <a href="javascript:void(0);" rel="m" class="resend">اضغط هنا </a>

                    </div>


            </div>
        </div>
    @endif
        <!-- // Global Form -->
        @if(\Auth::guard('members')->user()->is_active_email != 1 )
        <div class="primary-box">
            <div class="content-box">
                <h3 class="head">     العضوية بريد الكترونى  </h3>


                    <p class="txt-note txt-center">ستصلك رساله <b class="red-color">Email</b> على بريديك ({{ \Auth::guard('members')->user()->email }}) تحتوى على كود التفعيل</p>
                    <form class="form-ui row" method="post" name="form_active_email" id="form_active_email" action="{{ route('checkpoint-do-active') }}">

                        {!! csrf_field() !!}
                        <input type="hidden" name="active_type" id="active_type_email" value="email" />
                        <!-- Control -->
                        <div class="col-12 col-l-6">
                            <label >كود التفعيل </label>
                            <input  type="text"  placeholder="كود التفعيل" name="email_verify_code" id="email_verify_code"   dir="ltr" />
                            <input type="submit" value="تفعيل البريد" class="btn primary">
                        </div>

                    </form>
                    <div class="alert-warning ">

                        من فضلك قم بمراجعة البريد الالكترونى وممكن ان تكون الرسالة فى الرسائل المهملة junk mail  اذا لم تصل الرسالة من فضلك
                        <a href="javascript:void(0);" rel="e" class="resend">اضغط هنا </a>

                    </div>


            </div>
        </div>
        @endif

        @if(\Auth::guard('members')->user()->is_active==2)
            <div class="primary-box">
                <div class="content-box">
                    <h3 class="head">          تم اغلاق الحساب  </h3>
            <div class="alert alert-warning">
                <strong>تحذير!</strong>

                تم اغلاق الحساب الخاص بكم  واذا واجهت اى مشكلة من فضلك قبم بالتواصل معنا عبر
                <a href="{{ route('contactUs') }}" >اتصل بنا </a>
            </div>
                </div>
            </div>
        @endif

        @if(\Auth::guard('members')->user()->is_active_phone == 1 && \Auth::guard('members')->user()->is_active_email ==1 && \Auth::guard('members')->user()->is_active ==0)

            <div class="primary-box">
                <div class="content-box">
                    <h3 class="head">          انتظار تفعيل مدير الموقع  </h3>
            <div class="alert alert-info">
                <strong>معلومات!</strong>
                يرجى الانتظار حتى يتم تفعيل حسابكم من إدارة الموقع  واذا واجهت اى مشكلة يرجى مرسالتنا من خلال صفحة
                <a href="{{ route('contactUs') }}" >اتصل بنا </a>
            </div>
                </div>
            </div>

        @endif
    </div>
    <!-- // Page Content -->


    @endsection


@push('header')

    @endpush
@push('footer')
<script type="text/javascript">

$('a.resend').click(function(){
    var current_i = $(this).attr('rel');
    var current_row = $(this).closest('div.row');

    $.post('{{ route('checkpoint-do-resend') }}',{
        action:current_i,
        _token: '{{ csrf_token() }}'
    },function (result) {

        if(result[0]){
            toastr.success(result[1]);
        }else{
            toastr.warning(result[1]);
        }
    },'json');
})  ;

</script>

@endpush


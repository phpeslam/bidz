<!DOCTYPE html>
<html dir="{{ getCurrentLang('direction') }}" lang="{{ getCurrentLang('iso_code') }}">
    <head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="{{ $meta_description or '' }}">
        <meta name="keywords" content="{{ $meta_keywords or '' }}">
        <title>{{config('site_name')}}</title>
        <!-- Required CSS Files -->
        <link href="{{ asset('front/'.getCurrentLang('direction').'/css/tornado-rtl.min.css') }}" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        @stack('header')
    </head>
    <body  dir="{{ getCurrentLang('direction') }}">
        <!-- Header -->
        <header class="main-header" data-sticky>
            <div class="container">
                <a href="{{ route('home') }}" class="logo"><img src="{{ asset('front/'.getCurrentLang('direction').'/img/logo.png') }}" alt=""></a>
                <div class="navigation-menu" data-id="main-menu">
                    <a href="#" class="menu-btn ti-menu-round" data-id="main-menu"></a>
                    <ul>
                        <li><a href="{{ route('home')}} ">{{trans('global.main')}}</a></li>
                        <li><a href="{{ route('about-us')}}">{{trans('global.about_saudibidz')}}</a></li>
                        @if(\Auth::guard('members')->check())
                        <li><a href="{{ route('member-profile-index') }}">{{trans('global.profile_index')}}</a></li>
                        <li><a href="{{ route('member-logout') }}">{{trans('global.logOut')}}</a></li>
                        @else
                        <li><a href="{{ route('front-register') }}">{{trans('global.register')}}</a></li>
                        <li><a href="{{ route('front-login') }}">{{trans('global.login')}}</a></li>
                        @endif
                        <li><a href="{{ route('contactUs') }}">{{trans('global.contact_us')}}</a></li>
                    </ul>
                </div>

                <div class="social">
                    <a href="#" class="ti-search"></a>
                    @if(config('facebook'))
                    <a href="{{config('facebook')}}" class="ti-facebook"></a>
                    @endif
                    @if(config('twitter'))
                    <a href="{{config('twitter')}}" class="ti-twitter"></a>
                    @endif
                    @if(config('google_plus'))
                    <a href="{{config('google_plus')}}" class="ti-google-plus"></a>
                    @endif
                    @if(config('instagram'))
                    <a href="{{config('instagram')}}" class="ti-instagram"></a>
                    @endif
                    @if(getCurrentLang('iso_code')=='en')
                    <a href="{{ getLangUrl('ar') }}" class="lang">AR</a>
                    @else
                    <a href="{{ getLangUrl('en') }}" class="lang">EN</a>
                    @endif

                    <span class="time"><?php $now = new DateTime(); $month = $now->format('M'); $day = $now->format('D'); ?>{{ trans('home.'.$day) }} {{$now->format('d')}} {{ trans('home.'.$month) }} {{$now->format('Y')}}</span>
                </div>
            </div>
        </header>
        <!-- // Header -->

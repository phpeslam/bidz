
<!-- Footer -->
<footer class="main-footer">
    <div class="container">
        <div class="row">
            <!-- About -->
            <div class="col-12 col-m-6 col-l-4">
                <img src="{{ asset('front/'.getCurrentLang('direction').'/img/logo-w.png') }}" alt="" class="logo">
                @if(getCurrentLang('iso_code')=='ar')
                <p>{{config('about_site_ar')}}</p>
                @else
                <p>{{config('about_site_en')}}</p>
                @endif
            </div>
            <!-- Links -->
            <div class="col-12 col-m-3 col-l-2">
                <h3>{{trans('global.Sections')}}</h3>
                <ul>
                    <li><a href="{{url('OpenedAuctions')}}">{{trans('global.Open_auctions')}}</a></li>
                    <li><a href="{{url('ClosedAuctions')}}">{{trans('global.Closed_auctions')}}</a></li>
                    <li><a href="{{url('about')}}">{{trans('global.about_saudibidz')}}</a></li>
                    <li><a href="{{url('contactUs')}}">{{trans('global.contact_us')}}</a></li>
                </ul>
            </div>
            <!-- Links -->
            <div class="col-12 col-m-3 col-l-2">
                <h3>{{trans('global.imlinks')}}</h3>
                <ul>
                    <li><a href="{{url('terms')}}">{{trans('global.Policy')}}</a></li>
                    <li><a href="{{url('howsiteworks')}}">{{trans('global.how_work')}}</a></li>
                    <li><a href="{{url('faq')}}">{{trans('global.faq')}}</a></li>
                    <li><a href="{{url('register')}}">{{trans('global.register')}}</a></li>

                </ul>
            </div>
            <!-- App Download -->
            <div class="col-12 col-m-6 col-l-4 app-down">
                <h2>{{trans('home.store_text')}}</h2>
                <a href="{{config('ios_link')}}"><img src="{{ asset('front/'.getCurrentLang('direction').'/img/appstore.png') }}" alt=""></a>
                <a href="{{config('android_link')}}"><img src="{{ asset('front/'.getCurrentLang('direction').'/img/gplay.png') }}" alt=""></a>
            </div>
        </div>
    </div>
    <!-- Copyrights -->
    <div class="copyrights">
        <div class="container">
            <img src="{{ asset('front/'.getCurrentLang('direction').'/img/payment.png') }}" alt="">
            <span>{{trans('home.copy_right')}} <a href="http://www.glowapps.com.eg">Glowapps</a></span>
            <div class="social">
                @if(config('facebook'))
                <a href="{{config('facebook')}}" class="ti-facebook"></a>
                @endif
                @if(config('twitter'))
                <a href="{{config('twitter')}}" class="ti-twitter"></a>
                @endif
                @if(config('google_plus'))
                <a href="{{config('google_plus')}}" class="ti-google-plus"></a>
                @endif
                @if(config('instagram'))
                <a href="{{config('instagram')}}" class="ti-instagram"></a>
                @endif
            </div>
        </div>
    </div>
</footer>

<!-- Required JS Files -->
<script src="{{ asset('front/'.getCurrentLang('direction').'/js/jquery-3.3.1.min.js') }}"></script>
@if(getCurrentLang('iso_code')=='ar')
    <script src="{{ asset('front/'.getCurrentLang('direction').'/js/tornado.min.js') }}"></script>
@else
    <script src="{{ asset('front/'.getCurrentLang('direction').'/js/tornado.ltr.min.js') }}"></script>
@endif

<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
@stack('footer')
</body>
</html>
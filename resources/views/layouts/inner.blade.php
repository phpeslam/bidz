<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ getCurrentLang('direction') }}">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{  trans('global.site_title') }} @yield('page_title')</title>


    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/4.5.0/css/font-awesome.min.css') }}" />

    <!-- page specific plugin styles -->

    <!-- text fonts -->
    <link rel="stylesheet" href="{{ asset('assets/css/fonts.googleapis.com.css') }}" />

    <!-- ace styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/ace.min.css') }}" class="ace-main-stylesheet" id="main-ace-style" />

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{ asset('assets/css/ace-part2.min.css') }}" class="ace-main-stylesheet" />
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('assets/css/ace-skins.min.css') }}" />
    @if(getCurrentLang('direction')=='rtl')
        <link rel="stylesheet" href="{{ asset('assets/css/ace-rtl.min.css') }}" />
    @endif


    <!--[if lte IE 9]>
    <link rel="stylesheet" href="{{ asset('assets/css/ace-ie.min.css') }}" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->
    <script src="{{ asset('assets/js/ace-extra.min.js') }}"></script>

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="{{ asset('assets/js/html5shiv.min.js') }}"></script>
    <script src="{{ asset('assets/js/respond.min.js') }}"></script>
    <![endif]-->
        @if(isset($datatables))
            <link href="{{ asset('datatables/css/jquery.dataTables.min.css') }}" rel="stylesheet">
        @endif
        <script>
            window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
            var datatable_lang = {!! json_encode(trans('global.datatables')) !!}
        </script>
        @stack('page_header')
</head>

<body class="no-skin @if(getCurrentLang('direction')=='rtl') rtl @endif" dir="{{ getCurrentLang('direction') }}">
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler @if(getCurrentLang('direction')=='rtl') pull-right @else pull-left @endif" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header @if(getCurrentLang('direction')=='rtl') pull-right @else pull-left @endif">
            <a href="{{ url('/') }}" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    {{ config('app.name') }}
                </small>
            </a>
        </div>

        <div class="navbar-buttons navbar-header @if(getCurrentLang('direction')=='rtl') pull-left @else pull-right @endif " role="navigation">
            <ul class="nav ace-nav">
                @foreach(getLanguages() as $key=>$l)
                    @if($key!=getCurrentLang('iso_code'))
                <li class="purple dropdown-modal">
                    <a  class="dropdown-toggle" href="{{ getLangUrl($l['iso_code']) }}">
                     {{ $l['text'] }}
                    </a>
                </li>
                    @endif
                @endforeach
                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">

                        <span class="user-info">
									<small>{{ trans('global.welcome') }}</small>
									{{ \Auth::user()->name }}
								</span>

                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>

                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">


                        <li>
                            <a href="{{ route('admin-profile') }}">
                                <i class="ace-icon fa fa-user"></i>
                                {{ trans('global.profile') }}
                            </a>
                        </li>

                        <li class="divider"></li>

                        <li>
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="ace-icon fa fa-power-off"></i>
                                {{ trans('global.logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div><!-- /.navbar-container -->
</div>

<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try{ace.settings.loadState('main-container')}catch(e){}
    </script>
    @include('parts.menu')
    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                @yield('breadcrumb')
            </div>

            <div class="page-content">

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                    @yield('content')
                        <!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <div class="footer">
        <div class="footer-inner">
            <div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder">CP</span>
							{{ config('app.name', 'Laravel') }} &copy; 2018-2019
						</span>

            </div>
        </div>
    </div>

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->

<!-- basic scripts -->

<!--[if !IE]> -->
<script src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}"></script>
<![endif]-->
<script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='{{ asset('assets/js/jquery.mobile.custom.min.js') }}'>"+"<"+"/script>");
</script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

<!-- page specific plugin scripts -->

<!-- ace scripts -->
<script src="{{ asset('assets/js/ace-elements.min.js') }}"></script>
<script src="{{ asset('assets/js/ace.min.js') }}"></script>

    <script src="{{ asset('assets/js/bootbox.min.js') }}"></script>
    <script src="{{ asset('js/jquery.toaster.js') }}"></script>
    <script type="text/javascript">
        bootbox.addLocale('{{ config('app.locale') }}',{!! json_encode(trans('global.bootbox')) !!});
        bootbox.setLocale('{{ config('app.locale') }}');
        var bootbox_delete_confirm = '{{trans('global.bootbox_delete_confirm')}}';
    </script>

@if(isset($datatables))
    <script src="{{ asset('datatables/js/jquery.dataTables.min.js') }}"></script>
@endif
<!-- inline scripts related to this page -->
@stack('page_footer')
</body>
</html>
@include('parts._header')
<!-- Page Head -->
<div class="page-head">
    <div class="container">
        <div class="head-info">
            <h1>   {{ $page_title or '' }}</h1>
            <div class="breadcrumb">
                <a href="{{ route('front-home') }}">الرئيسيه</a>
                <a href="{{ url()->current() }}">  {{ $path_I or '' }}</a>
            </div>
        </div>
        @if(\Auth::guard('members')->user())
        <div class="user-area">
            <a href="#" class="user-btn"> {{ \Auth::guard('members')->user()->first_name }}  {{ \Auth::guard('members')->user()->last_name }}<span data-notfic="{{getCountOfNotifications()}}" class="ti-notifications-bill"></span> </a>
            <ul class="user-menu">
                <li><a href="{{ route('member-profile-index') }}">ملفي الشخصي</a></li>
                <li><a href="{{ route('member-bid-add') }}">  فتح مزايدة</a></li>
                <li><a href="{{ route('member-notifications') }}">الإشعارات</a></li>
                <li><a href="{{route('member-bid-list')}}">مناقصاتي</a></li>
                <li><a href="{{route('all-follow')}}">قائمة متابعاتي</a></li>
                <li><a href="{{route('myBalance')}}">رصيدي</a></li>
                <li><a href="{{ route('member-logout') }}">تسجيل الخروج</a></li>
            </ul>
        </div>
            @endif
    </div>
</div>
<!-- // Page Head -->
<div class="container page-content">
    @yield('page_content')
</div>
<!-- // Page Content -->

@include('parts._footer')
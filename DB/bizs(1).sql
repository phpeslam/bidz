-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 09, 2018 at 07:05 AM
-- Server version: 5.7.21-0ubuntu0.17.10.1
-- PHP Version: 7.1.15-1+ubuntu17.10.1+deb.sury.org+2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bizs`
--

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE `auctions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sub_category_id` int(10) UNSIGNED DEFAULT NULL,
  `car_color_id` int(10) UNSIGNED DEFAULT NULL,
  `car_type_id` int(10) UNSIGNED DEFAULT NULL,
  `engine_type_id` int(10) UNSIGNED DEFAULT NULL,
  `fuel_id` int(10) UNSIGNED DEFAULT NULL,
  `import_id` int(10) UNSIGNED DEFAULT NULL,
  `motion_vector_id` int(10) UNSIGNED DEFAULT NULL,
  `specification_id` int(10) UNSIGNED DEFAULT NULL,
  `manufacturing_year` int(4) DEFAULT NULL,
  `engine_size` int(10) DEFAULT NULL,
  `vaild_form` tinyint(1) DEFAULT '1',
  `form_years` int(3) DEFAULT '0',
  `is_periodic_inspection` tinyint(1) DEFAULT '1',
  `mileage` varchar(100) DEFAULT NULL,
  `chassis_no` varchar(100) DEFAULT NULL,
  `bid_type` tinyint(1) DEFAULT '1',
  `minimum_bid` int(11) DEFAULT NULL,
  `bid_days` int(5) DEFAULT NULL,
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `provider_id` int(10) UNSIGNED DEFAULT NULL,
  `provider_accept` tinyint(1) DEFAULT '0',
  `is_active` tinyint(15) DEFAULT '0',
  `start_at` datetime DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `title`, `description`, `category_id`, `sub_category_id`, `car_color_id`, `car_type_id`, `engine_type_id`, `fuel_id`, `import_id`, `motion_vector_id`, `specification_id`, `manufacturing_year`, `engine_size`, `vaild_form`, `form_years`, `is_periodic_inspection`, `mileage`, `chassis_no`, `bid_type`, `minimum_bid`, `bid_days`, `member_id`, `provider_id`, `provider_accept`, `is_active`, `start_at`, `views`, `created_at`, `updated_at`) VALUES
(4, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 1, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(5, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(6, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(7, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(8, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(9, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(10, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(11, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(12, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(13, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(14, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(15, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(16, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(17, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(18, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(19, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(20, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(21, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(22, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(23, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(24, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(25, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(26, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(27, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(28, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(29, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(30, '5345345345', 'ثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضص', 5, 5, 3, 1, 1, 2, 1, 2, 1, 2002, 454, 1, 3, 1, '5454', '435345345', 2, 1600, 4, 11, 12, 1, 0, '2018-05-09 04:37:09', NULL, '2018-05-09 00:19:53', '2018-05-09 00:19:53');

-- --------------------------------------------------------

--
-- Table structure for table `auction_files`
--

CREATE TABLE `auction_files` (
  `auction_id` int(11) UNSIGNED NOT NULL,
  `order` int(5) NOT NULL,
  `file` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auction_files`
--

INSERT INTO `auction_files` (`auction_id`, `order`, `file`) VALUES
(4, 8, '413ffd4dfd081b8eeb1911196966b32e.png'),
(30, 0, '67b906de5b7f8226de1b99173ac7b06f.png');

-- --------------------------------------------------------

--
-- Table structure for table `auction_images`
--

CREATE TABLE `auction_images` (
  `auction_id` int(11) UNSIGNED NOT NULL,
  `order` int(5) NOT NULL,
  `image` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auction_images`
--

INSERT INTO `auction_images` (`auction_id`, `order`, `image`) VALUES
(4, 4, '213ce81691a98b3f7c0900d8abbb1a4c.png'),
(30, 0, '10b4f001de3f05ba51ed4a749b56e4f9.png'),
(30, 1, '44be43486223121b066664f56e34c867.png'),
(30, 2, '63c110592c3a6303fb8cac8b8b712d7d.png'),
(30, 3, '0f47399ecad039f7646987c96c67c997.png'),
(30, 4, '8df6500ab0682b7a7b0cbe3d799a9c9f.png');

-- --------------------------------------------------------

--
-- Table structure for table `auction_other_specifications`
--

CREATE TABLE `auction_other_specifications` (
  `auction_id` int(10) UNSIGNED NOT NULL,
  `other_specification_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auction_other_specifications`
--

INSERT INTO `auction_other_specifications` (`auction_id`, `other_specification_id`) VALUES
(30, 1),
(30, 25),
(4, 30),
(30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `car_colors`
--

CREATE TABLE `car_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_colors`
--

INSERT INTO `car_colors` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL),
(4, 1, NULL, NULL),
(5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_color_languages`
--

CREATE TABLE `car_color_languages` (
  `car_color_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_color_languages`
--

INSERT INTO `car_color_languages` (`car_color_id`, `language_id`, `title`) VALUES
(1, 1, 'احمر'),
(1, 2, 'احمر'),
(2, 1, 'اخضر'),
(2, 2, 'اخضر'),
(3, 1, 'نبيتى'),
(3, 2, 'نبيتى'),
(4, 1, 'اسود '),
(4, 2, 'اسود '),
(5, 1, 'برتقالى'),
(5, 2, 'برتقالى');

-- --------------------------------------------------------

--
-- Table structure for table `car_types`
--

CREATE TABLE `car_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_types`
--

INSERT INTO `car_types` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_type_languages`
--

CREATE TABLE `car_type_languages` (
  `car_type_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_type_languages`
--

INSERT INTO `car_type_languages` (`car_type_id`, `language_id`, `title`) VALUES
(1, 1, 'سيدان'),
(1, 2, 'سيدان'),
(2, 1, 'هاتش باك'),
(2, 2, 'هاتش باك');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-04-30 06:07:19', '2018-04-30 06:07:22'),
(2, 1, '2018-04-30 06:07:19', NULL),
(3, 1, '2018-04-30 06:07:19', NULL),
(4, 1, '2018-04-30 06:07:19', NULL),
(5, 1, '2018-04-30 06:07:19', NULL),
(6, 1, '2018-04-30 06:07:19', NULL),
(7, 1, '2018-04-30 06:07:19', NULL),
(8, 1, '2018-04-30 06:07:19', NULL),
(9, 1, '2018-04-30 06:07:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_languages`
--

CREATE TABLE `category_languages` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_languages`
--

INSERT INTO `category_languages` (`category_id`, `language_id`, `title`) VALUES
(1, 1, 'dd'),
(1, 2, 'asdas'),
(2, 1, 'dfsd'),
(2, 2, 'fsdf'),
(3, 1, 'sdfs'),
(3, 2, 'fsdfs'),
(4, 1, 'fsdf'),
(4, 2, 'dfs'),
(5, 1, 'fsdsdf'),
(5, 2, 'sdf'),
(6, 1, 'sdfs'),
(6, 2, 'sdf'),
(7, 1, 'sf'),
(7, 2, 'sdf'),
(8, 1, 'sdf'),
(8, 2, 'sd'),
(9, 1, 'sdf'),
(9, 2, 'sdf');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `is_active`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-05-03 17:01:37', NULL),
(2, 1, 1, '2018-05-03 17:01:37', NULL),
(3, 1, 1, '2018-05-03 17:01:37', NULL),
(4, 1, 1, '2018-05-03 17:01:37', NULL),
(5, 1, 1, '2018-05-03 17:01:37', NULL),
(6, 1, 2, '2018-05-03 17:01:37', NULL),
(7, 1, 2, '2018-05-03 17:01:37', NULL),
(8, 1, 2, '2018-05-03 17:01:37', NULL),
(9, 1, 2, '2018-05-03 17:01:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city_languages`
--

CREATE TABLE `city_languages` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city_languages`
--

INSERT INTO `city_languages` (`city_id`, `language_id`, `title`) VALUES
(1, 1, 'city c1'),
(1, 2, 'city c1'),
(2, 1, 'city c1'),
(2, 2, 'city c1'),
(3, 1, 'city c 1'),
(3, 2, 'city c 1'),
(4, 1, 'city c1'),
(4, 2, 'city c1'),
(5, 1, 'city c1'),
(5, 2, 'city c1'),
(6, 1, 'city c2'),
(6, 2, 'city c2'),
(7, 1, 'city c2'),
(7, 2, 'city c2'),
(8, 1, 'city c2'),
(8, 2, 'city c2'),
(9, 1, 'city c2'),
(9, 2, 'city c2');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-05-03 17:00:31', NULL),
(2, 1, '2018-05-03 17:00:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country_languages`
--

CREATE TABLE `country_languages` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country_languages`
--

INSERT INTO `country_languages` (`country_id`, `language_id`, `title`) VALUES
(1, 1, 'مصر'),
(1, 2, 'Egypt'),
(2, 1, 'السعودية'),
(2, 2, 'Sa');

-- --------------------------------------------------------

--
-- Table structure for table `engine_types`
--

CREATE TABLE `engine_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `engine_types`
--

INSERT INTO `engine_types` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine_type_languages`
--

CREATE TABLE `engine_type_languages` (
  `engine_type_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `engine_type_languages`
--

INSERT INTO `engine_type_languages` (`engine_type_id`, `language_id`, `title`) VALUES
(1, 1, 'عادى'),
(1, 2, 'عادى'),
(2, 1, 'تيربو'),
(2, 2, 'تيربو');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(3, 1, '2018-01-29 23:32:41', '2018-04-30 02:08:56'),
(4, 1, '2018-01-30 00:07:20', '2018-04-30 02:08:55'),
(5, 1, '2018-01-30 00:07:27', '2018-04-30 02:08:53'),
(6, 1, '2018-01-30 00:07:42', '2018-04-30 02:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `faq_languages`
--

CREATE TABLE `faq_languages` (
  `faq_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_languages`
--

INSERT INTO `faq_languages` (`faq_id`, `language_id`, `title`, `desc`) VALUES
(3, 1, 'd', 'vxcvxcvxcvxvvv'),
(3, 2, 'ccccccccccccccc1', 'ccccccccccccccccccccccccccccccccccccc1'),
(4, 1, 'vxcvxcv', 'xcvxcvxcv'),
(4, 2, 'ewrwerwe', 'rwerwe'),
(5, 1, 'dasdasd', 'asdasdas'),
(5, 2, 'dasdasd', 'adasd'),
(6, 1, 'sdfsdfsdf', 'sdfsdf'),
(6, 2, 'sdfsdfsd', 'fsdfsdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `fuels`
--

CREATE TABLE `fuels` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fuels`
--

INSERT INTO `fuels` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fuel_languages`
--

CREATE TABLE `fuel_languages` (
  `fuel_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fuel_languages`
--

INSERT INTO `fuel_languages` (`fuel_id`, `language_id`, `title`) VALUES
(1, 1, 'جاز'),
(1, 2, 'جاز'),
(2, 1, 'بترول'),
(2, 2, 'بترول'),
(3, 1, 'غاز'),
(3, 2, 'غاز');

-- --------------------------------------------------------

--
-- Table structure for table `imports`
--

CREATE TABLE `imports` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imports`
--

INSERT INTO `imports` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `import_languages`
--

CREATE TABLE `import_languages` (
  `import_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `import_languages`
--

INSERT INTO `import_languages` (`import_id`, `language_id`, `title`) VALUES
(1, 1, 'خليجى'),
(1, 2, 'خليجى'),
(2, 1, 'سعودى'),
(2, 2, 'سعودى');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `iso_code` varchar(3) DEFAULT NULL,
  `text` varchar(50) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `direction` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `iso_code`, `text`, `is_default`, `direction`) VALUES
(1, 'ar', 'اللغة العربية', 1, 'rtl'),
(2, 'en', 'English', 0, 'ltr');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_general_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `membership_type` tinyint(1) DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `fcm_token` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `authorization` varchar(255) COLLATE utf8_general_ci DEFAULT NULL,
  `device_id` varchar(20) COLLATE utf8_general_ci DEFAULT NULL,
  `is_active_phone` tinyint(1) DEFAULT '0',
  `is_active_email` tinyint(1) DEFAULT '1',
  `phone_act_code` varchar(8) COLLATE utf8_general_ci DEFAULT NULL,
  `email_act_code` varchar(30) COLLATE utf8_general_ci DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `file` varchar(50) COLLATE utf8_general_ci DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `first_name`, `last_name`, `email`, `password`, `city_id`, `country_id`, `phone_number`, `address`, `membership_type`, `remember_token`, `fcm_token`, `authorization`, `device_id`, `is_active_phone`, `is_active_email`, `phone_act_code`, `email_act_code`, `is_active`, `file`, `balance`, `created_at`, `updated_at`) VALUES
(11, 'اسلام', 'قنديل', 'eslam1@php-eg.com', '$2y$10$ralsdFwv0HRbhc70RABT/.9Od30BQhuYHyFd.LAfyAfsg8M1uMeca', 3, 1, '01110633087', 'ميت عمر دقهلية', 1, '1xUJkATk4VetyoozwMNxUHYdw0jj9q9iRCtA6VJW9xmKLZn4Kwg1Ec4QdCeY', NULL, NULL, NULL, 1, 1, NULL, NULL, 1, NULL, NULL, '2018-05-04 02:06:09', '2018-05-04 03:22:28'),
(12, 'اسلام', 'Ahmed', 'eslam@php-eg.com', '$2y$10$ralsdFwv0HRbhc70RABT/.9Od30BQhuYHyFd.LAfyAfsg8M1uMeca', 5, 1, '01110633084', 'sadasd', 2, '3y2gqGgjeOknWoa4Xr8rOKPUNncRLRYOoJ5MaIHhQhXelBuLmt06UllwRY4M', NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 'c7b326b492fd9c12f78acd9182d45430.png', NULL, '2018-05-04 03:25:21', '2018-05-04 05:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2016_09_04_000000_create_roles_table', 2),
(7, '2016_09_04_100000_create_role_user_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `motion_vectors`
--

CREATE TABLE `motion_vectors` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `motion_vectors`
--

INSERT INTO `motion_vectors` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `motion_vector_languages`
--

CREATE TABLE `motion_vector_languages` (
  `motion_vector_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `motion_vector_languages`
--

INSERT INTO `motion_vector_languages` (`motion_vector_id`, `language_id`, `title`) VALUES
(1, 1, 'عادى'),
(1, 2, 'عادى'),
(2, 1, 'اتوماتك'),
(2, 2, 'اتوماتك');

-- --------------------------------------------------------

--
-- Table structure for table `other_specifications`
--

CREATE TABLE `other_specifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_specifications`
--

INSERT INTO `other_specifications` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL),
(4, 1, NULL, NULL),
(5, 1, NULL, NULL),
(6, 1, NULL, NULL),
(7, 1, NULL, NULL),
(8, 1, NULL, NULL),
(9, 1, NULL, NULL),
(10, 1, NULL, NULL),
(11, 1, NULL, NULL),
(12, 1, NULL, NULL),
(13, 1, NULL, NULL),
(14, 1, NULL, NULL),
(15, 1, NULL, NULL),
(16, 1, NULL, NULL),
(17, 1, NULL, NULL),
(18, 1, NULL, NULL),
(19, 1, NULL, NULL),
(20, 1, NULL, NULL),
(21, 1, NULL, NULL),
(22, 1, NULL, NULL),
(23, 1, NULL, NULL),
(24, 1, NULL, NULL),
(25, 1, NULL, NULL),
(26, 1, NULL, NULL),
(27, 1, NULL, NULL),
(28, 1, NULL, NULL),
(29, 1, NULL, NULL),
(30, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `other_specification_languages`
--

CREATE TABLE `other_specification_languages` (
  `other_specification_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_specification_languages`
--

INSERT INTO `other_specification_languages` (`other_specification_id`, `language_id`, `title`) VALUES
(1, 1, 'وسائد هوائية للسائق'),
(1, 2, 'وسائد هوائية للسائق'),
(2, 1, 'وسائد هوائية للراكب'),
(2, 2, 'وسائد هوائية للراكب'),
(3, 1, 'كراسي كهربائية'),
(3, 2, 'كراسي كهربائية'),
(4, 1, 'نظام فرامل ABS'),
(4, 2, 'نظام فرامل ABS'),
(5, 1, 'توزيع اليكتروني للفرامل EBD'),
(5, 2, 'توزيع اليكتروني للفرامل EBD'),
(6, 1, 'برنامج التوازن الإلكتروني ESP'),
(6, 2, 'برنامج التوازن الإلكتروني ESP'),
(7, 1, 'نظام تنبية ضد السرقة'),
(7, 2, 'نظام تنبية ضد السرقة'),
(8, 1, 'نظام إيموبليزر ضد السرقة'),
(8, 2, 'نظام إيموبليزر ضد السرقة'),
(9, 1, 'جنوط رياضية'),
(9, 2, 'جنوط رياضية'),
(10, 1, 'فوانيس ضباب امامية'),
(10, 2, 'فوانيس ضباب امامية'),
(11, 1, 'مرايات جانبية كهربائية'),
(11, 2, 'مرايات جانبية كهربائية'),
(12, 1, 'طي المرايات الجانبية كهربائيا'),
(12, 2, 'طي المرايات الجانبية كهربائيا'),
(13, 1, 'إضاءة المصابيح زنون'),
(13, 2, 'إضاءة المصابيح زنون'),
(14, 1, 'إضاءة المصابيح الأمامية LED'),
(14, 2, 'إضاءة المصابيح الأمامية LED'),
(15, 1, 'إضاءة المصابيح الخلفية LED'),
(15, 2, 'إضاءة المصابيح الخلفية LED'),
(16, 1, 'النظام الذكى لركن السيارة'),
(16, 2, 'النظام الذكى لركن السيارة'),
(17, 1, 'النظام الصوتي'),
(17, 2, 'النظام الصوتي'),
(18, 1, 'مدخل AUX'),
(18, 2, 'مدخل AUX'),
(19, 1, 'مدخل USB'),
(19, 2, 'مدخل USB'),
(20, 1, 'بلوتوث'),
(20, 2, 'بلوتوث'),
(21, 1, 'زجاج كهربائي'),
(21, 2, 'زجاج كهربائي'),
(22, 1, 'تحكم عن بعد في غلق وفتح الأبواب'),
(22, 2, 'تحكم عن بعد في غلق وفتح الأبواب'),
(23, 1, 'فرش جلد'),
(23, 2, 'فرش جلد'),
(24, 1, 'المفتاح الذكي'),
(24, 2, 'المفتاح الذكي'),
(25, 1, 'كاميرا خلفية'),
(25, 2, 'كاميرا خلفية'),
(26, 1, 'كمبيوتر رحلات'),
(26, 2, 'كمبيوتر رحلات'),
(27, 1, 'تحكم في نظام الصوت من عجلة القيادة'),
(27, 2, 'تحكم في نظام الصوت من عجلة القيادة'),
(28, 1, 'مثبت سرعة'),
(28, 2, 'مثبت سرعة'),
(29, 1, 'إمكانية طى ظهر مقاعد الخلفية'),
(29, 2, 'إمكانية طى ظهر مقاعد الخلفية'),
(30, 1, 'تكييف'),
(30, 2, 'تكييف');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-04-30 18:59:45', '2018-04-30 18:59:45'),
(2, 1, '2018-04-30 19:00:40', '2018-04-30 19:00:40'),
(3, 1, '2018-04-30 19:00:55', '2018-04-30 19:00:55'),
(4, 1, '2018-04-30 19:33:18', '2018-04-30 19:33:18');

-- --------------------------------------------------------

--
-- Table structure for table `page_languages`
--

CREATE TABLE `page_languages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_languages`
--

INSERT INTO `page_languages` (`page_id`, `language_id`, `title`, `desc`) VALUES
(1, 1, 'Expernice', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.</p>'),
(1, 2, 'Expernice', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.</p>'),
(2, 1, 'Vision', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention.</p>'),
(2, 2, 'Vision', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention.</p>'),
(3, 1, 'Mission', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.</p>'),
(3, 2, 'Mission', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.</p>'),
(4, 1, 'and other tan', '<p>and other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tan</p>'),
(4, 2, 'and other', '<p>and other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tanand other tan</p>');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('eslam@php-eg.com', '$2y$10$QckkbORcNLQEndBsvEc6A.HxGTWyS924W3x7KD6Cd6.yJfm.JLTna', '2018-01-27 21:25:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group` varchar(255) NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `group`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin', 'default', NULL, NULL),
(2, 'masjed haram moshref', 'masjed_haram', 'masjed haram moshref', 'default', NULL, NULL),
(3, 'masjed_nabawy', 'masjed_nabawy', 'masjed_nabawy', 'default', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 1, 1, NULL, NULL),
(4, 1, 4, '2018-02-01 12:36:41', '2018-02-01 12:36:41'),
(7, 2, 5, '2018-02-01 12:39:10', '2018-02-01 12:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `key` varchar(50) NOT NULL,
  `value` text,
  `is_serialize` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`key`, `value`, `is_serialize`) VALUES
('about_site', '<p>fgdf</p>', 0),
('ads_code_pop_code', NULL, 0),
('amount_of_insurance', NULL, 0),
('app_url', NULL, 0),
('contcat_us', '<p>gdfg</p>', 0),
('facebook_url', NULL, 0),
('instagram_url', NULL, 0),
('pop_header_js', NULL, 0),
('site_meta_desc', 'وصف ميتا تاج', 0),
('site_meta_tag', 'setting.site_meta_tag', 0),
('site_name', 'كورا اكسترا', 0),
('twitter_url', NULL, 0),
('youtube_url', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `specifications`
--

CREATE TABLE `specifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specifications`
--

INSERT INTO `specifications` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `specification_languages`
--

CREATE TABLE `specification_languages` (
  `specification_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specification_languages`
--

INSERT INTO `specification_languages` (`specification_id`, `language_id`, `title`) VALUES
(1, 1, 'فل'),
(1, 2, 'فل'),
(2, 1, 'ستاندر'),
(2, 2, 'ستاندر');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `is_active`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 1, 3, NULL, NULL),
(4, 1, 4, NULL, NULL),
(5, 1, 5, NULL, NULL),
(6, 1, 6, NULL, NULL),
(7, 1, 7, NULL, NULL),
(8, 1, 8, NULL, NULL),
(9, 1, 9, NULL, NULL),
(10, 1, 1, NULL, NULL),
(11, 1, 2, NULL, NULL),
(12, 1, 3, NULL, NULL),
(13, 1, 4, NULL, NULL),
(14, 1, 5, NULL, NULL),
(15, 1, 6, NULL, NULL),
(16, 1, 7, NULL, NULL),
(17, 1, 8, NULL, NULL),
(18, 1, 9, NULL, NULL),
(19, 1, 1, NULL, NULL),
(20, 1, 2, NULL, NULL),
(21, 1, 3, NULL, NULL),
(22, 1, 4, NULL, NULL),
(23, 1, 5, NULL, NULL),
(24, 1, 6, NULL, NULL),
(25, 1, 7, NULL, NULL),
(26, 1, 8, NULL, NULL),
(27, 1, 9, NULL, NULL),
(28, 1, 1, NULL, NULL),
(29, 1, 2, NULL, NULL),
(30, 1, 3, NULL, NULL),
(31, 1, 4, NULL, NULL),
(32, 1, 5, NULL, NULL),
(33, 1, 6, NULL, NULL),
(34, 1, 7, NULL, NULL),
(35, 1, 8, NULL, NULL),
(36, 1, 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category_languages`
--

CREATE TABLE `sub_category_languages` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category_languages`
--

INSERT INTO `sub_category_languages` (`sub_category_id`, `language_id`, `title`) VALUES
(1, 1, 'يسشيششسش'),
(1, 2, 'dfajkfjdkfjs'),
(2, 1, 'يسشيششسش'),
(2, 2, 'dfajkfjdkfjs'),
(3, 1, 'يسشيششسش'),
(3, 2, 'dfajkfjdkfjs'),
(4, 1, 'يسشيششسش'),
(4, 2, 'dfajkfjdkfjs'),
(5, 1, 'يسشيششسش'),
(5, 2, 'dfajkfjdkfjs'),
(6, 1, 'يسشيششسش'),
(6, 2, 'dfajkfjdkfjs'),
(7, 1, 'يسشيششسش'),
(7, 2, 'dfajkfjdkfjs'),
(8, 1, 'يسشيششسش'),
(8, 2, 'dfajkfjdkfjs'),
(9, 1, 'يسشيششسش'),
(9, 2, 'dfajkfjdkfjs'),
(10, 1, 'يسشيششسش'),
(10, 2, 'dfajkfjdkfjs'),
(11, 1, 'يسشيششسش'),
(11, 2, 'dfajkfjdkfjs'),
(12, 1, 'يسشيششسش'),
(12, 2, 'dfajkfjdkfjs'),
(13, 1, 'يسشيششسش'),
(13, 2, 'dfajkfjdkfjs'),
(14, 1, 'يسشيششسش'),
(14, 2, 'dfajkfjdkfjs'),
(15, 1, 'يسشيششسش'),
(15, 2, 'dfajkfjdkfjs'),
(16, 1, 'يسشيششسش'),
(16, 2, 'dfajkfjdkfjs'),
(17, 1, 'يسشيششسش'),
(17, 2, 'dfajkfjdkfjs'),
(18, 1, 'يسشيششسش'),
(18, 2, 'dfajkfjdkfjs'),
(19, 1, 'يسشيششسش'),
(19, 2, 'dfajkfjdkfjs'),
(20, 1, 'يسشيششسش'),
(20, 2, 'dfajkfjdkfjs'),
(21, 1, 'يسشيششسش'),
(21, 2, 'dfajkfjdkfjs'),
(22, 1, 'يسشيششسش'),
(22, 2, 'dfajkfjdkfjs'),
(23, 1, 'يسشيششسش'),
(23, 2, 'dfajkfjdkfjs'),
(24, 1, 'يسشيششسش'),
(24, 2, 'dfajkfjdkfjs'),
(25, 1, 'يسشيششسش'),
(25, 2, 'dfajkfjdkfjs'),
(26, 1, 'يسشيششسش'),
(26, 2, 'dfajkfjdkfjs'),
(27, 1, 'يسشيششسش'),
(27, 2, 'dfajkfjdkfjs'),
(28, 1, 'يسشيششسش'),
(28, 2, 'dfajkfjdkfjs'),
(29, 1, 'يسشيششسش'),
(29, 2, 'dfajkfjdkfjs'),
(30, 1, 'يسشيششسش'),
(30, 2, 'dfajkfjdkfjs'),
(31, 1, 'يسشيششسش'),
(31, 2, 'dfajkfjdkfjs'),
(32, 1, 'يسشيششسش'),
(32, 2, 'dfajkfjdkfjs'),
(33, 1, 'يسشيششسش'),
(33, 2, 'dfajkfjdkfjs'),
(34, 1, 'يسشيششسش'),
(34, 2, 'dfajkfjdkfjs'),
(35, 1, 'يسشيششسش'),
(35, 2, 'dfajkfjdkfjs'),
(36, 1, 'يسشيششسش'),
(36, 2, 'dfajkfjdkfjs');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_general_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'اسلام قنديل', 'eslam@php-eg.com', '$2y$10$lQCsqu.iVH2g96zPPmixDOLDw8GbuA/yIArswc14raW8r4MXsUkba', 'fY31nxiwhYZZMWEdEMXwmNEEiK9AUphrr3eoYnzsN29adhSBFEAm0LKnNJ0t', '2018-01-10 22:52:57', '2018-02-04 19:54:33'),
(4, 'eslam2', 'islam@php-eg.com', '$2y$10$NZEqPXcqLDa0ijFg9YLD3OBkCfTJV.ADQkNVRpmG3MnfIFQ0j5PYy', NULL, '2018-02-01 10:08:27', '2018-02-01 10:08:27'),
(5, 'trterter', 'tret@kfdskd.com', '$2y$10$Su/Ecevu46AHEeJLZzoIhOJK.GsFT4lunsMs1CMVdG/9LFEpDKNMq', NULL, '2018-02-01 12:39:10', '2018-02-01 12:39:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auctions`
--
ALTER TABLE `auctions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`,`sub_category_id`),
  ADD KEY `car_color_id` (`car_color_id`),
  ADD KEY `car_type_id` (`car_type_id`),
  ADD KEY `engine_type_id` (`engine_type_id`),
  ADD KEY `fuel_id` (`fuel_id`),
  ADD KEY `import_id` (`import_id`),
  ADD KEY `motion_vector_id` (`motion_vector_id`),
  ADD KEY `specification_id` (`specification_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `auction_files`
--
ALTER TABLE `auction_files`
  ADD PRIMARY KEY (`auction_id`,`order`);

--
-- Indexes for table `auction_images`
--
ALTER TABLE `auction_images`
  ADD PRIMARY KEY (`auction_id`,`order`);

--
-- Indexes for table `auction_other_specifications`
--
ALTER TABLE `auction_other_specifications`
  ADD PRIMARY KEY (`auction_id`,`other_specification_id`),
  ADD KEY `other_specification_id` (`other_specification_id`);

--
-- Indexes for table `car_colors`
--
ALTER TABLE `car_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_color_languages`
--
ALTER TABLE `car_color_languages`
  ADD PRIMARY KEY (`car_color_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `car_types`
--
ALTER TABLE `car_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_type_languages`
--
ALTER TABLE `car_type_languages`
  ADD PRIMARY KEY (`car_type_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_languages`
--
ALTER TABLE `category_languages`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `id` (`id`,`country_id`);

--
-- Indexes for table `city_languages`
--
ALTER TABLE `city_languages`
  ADD PRIMARY KEY (`city_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_languages`
--
ALTER TABLE `country_languages`
  ADD PRIMARY KEY (`country_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `engine_types`
--
ALTER TABLE `engine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `engine_type_languages`
--
ALTER TABLE `engine_type_languages`
  ADD PRIMARY KEY (`engine_type_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_languages`
--
ALTER TABLE `faq_languages`
  ADD PRIMARY KEY (`faq_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `fuels`
--
ALTER TABLE `fuels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fuel_languages`
--
ALTER TABLE `fuel_languages`
  ADD PRIMARY KEY (`fuel_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `imports`
--
ALTER TABLE `imports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `import_languages`
--
ALTER TABLE `import_languages`
  ADD PRIMARY KEY (`import_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD UNIQUE KEY `authorization` (`authorization`),
  ADD KEY `city_id` (`city_id`,`country_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motion_vectors`
--
ALTER TABLE `motion_vectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motion_vector_languages`
--
ALTER TABLE `motion_vector_languages`
  ADD PRIMARY KEY (`motion_vector_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `other_specifications`
--
ALTER TABLE `other_specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_specification_languages`
--
ALTER TABLE `other_specification_languages`
  ADD PRIMARY KEY (`other_specification_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_languages`
--
ALTER TABLE `page_languages`
  ADD PRIMARY KEY (`page_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `specifications`
--
ALTER TABLE `specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specification_languages`
--
ALTER TABLE `specification_languages`
  ADD PRIMARY KEY (`specification_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`,`id`);

--
-- Indexes for table `sub_category_languages`
--
ALTER TABLE `sub_category_languages`
  ADD PRIMARY KEY (`sub_category_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auctions`
--
ALTER TABLE `auctions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `car_colors`
--
ALTER TABLE `car_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `car_types`
--
ALTER TABLE `car_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `engine_types`
--
ALTER TABLE `engine_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `fuels`
--
ALTER TABLE `fuels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `imports`
--
ALTER TABLE `imports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `motion_vectors`
--
ALTER TABLE `motion_vectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `other_specifications`
--
ALTER TABLE `other_specifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `specifications`
--
ALTER TABLE `specifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auctions`
--
ALTER TABLE `auctions`
  ADD CONSTRAINT `auctions_ibfk_1` FOREIGN KEY (`category_id`,`sub_category_id`) REFERENCES `sub_categories` (`category_id`, `id`),
  ADD CONSTRAINT `auctions_ibfk_10` FOREIGN KEY (`provider_id`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `auctions_ibfk_2` FOREIGN KEY (`car_color_id`) REFERENCES `car_colors` (`id`),
  ADD CONSTRAINT `auctions_ibfk_3` FOREIGN KEY (`car_type_id`) REFERENCES `car_types` (`id`),
  ADD CONSTRAINT `auctions_ibfk_4` FOREIGN KEY (`engine_type_id`) REFERENCES `engine_types` (`id`),
  ADD CONSTRAINT `auctions_ibfk_5` FOREIGN KEY (`fuel_id`) REFERENCES `fuels` (`id`),
  ADD CONSTRAINT `auctions_ibfk_6` FOREIGN KEY (`import_id`) REFERENCES `imports` (`id`),
  ADD CONSTRAINT `auctions_ibfk_7` FOREIGN KEY (`motion_vector_id`) REFERENCES `motion_vectors` (`id`),
  ADD CONSTRAINT `auctions_ibfk_8` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`),
  ADD CONSTRAINT `auctions_ibfk_9` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`);

--
-- Constraints for table `auction_files`
--
ALTER TABLE `auction_files`
  ADD CONSTRAINT `auction_files_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`);

--
-- Constraints for table `auction_images`
--
ALTER TABLE `auction_images`
  ADD CONSTRAINT `auction_images_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`);

--
-- Constraints for table `auction_other_specifications`
--
ALTER TABLE `auction_other_specifications`
  ADD CONSTRAINT `auction_other_specifications_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auction_other_specifications_ibfk_2` FOREIGN KEY (`other_specification_id`) REFERENCES `other_specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `car_color_languages`
--
ALTER TABLE `car_color_languages`
  ADD CONSTRAINT `car_color_languages_ibfk_1` FOREIGN KEY (`car_color_id`) REFERENCES `car_colors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `car_color_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `car_type_languages`
--
ALTER TABLE `car_type_languages`
  ADD CONSTRAINT `car_type_languages_ibfk_1` FOREIGN KEY (`car_type_id`) REFERENCES `car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `car_type_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_languages`
--
ALTER TABLE `category_languages`
  ADD CONSTRAINT `category_languages_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `city_languages`
--
ALTER TABLE `city_languages`
  ADD CONSTRAINT `city_languages_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `city_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `country_languages`
--
ALTER TABLE `country_languages`
  ADD CONSTRAINT `country_languages_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `country_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `engine_type_languages`
--
ALTER TABLE `engine_type_languages`
  ADD CONSTRAINT `engine_type_languages_ibfk_1` FOREIGN KEY (`engine_type_id`) REFERENCES `engine_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `engine_type_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faq_languages`
--
ALTER TABLE `faq_languages`
  ADD CONSTRAINT `faq_languages_ibfk_1` FOREIGN KEY (`faq_id`) REFERENCES `faqs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `faq_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fuel_languages`
--
ALTER TABLE `fuel_languages`
  ADD CONSTRAINT `fuel_languages_ibfk_1` FOREIGN KEY (`fuel_id`) REFERENCES `fuels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fuel_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `import_languages`
--
ALTER TABLE `import_languages`
  ADD CONSTRAINT `import_languages_ibfk_1` FOREIGN KEY (`import_id`) REFERENCES `imports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `import_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`city_id`,`country_id`) REFERENCES `cities` (`id`, `country_id`);

--
-- Constraints for table `motion_vector_languages`
--
ALTER TABLE `motion_vector_languages`
  ADD CONSTRAINT `motion_vector_languages_ibfk_1` FOREIGN KEY (`motion_vector_id`) REFERENCES `motion_vectors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `motion_vector_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `other_specification_languages`
--
ALTER TABLE `other_specification_languages`
  ADD CONSTRAINT `other_specification_languages_ibfk_1` FOREIGN KEY (`other_specification_id`) REFERENCES `other_specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `other_specification_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_languages`
--
ALTER TABLE `page_languages`
  ADD CONSTRAINT `page_languages_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `page_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `specification_languages`
--
ALTER TABLE `specification_languages`
  ADD CONSTRAINT `specification_languages_ibfk_1` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `specification_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `sub_category_languages`
--
ALTER TABLE `sub_category_languages`
  ADD CONSTRAINT `sub_category_languages_ibfk_1` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_category_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2018 at 09:49 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bidz`
--

-- --------------------------------------------------------

--
-- Table structure for table `ads`
--

CREATE TABLE `ads` (
  `id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads`
--

INSERT INTO `ads` (`id`, `is_active`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ads_languages`
--

CREATE TABLE `ads_languages` (
  `ads_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads_languages`
--

INSERT INTO `ads_languages` (`ads_id`, `language_id`, `title`, `link`, `image`) VALUES
(1, 1, '1', NULL, NULL),
(1, 2, '1', NULL, NULL),
(2, 1, '2', NULL, NULL),
(2, 2, '2', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ads_slider`
--

CREATE TABLE `ads_slider` (
  `id` int(11) NOT NULL,
  `auctionID` int(11) NOT NULL,
  `start_at` date DEFAULT NULL,
  `end_at` date DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ads_slider`
--

INSERT INTO `ads_slider` (`id`, `auctionID`, `start_at`, `end_at`, `image`, `is_active`, `created_at`) VALUES
(4, 32, '2018-05-26', '2018-05-31', '1527258983.jpg', 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `auctions`
--

CREATE TABLE `auctions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `sub_category_id` int(10) UNSIGNED DEFAULT NULL,
  `car_color_id` int(10) UNSIGNED DEFAULT NULL,
  `car_type_id` int(10) UNSIGNED DEFAULT NULL,
  `engine_type_id` int(10) UNSIGNED DEFAULT NULL,
  `fuel_id` int(10) UNSIGNED DEFAULT NULL,
  `import_id` int(10) UNSIGNED DEFAULT NULL,
  `motion_vector_id` int(10) UNSIGNED DEFAULT NULL,
  `specification_id` int(10) UNSIGNED DEFAULT NULL,
  `manufacturing_year` int(4) DEFAULT NULL,
  `engine_size` int(10) DEFAULT NULL,
  `vaild_form` tinyint(1) DEFAULT '1',
  `form_years` int(3) DEFAULT '0',
  `is_periodic_inspection` tinyint(1) DEFAULT '1',
  `mileage` varchar(100) DEFAULT NULL,
  `chassis_no` varchar(100) DEFAULT NULL,
  `bid_type` tinyint(1) DEFAULT '1',
  `minimum_bid` int(11) DEFAULT NULL,
  `bid_days` int(5) DEFAULT NULL,
  `total_bids` int(11) NOT NULL DEFAULT '0',
  `member_id` int(10) UNSIGNED DEFAULT NULL,
  `provider_id` int(10) UNSIGNED DEFAULT NULL,
  `provider_accept` tinyint(1) DEFAULT '0',
  `is_active` tinyint(15) DEFAULT '0',
  `start_at` datetime DEFAULT NULL,
  `car_price` int(11) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auctions`
--

INSERT INTO `auctions` (`id`, `title`, `description`, `category_id`, `sub_category_id`, `car_color_id`, `car_type_id`, `engine_type_id`, `fuel_id`, `import_id`, `motion_vector_id`, `specification_id`, `manufacturing_year`, `engine_size`, `vaild_form`, `form_years`, `is_periodic_inspection`, `mileage`, `chassis_no`, `bid_type`, `minimum_bid`, `bid_days`, `total_bids`, `member_id`, `provider_id`, `provider_accept`, `is_active`, `start_at`, `car_price`, `views`, `created_at`, `updated_at`) VALUES
(4, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 1, '2018-05-07 13:45:29', 3200000, 5, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(5, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(6, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(7, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(8, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(9, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(10, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(11, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(12, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(13, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(14, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(15, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(16, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(17, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(18, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(19, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(20, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(21, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(22, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(23, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(24, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(25, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(26, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(27, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(28, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 0, '2018-05-07 13:45:29', NULL, NULL, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(29, '34534', '53453453', 2, 2, 2, 2, 1, 2, 2, 1, 1, 2004, 34534, 1, 1, 1, '43545', '345345', 1, 300, 14, 0, 12, 12, 1, 1, '2018-05-07 13:45:29', NULL, 1, '2018-05-07 11:45:29', '2018-05-07 11:45:29'),
(30, '5345345345', 'ثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضصثثضصثضصثضصثثضصثض  صثضصثثضصثضصثضص ثثضصثضصثضصثثضصث ضصثضصثثضصث ضصثضص', 5, 5, 3, 1, 1, 2, 1, 2, 1, 2002, 454, 1, 3, 1, '5454', '435345345', 2, 1600, 4, 0, 11, 12, 1, 1, '2018-05-09 04:37:09', NULL, 2, '2018-05-09 00:19:53', '2018-05-09 00:19:53'),
(31, 'Lorem Ipsum', 'sacasacasc', 1, 1, 1, 1, 1, 1, 1, 1, 1, 2002, 1600, 1, 2, 1, '1500', '123456', 1, 1400, 12, 0, 12, 12, 1, 1, '2018-05-25 16:10:31', NULL, 1, '2018-05-25 14:10:31', '2018-05-25 14:10:31'),
(32, 'aa', 'wqqwewrwrweewrew', 2, 11, 1, 1, 1, 1, 1, 1, 1, 2018, 324234, 1, 1, 0, '1500', '3423423423423', 1, 100, 12, 0, 12, 12, 1, 1, '2018-05-25 21:26:32', NULL, 2, '2018-05-25 19:26:32', '2018-05-25 19:26:32'),
(33, 'سيات ابيزا 2ف22ي2ا112', 'تجربه اضافة مزايده', 1, 1, 1, 1, 1, 1, 1, 1, 1, 2006, 222, 1, 2, 1, '222', '222', 1, 1300, 1, 0, 12, 12, 1, 1, '2018-05-25 21:36:38', NULL, 30, '2018-05-25 19:36:38', '2018-05-25 19:36:38');

-- --------------------------------------------------------

--
-- Table structure for table `auction_files`
--

CREATE TABLE `auction_files` (
  `auction_id` int(11) UNSIGNED NOT NULL,
  `order` int(5) NOT NULL,
  `file` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auction_files`
--

INSERT INTO `auction_files` (`auction_id`, `order`, `file`) VALUES
(33, 0, '2229694028a3957c6d1e2c94b254dd57.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `auction_images`
--

CREATE TABLE `auction_images` (
  `auction_id` int(11) UNSIGNED NOT NULL,
  `order` int(5) NOT NULL,
  `image` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auction_images`
--

INSERT INTO `auction_images` (`auction_id`, `order`, `image`) VALUES
(33, 0, 'd3bce2c856a9ff7ba059dd89331b957a.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `auction_other_specifications`
--

CREATE TABLE `auction_other_specifications` (
  `auction_id` int(10) UNSIGNED NOT NULL,
  `other_specification_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auction_other_specifications`
--

INSERT INTO `auction_other_specifications` (`auction_id`, `other_specification_id`) VALUES
(4, 30),
(30, 1),
(30, 25),
(30, 30);

-- --------------------------------------------------------

--
-- Table structure for table `bank_transaction`
--

CREATE TABLE `bank_transaction` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `date` date NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `value` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `auctionID` int(11) NOT NULL DEFAULT '0',
  `duration` varchar(255) DEFAULT NULL,
  `create_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `car_colors`
--

CREATE TABLE `car_colors` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_colors`
--

INSERT INTO `car_colors` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL),
(4, 1, NULL, NULL),
(5, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_color_languages`
--

CREATE TABLE `car_color_languages` (
  `car_color_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_color_languages`
--

INSERT INTO `car_color_languages` (`car_color_id`, `language_id`, `title`) VALUES
(1, 1, 'احمر'),
(1, 2, 'احمر'),
(2, 1, 'اخضر'),
(2, 2, 'اخضر'),
(3, 1, 'نبيتى'),
(3, 2, 'نبيتى'),
(4, 1, 'اسود '),
(4, 2, 'اسود '),
(5, 1, 'برتقالى'),
(5, 2, 'برتقالى');

-- --------------------------------------------------------

--
-- Table structure for table `car_types`
--

CREATE TABLE `car_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_types`
--

INSERT INTO `car_types` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_type_languages`
--

CREATE TABLE `car_type_languages` (
  `car_type_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_type_languages`
--

INSERT INTO `car_type_languages` (`car_type_id`, `language_id`, `title`) VALUES
(1, 1, 'سيدان'),
(1, 2, 'سيدان'),
(2, 1, 'هاتش باك'),
(2, 2, 'هاتش باك');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-04-30 06:07:19', '2018-04-30 06:07:22'),
(2, 1, '2018-04-30 06:07:19', NULL),
(3, 1, '2018-04-30 06:07:19', NULL),
(4, 1, '2018-04-30 06:07:19', NULL),
(5, 1, '2018-04-30 06:07:19', NULL),
(6, 1, '2018-04-30 06:07:19', NULL),
(7, 1, '2018-04-30 06:07:19', NULL),
(8, 1, '2018-04-30 06:07:19', NULL),
(9, 1, '2018-04-30 06:07:19', NULL),
(10, 1, '2018-04-30 06:07:19', NULL),
(11, 1, '2018-04-30 06:07:19', NULL),
(12, 1, '2018-04-30 06:07:19', NULL),
(13, 1, '2018-04-30 06:07:19', NULL),
(14, 1, '2018-04-30 06:07:19', NULL),
(15, 1, '2018-04-30 06:07:19', NULL),
(16, 1, '2018-04-30 06:07:19', NULL),
(17, 1, '2018-04-30 06:07:19', NULL),
(18, 1, '2018-04-30 06:07:19', NULL),
(19, 1, '2018-04-30 06:07:19', NULL),
(20, 1, '2018-04-30 06:07:19', NULL),
(21, 1, '2018-04-30 06:07:19', NULL),
(22, 1, '2018-04-30 06:07:19', NULL),
(23, 1, '2018-04-30 06:07:19', NULL),
(24, 1, '2018-04-30 06:07:19', NULL),
(25, 1, '2018-04-30 06:07:19', NULL),
(26, 1, '2018-04-30 06:07:19', NULL),
(27, 1, NULL, NULL),
(28, 1, '2018-04-30 06:07:19', NULL),
(29, 1, '2018-04-30 06:07:19', NULL),
(30, 1, '2018-04-30 06:07:19', NULL),
(31, 1, '2018-04-30 06:07:19', NULL),
(32, 1, '2018-04-30 06:07:19', NULL),
(33, 1, '2018-04-30 06:07:19', NULL),
(34, 1, '2018-04-30 06:07:19', NULL),
(35, 1, '2018-04-30 06:07:19', NULL),
(36, 1, '2018-04-30 06:07:19', NULL),
(37, 1, '2018-04-30 06:07:19', NULL),
(38, 1, '2018-04-30 06:07:19', NULL),
(39, 1, '2018-04-30 06:07:19', NULL),
(40, 1, '2018-04-30 06:07:19', NULL),
(41, 1, '2018-04-30 06:07:19', NULL),
(42, 1, '2018-04-30 06:07:19', NULL),
(43, 1, '2018-04-30 06:07:19', NULL),
(44, 1, '2018-04-30 06:07:19', NULL),
(45, 1, '2018-04-30 06:07:19', NULL),
(46, 1, '2018-04-30 06:07:19', NULL),
(47, 1, '2018-04-30 06:07:19', NULL),
(48, 1, '2018-04-30 06:07:19', NULL),
(49, 1, '2018-04-30 06:07:19', NULL),
(50, 1, '2018-04-30 06:07:19', NULL),
(51, 1, '2018-04-30 06:07:19', NULL),
(52, 1, '2018-04-30 06:07:19', NULL),
(53, 1, '2018-04-30 06:07:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_languages`
--

CREATE TABLE `category_languages` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_languages`
--

INSERT INTO `category_languages` (`category_id`, `language_id`, `title`) VALUES
(1, 1, 'تويوتا'),
(1, 2, 'Toyota'),
(2, 1, 'شيفروليه'),
(2, 2, 'Chevrolet'),
(3, 1, 'نيسان'),
(3, 2, 'Nissan'),
(4, 1, 'فورد'),
(4, 2, 'Ford'),
(5, 1, 'مرسيدس'),
(5, 2, 'Mercedes'),
(6, 1, 'جي ام سي'),
(6, 2, 'GMC'),
(7, 1, 'بي ام دبليو'),
(7, 2, 'BMW'),
(8, 1, 'لكزس'),
(8, 2, 'Lexus'),
(9, 1, 'جيب'),
(9, 2, 'Jeep'),
(10, 1, 'هونداي'),
(10, 2, 'Hundayi'),
(11, 1, 'هوندا'),
(11, 2, 'Honda'),
(12, 1, 'همر'),
(12, 2, 'Hummer'),
(13, 1, 'انفنيتي'),
(13, 2, 'Infinity'),
(14, 1, 'لاند روفر'),
(14, 2, 'Land rover'),
(15, 1, 'مازدا'),
(15, 2, 'Mazda'),
(16, 1, 'ميركوري'),
(16, 2, 'Mercury'),
(17, 1, 'فولكس واجن'),
(17, 2, 'Volex'),
(18, 1, 'ميتسوبيشي'),
(18, 2, 'Mitsubishi'),
(19, 1, 'لنكولن'),
(19, 2, 'Lincoln'),
(20, 1, 'اوبل'),
(20, 2, 'Opel'),
(21, 1, 'ايسوزو'),
(21, 2, 'Isuzu'),
(22, 1, 'بورش'),
(22, 2, 'Borsh'),
(23, 1, 'كيا'),
(23, 2, 'Kia'),
(24, 1, 'مازيراتي'),
(24, 2, 'Maserati'),
(25, 1, 'بنتلي'),
(25, 2, 'Bentley'),
(26, 1, 'استون مارتن'),
(26, 2, 'Aston Martin'),
(27, 1, 'كاديلاك'),
(27, 2, 'Cadillac'),
(28, 1, 'كرايزلر'),
(28, 2, 'Chrysler'),
(29, 1, 'سيتروين'),
(29, 2, 'Citroën'),
(30, 1, 'دايو'),
(30, 2, 'Daewoo'),
(31, 1, 'ديهاتسو'),
(31, 2, 'Daihatsu'),
(32, 1, 'دودج'),
(32, 2, 'Dodge'),
(33, 1, 'فيراري'),
(33, 2, 'Ferrari '),
(34, 1, 'فيات'),
(34, 2, 'Fiat'),
(35, 1, 'جاكوار'),
(35, 2, 'Jaguar'),
(36, 1, 'لامبورجيني'),
(36, 2, 'Lamborghini'),
(37, 1, 'رولز رويس'),
(37, 2, 'Rolls-Royce'),
(38, 1, 'بيجو'),
(38, 2, 'Peugeot'),
(39, 1, 'سوبارو'),
(39, 2, 'Subaru'),
(40, 1, 'سوزوكي'),
(40, 2, 'Suzuki'),
(41, 1, 'فولفو'),
(41, 2, 'Volvo'),
(42, 1, 'سكودا'),
(42, 2, 'ŠKODA'),
(43, 1, 'اودي'),
(43, 2, 'Audi'),
(44, 1, 'رينو'),
(44, 2, 'Renault'),
(45, 1, 'بيوك'),
(45, 2, 'Buick'),
(46, 1, 'ساب'),
(46, 2, 'SAAB'),
(47, 1, 'سيات'),
(47, 2, 'Seat'),
(48, 1, 'MG'),
(48, 2, 'MG'),
(49, 1, 'بروتون'),
(49, 2, 'proton'),
(50, 1, 'سانج يونج'),
(50, 2, 'Sangyong'),
(51, 1, 'تشيري'),
(51, 2, 'Chery'),
(52, 1, 'جيلي'),
(52, 2, 'Gelly'),
(53, 1, 'ZXAUTO'),
(53, 2, 'ZXAUTO');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `is_active`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-05-03 17:01:37', NULL),
(2, 1, 1, '2018-05-03 17:01:37', NULL),
(3, 1, 1, '2018-05-03 17:01:37', NULL),
(4, 1, 1, '2018-05-03 17:01:37', NULL),
(5, 1, 1, '2018-05-03 17:01:37', NULL),
(6, 1, 2, '2018-05-03 17:01:37', NULL),
(7, 1, 2, '2018-05-03 17:01:37', NULL),
(8, 1, 2, '2018-05-03 17:01:37', NULL),
(9, 1, 2, '2018-05-03 17:01:37', NULL),
(10, 1, 2, '2018-05-03 17:01:37', NULL),
(11, 1, 2, '2018-05-03 17:01:37', NULL),
(12, 1, 2, '2018-05-03 17:01:37', NULL),
(13, 1, 2, '2018-05-03 17:01:37', NULL),
(14, 1, 2, '2018-05-03 17:01:37', NULL),
(15, 1, 2, '2018-05-03 17:01:37', NULL),
(16, 1, 2, '2018-05-03 17:01:37', NULL),
(17, 1, 2, '2018-05-03 17:01:37', NULL),
(18, 1, 2, '2018-05-03 17:01:37', NULL),
(19, 1, 2, '2018-05-03 17:01:37', NULL),
(20, 1, 2, '2018-05-03 17:01:37', NULL),
(21, 1, 2, '2018-05-03 17:01:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city_languages`
--

CREATE TABLE `city_languages` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city_languages`
--

INSERT INTO `city_languages` (`city_id`, `language_id`, `title`) VALUES
(1, 1, 'city c1'),
(1, 2, 'city c1'),
(2, 1, 'city c1'),
(2, 2, 'city c1'),
(3, 1, 'city c 1'),
(3, 2, 'city c 1'),
(4, 1, 'city c1'),
(4, 2, 'city c1'),
(5, 1, 'city c1'),
(5, 2, 'city c1'),
(6, 1, 'الرياض'),
(6, 2, 'Riyadh'),
(7, 1, 'المدينة'),
(7, 2, 'Maddinah'),
(8, 1, 'جدة'),
(8, 2, 'Geddah'),
(9, 1, 'ابها'),
(9, 2, 'Abha'),
(10, 1, 'الشرقية'),
(10, 2, NULL),
(11, 1, 'ينبع'),
(11, 2, NULL),
(12, 1, 'حفر الباطن'),
(12, 2, NULL),
(13, 1, 'الطائف'),
(13, 2, NULL),
(14, 1, 'تبوك'),
(14, 2, NULL),
(15, 1, 'القصيم'),
(15, 2, NULL),
(16, 1, 'حائل'),
(16, 2, NULL),
(17, 1, 'الباحة'),
(17, 2, NULL),
(18, 1, 'جيزان'),
(18, 2, NULL),
(19, 1, 'نجران'),
(19, 2, NULL),
(20, 1, 'الجوف'),
(20, 2, NULL),
(21, 1, 'عرعر'),
(21, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contact_messages`
--

CREATE TABLE `contact_messages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` text,
  `create_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 0, '2018-05-03 17:00:31', NULL),
(2, 1, '2018-05-03 17:00:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `country_languages`
--

CREATE TABLE `country_languages` (
  `country_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country_languages`
--

INSERT INTO `country_languages` (`country_id`, `language_id`, `title`) VALUES
(1, 1, 'مصر'),
(1, 2, 'Egypt'),
(2, 1, 'السعودية'),
(2, 2, 'Sa');

-- --------------------------------------------------------

--
-- Table structure for table `engine_types`
--

CREATE TABLE `engine_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `engine_types`
--

INSERT INTO `engine_types` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `engine_type_languages`
--

CREATE TABLE `engine_type_languages` (
  `engine_type_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `engine_type_languages`
--

INSERT INTO `engine_type_languages` (`engine_type_id`, `language_id`, `title`) VALUES
(1, 1, 'عادى'),
(1, 2, 'عادى'),
(2, 1, 'تيربو'),
(2, 2, 'تيربو');

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(3, 1, '2018-01-29 23:32:41', '2018-04-30 02:08:56'),
(4, 1, '2018-01-30 00:07:20', '2018-04-30 02:08:55'),
(5, 1, '2018-01-30 00:07:27', '2018-04-30 02:08:53'),
(6, 1, '2018-01-30 00:07:42', '2018-04-30 02:08:57');

-- --------------------------------------------------------

--
-- Table structure for table `faq_languages`
--

CREATE TABLE `faq_languages` (
  `faq_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `faq_languages`
--

INSERT INTO `faq_languages` (`faq_id`, `language_id`, `title`, `desc`) VALUES
(3, 1, 'd', 'vxcvxcvxcvxvvv'),
(3, 2, 'ccccccccccccccc1', 'ccccccccccccccccccccccccccccccccccccc1'),
(4, 1, 'vxcvxcv', 'xcvxcvxcv'),
(4, 2, 'ewrwerwe', 'rwerwe'),
(5, 1, 'dasdasd', 'asdasdas'),
(5, 2, 'dasdasd', 'adasd'),
(6, 1, 'sdfsdfsdf', 'sdfsdf'),
(6, 2, 'sdfsdfsd', 'fsdfsdfsd');

-- --------------------------------------------------------

--
-- Table structure for table `follow_auction`
--

CREATE TABLE `follow_auction` (
  `id` int(10) UNSIGNED NOT NULL,
  `auction_id` tinyint(1) DEFAULT NULL,
  `user_id` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `follow_auction`
--

INSERT INTO `follow_auction` (`id`, `auction_id`, `user_id`, `created_at`, `updated_at`) VALUES
(5, 33, 15, NULL, NULL),
(11, 32, 15, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fuels`
--

CREATE TABLE `fuels` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fuels`
--

INSERT INTO `fuels` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fuel_languages`
--

CREATE TABLE `fuel_languages` (
  `fuel_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fuel_languages`
--

INSERT INTO `fuel_languages` (`fuel_id`, `language_id`, `title`) VALUES
(1, 1, 'جاز'),
(1, 2, 'جاز'),
(2, 1, 'بترول'),
(2, 2, 'بترول'),
(3, 1, 'غاز'),
(3, 2, 'غاز');

-- --------------------------------------------------------

--
-- Table structure for table `imports`
--

CREATE TABLE `imports` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `imports`
--

INSERT INTO `imports` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `import_languages`
--

CREATE TABLE `import_languages` (
  `import_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `import_languages`
--

INSERT INTO `import_languages` (`import_id`, `language_id`, `title`) VALUES
(1, 1, 'خليجى'),
(1, 2, 'خليجى'),
(2, 1, 'سعودى'),
(2, 2, 'سعودى');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `iso_code` varchar(3) DEFAULT NULL,
  `text` varchar(50) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  `direction` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `iso_code`, `text`, `is_default`, `direction`) VALUES
(1, 'ar', 'اللغة العربية', 1, 'rtl'),
(2, 'en', 'English', 0, 'ltr');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `membership_type` tinyint(1) DEFAULT '1',
  `remember_token` varchar(100) DEFAULT NULL,
  `fcm_token` varchar(255) DEFAULT NULL,
  `authorization` varchar(255) DEFAULT NULL,
  `device_id` varchar(20) DEFAULT NULL,
  `is_active_phone` tinyint(1) DEFAULT '0',
  `is_active_email` tinyint(1) DEFAULT '1',
  `phone_act_code` varchar(8) DEFAULT NULL,
  `email_act_code` varchar(30) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  `file` varchar(50) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `market_image` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `first_name`, `last_name`, `email`, `password`, `city_id`, `country_id`, `phone_number`, `address`, `membership_type`, `remember_token`, `fcm_token`, `authorization`, `device_id`, `is_active_phone`, `is_active_email`, `phone_act_code`, `email_act_code`, `is_active`, `file`, `balance`, `market_image`, `created_at`, `updated_at`) VALUES
(11, 'اسلام', 'قنديل', 'eslam1@php-eg.com', '$2y$10$ralsdFwv0HRbhc70RABT/.9Od30BQhuYHyFd.LAfyAfsg8M1uMeca', 3, 1, '01110633087', 'ميت عمر دقهلية', 1, '1xUJkATk4VetyoozwMNxUHYdw0jj9q9iRCtA6VJW9xmKLZn4Kwg1Ec4QdCeY', NULL, NULL, NULL, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, '2018-05-04 02:06:09', '2018-05-04 03:22:28'),
(12, 'اسلام', 'Ahmed', 'eslam@php-eg.com', '$2y$10$ralsdFwv0HRbhc70RABT/.9Od30BQhuYHyFd.LAfyAfsg8M1uMeca', 5, 1, '01110633084', 'sadasd', 2, 'BJAnWS7Mg3L9JUCOorx3ejnAPrUTvTQwv8wpD4ZVvCtljYa2Z6Fh53fj4iOI', NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 'c7b326b492fd9c12f78acd9182d45430.png', NULL, NULL, '2018-05-04 03:25:21', '2018-05-04 05:54:41'),
(13, 'weqeqwe', 'qweqweqwe', 'a@a.com', '$2y$10$vS1v5TKNlKYTw8phP.MzLerrIkSeq1LQ3VumuRUE4G3t.zTAMqXeC', 6, 2, '23123123', 'dadasdasd', 2, NULL, NULL, NULL, NULL, 0, 0, '8339', 'vA7niMeF9tUCaMVOM0QdZ1b63fLvfd', 0, NULL, NULL, '1527272026.jpg', '2018-05-25 18:13:46', '2018-05-25 18:13:46'),
(15, 'اسلام', 'Ahmed', 'eslam@phpz-eg.com', '$2y$10$ralsdFwv0HRbhc70RABT/.9Od30BQhuYHyFd.LAfyAfsg8M1uMeca', 5, 1, '011106330844', 'sadasd', 2, '4DKFycTLaEME5pl7Pf5FojC01KJfnX6DGBa88A5AleeNM3q3xcOrN2sNlWFJ', NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 'c7b326b492fd9c12f78acd9182d45430.png', NULL, NULL, '2018-05-04 03:25:21', '2018-05-04 05:54:41'),
(16, 'عبدالرحمن', 'ابوشادي', 's@s.com', '$2y$10$HT.Ua0hcmYH26CUPdKsXgu6F8VZSxG9fv.QPb5xmNWxziIXPOksTu', 6, 2, '1233231232', 'qeqweqweqweqwe', 2, NULL, NULL, NULL, NULL, 0, 0, '1728', '8X8KugNVCG0keAmaGfsQUHNPncTMZD', 0, NULL, NULL, '1527342306.jpg', '2018-05-26 13:45:06', '2018-05-26 13:45:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2016_09_04_000000_create_roles_table', 2),
(7, '2016_09_04_100000_create_role_user_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `motion_vectors`
--

CREATE TABLE `motion_vectors` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `motion_vectors`
--

INSERT INTO `motion_vectors` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `motion_vector_languages`
--

CREATE TABLE `motion_vector_languages` (
  `motion_vector_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `motion_vector_languages`
--

INSERT INTO `motion_vector_languages` (`motion_vector_id`, `language_id`, `title`) VALUES
(1, 1, 'عادى'),
(1, 2, 'عادى'),
(2, 1, 'اتوماتك'),
(2, 2, 'اتوماتك');

-- --------------------------------------------------------

--
-- Table structure for table `other_specifications`
--

CREATE TABLE `other_specifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_specifications`
--

INSERT INTO `other_specifications` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL),
(3, 1, NULL, NULL),
(4, 1, NULL, NULL),
(5, 1, NULL, NULL),
(6, 1, NULL, NULL),
(7, 1, NULL, NULL),
(8, 1, NULL, NULL),
(9, 1, NULL, NULL),
(10, 1, NULL, NULL),
(11, 1, NULL, NULL),
(12, 1, NULL, NULL),
(13, 1, NULL, NULL),
(14, 1, NULL, NULL),
(15, 1, NULL, NULL),
(16, 1, NULL, NULL),
(17, 1, NULL, NULL),
(18, 1, NULL, NULL),
(19, 1, NULL, NULL),
(20, 1, NULL, NULL),
(21, 1, NULL, NULL),
(22, 1, NULL, NULL),
(23, 1, NULL, NULL),
(24, 1, NULL, NULL),
(25, 1, NULL, NULL),
(26, 1, NULL, NULL),
(27, 1, NULL, NULL),
(28, 1, NULL, NULL),
(29, 1, NULL, NULL),
(30, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `other_specification_languages`
--

CREATE TABLE `other_specification_languages` (
  `other_specification_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_specification_languages`
--

INSERT INTO `other_specification_languages` (`other_specification_id`, `language_id`, `title`) VALUES
(1, 1, 'وسائد هوائية للسائق'),
(1, 2, 'وسائد هوائية للسائق'),
(2, 1, 'وسائد هوائية للراكب'),
(2, 2, 'وسائد هوائية للراكب'),
(3, 1, 'كراسي كهربائية'),
(3, 2, 'كراسي كهربائية'),
(4, 1, 'نظام فرامل ABS'),
(4, 2, 'نظام فرامل ABS'),
(5, 1, 'توزيع اليكتروني للفرامل EBD'),
(5, 2, 'توزيع اليكتروني للفرامل EBD'),
(6, 1, 'برنامج التوازن الإلكتروني ESP'),
(6, 2, 'برنامج التوازن الإلكتروني ESP'),
(7, 1, 'نظام تنبية ضد السرقة'),
(7, 2, 'نظام تنبية ضد السرقة'),
(8, 1, 'نظام إيموبليزر ضد السرقة'),
(8, 2, 'نظام إيموبليزر ضد السرقة'),
(9, 1, 'جنوط رياضية'),
(9, 2, 'جنوط رياضية'),
(10, 1, 'فوانيس ضباب امامية'),
(10, 2, 'فوانيس ضباب امامية'),
(11, 1, 'مرايات جانبية كهربائية'),
(11, 2, 'مرايات جانبية كهربائية'),
(12, 1, 'طي المرايات الجانبية كهربائيا'),
(12, 2, 'طي المرايات الجانبية كهربائيا'),
(13, 1, 'إضاءة المصابيح زنون'),
(13, 2, 'إضاءة المصابيح زنون'),
(14, 1, 'إضاءة المصابيح الأمامية LED'),
(14, 2, 'إضاءة المصابيح الأمامية LED'),
(15, 1, 'إضاءة المصابيح الخلفية LED'),
(15, 2, 'إضاءة المصابيح الخلفية LED'),
(16, 1, 'النظام الذكى لركن السيارة'),
(16, 2, 'النظام الذكى لركن السيارة'),
(17, 1, 'النظام الصوتي'),
(17, 2, 'النظام الصوتي'),
(18, 1, 'مدخل AUX'),
(18, 2, 'مدخل AUX'),
(19, 1, 'مدخل USB'),
(19, 2, 'مدخل USB'),
(20, 1, 'بلوتوث'),
(20, 2, 'بلوتوث'),
(21, 1, 'زجاج كهربائي'),
(21, 2, 'زجاج كهربائي'),
(22, 1, 'تحكم عن بعد في غلق وفتح الأبواب'),
(22, 2, 'تحكم عن بعد في غلق وفتح الأبواب'),
(23, 1, 'فرش جلد'),
(23, 2, 'فرش جلد'),
(24, 1, 'المفتاح الذكي'),
(24, 2, 'المفتاح الذكي'),
(25, 1, 'كاميرا خلفية'),
(25, 2, 'كاميرا خلفية'),
(26, 1, 'كمبيوتر رحلات'),
(26, 2, 'كمبيوتر رحلات'),
(27, 1, 'تحكم في نظام الصوت من عجلة القيادة'),
(27, 2, 'تحكم في نظام الصوت من عجلة القيادة'),
(28, 1, 'مثبت سرعة'),
(28, 2, 'مثبت سرعة'),
(29, 1, 'إمكانية طى ظهر مقاعد الخلفية'),
(29, 2, 'إمكانية طى ظهر مقاعد الخلفية'),
(30, 1, 'تكييف'),
(30, 2, 'تكييف');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(5, 1, '2018-05-25 17:13:36', '2018-05-25 17:23:38'),
(6, 1, '2018-05-25 17:23:26', '2018-05-25 17:24:05'),
(7, 1, '2018-05-25 17:52:35', '2018-05-25 17:52:35');

-- --------------------------------------------------------

--
-- Table structure for table `page_languages`
--

CREATE TABLE `page_languages` (
  `page_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `desc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `page_languages`
--

INSERT INTO `page_languages` (`page_id`, `language_id`, `title`, `desc`) VALUES
(5, 1, 'عن المزادات', '<p>ذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، e 3e33e3eاء لغوية، مولد النص العربى</p>'),
(5, 2, 'About saudi bidz', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>'),
(6, 1, 'الشروط والأحكام', '<p>إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، e 3e33e3eاء لغوية، مولد النص العربى</p>'),
(6, 2, 'Terms and conditions', '<p>The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.The advantage of its Latin origin and the relative meaninglessness of Lorum Ipsum is that the text does not attract attention to itself or distract the viewer&#39;s attention from the layout.The advantage of its Latin origin and the relative.</p>'),
(7, 1, 'شرح استخدام الموقع', '<p><strong>شرح استخدام الموقع</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>شرح استخدام الموقع&nbsp;شرح استخدام الموقع&nbsp;شرح استخدام الموقعشرح استخدام الموقع&nbsp;شرح استخدام الموقع&nbsp;شرح استخدام الموقع&nbsp;</p>'),
(7, 2, 'How Site works', '<p><strong>How Site works</strong></p>\r\n\r\n<p>How Site works How Site works Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;&nbsp;Sire Terms&nbsp;</p>');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Mohamed Hamdy', 'logo.png', '2018-05-26 12:36:37', '2018-05-26 12:36:37'),
(3, 'Mohamed Hamdy', 'logo.png', '2018-05-26 12:36:37', '2018-05-26 12:36:37'),
(4, 'Mohamed Hamdy', 'logo.png', '2018-05-26 12:36:37', '2018-05-26 12:36:37'),
(5, 'Mohamed Hamdy', 'logo.png', '2018-05-26 12:36:37', '2018-05-26 12:36:37'),
(6, 'Mohamed Hamdy', 'logo.png', '2018-05-26 12:36:37', '2018-05-26 12:36:37'),
(7, 'Mohamed Hamdy', 'logo.png', '2018-05-26 12:36:37', '2018-05-26 12:36:37');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('eslam@php-eg.com', '$2y$10$QckkbORcNLQEndBsvEc6A.HxGTWyS924W3x7KD6Cd6.yJfm.JLTna', '2018-01-27 21:25:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `group` varchar(255) NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `group`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'admin', 'default', NULL, NULL),
(2, 'masjed haram moshref', 'masjed_haram', 'masjed haram moshref', 'default', NULL, NULL),
(3, 'masjed_nabawy', 'masjed_nabawy', 'masjed_nabawy', 'default', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 1, 1, NULL, NULL),
(4, 1, 4, '2018-02-01 12:36:41', '2018-02-01 12:36:41'),
(7, 2, 5, '2018-02-01 12:39:10', '2018-02-01 12:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `is_serialize` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `key`, `value`, `is_serialize`) VALUES
(1, 'About Arabic site', 'about_site_ar', 'إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، e 3e33e3eاء لغوية، مولد النص العربى', 0),
(2, 'Ads code pop code', 'ads_code_pop_code', '', 0),
(3, 'Amount of insurance', 'amount_of_insurance', '', 0),
(4, 'App url', 'app_url', '', 0),
(5, 'Contact us', 'contcat_us', 'dfg', 0),
(6, 'Facebook', 'facebook', 'http://www.youtube.com', 0),
(7, 'Instagram', 'instagram', 'http://www.youtube.com', 0),
(8, 'Pop header js', 'pop_header_js', '', 0),
(9, 'Site Description', 'site_meta_desc', 'وصف ميتا تاج', 0),
(10, 'Site meta tag', 'site_meta_tag', 'setting.site_meta_tag', 0),
(11, 'Site name', 'site_name', 'RIDZ SB', 0),
(12, 'Twitter', 'twitter', 'http://www.youtube.com', 0),
(13, 'Youtube', 'youtube', 'http://www.youtube.com', 0),
(14, 'IOS link', 'ios_link', 'http://www.youtube.com', 0),
(15, 'Android link', 'android_link', 'http://www.youtube.com', 0),
(16, 'Google plus', 'google_plus', 'http://www.google.com', 0),
(17, 'About English site', 'about_site_en', 'wdadasdw wwwqeqwewqe ewwrewrewrwewdadasdw wwwqeqwewqe ewwrewrewrwewdadasdw wwwqeqwewqe ewwrewrewrwewdadasdw wwwqeqwewqe ewwrewrewrwewdadasdw wwwqeqwewqe ewwrewrewrwewdadasdw wwwqeqwewqe ewwrewrewrwe', 0);

-- --------------------------------------------------------

--
-- Table structure for table `specifications`
--

CREATE TABLE `specifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specifications`
--

INSERT INTO `specifications` (`id`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `specification_languages`
--

CREATE TABLE `specification_languages` (
  `specification_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specification_languages`
--

INSERT INTO `specification_languages` (`specification_id`, `language_id`, `title`) VALUES
(1, 1, 'فل'),
(1, 2, 'فل'),
(2, 1, 'ستاندر'),
(2, 2, 'ستاندر');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `is_active`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(2, 1, 2, NULL, NULL),
(3, 1, 3, NULL, NULL),
(4, 1, 4, NULL, NULL),
(5, 1, 5, NULL, NULL),
(6, 1, 6, NULL, NULL),
(7, 1, 7, NULL, NULL),
(8, 1, 8, NULL, NULL),
(9, 1, 9, NULL, NULL),
(10, 1, 1, NULL, NULL),
(11, 1, 2, NULL, NULL),
(12, 1, 3, NULL, NULL),
(13, 1, 4, NULL, NULL),
(14, 1, 5, NULL, NULL),
(15, 1, 6, NULL, NULL),
(16, 1, 7, NULL, NULL),
(17, 1, 8, NULL, NULL),
(18, 1, 9, NULL, NULL),
(19, 1, 1, NULL, NULL),
(20, 1, 2, NULL, NULL),
(21, 1, 3, NULL, NULL),
(22, 1, 4, NULL, NULL),
(23, 1, 5, NULL, NULL),
(24, 1, 6, NULL, NULL),
(25, 1, 7, NULL, NULL),
(26, 1, 8, NULL, NULL),
(27, 1, 9, NULL, NULL),
(28, 1, 1, NULL, NULL),
(29, 1, 2, NULL, NULL),
(30, 1, 3, NULL, NULL),
(31, 1, 4, NULL, NULL),
(32, 1, 5, NULL, NULL),
(33, 1, 6, NULL, NULL),
(34, 1, 7, NULL, NULL),
(35, 1, 8, NULL, NULL),
(36, 1, 9, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category_languages`
--

CREATE TABLE `sub_category_languages` (
  `sub_category_id` int(10) UNSIGNED NOT NULL,
  `language_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category_languages`
--

INSERT INTO `sub_category_languages` (`sub_category_id`, `language_id`, `title`) VALUES
(1, 1, 'كرورلا'),
(1, 2, 'Corrola'),
(2, 1, 'يسشيششسش'),
(2, 2, 'dfajkfjdkfjs'),
(3, 1, 'يسشيششسش'),
(3, 2, 'dfajkfjdkfjs'),
(4, 1, 'يسشيششسش'),
(4, 2, 'dfajkfjdkfjs'),
(5, 1, 'يسشيششسش'),
(5, 2, 'dfajkfjdkfjs'),
(6, 1, 'يسشيششسش'),
(6, 2, 'dfajkfjdkfjs'),
(7, 1, 'يسشيششسش'),
(7, 2, 'dfajkfjdkfjs'),
(8, 1, 'يسشيششسش'),
(8, 2, 'dfajkfjdkfjs'),
(9, 1, 'يسشيششسش'),
(9, 2, 'dfajkfjdkfjs'),
(10, 1, 'يسشيششسش'),
(10, 2, 'dfajkfjdkfjs'),
(11, 1, 'يسشيششسش'),
(11, 2, 'dfajkfjdkfjs'),
(12, 1, 'يسشيششسش'),
(12, 2, 'dfajkfjdkfjs'),
(13, 1, 'يسشيششسش'),
(13, 2, 'dfajkfjdkfjs'),
(14, 1, 'يسشيششسش'),
(14, 2, 'dfajkfjdkfjs'),
(15, 1, 'يسشيششسش'),
(15, 2, 'dfajkfjdkfjs'),
(16, 1, 'يسشيششسش'),
(16, 2, 'dfajkfjdkfjs'),
(17, 1, 'يسشيششسش'),
(17, 2, 'dfajkfjdkfjs'),
(18, 1, 'يسشيششسش'),
(18, 2, 'dfajkfjdkfjs'),
(19, 1, 'يسشيششسش'),
(19, 2, 'dfajkfjdkfjs'),
(20, 1, 'يسشيششسش'),
(20, 2, 'dfajkfjdkfjs'),
(21, 1, 'يسشيششسش'),
(21, 2, 'dfajkfjdkfjs'),
(22, 1, 'يسشيششسش'),
(22, 2, 'dfajkfjdkfjs'),
(23, 1, 'يسشيششسش'),
(23, 2, 'dfajkfjdkfjs'),
(24, 1, 'يسشيششسش'),
(24, 2, 'dfajkfjdkfjs'),
(25, 1, 'يسشيششسش'),
(25, 2, 'dfajkfjdkfjs'),
(26, 1, 'يسشيششسش'),
(26, 2, 'dfajkfjdkfjs'),
(27, 1, 'يسشيششسش'),
(27, 2, 'dfajkfjdkfjs'),
(28, 1, 'يسشيششسش'),
(28, 2, 'dfajkfjdkfjs'),
(29, 1, 'يسشيششسش'),
(29, 2, 'dfajkfjdkfjs'),
(30, 1, 'يسشيششسش'),
(30, 2, 'dfajkfjdkfjs'),
(31, 1, 'يسشيششسش'),
(31, 2, 'dfajkfjdkfjs'),
(32, 1, 'يسشيششسش'),
(32, 2, 'dfajkfjdkfjs'),
(33, 1, 'يسشيششسش'),
(33, 2, 'dfajkfjdkfjs'),
(34, 1, 'يسشيششسش'),
(34, 2, 'dfajkfjdkfjs'),
(35, 1, 'يسشيششسش'),
(35, 2, 'dfajkfjdkfjs'),
(36, 1, 'يسشيششسش'),
(36, 2, 'dfajkfjdkfjs');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'اسلام قنديل', 'eslam@php-eg.com', '$2y$10$lQCsqu.iVH2g96zPPmixDOLDw8GbuA/yIArswc14raW8r4MXsUkba', 'VRs9OD255yaqgXagwVBmvGIPGhmKvbd0prSyxCaTk9Ji19JemHaGWnR9tvVJ', '2018-01-10 22:52:57', '2018-02-04 19:54:33'),
(4, 'eslam', 'islam@php-eg.com', '$2y$10$NZEqPXcqLDa0ijFg9YLD3OBkCfTJV.ADQkNVRpmG3MnfIFQ0j5PYy', NULL, '2018-02-01 10:08:27', '2018-05-25 06:10:18'),
(5, 'trterter', 'tret@kfdskd.com', '$2y$10$Su/Ecevu46AHEeJLZzoIhOJK.GsFT4lunsMs1CMVdG/9LFEpDKNMq', NULL, '2018-02-01 12:39:10', '2018-02-01 12:39:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_slider`
--
ALTER TABLE `ads_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auctions`
--
ALTER TABLE `auctions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`,`sub_category_id`),
  ADD KEY `car_color_id` (`car_color_id`),
  ADD KEY `car_type_id` (`car_type_id`),
  ADD KEY `engine_type_id` (`engine_type_id`),
  ADD KEY `fuel_id` (`fuel_id`),
  ADD KEY `import_id` (`import_id`),
  ADD KEY `motion_vector_id` (`motion_vector_id`),
  ADD KEY `specification_id` (`specification_id`),
  ADD KEY `member_id` (`member_id`),
  ADD KEY `provider_id` (`provider_id`);

--
-- Indexes for table `auction_images`
--
ALTER TABLE `auction_images`
  ADD PRIMARY KEY (`auction_id`,`order`);

--
-- Indexes for table `auction_other_specifications`
--
ALTER TABLE `auction_other_specifications`
  ADD PRIMARY KEY (`auction_id`,`other_specification_id`),
  ADD KEY `other_specification_id` (`other_specification_id`);

--
-- Indexes for table `bank_transaction`
--
ALTER TABLE `bank_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_colors`
--
ALTER TABLE `car_colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_color_languages`
--
ALTER TABLE `car_color_languages`
  ADD PRIMARY KEY (`car_color_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `car_types`
--
ALTER TABLE `car_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `car_type_languages`
--
ALTER TABLE `car_type_languages`
  ADD PRIMARY KEY (`car_type_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_languages`
--
ALTER TABLE `category_languages`
  ADD PRIMARY KEY (`category_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `id` (`id`,`country_id`);

--
-- Indexes for table `city_languages`
--
ALTER TABLE `city_languages`
  ADD PRIMARY KEY (`city_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `contact_messages`
--
ALTER TABLE `contact_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_languages`
--
ALTER TABLE `country_languages`
  ADD PRIMARY KEY (`country_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `engine_types`
--
ALTER TABLE `engine_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `engine_type_languages`
--
ALTER TABLE `engine_type_languages`
  ADD PRIMARY KEY (`engine_type_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq_languages`
--
ALTER TABLE `faq_languages`
  ADD PRIMARY KEY (`faq_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `follow_auction`
--
ALTER TABLE `follow_auction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fuels`
--
ALTER TABLE `fuels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fuel_languages`
--
ALTER TABLE `fuel_languages`
  ADD PRIMARY KEY (`fuel_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `imports`
--
ALTER TABLE `imports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `import_languages`
--
ALTER TABLE `import_languages`
  ADD PRIMARY KEY (`import_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD UNIQUE KEY `authorization` (`authorization`),
  ADD KEY `city_id` (`city_id`,`country_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motion_vectors`
--
ALTER TABLE `motion_vectors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `motion_vector_languages`
--
ALTER TABLE `motion_vector_languages`
  ADD PRIMARY KEY (`motion_vector_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `other_specifications`
--
ALTER TABLE `other_specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_specification_languages`
--
ALTER TABLE `other_specification_languages`
  ADD PRIMARY KEY (`other_specification_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_languages`
--
ALTER TABLE `page_languages`
  ADD PRIMARY KEY (`page_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specifications`
--
ALTER TABLE `specifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specification_languages`
--
ALTER TABLE `specification_languages`
  ADD PRIMARY KEY (`specification_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`,`id`);

--
-- Indexes for table `sub_category_languages`
--
ALTER TABLE `sub_category_languages`
  ADD PRIMARY KEY (`sub_category_id`,`language_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ads_slider`
--
ALTER TABLE `ads_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `auctions`
--
ALTER TABLE `auctions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `bank_transaction`
--
ALTER TABLE `bank_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `car_colors`
--
ALTER TABLE `car_colors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `car_types`
--
ALTER TABLE `car_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `contact_messages`
--
ALTER TABLE `contact_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `engine_types`
--
ALTER TABLE `engine_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `follow_auction`
--
ALTER TABLE `follow_auction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `fuels`
--
ALTER TABLE `fuels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `imports`
--
ALTER TABLE `imports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `motion_vectors`
--
ALTER TABLE `motion_vectors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `other_specifications`
--
ALTER TABLE `other_specifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `specifications`
--
ALTER TABLE `specifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auctions`
--
ALTER TABLE `auctions`
  ADD CONSTRAINT `auctions_ibfk_1` FOREIGN KEY (`category_id`,`sub_category_id`) REFERENCES `sub_categories` (`category_id`, `id`),
  ADD CONSTRAINT `auctions_ibfk_10` FOREIGN KEY (`provider_id`) REFERENCES `members` (`id`),
  ADD CONSTRAINT `auctions_ibfk_2` FOREIGN KEY (`car_color_id`) REFERENCES `car_colors` (`id`),
  ADD CONSTRAINT `auctions_ibfk_3` FOREIGN KEY (`car_type_id`) REFERENCES `car_types` (`id`),
  ADD CONSTRAINT `auctions_ibfk_4` FOREIGN KEY (`engine_type_id`) REFERENCES `engine_types` (`id`),
  ADD CONSTRAINT `auctions_ibfk_5` FOREIGN KEY (`fuel_id`) REFERENCES `fuels` (`id`),
  ADD CONSTRAINT `auctions_ibfk_6` FOREIGN KEY (`import_id`) REFERENCES `imports` (`id`),
  ADD CONSTRAINT `auctions_ibfk_7` FOREIGN KEY (`motion_vector_id`) REFERENCES `motion_vectors` (`id`),
  ADD CONSTRAINT `auctions_ibfk_8` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`),
  ADD CONSTRAINT `auctions_ibfk_9` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`);

--
-- Constraints for table `auction_images`
--
ALTER TABLE `auction_images`
  ADD CONSTRAINT `auction_images_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`);

--
-- Constraints for table `auction_other_specifications`
--
ALTER TABLE `auction_other_specifications`
  ADD CONSTRAINT `auction_other_specifications_ibfk_1` FOREIGN KEY (`auction_id`) REFERENCES `auctions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auction_other_specifications_ibfk_2` FOREIGN KEY (`other_specification_id`) REFERENCES `other_specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `car_color_languages`
--
ALTER TABLE `car_color_languages`
  ADD CONSTRAINT `car_color_languages_ibfk_1` FOREIGN KEY (`car_color_id`) REFERENCES `car_colors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `car_color_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `car_type_languages`
--
ALTER TABLE `car_type_languages`
  ADD CONSTRAINT `car_type_languages_ibfk_1` FOREIGN KEY (`car_type_id`) REFERENCES `car_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `car_type_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `category_languages`
--
ALTER TABLE `category_languages`
  ADD CONSTRAINT `category_languages_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `city_languages`
--
ALTER TABLE `city_languages`
  ADD CONSTRAINT `city_languages_ibfk_1` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `city_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `country_languages`
--
ALTER TABLE `country_languages`
  ADD CONSTRAINT `country_languages_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `country_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `engine_type_languages`
--
ALTER TABLE `engine_type_languages`
  ADD CONSTRAINT `engine_type_languages_ibfk_1` FOREIGN KEY (`engine_type_id`) REFERENCES `engine_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `engine_type_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `faq_languages`
--
ALTER TABLE `faq_languages`
  ADD CONSTRAINT `faq_languages_ibfk_1` FOREIGN KEY (`faq_id`) REFERENCES `faqs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `faq_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fuel_languages`
--
ALTER TABLE `fuel_languages`
  ADD CONSTRAINT `fuel_languages_ibfk_1` FOREIGN KEY (`fuel_id`) REFERENCES `fuels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fuel_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `import_languages`
--
ALTER TABLE `import_languages`
  ADD CONSTRAINT `import_languages_ibfk_1` FOREIGN KEY (`import_id`) REFERENCES `imports` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `import_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `members`
--
ALTER TABLE `members`
  ADD CONSTRAINT `members_ibfk_1` FOREIGN KEY (`city_id`,`country_id`) REFERENCES `cities` (`id`, `country_id`);

--
-- Constraints for table `motion_vector_languages`
--
ALTER TABLE `motion_vector_languages`
  ADD CONSTRAINT `motion_vector_languages_ibfk_1` FOREIGN KEY (`motion_vector_id`) REFERENCES `motion_vectors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `motion_vector_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `other_specification_languages`
--
ALTER TABLE `other_specification_languages`
  ADD CONSTRAINT `other_specification_languages_ibfk_1` FOREIGN KEY (`other_specification_id`) REFERENCES `other_specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `other_specification_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_languages`
--
ALTER TABLE `page_languages`
  ADD CONSTRAINT `page_languages_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `page_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `specification_languages`
--
ALTER TABLE `specification_languages`
  ADD CONSTRAINT `specification_languages_ibfk_1` FOREIGN KEY (`specification_id`) REFERENCES `specifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `specification_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `sub_category_languages`
--
ALTER TABLE `sub_category_languages`
  ADD CONSTRAINT `sub_category_languages_ibfk_1` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_category_languages_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

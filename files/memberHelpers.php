<?php
//get count of notifications
function getCountOfNotifications()
{
    $Memper = \App\Model\notifications::where(['userid' => \Auth::guard('members')->user()->id,'view' => 0])->get();
    return $Memper->count();
}
// GET Memper NAME
function getMemperName($MemperId)
{
    $Memper = \App\Model\Clients::where(['id' => $MemperId])->first();
    return $Memper;
}
// GET COUNTRY NAME
function getCountryName($countryId)
{
    $name = \App\Model\Country::where(['id' => $countryId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET CITY NAME
function getCityName($CityId)
{
    $name = \App\Model\City::where(['id' => $CityId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET Category car NAME
function getMarkaName($MarkaId)
{
    $name = \App\Model\Category::where(['id' => $MarkaId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  SubCategory car NAME
function getModelName($ModelId)
{
    $name = \App\Model\SubCategory::where(['id' => $ModelId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}

// GET Color NAME
function getColorName($ColorId)
{
    $name = \App\Model\CarColor::where(['id' => $ColorId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  Car Type  NAME
function getCarTypeName($CarTypeId)
{
    $name = \App\Model\CarType::where(['id' => $CarTypeId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  Engine Type  NAME
function getEngineTypeName($EngineTypeId)
{
    $name = \App\Model\EngineType::where(['id' => $EngineTypeId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  Fuel  NAME
function getFuelName($FuelId)
{
    $name = \App\Model\Fuel::where(['id' => $FuelId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  Import  NAME
function getImportName($ImportId)
{
    $name = \App\Model\Import::where(['id' => $ImportId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  Motion Vector  NAME
function getMotionVectorName($MotionVectorId)
{
    $name = \App\Model\MotionVector::where(['id' => $MotionVectorId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET  Specification NAME
function getSpecificationName($SpecificationId)
{
    $name = \App\Model\Specification::where(['id' => $SpecificationId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}
// GET Other Specification NAME
function getOtherSpecificationName($ospacificId)
{
    $name = \App\Model\OtherSpecification::where(['id' => $ospacificId])->with('currentLanguage')->first();
    return $name->currentLanguage->title;
}



?>
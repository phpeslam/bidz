<?php



/**
 * @param $lang
 * @return \Illuminate\Contracts\Routing\UrlGenerator|string
 */
function getLangUrl($lang)
{
    $path = \Request::path();
    $current_paths = explode('/',\Request::path()) ;
    if(getCurrentLang('iso_code') == getDefaultLang()['iso_code'] && $current_paths[0]!=getDefaultLang()['iso_code']){
        $path = getDefaultLang()['iso_code'].'/'.$path;
    }
    if($lang==getDefaultLang()['iso_code']){
        $lang ='';
    }
    return url(str_replace(getCurrentLang('iso_code'),$lang,$path));
}

/**
 * @return array
 */
function getLanguages()
{
    static $all_lang;
    if(!is_array($all_lang)) {
        $all_lang = [];
        foreach (\App\Model\Language::all()->toArray() as $item) {
            $all_lang[$item['iso_code']] = $item;
        }
    }
    return $all_lang;
}

/**
 * @return array
 */
function getLanguagesWithIds()
{
    static $all_lang_with_ids;
    if(!is_array($all_lang_with_ids)) {
        $all_lang_with_ids = [];
        foreach (getLanguages() as $item) {
            $all_lang_with_ids[$item['id']] = $item;
        }
    }
    return $all_lang_with_ids;
}

/**
 * @return mixed|string
 */
function getDefaultLang()
{
    $def = [];
    foreach (getLanguages() as $language){
        if($language['is_default']==1){
            $def = $language; break;
        }
    }
    return $def;
}

/**
 * @return mixed
 */
function getCurrentLang($key=null)
{
    if(!is_null($key)){
        return getLanguages()[\App::getLocale()][$key];
    }
    return getLanguages()[\App::getLocale()];
}

define('NO_IMAGE','global/no_image_found.png');
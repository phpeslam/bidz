// var table = '';
// $(function () {
//     $('table#pages-table').on('click','a.delete_row',function () {
//         var href_link = $(this).attr('href');
//         bootbox.confirm(bootbox_delete_confirm, function(result){
//             if(result){
//                 $.get(href_link,function (result) {
//                     if(result[0]){
//                         table.ajax.reload(null, false );
//                         $.toaster(result[1], null, 'success');
//                     }else{
//                         $.toaster(result[1], null, 'danger');
//                     }
//
//                 },'json') ;
//             }
//         });
//        return false;
//     });
//
//     $('#pages-table').on('click','a.status_row',function () {
//         var href_link = $(this).attr('href');
//
//         $.get(href_link,function (result) {
//             if(result[0]){
//                 table.ajax.reload(null, false );
//                 $.toaster(result[1], null, 'success');
//             }else{
//                 $.toaster(result[1], null, 'danger');
//             }
//         },'json') ;
//
//
//         return false;
//     });
//    table =  $('table#pages-table').DataTable({
//         serverSide: true,
//         processing: true,
//         stateSave: true,
//         ajax: {
//             url:page_show,
//             data:{}
//         },
//         order: [[ 0, "desc" ]],
//         oLanguage:datatable_lang,
//         columns: [
//             {data: 'id',className:'text-center'},
//             {data: 'current_language.title', orderable: false},
//             {data: 'action', orderable: false, searchable: false,className:'text-center'}
//         ]
//     });
// });


var DatatableRemoteAjaxDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: page_show,
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'current_language.page_id',
                    title: '#',
                },
                {
                    field: 'current_language.title',
                    title: 'title',
                },
                {
                    field: 'action',
                    title: 'action',
                },
            ],
        });

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    DatatableRemoteAjaxDemo.init();
});

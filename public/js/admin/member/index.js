var DatatableRemoteAjaxDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: member_show,
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width: 40,
                },
                {
                    field: 'first_name',
                    title: 'first name',
                },
                {
                    field: 'last_name',
                    title: 'last name',
                },
                {
                    field: 'email',
                    title: 'email',
                },
                {
                    field: 'membership_type',
                    title: 'membership',
                    template: function (row) {
                        var membership_type = {
                            1: {'title': 'User', 'class': 'm-badge--brand'},
                            2: {'title': 'agency', 'class': ' m-badge--success'},
                        };
                        return '<span class="m-badge ' + membership_type[row.membership_type].class + ' m-badge--wide">' + membership_type[row.membership_type].title + '</span>';
                    },
                },
                {
                    field: 'created_at',
                    title: 'Created',
                }, {
                    field: 'action',
                    title: 'action',
                },

            ],
        });

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    DatatableRemoteAjaxDemo.init();
});
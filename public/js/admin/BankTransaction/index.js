

var DatatableRemoteAjaxDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: BankTransaction_show,
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width: 40,
                },
                {
                    field: 'type',
                    title: 'type',
                    template: function (row) {
                        var type = {
                            1: {'title': 'Add Balance', 'class': 'm-badge--brand'},
                            2: {'title': 'Insurance', 'class': ' m-badge--success'},
                            3: {'title': 'Special Ads', 'class': ' m-badge--default'},
                        };
                        return '<span class="m-badge ' + type[row.type].class + ' m-badge--wide">' + type[row.type].title + '</span>';
                    },
                },
                {
                    field: 'date',
                    title: 'date',
                },
                {
                    field: 'value',
                    title: 'cash',
                },
                {
                    field: 'action',
                    title: 'action',
                },
            ],
        });

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    DatatableRemoteAjaxDemo.init();
});

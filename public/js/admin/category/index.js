var table = '';
$(function () {
    $('table#categories-table').on('click','a.delete_row',function () {
        var href_link = $(this).attr('href');
        bootbox.confirm(bootbox_delete_confirm, function(result){
            if(result){
                $.get(href_link,function (result) {
                    if(result[0]){
                        table.ajax.reload(null, false );
                        $.toaster(result[1], null, 'success');
                    }else{
                        $.toaster(result[1], null, 'danger');
                    }

                },'json') ;
            }
        });
       return false;
    });

    $('#categories-table').on('click','a.status_row',function () {
        var href_link = $(this).attr('href');

        $.get(href_link,function (result) {
            if(result[0]){
                table.ajax.reload(null, false );
                $.toaster(result[1], null, 'success');
            }else{
                $.toaster(result[1], null, 'danger');
            }
        },'json') ;


        return false;
    });
   table =  $('table#categories-table').DataTable({
        serverSide: true,
        processing: true,
        stateSave: true,
        ajax: {
            url:category_show,
            data:{}
        },
        order: [[ 0, "desc" ]],
        oLanguage:datatable_lang,
        columns: [
            {data: 'id',className:'text-center'},
            {data: 'current_language.title', orderable: false},
            {data: 'projects_count',className:'text-center'},
            {data: 'action', orderable: false, searchable: false,className:'text-center'}
        ]
    });
});
// var table = '';
// $(function () {
//     $('#users-table').on('click','a.delete_row',function () {
//         var href_link = $(this).attr('href');
//         bootbox.confirm(bootbox_delete_confirm, function(result){
//             if(result){
//                 $.get(href_link,function (result) {
//                     if(result[0]){
//                         table.ajax.reload(null, false );
//                     }
//                     bootbox.alert(result[1]);
//                 },'json') ;
//             }
//         });
//        return false;
//     });
//     $('select.filter_field').change(function () {
//         table.draw();
//     });
//    table =  $('#users-table').DataTable({
//         serverSide: true,
//         processing: true,
//
//        ajax: {
//            url:user_show,
//            data:function (d) {
//                d.fl_role = $('select#fl_role').val();
//            }
//        },
//         order: [[ 0, "desc" ]],
//         oLanguage:datatable_lang,
//         columns: [
//             {data: 'id',className:'text-center'},
//
//             {data: 'name'},
//             {data: 'email',className:'text-left'},
//             {data: 'role',orderable: false,className:'text-center', searchable: false},
//             {data: 'created_at',className:'text-left'},
//
//             {data: 'action', orderable: false, searchable: false,className:'text-center'}
//         ]
//     });
// });

//== Class definition

var DatatableRemoteAjaxDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: user_show,
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '#',
                },
                {
                    field: 'name',
                    title: 'name',
                },
                {
                    field: 'email',
                    title: 'email',
                },
                {
                    field: 'role',
                    title: 'role',
                    // template: function (row) {
                    //     var status  = {
                    //         1: {'title': 'admin', 'state': 'admin'},
                    //         2: {'title': 'masjed_haram', 'state': 'masjed_haram'},
                    //         3: {'title': 'masjed_nabawy', 'state': 'masjed_nabawy'},
                    //     };
                    //     return '<span class="m-badge m-badge--' + status[row.role].state + ' m-badge--dot"></span>&nbsp;<span class="m--font-bold m--font-' + status[row.role].state + '">' +
                    //         status[row.role].title + '</span>';
                    // },
                },
                {
                    field: 'created_at',
                    title: 'Created',
                }, {
                    field: 'action',
                    title: 'action',
                },


                // {data: 'email', className: 'text-left'},
                // {data: 'role', orderable: false, className: 'text-center', searchable: false},
                //  {data: 'created_at', className: 'text-left'},

                //  {data: 'action', orderable: false, searchable: false, className: 'text-center'}
            ],
        });

        $('#fl_role').on('change', function () {
            datatable.search($(this).val().toLowerCase(), 'Status');
        });

        $('#fl_role').selectpicker();

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    DatatableRemoteAjaxDemo.init();
});
var DatatableRemoteAjaxDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // sample GET method
                        method: 'GET',
                        url: auction_show,
                        map: function (raw) {
                            // sample data mapping
                            var dataSet = raw;
                            if (typeof raw.data !== 'undefined') {
                                dataSet = raw.data;
                            }
                            return dataSet;
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                scroll: false,
                footer: false
            },

            // column sorting
            sortable: true,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '#',
                    width: 40,
                },
                {
                    field: 'title',
                    title: 'title',
                },
                {
                    field: 'member.first_name',
                    title: 'Client',
                },
                {
                    field: 'minimum_bid',
                    title: 'Minimum Bid',
                },
                {
                    field: 'bid_type',
                    title: 'Bid Type',
                    template: function (row) {
                        var bid_type = {
                            1: {'title': 'Highest Price', 'class': 'm-badge--brand'},
                            2: {'title': 'Client Agree', 'class': 'm-badge--warning'},
                        };
                        return '<span class="m-badge ' + bid_type[row.bid_type].class + ' m-badge--wide">' + bid_type[row.bid_type].title + '</span>';
                    },
                },
                {
                    field: 'total_bids',
                    title: 'Total Bids',
                },

                {
                    field: 'created_at',
                    title: 'Created',
                }, {
                    field: 'action',
                    title: 'action',
                },

            ],
        });

    };

    return {
        // public functions
        init: function () {
            demo();
        },
    };
}();

jQuery(document).ready(function () {
    DatatableRemoteAjaxDemo.init();
});
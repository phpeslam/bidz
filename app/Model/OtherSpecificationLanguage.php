<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OtherSpecificationLanguage extends Model
{


    protected $fillable = ['other_specification','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function otherSpecification()
    {
        return $this->belongsTo('\App\Model\OtherSpecification');
    }
}

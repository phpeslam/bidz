<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{

    protected $fillable = ['is_active'];
    protected $dates  = ['created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function importLanguages()
    {
        return $this->hasMany('\App\Model\ImportLanguage');
    }



    /**
     * @return $this
     */
    public function currentLanguage()
    {
        return $this->hasOne('\App\Model\ImportLanguage')
            ->where('language_id',getCurrentLang('id'));
    }
    public function arLang()
    {
        return $this->hasOne('\App\Model\ImportLanguage')
            ->where('language_id',1);
    }

    public function enLang()
    {
        return $this->hasOne('\App\Model\ImportLanguage')
            ->where('language_id',2);
    }
}

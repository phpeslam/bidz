<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuctionImage extends Model
{
    protected $table = 'auction_images';
    const IMAGE_PATH = 'auction_image';
    protected $appends = ['image_found'];
    protected $fillable = ['auction_id','order','image'];
    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    public function getImageFoundAttribute()
    {
        return \Storage::exists(self::IMAGE_PATH.DIRECTORY_SEPARATOR.$this->attributes['image']);
    }

    public function getImageAttribute()
    {
        if(!\Storage::exists(self::IMAGE_PATH.DIRECTORY_SEPARATOR.$this->attributes['image'])){
            return asset('storage/global/no_image_found.png');
        }
        return asset('storage/'.self::IMAGE_PATH.'/'.$this->attributes['image']);
    }

    public static function removeOld($image)
    {
        if(!empty($image) &&  \Storage::exists(self::IMAGE_PATH.DIRECTORY_SEPARATOR.$image)){
            \Storage::delete(self::IMAGE_PATH.DIRECTORY_SEPARATOR.$image);
        }
    }

    public function delete()
    {
        $parent_status = parent::delete();
        if(!empty($this->getAttributes()['image']) && $parent_status && \Storage::exists(self::IMAGE_PATH.DIRECTORY_SEPARATOR.$this->attributes['image'])){
            \Storage::delete(self::IMAGE_PATH.DIRECTORY_SEPARATOR.$this->attributes['image']);
        }
        return $parent_status;
    }

}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CityLanguage extends Model
{
    protected $fillable = ['city_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('\App\Model\City');
    }
}

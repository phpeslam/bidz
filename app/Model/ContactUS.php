<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContactUS extends Model
{
    protected $table = 'contact_messages';
    public $timestamps = false; 
}

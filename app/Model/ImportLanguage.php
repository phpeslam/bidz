<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ImportLanguage extends Model
{


    protected $fillable = ['import_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function import()
    {
        return $this->belongsTo('\App\Model\Import');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SubCategoryLanguage extends Model
{


    protected $fillable = ['sub_category_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subCategory()
    {
        return $this->belongsTo('\App\Model\SubCategory');
    }
}

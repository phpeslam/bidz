<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SpecificationLanguage extends Model
{


    protected $fillable = ['category_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('\App\Model\Category');
    }
}

<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $fillable = ['iso_code','text','is_default','direction'];
    public $timestamps = false;
}

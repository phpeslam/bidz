<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class Member
 * @package App\Model
 */

class Member extends Authenticatable
{
    const FILE_PATH = 'member_files';
    protected $appends = ['file_found'];
    protected $fillable =['first_name',
        'last_name',
        'email',
        'password',
        'city_id',
        'country_id',
        'phone_number',
        'address',
        'membership_type',
        'remember_token',
        'fcm_token',
        'authorization',
        'device_id',
        'is_active_phone',
        'is_active_email',
        'phone_act_code',
        'is_active',
        'file',
        'balance',
        'email_act_code',
        'fcm_token'
    ];

    protected $hidden = ['password','phone_act_code','email_act_code','fcm_token'];



    public function getFileFoundAttribute()
    {
       // return \Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file']);
        if(!isset($this->attributes['file'])||$this->attributes['file'] ==null)
        {
            return asset('storage/global/no_image_found.png');
        }
        if(!\Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file'])){
            return asset('storage/global/no_image_found.png');
        }
        return asset('storage/'.self::FILE_PATH.'/'.$this->attributes['file']);
    }

    public function getFileAttribute()
    {
        if(!\Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file'])){
            return asset('storage/global/no_image_found.png');
        }
        return asset('storage/'.self::FILE_PATH.'/'.$this->attributes['file']);
    }

    public static function removeOld($file)
    {
        if(!empty($file) &&  \Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$file)){
            \Storage::delete(self::FILE_PATH.DIRECTORY_SEPARATOR.$file);
        }
    }

    public function delete()
    {
        $parent_status = parent::delete();
        if(!empty($this->getAttributes()['file']) && $parent_status && \Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file'])){
            \Storage::delete(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file']);
        }
        return $parent_status;
    }

}

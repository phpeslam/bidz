<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class Ads extends Model
{
    protected $table = 'ads';
    protected $fillable = ['is_active'];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function AdsLanguages()
    {
        return $this->hasMany('\App\Model\AdsLanguage');
    }



    /**
     * @return $this
     */
    public function currentLanguage()
    {
        return $this->hasOne('\App\Model\AdsLanguage')
            ->where('language_id',getCurrentLang('id'));
    }
}
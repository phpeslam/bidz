<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class Auction extends Model
{
    protected $table = 'auctions';
    protected $fillable = [
        'bid_days',
        'title',
        'description',
        'category_id',
        'sub_category_id',
        'car_color_id',
        'car_type_id',
        'engine_type_id',
        'fuel_id',
        'import_id',
        'motion_vector_id',
        'specification_id',
        'manufacturing_year',
        'engine_size',
        'vaild_form',
        'form_years',
        'is_periodic_inspection',
        'mileage',
        'chassis_no',
        'bid_type',
        'minimum_bid',
        'member_id',
        'provider_id',
        'provider_accept',
        'start_at',
        'views',
        'is_active',
        'current_time'
    ];
    protected $dates = ['start_at'];
    protected $appends = ['image'];
      

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    // public function getCurrentAttribute() {
    //     return date('Y-m-d h:i:s');
    // }

    public function getImageAttribute()
    {
        $first_image = AuctionImage::where('auction_id',$this->id)->first();
        if($first_image){
            return $first_image->image;
        }
        return asset('storage/global/no_image_found.png');
    }

    public function getFileAttribute()
    {
        $first_image = AuctionFile::where('auction_id',$this->id)->first();
        if($first_image){
           // return $first_image->file;
            return asset('storage/auction_file/'.$first_image->file);
        }
        return asset('storage/global/no_image_found.png');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carColor()
    {
        return $this->belongsTo(CarColor::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carType()
    {
        return $this->belongsTo(CarType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function engineType()
    {
        return $this->belongsTo(EngineType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fuel()
    {
        return $this->belongsTo(Fuel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function import()
    {
        return $this->belongsTo(Import::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function motionVector()
    {
        return $this->belongsTo(MotionVector::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function specification()
    {
        return $this->belongsTo(Specification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Member::class,'provider_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function member()
    {
        return $this->belongsTo(Member::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionOtherSpecifications()
    {
        return $this->hasMany( AuctionOtherSpecification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function otherSpecifications()
    {
        return $this->belongsToMany(OtherSpecification::class,(new AuctionOtherSpecification())->getTable());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionFiles()
    {
        return $this->hasOne(AuctionFile::class,'auction_id');
    }

    // public function auctionFiles()
    // {
    //     $first_image = AuctionFile::where('auction_id',$this->id)->first();dd($this->id,$first_image);
    //     if($first_image){
    //        // return $first_image->file;
    //         return asset('storage/auction_file/'.$first_image->file);
    //     }
    //     return asset('storage/global/no_image_found.png');
    // }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function auctionImages()
    {
        return $this->hasMany(AuctionImage::class);
    }

    public function notifications()
    {
        return $this->belongsTo(notifications::class, 'auctionID' );
    }

}

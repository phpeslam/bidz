<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FuelLanguage extends Model
{


    protected $fillable = ['fuel_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fuel()
    {
        return $this->belongsTo('\App\Model\Fuel');
    }
}

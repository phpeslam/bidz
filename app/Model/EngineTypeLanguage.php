<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EngineTypeLanguage extends Model
{


    protected $fillable = ['engine_type_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function engineType()
    {
        return $this->belongsTo('\App\Model\EngineType');
    }
}

<?php


namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Bidding extends Model
{
    protected $table = 'bidding';
    public $timestamps = false;
}
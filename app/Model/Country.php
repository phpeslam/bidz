<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $fillable = ['is_active'];
    protected $dates  = ['created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function countryLanguages()
    {
        return $this->hasMany('\App\Model\CountryLanguage');
    }

    public function countries()
    {
        return $this->hasMany(City::class);
    }

    public function arLang()
    {
        return $this->hasOne('\App\Model\CountryLanguage')
            ->where('language_id',1);
    }

    public function enLang()
    {
        return $this->hasOne('\App\Model\CountryLanguage')
            ->where('language_id',2);
    }

    /**
     * @return $this
     */
    public function currentLanguage()
    {
        return $this->hasOne('\App\Model\CountryLanguage')
            ->where('language_id',getCurrentLang('id'));
    }
}

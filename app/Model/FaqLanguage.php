<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FaqLanguage extends Model
{
    protected $fillable = ['faq_id','language_id','title','desc'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function faq()
    {
        return $this->belongsTo('\App\Model\Faq');
    }
}

<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PageLanguage extends Model
{
    protected $fillable = ['page_id','language_id','title','desc'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo('\App\Model\Page');
    }
}

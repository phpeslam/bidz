<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class AdsSlider extends Model
{
    protected $table = 'ads_slider';
    protected $fillable = ['is_active'];
    public $timestamps = false;

    public function Auction()
    {
        return $this->hasOne('App\Model\Auction','id','auctionID');
    }
}
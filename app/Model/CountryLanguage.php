<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CountryLanguage extends Model
{


    protected $fillable = ['country_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('\App\Model\Country');
    }
}

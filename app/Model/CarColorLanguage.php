<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CarColorLanguage extends Model
{

    protected $fillable = ['car_color_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carColor()
    {
        return $this->belongsTo('\App\Model\CarColor');
    }
}

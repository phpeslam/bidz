<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuctionFile extends Model
{
    const FILE_PATH = 'auction_file';
    protected $appends = ['file_found'];
    protected $fillable = ['auction_id','order','file'];
    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    public function getFileFoundAttribute()
    {
        return \Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file']);
    }

    // public function getFileAttribute()
    // {
    //     // dd('---------------------------------------',self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file']);
    //     if(!\Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file'])){
    //         return asset('storage/global/no_image_found.png');
    //     }
    //     return asset('storage/'.self::FILE_PATH.'/'.$this->attributes['file']);
    // }
    // public function getFileAttribute()
    // {
    //     $first_image = AuctionFile::where('auction_id',$this->id)->first();
    //     if($first_image){
    //        // return $first_image->file;
    //         return asset('storage/auction_file/'.$first_image->file);
    //     }
    //     return asset('storage/global/no_image_found.png');
    // }

    public static function removeOld($file)
    {
        if(!empty($file) &&  \Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$file)){
            \Storage::delete(self::FILE_PATH.DIRECTORY_SEPARATOR.$file);
        }
    }

    public function delete()
    {
        $parent_status = parent::delete();
        if(!empty($this->getAttributes()['file']) && $parent_status && \Storage::exists(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file'])){
            \Storage::delete(self::FILE_PATH.DIRECTORY_SEPARATOR.$this->attributes['file']);
        }
        return $parent_status;
    }

}

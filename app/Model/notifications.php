<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class notifications  extends Model
{
    protected $table = 'notifications';

    public function auction()
    {
        return $this->belongsTo(Auction::class, 'auctionID', 'id');
    }

}
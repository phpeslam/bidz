<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['is_active'];
    protected $dates  = ['created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryLanguages()
    {
       return $this->hasMany('\App\Model\CategoryLanguage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('\App\Model\Project');
    }

    /**
     * @return $this
     */
    public function currentLanguage()
    {
        return $this->hasOne('\App\Model\CategoryLanguage')
            ->where('language_id',getCurrentLang('id'));
    }

    public function arLang()
    {
        return $this->hasOne('\App\Model\CategoryLanguage')
            ->where('language_id',1);
    }

    public function enLang()
    {
        return $this->hasOne('\App\Model\CategoryLanguage')
            ->where('language_id',2);
    }
}

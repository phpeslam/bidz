<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FollowAuction extends Model {

    protected $table = 'follow_auction';
    protected $fillable = ['auction_id','user_id'];
    protected $dates = ['created_at', 'updated_at'];

}
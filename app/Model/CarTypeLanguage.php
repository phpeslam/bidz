<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CarTypeLanguage extends Model
{


    protected $fillable = ['car_type_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function carType()
    {
        return $this->belongsTo('\App\Model\CarType');
    }
}

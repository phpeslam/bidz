<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuctionOtherSpecification extends Model
{
    protected $fillable = ['auction_id','other_specification_id'];
    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function auction()
    {
        return $this->belongsTo(Auction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function otherSpecification()
    {
        return $this->belongsTo(OtherSpecification::class);
    }
}

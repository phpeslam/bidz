<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BankTransaction extends Model
{
    const FILE_PATH = 'banktransaction';
    protected $table = 'bank_transaction';
    public $timestamps = false;


    public function member()
    {
        return $this->belongsTo(Member::class,'userID');
    }

    public function Auction()
    {
        return $this->belongsTo(Auction::class,'auctionID');
    }
}

<?php


namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AdsLanguage extends Model
{
    protected $fillable = ['ADS_id','language_id','title','link','image'];
    const IMAGE_PATH = 'ads_image';
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Ads()
    {
        return $this->belongsTo('\App\Model\Ads');
    }
}
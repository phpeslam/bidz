<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model {

    protected $table = 'partners';
    protected $fillable = ['name','image'];
    protected $dates = ['created_at', 'updated_at'];

}

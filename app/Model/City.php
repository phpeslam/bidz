<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class City extends Model
{

    protected $fillable = ['is_active'];
    protected $dates  = ['created_at','updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cityLanguages()
    {
        return $this->hasMany('\App\Model\CityLanguage');
    }

    /**
     * @return HasOne
     */
    public function currentLanguage()
    {
        return $this->hasOne('\App\Model\CityLanguage')
            ->where('language_id',getCurrentLang('id'));
    }

    public function arLang()
    {
        return $this->hasOne('\App\Model\CityLanguage')
            ->where('language_id',1);
    }

    public function enLang()
    {
        return $this->hasOne('\App\Model\CityLanguage')
            ->where('language_id',2);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}

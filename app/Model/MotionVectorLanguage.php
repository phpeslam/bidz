<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MotionVectorLanguage extends Model
{


    protected $fillable = ['motion_vector_id','language_id','title'];
    public $timestamps = false;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('\App\Model\Language');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function motionVector()
    {
        return $this->belongsTo('\App\Model\MotionVector');
    }
}

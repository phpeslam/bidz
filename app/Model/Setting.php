<?php
namespace App\Model;



use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $primaryKey = 'key';
    protected $keyType ='string';
    public $incrementing = false;
    protected $fillable = ['key','value','is_serialize'];

    public $timestamps = false;


}

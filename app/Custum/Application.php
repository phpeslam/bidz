<?php
/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 27/01/18
 * Time: 09:07 م
 */

namespace App\Custum;


use Illuminate\Http\Request;

class Application extends \Illuminate\Foundation\Application
{
    public function langPath()
    {
        if(!in_array('admin',Request::capture()->segments())){
            return parent::langPath();
        }
        return $this->basePath('admin'.DIRECTORY_SEPARATOR.'lang');
    }
}
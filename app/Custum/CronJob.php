<?php

namespace App\Custum;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;




use App;
use Hash;
use Validator;

use App\Model\Auction;
use App\Model\AuctionFile;
use App\Model\AuctionImage;
use App\Model\AuctionOtherSpecification;
use App\Model\CarColor;
use App\Model\CarType;
use App\Model\Category;
use App\Model\EngineType;
use App\Model\Fuel;
use App\Model\Import;
use App\Model\Member;
use App\Model\MotionVector;
use App\Model\notifications;
use App\Model\OtherSpecification;
use App\Model\Specification;
use App\Model\SubCategory;
use App\Model\Bidding;
use App\Model\FollowAuction;
use App\Model\BankTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class CronJob
{
    public static function sds()
    {
        echo 'hello';
    }

    public static function cronJob()
    {
        $auctions = Auction::where('is_active',1)->where('isWork',1)->get();
        //dd($auctions);
            $now = Carbon::now();
            foreach($auctions as $auction)
            {
                $new = Carbon::parse($auction->start_at)->addDays($auction->bid_days);
                if($new < $now)
                {
                    $mysqldate = date( 'Y-m-d H:i:s');
                    $date = strtotime( $mysqldate );

                    if($auction->bid_type == 1)
                    {
                        $auc = Auction::find($auction->id);
                        $auc->isWork = 0;
                        $auc->save();

                        $owner = Member::find($auction->member_id);
                        $lastBid = Bidding::where('auctionID',$auction->id)->where('price',$auction->current_price)->first();
                        
                        if($lastBid)
                        {
                            $deleteAllLoserBidPrice = BankTransaction::where('auctionID',$auction->id)->where('value','<>',$auction->current_price)->delete();
                            $winner = Member::find($lastBid->memberID);
                            if($winner)
                            {
                                
                                $messa =   " تهانيناً ,, تم فوزك بالمناقصة رقم#".$auction->id." ويرجي الدفع قبل 48 ساعة والا سيتم خسارة السيارة ومبلغ التأمين ";
                                (new self)->sendGCM($messa,$winner->fcm_token);
                                $note = new notifications();
                                $note->userid = $winner->id;
                                $note->type = 5;
                                $note->auctionID = $auction->id;
                                $note->message = $messa;
                                $note->created_at = Carbon::now();
                                $note->save();
                                //dd($note);
                                
                                $changBalanceStatus = BankTransaction::where('auctionID',$auction->id)->where('userID',$winner->id)->first();
                                $changBalanceStatus->case_balance = 'withdraw';
                                $changBalanceStatus->save();

                               // $messaM =  " ريال سعودي ".$auction->current_price." ووصول السيارة لأعلي سعر".$auction->id . " تهانيناً ,, هناك شخص قد ربح مناقصتك رقم #";
                                
                                $messaM =" تهانيناً ,, هناك شخص قد ربح مناقصتك رقم #".$auction->id ." ووصول السيارة لأعلي سعر".$auction->current_price." ريال سعودي ";
                                (new self)->sendGCM($messaM,$owner->fcm_token);

                                $note = new notifications();
                                $note->userid = $owner->id;
                                $note->type = 8;
                                $note->auctionID = $auction->id;
                                $note->message = $messaM;
                                $note->created_at = Carbon::now();
                                $note->save();
                            }
                        }
                    }
                    elseif($auction->bid_type == 2)
                    {
                        $auc = Auction::where('id',$auction->id)->where('approve_member',0)->first();
                        $auc->isWork = 0;
                        $auc->approve_member_date = Carbon::now();
                        $auc->save();

                        $owner = Member::find($auction->member_id);
                        $lastBid = Bidding::where('auctionID',$auction->id)->where('price',$auction->current_price)->first();
                        
                        if($lastBid)
                        {
                            $deleteAllLoserBidPrice = BankTransaction::where('auctionID',$auction->id)->where('value','<>',$auction->current_price)->delete();
                            $winner = Member::find($lastBid->memberID);
                            if($winner)
                            {
                                $messa = " تهانيناً ,, تم فوزك بالمناقصة رقم#". $auction->id ." ويرجي انتظار موافقة المالك في خلال 48 ساعة";
                                
                                (new self)->sendGCM($messa,$winner->fcm_token);
                                $note = new notifications();
                                $note->userid = $winner->id;
                                $note->type = 6;
                                $note->auctionID = $auction->id;
                                $note->message = $messa;
                                $note->created_at = Carbon::now();
                                $note->save();
                                
                                $messaM =  " تهانيناً ,, هناك شخص قد ربح مناقصتك رقم #" . $auction->id ." ووصول السيارة لأعلي سعر".$auction->current_price."ريال سعودي و الرابح في انتظار الرد يرجي الرد قبل 48 ساعة والا ستفقد الناقصة";
                                
                                (new self)->sendGCM($messaM,$owner->fcm_token);

                                $note = new notifications();
                                $note->userid = $owner->id;
                                $note->type = 9;
                                $note->auctionID = $auction->id;
                                $note->message = $messaM;
                                $note->created_at = Carbon::now();
                                $note->save();
                            }
                        }
                    }
                }
            }
    }

    public static function ownerAccept()
    {
        $now = Carbon::now();
        
        $notifications = notifications::where('type',9)->whereNull('action')->get();
        foreach($notifications as $notification)
        {
            $new = Carbon::parse($notification->created_at)->addDays(2);
            if($now > $new)
            {
                $noteUpdate = notifications::find($notification->id);
                $noteUpdate->action = 'owner_reject';
                $noteUpdate->save();

                $messaM =  "تم غلق المناقصة";
                (new self)->sendGCM($messaM,$owner->fcm_token);
                $note = new notifications();
                $note->userid = $notification->userid;
                $note->type = 11;
                $note->auctionID = $notification->auctionID;
                $note->message = $messaM;
                $note->created_at = Carbon::now();
                $note->save();
            }
        }
    }


    public function sendGCM($message, $id) {


        $url = 'https://fcm.googleapis.com/fcm/send';
    
        $fields = array (
                'registration_ids' => array (
                        $id
                ),
                'notification' => array (
                    "body" => $message,
                    'title'     => "saudibidz",
                    'vibrate'   => 1,
                    'sound'     => 1,
                    "icon" => "myicon",
                    "color" => "#2bc0d1"
            )
        );
        $fields = json_encode ( $fields );
    
        $headers = array (
                'Authorization: key=' . "AAAAH6gDKps:APA91bG2gexrxQ0JshjD4I4tWEIGMeMORu77VYH9111OB48Idhc1GDsCmXDfCRrtwjl2pldQCAp1D68G9ENyTaJL8svOYE1xheIsYim8UlDVZGu7sNO4FgTxtKr3lyQkF29RnWntvUKYO6X-WkW0U3n9KlX-PYCfqw",
                'Content-Type: application/json'
        );
    
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
    
        $result = curl_exec ( $ch );
      //  echo $result;dd($result,"ddddddddddd");
        curl_close ( $ch );
    }
}

<?php

namespace App\Mail;

use App\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var Member
     */
    public $user;

    public $new_password;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $user, $new_password)
    {
        $this->user = $user;
        $this->new_password = $new_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.register.reset')
                      ->with([
                                'email' => $this->user,
                                'password' => $this->new_password,
                            ]);;
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\ContactUS;
use App\Model\Setting;
use Session;
use Illuminate\Http\Request;

class ContactUSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $setting = Setting::all();
        $data = [];
        //dd($setting);
        $data['setting'] = $setting;
        $data['page_title'] = trans('global.contact_us');
        $data['path_I'] = trans('global.contact_us');
        return view('pages.contactUs', $data);
    }

    public function send_Message(Request $request)
    {

        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'message' => 'required',
        ]);

        $msg = new ContactUS();
        $msg->name = $request->name;
        $msg->email = $request->email;
        $msg->message = $request->message;
        $msg->create_at = date('Y-m-d h:i:s');
        if ($msg->save()) {
            Session::flash('alert-class', 'alert-success');
            Session::flash('message', trans('alerts.success-message'));
        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('alerts.error-message'));
        }
        return back();
    }

}

<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\Auction;
use Illuminate\Http\Request;



class AuctionsController extends Controller {
    public function index() {

    }
    public function opened(){
        $data = [];
        $data['page_title']  = trans('global.Open_auctions');
        $data['items'] = Auction::where(['is_active' =>  1,'isWork'=>1 , 'provider_accept' => 1])->orderby('id', 'desc')->paginate(12);
        $data['path_I'] = trans('global.Open_auctions');
        return view('front.auction.index', $data);
    }
    public function closed(){
        $data = [];
        $data['page_title']  = trans('global.Closed_auctions');
        $data['items'] = Auction::where(['is_active' =>  1 ,'isWork'=>0, 'provider_accept' => 1])->orderby('id', 'desc')->paginate(12);
        $data['path_I'] = trans('global.Closed_auctions');
        return view('front.auction.index', $data);
    }
}
<?php

/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 04/05/18
 * Time: 01:53 ص
 */

namespace App\Http\Controllers\Members;

use App;
use Hash;
use Validator;

use App\Http\Controllers\Controller;
use App\Model\Auction;
use App\Model\AuctionFile;
use App\Model\AuctionImage;
use App\Model\AuctionOtherSpecification;
use App\Model\CarColor;
use App\Model\CarType;
use App\Model\Category;
use App\Model\EngineType;
use App\Model\Fuel;
use App\Model\Import;
use App\Model\Member;
use App\Model\MotionVector;
use App\Model\notifications;
use App\Model\OtherSpecification;
use App\Model\Specification;
use App\Model\SubCategory;
use App\Model\Bidding;
use App\Model\FollowAuction;
use App\Model\BankTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Input;

use Image;
class BidController extends Controller
{

    public function add()
    {

        $data = [];
        $data['page_title'] = trans('global.open-bid');
        $data['categories'] = Category::where('is_active', 1)->with('currentLanguage')->get();
        $data['car_colors'] = CarColor::where('is_active', 1)->with('currentLanguage')->get();
        $data['car_types'] = CarType::where('is_active', 1)->with('currentLanguage')->get();
        $data['engine_types'] = EngineType::where('is_active', 1)->with('currentLanguage')->get();
        $data['fuels'] = Fuel::where('is_active', 1)->with('currentLanguage')->get();
        $data['imports'] = Import::where('is_active', 1)->with('currentLanguage')->get();
        $data['motion_vectors'] = MotionVector::where('is_active', 1)->with('currentLanguage')->get();
        $data['specifications'] = Specification::where('is_active', 1)->with('currentLanguage')->get();
        $data['other_specifications'] = OtherSpecification::where('is_active', 1)->with('currentLanguage')->get();
        if (\Auth::guard('members')->user()->membership_type == 1) {
            $data['providers'] = Member::where('membership_type', 2)
                ->where('is_active', 1)->orderBy('first_name')->get();
        }

        if (!empty(old('category_id'))) {
            $sub_categories = SubCategory::where('is_active', 1)->where('category_id', old('category_id'))->with('currentLanguage')->get();
            $data['sub_categories'] = [];
            foreach ($sub_categories as $sub_category) {
                $data['sub_categories'][] = ['id' => $sub_category->id, 'title' => $sub_category->currentLanguage->title];
            }
        }
        return view('members.bid.add', $data);
    }

    public function addSave(Request $request)
    {
        $rules = [
            "bid_type" => ['required', Rule::in([1, 2])],
            "minimum_bid" => ['required', Rule::in(range(100, 3000, 100))],
            "bid_days" => ['required', Rule::in(range(1, 15))],
            "category_id" => ['required', Rule::exists((new Category())->getTable(), 'id')],
            "sub_category_id" => ['required', Rule::exists((new SubCategory())->getTable(), 'id')->where('category_id', $request->category_id)],
            "car_color_id" => ['required', Rule::exists((new CarColor())->getTable(), 'id')],
            "car_type_id" => ['required', Rule::exists((new CarType())->getTable(), 'id')],
            "engine_type_id" => ['required', Rule::exists((new EngineType())->getTable(), 'id')],
            "fuel_id" => ['required', Rule::exists((new Fuel())->getTable(), 'id')],
            "import_id" => ['required', Rule::exists((new Import())->getTable(), 'id')],
            "motion_vector_id" => ['required', Rule::exists((new MotionVector())->getTable(), 'id')],
            "specification_id" => ['required', Rule::exists((new Specification())->getTable(), 'id')],
            "manufacturing_year" => ['required', Rule::in(range(1980, date('Y')))],
            "form_years" => ['required', Rule::in(range(1, 3))],
            "mileage" => ['required'],
            "chassis_no" => ['required'],
            "engine_size" => ['required'],
            "title" => ['required'],
            "provider_id" => ['required', Rule::exists((new Member())->getTable(), 'id')->where('membership_type', 2)],
            "description" => ['required'],
            "images" => ['array','required','max:500'],
            "other_specifications" => ['array'],
            "files" => ['array','required','max:500']
        ];

        //dd($request->all());
        if (\Auth::guard('members')->user()->membership_type == 2) {
            unset($rules['provider_id']);
        }
        if (is_array($request->file('images')) && count($request->file('images')) > 0) {
            foreach ($request->file('images') as $k => $v) {
                $rules["images.$k"] = ['file', 'image'];
            }
        }

        if (is_array($request->file('files')) && count($request->file('files')) > 0) {
            foreach ($request->file('files') as $k => $v) {
                $rules["files.$k"] = ['file', 'mimes:jpeg,pdf,rar,zip,bmp,png,jpg'];
            }
        }
        if (is_array($request->other_specifications) && count($request->other_specifications) > 0) {
            foreach ($request->other_specifications as $k => $v) {
                $rules["other_specifications.$k"] = [Rule::exists((new OtherSpecification())->getTable(), 'id')];
            }
        }
        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }

        $auctest = Auction::where('chassis_no',$request->chassis_no)->where('manufacturing_year',$request->manufacturing_year)->where('category_id',$request->category_id)->where('sub_category_id',$request->sub_category_id)->first();
        if($auctest)
        {
            return redirect()->back()->with('alert-danger', 'هذة المناقصة موجودة من قبل')->withInput();
            
        }

        $file_upload = [];

        if (is_array($request->file('files')) && count($request->file('files')) > 0) {
            $i=0;
            foreach ($request->file('files') as $k => $r) {

                if ($r && $r->isValid()) {
                    $file_upload[$k] = $r->getClientOriginalName();
                    try {
                        $r->storeAs(AuctionFile::FILE_PATH, $file_upload[$k]);
                        ###
                        
                        $img = Image::make($request->file('files')[$i]);
                        $file = $request->file('files')[$i];
                        $filename1 = $file_upload[$k];
                        $img->save(public_path('/storage/auction_file/' . $filename1 ));
                        ###
                    } catch (\Exception $e) {
                        foreach ($file_upload as $l) {
                            \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
                        }
                        return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
                    }
                }
                $i++;
            }
        }

        $image_upload = [];
        if (is_array($request->file('images')) && count($request->file('images')) > 0) {
            $i=0;
            foreach ($request->file('images') as $k => $r) {
                if ($r && $r->isValid()) {
                    $image_upload[$k] = md5(time() . str_random(16)) . '.' . $r->clientExtension();
                    try {
                        $r->storeAs(AuctionImage::IMAGE_PATH, $image_upload[$k]);

                        $img = Image::make($request->file('images')[$i]);

                        // Insert the image above with the watermarked image, and center the watermark
                        $img->insert('http://www.saudibidz.com/beta/public/front/rtl/img/logo.png', 'center');
                        //$img->save(base_path() . '/public/images/gallery/pre/'.$upload->filename); 
                        ###
                        $img->resize(300, 300);
                        $file = $request->file('images')[$i];
                        $filename1 = $image_upload[$k];
                        $path = base_path() . '/storage/auction_image';
                        $img->save(public_path('/storage/auction_image/' . $filename1 ));
                        
                        //$file->move($path, $filename1);
                        ###

                        
                    } catch (\Exception $e) {
                        foreach ($image_upload as $l) {
                            \Storage::delete(AuctionImage::IMAGE_PATH . DIRECTORY_SEPARATOR . $l);
                        }
                        foreach ($file_upload as $l) {
                            \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
                        }
                        return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
                    }
                }
                $i++;
            }
        }
        foreach ($file_upload as $l) {
            \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
        }

        $row = new Auction();
        $row->bid_days = $request->bid_days;
        $row->title = $request->title;
        $row->description = $request->description;
        $row->category_id = $request->category_id;
        $row->sub_category_id = $request->sub_category_id;
        $row->car_color_id = $request->car_color_id;
        $row->car_type_id = $request->car_type_id;
        $row->engine_type_id = $request->engine_type_id;
        $row->fuel_id = $request->fuel_id;
        $row->import_id = $request->import_id;
        $row->motion_vector_id = $request->motion_vector_id;
        $row->specification_id = $request->specification_id;
        $row->manufacturing_year = $request->manufacturing_year;
        $row->engine_size = $request->engine_size;
        $row->vaild_form = (int)$request->vaild_form;
        $row->form_years = $request->form_years;
        $row->mileage = $request->mileage;
        $row->chassis_no = $request->chassis_no;
        $row->bid_type = $request->bid_type;
        if($request->bid_type == 2)
        {
            $row->approve_member = 0;
        }
        $row->minimum_bid = $request->minimum_bid;
        $row->form_years = $request->form_years;
        $row->member_id = \Auth::guard('members')->user()->id;
        $row->is_periodic_inspection = (int)$request->is_periodic_inspection;
        $row->car_price = $request->car_price;
        $row->isWork = 1;
        if (\Auth::guard('members')->user()->membership_type == 1) {
            unset($rules['provider_id']);
            $row->provider_id = $request->provider_id;
            $row->provider_accept = 0;
            $row->start_at = Carbon::now();
        } else {
            $row->provider_id = \Auth::guard('members')->user()->id;
            $row->provider_accept = 1;
            $row->is_active = 1;
            $row->start_at = Carbon::now();
        }
        \DB::beginTransaction();
        try {
            $row->save();
            $files = [];
            foreach ($file_upload as $k => $f) {
                $files[] = ['auction_id' => $row->id, 'order' => $k, 'file' => $f];
            }

            $images = [];
            foreach ($image_upload as $k => $f) {
                $images[] = ['auction_id' => $row->id, 'order' => $k, 'image' => $f];
            }
            if (count($files) > 0) {
                AuctionFile::insert($files);
            }

            if (count($images) > 0) {
                AuctionImage::insert($images);
            }
            $o = [];
            if (is_array($request->other_specifications) && count($request->other_specifications) > 0) {
                foreach ($request->other_specifications as $other_specification) {
                    $o[] = ['auction_id' => $row->id, 'other_specification_id' => $other_specification];
                }
            }
            if (count($o) > 0) {
                AuctionOtherSpecification::insert($o);
            }
            $success_message = trans('alerts.bid-add');
            if ($row->provider_accept == 0) {
                $success_message = trans('alerts.wait-approve');
            }
            \DB::commit();
            
            if (\Auth::guard('members')->user()->membership_type == 1) {
                $messa = $row->id." طلب موافقة بيع من خلال المعرض للمزايدة رقم  ";
      //  dd($messa);
                $note = new notifications();
                $note->userid = $request->provider_id;
                $note->type = 1;
                $note->auctionID = $row->id;
                $note->message = $messa;
                $note->created_at = Carbon::now();
                $note->save();
            }

            return redirect()->route('member-bid-list')
                ->with('alert-success', $success_message);
        } catch (\PDOException $e) {
            foreach ($image_upload as $l) {
                \Storage::delete(AuctionImage::IMAGE_PATH . DIRECTORY_SEPARATOR . $l);
            }
            foreach ($file_upload as $l) {
                \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
            }
            \DB::rollBack();
            return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
        }
    }

    public function myBids()
    {
        $data['page_title'] = trans('alerts.my-bids');
        $data['items'] = Auction::where('member_id', \Auth::guard('members')->user()->id)->orderby('id', 'desc')->paginate(6);
        $data['path_I'] = trans('alerts.my-bids');
        return view('members.bid.list', $data);
    }

    public function addbidding($info)
    {
        $data['page_title'] = trans('alerts.Auction');;
        $info = decrypt($info);
        //dd($info);
        $data['acuid'] = $info[0];
        $data['title'] = $info[1];
        $data['min'] = $info[2];
        //dd($data);
        $auct = Auction::where('id',$info[0])->where('is_active',1)->first();
        $lastBid = Bidding::where('auctionID',$info[0])->orderBy('id', 'desc')->first();
        if($auct)
        {
            if($lastBid)
            {
                $myBid = $lastBid->price + $auct->minimum_bid;
                $data['lastBid'] =  $lastBid->price;
            }
            else
            {
                $myBid = $auct->minimum_bid + $auct->car_price;
                $data['lastBid'] = $auct->car_price;
            }
        }
        $data['myBid'] = $myBid;
        $data['path_I'] = trans('alerts.Auction');
        return view('members.bid.addbidding', $data);
    }

    public function doaddbidding(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'price' => 'required|numeric',
        ]);
        $user = \Auth::guard('members')->user();
        $auctionID = $request->id;
        if($user->membership_type == 2)
         {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('alerts.member is provider he canot bid'));
         }
         if($user->membership_type == 1 && $user->member_id == $user->id)
         {
            
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('alerts.member is owner this auction'));
            
         }
            if($user && $user->is_active == 1)
            {
                $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                $total = 0;
                foreach($blances as $blance)
                {
                    if($blance->case_balance == 'deposited')
                    {
                        $total+=$blance->value;
                    }
                    elseif( $blance->case_balance == 'add_balance' || $blance->case_balance == 'Special Ads')
                    {
                        $total= $total;
                    }
                    else
                    {
                        $total-=$blance->value;
                    }
                }
                $auct = Auction::where('id',$request->id)->where('is_active',1)->first();
                $lastBid = Bidding::where('auctionID',$request->id)->orderBy('id', 'desc')->first();
                if($auct)
                {
                    if($lastBid)
                    {
                        $myBid = $lastBid->price + $auct->minimum_bid;
                    }
                    else
                    {
                        $myBid = $auct->minimum_bid + $auct->car_price;
                    }
                }
                else
                {
                    
                    Session::flash('alert-class', 'alert-danger');
                    Session::flash('message', trans('alerts.auction not active'));
                }
                

                $now = Carbon::now();
        
                $new = Carbon::parse($auct->start_at)->addDays($auct->bid_days);
                if($new >=$now)
                {
                    $au = Auction::find($auct->id);
                    $au->isWork = 1;
                    $au->save();
                }
                
                if($request->price >= $myBid && $new >=$now)
                {
                    $minimum_bid = $request->price/100;
                    if($total >= (5*$minimum_bid))
                    {
                        ///encrypt($request->id);
                        
                        $Bid = Bidding::where('auctionID',$auctionID)->where('memberID',$user->id)->first();
                        if($Bid)
                        {
                            $Bid->memberID = $user->id;
                            $Bid->auctionID = $auctionID;
                            $Bid->price = $request->price;
                            $Bid->created_at = date('Y-m-d h:i:s');
                            $Bid->save();
                        }
                        else
                        {
                            $Bid = new Bidding();
                            $Bid->memberID = $user->id;
                            $Bid->auctionID = $auctionID;
                            $Bid->price = $request->price;
                            $Bid->created_at = date('Y-m-d h:i:s');
                            $Bid->save();
                        }

                        
                        


                        $aucut = Auction::find($auct->id);
                        $aucut->total_bids = $aucut->total_bids + 1;
                        $aucut->current_price = $request->price;
                        $aucut->save();

                        $CountBids = $auct->total_bids;//Bidding::where('auctionID',$auctionID)->get()->count();
                        if ($CountBids%5 == 0) {
                           
                            $aucNot = Auction::find($auctionID);
                            ///$messa = $aucNot->title . "مزيدات علي مزادك ".$CountBids. " تم عمل ";
                            $messa =  " تم عمل ".$CountBids."مزيدات علي مزادك ".$aucNot->title;
                            $userNot = Member::find($aucNot->member_id);
                            $note = new notifications();
                            $note->userid = $aucNot->provider_id;
                            $note->type = 4;
                            $note->auctionID = $auctionID;
                            $note->message = $messa;
                            $note->created_at = Carbon::now();
                            $note->save();
                           
                        }

                        $bala = BankTransaction::where('auctionID',$auctionID)->where('userID',$user->id)->first();
                        if($bala)
                        {
                            $bala->date = date('Y-m-d h:i:s');
                            $bala->value = (5*$minimum_bid);//$request->price;
                            $bala->userID = $user->id;
                            $bala->type_balance = 'bidding';
                            $bala->case_balance = 'freezing';
                            $bala->type = 2;
                            $bala->auctionID = $auctionID;
                            $bala->save();
                        }
                        else
                        {
                            $bala = new BankTransaction;
                            $bala->date = date('Y-m-d h:i:s');
                            $bala->value = (5*$minimum_bid);//$request->price;
                            $bala->userID = $user->id;
                            $bala->type_balance = 'bidding';
                            $bala->case_balance = 'freezing';
                            $bala->type = 2;
                            $bala->auctionID = $auctionID;
                            $bala->save();
                        }
                        
                        Session::flash('alert-class', 'alert-success');
                        Session::flash('message', trans('alerts.secc-bidder'));
                    }
                    else
                    {
                       
                        Session::flash('alert-class', 'alert-danger');
                         Session::flash('message', trans('alerts.please add balance'));
                    }
                }
                else if($new < $now)
                {
                    Session::flash('alert-class', 'alert-danger');
                    Session::flash('message', trans('alerts.Auction is close'));
                }
                else
                {
                    Session::flash('alert-class', 'alert-danger');
                    Session::flash('message', trans('alerts.The :price least minimum_bid'));
                }
                
                
            }

        return redirect()->route('front-bid-detail', $auctionID);
    }


    /**
     *
     * @param type $id
     * @return type
     */
    public function followAuction($id)
    {
        $data['user_id'] = Auth::guard('members')->user()->id;
        $data['auction_id'] = $id;
        $find = FollowAuction::where(['auction_id' => $id])->where('user_id' ,\Auth::guard('members')->user()->id)->first();
        if ($find) {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('alerts.error-added'));
            return redirect()->route('all-follow');
        } else {
            $add = FollowAuction::Insert($data);
            Session::flash('alert-class', 'alert-success');
            Session::flash('message', trans('alerts.added-follow'));
            return redirect()->route('all-follow');
        }
    }

    /**
     *
     * @return type
     */
    public function allFollowAuction()
    {
        $follows = FollowAuction::select('auction_id')->where(['user_id' => \Auth::guard('members')->user()->id])->orderby('id', 'desc')->get()->toArray();

        $all = array();
        foreach ($follows as $follow) {
            $all[] = $follow['auction_id'];
        }
        $data['auctions'] = Auction::whereIn('id', $all)->get();
        $data['path_I'] = trans('alerts.my following');
        return view('members.following', $data);
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function deleteFollowAuction($id)
    {
        $delete = FollowAuction::where(['auction_id' => $id])->delete();
        if ($delete) {
            Session::flash('alert-class', 'alert-success');
            Session::flash('message', trans('alerts.success-delete'));
            return redirect()->route('all-follow');
        }
    }


    // approveAuction
    public function approveAuction($id)
    {
        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'provider_id' => \Auth::guard('members')->user()->id])->first();
        if ($Auction) {
            \DB::table('auctions')
                ->where('id', $id)
                ->update([
                    'provider_accept' => 1,
                    'is_active' => 1,
                    'start_at' => Carbon::now(),
                ]);
            \DB::table('notifications')
                ->where(['type' => 1, 'userid' => \Auth::guard('members')->user()->id, 'auctionID' => $id])
                ->update([
                    'action' => json_encode(
                        array(
                            'status' => 'car_approveed',
                        )
                    ),
                    'updated_at' => Carbon::now(),
                ]);
            $note = new notifications();
            $note->userid = $Auction->member_id;
            $note->type = 2;
            $note->auctionID = $id;
            $note->created_at = Carbon::now();
            $note->save();

            Session::flash('alert-class', 'alert-success');
            Session::flash('message', sprintf(trans('notifications.car_approveed'), $Auction->title));

        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
        return back();
    }

    //rejectAuction
    public function rejectAuction($id)
    {
        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'provider_id' => \Auth::guard('members')->user()->id, 'provider_accept' => 0])->first();

        if ($Auction) {
            \DB::table('auctions')
                ->where('id', $id)
                ->update(['provider_accept' => 0]);

            \DB::table('notifications')
                ->where(['type' => 1, 'userid' => \Auth::guard('members')->user()->id, 'auctionID' => $id])
                ->update([
                    'action' => json_encode(
                        array(
                            'status' => 'car_reject',
                        )
                    ),
                    'updated_at' => Carbon::now(),
                ]);
            $note = new notifications();
            $note->userid = $Auction->member_id;
            $note->type = 3;
            $note->auctionID = $id;
            $note->created_at = Carbon::now();
            $note->save();

            Session::flash('alert-class', 'alert-success');
            Session::flash('message', sprintf(trans('notifications.car_reject'), $Auction->title));

        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
        return back();
    }











    

    public function approveOwnerAuction($id)
    {
	
	


        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'member_id' => \Auth::guard('members')->user()->id])->first();
        
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',9)->where('userid',$Auction->member_id)->first();
            $now = Carbon::now();
            $new = Carbon::parse($notTest->created_at)->addDays(2);
            if($now > $new)
            {
                Session::flash('alert-class', 'alert-danger');
                Session::flash('message', 'وقت السماح انتهي');
            }
            if($notTest && $notTest->action !=null)
            {
                Session::flash('alert-class', 'alert-danger');
                Session::flash('message', 'تمت مشاهدتها من قبل');
            }
            if($notTest && $notTest->action == null)
            {
                \DB::table('auctions')
                ->where('id', $id)
                ->update([
                    'approve_member' => 1,
                    'approve_member_date' => Carbon::now(),
                ]);
                \DB::table('notifications')
                    ->where(['type' => 9, 'userid' => \Auth::guard('members')->user()->id, 'auctionID' => $id])
                    ->update([
                        'action' => 'owner_approveed',
                        'updated_at' => Carbon::now(),'view' => 1,
                    ]);

                $lastBid = Bidding::where('auctionID',$Auction->id)->where('price',$Auction->current_price)->first();
                

                $messa =   " تهانيناً ,, تم فوزك بالمناقصة رقم#" . $Auction->id ." ويرجي الدفع قبل 48 ساعة والا سيتم خسارة السيارة ومبلغ التأمين"; 
                
                $note = new notifications();
                $note->userid = $lastBid->memberID;
                $note->type = 5;
                $note->auctionID = $id;
                $note->message = $messa;
                $note->created_at = Carbon::now();
                $note->save();
                
                $userNot = Member::find($lastBid->memberID);
                
                Session::flash('alert-class', 'alert-success');
                Session::flash('message', sprintf(' تمت الموافقة بنجاح'));
            }
            
            

        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	
        
    }

    //rejectAuction
    public function rejectOwnerAuction($id)
    {
	

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'member_id' => \Auth::guard('members')->user()->id])->first();
            
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',9)->where('userid',$Auction->member_id)->first();
            $now = Carbon::now();
            $new = Carbon::parse($notTest->created_at)->addDays(2);
            if($now > $new)
            {
                Session::flash('alert-class', 'alert-danger');
                Session::flash('message', 'وقت السماح انتهي');
            }
            if($notTest && $notTest->action !=null)
            {
                Session::flash('alert-class', 'alert-danger');
                Session::flash('message', 'تمت مشاهدتها من قبل');
            }

            if($notTest && $notTest->action ==null)
            {
                \DB::table('auctions')
                ->where('id', $id)
                ->update([
                    'approve_member' => 0,
                    'approve_member_date' => Carbon::now(),
                ]);

                \DB::table('notifications')
                    ->where(['type' => 9, 'userid' => \Auth::guard('members')->user()->id, 'auctionID' => $id])
                    ->update([
                        'action' => 'owner_reject',
                        'updated_at' => Carbon::now(),'view' => 1,
                    ]);

                $lastBid = Bidding::where('auctionID',$Auction->id)->where('price',$Auction->current_price)->first();    
                $deleteAllLoserBidPrice = BankTransaction::where('auctionID',$Auction->id)->where('value',$Auction->current_price)->delete();
                $messa = " نأسف لحضرتك تم رفض البيع من المالك علي المزايدة رقم   " . $id;
                $messaM = " لاعادة نشر المزايدة رقم " . $id;
                
                $note = new notifications();
                $note->userid = $lastBid->memberID;
                $note->type = 7;
                $note->auctionID = $id;
                $note->message = $messa;
                $note->created_at = Carbon::now();
                $note->save();

                $note = new notifications();
                $note->userid = $Auction->member_id;
                $note->type = 10;
                $note->auctionID = $id;
                $note->message = $messaM;
                $note->created_at = Carbon::now();
                $note->save();
                
                $owner = Member::find($Auction->member_id);
                $user = Member::find($lastBid->memberID);

                

                Session::flash('alert-class', 'alert-success');
                Session::flash('message', sprintf(' تم رفض البيع', $Auction->title));

            }
            
        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	
    }



    public function republish($id)
    {
	    

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'member_id' => \Auth::guard('members')->user()->id])->first();
            
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',10)->where('userid',$Auction->member_id)->first();
            if($notTest && $notTest->action !=null)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'this auction is view previously']);
            }

            if($notTest && $notTest->action ==null)
            {
                    \DB::table('auctions')
                    ->where('id', $id)
                    ->update([
                        'is_active' => 1,
                        'total_bids'=>0,
                        'current_price'=>0,
                        'start_at' => Carbon::now(),
                        'isWork' => 1,
                    ]);

                    \DB::table('notifications')
                        ->where(['type' => 10, 'userid' => \Auth::guard('members')->user()->id, 'auctionID' => $id])
                        ->update([
                            'action' => 'republich',
                            'updated_at' => Carbon::now(),'view' => 1,
                        ]);

                    $deleteAllBids = Bidding::where('auctionID',$Auction->id)->delete(); 
                    
                   /// return response()->json(['status'=>(int)200 , 'message'=>' تم النشر'.$Auction->title]);

                    Session::flash('alert-class', 'alert-success');
                    Session::flash('message', ' تم النشر'.$Auction->title);

                
                
            }
            
        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	
    }







}

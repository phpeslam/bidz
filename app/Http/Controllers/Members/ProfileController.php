<?php
/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 04/05/18
 * Time: 01:53 ص
 */

namespace App\Http\Controllers\Members;

use App\Model\Member;
use App\Model\City;
use App\Model\Country;
use App\Model\BankTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    public function index()
    {
        $user = \Auth::guard('members')->user();
        $data = [];
        $data['user'] = $user;
            $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                $total = 0;
                foreach($blances as $blance)
                {
                    if($blance->case_balance == 'deposited')
                    {
                        $total+=$blance->value;
                    }
                    elseif( $blance->case_balance == 'add_balance' || $blance->case_balance == 'Special Ads')
                    {
                        $total= $total;
                    }
                    else
                    {
                        $total-=$blance->value;
                    }
                }
        $data['balance'] = $total;
        $data['page_title'] = trans('alerts.profile');
        $data['path_I'] = trans('alerts.profile');
        return view('members.profile',$data);
    }
    public function myBalance()
    {
        $user = \Auth::guard('members')->user();
        $data = [];
        $data['user'] = $user;
            $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                $total = 0;
                foreach($blances as $blance)
                {
                    if($blance->case_balance == 'deposited')
                    {
                        $total+=$blance->value;
                    }
                    elseif( $blance->case_balance == 'add_balance' || $blance->case_balance == 'Special Ads')
                    {
                        $total= $total;
                    }
                    else
                    {
                        $total-=$blance->value;
                    }
                }
        $data['balance'] = $total;
        $data['blances'] = $blances;
        $data['page_title'] = trans('alerts.profile');
        $data['path_I'] = trans('alerts.myBalance');
        return view('members.balance',$data);
    }
    public function edit()
    {
        $data = [];
        $data['page_title'] = trans('register.update_profile');
        $user =  Member::find(\Auth::guard('members')->user()->id);
        $data['countries'] = Country::where('is_active', 1)->with('currentLanguage')->get();
        $country_id = $user->country_id  ?? old('country_id');
        if ($country_id) {
            $cities = City::where('is_active', 1)->where('country_id', $country_id)->with('currentLanguage')->get();
            $data['cities'] = [];
            foreach ($cities as $city) {
                $data['cities'][] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
            }
        }
        $data['user'] = $user;
        $data['path_I'] = trans('alerts.edit');
        return view('members.editform',$data);
    }


    public function updateProfie(Request $request){
        if(isset($request->password) && !empty($request->password))
        {
            $rules = [
                'password' => 'required|min:5|max:20|confirmed',
            ];
        }
        if(isset($request->image) && !empty($request->image))
        {
            $rules = [
                'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            ];
        }
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address' => 'required|max:255',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City())->getTable() . ',id',
            'phone_number' => ['required'],
           // 'password' => 'required|min:5|max:20|confirmed',
            'email' => ['required', 'email'],
            
        ];

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        

        if ($request->hasFile('image')) {
            ###
            $user = Member::find(\Auth::guard('members')->user()->id);
            $file = $request->image;
            $filename = time() . '_' . $file->getClientOriginalName();
            $path = 'uploads/images';
            $file->move($path, $filename);
            ###
            $user->image = $path . '/' . $filename;
            $user->save();
        } 
        if(isset($request->password) && !empty($request->password))
        {
            $updatesProfile = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'phone_number' => $request->phone_number,
                'password' =>  \Hash::make($request->get('password')),
                'email' => $request->email,
            ];
        }
        else
        {
            $updatesProfile = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'phone_number' => $request->phone_number,
                'email' => $request->email,
            ];
        }
        \DB::table('members')
            ->where('id', \Auth::guard('members')->user()->id)
            ->update($updatesProfile);

            $user = \Auth::guard('members')->user();
            $data = [];
            $data['user'] = $user;
                $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                    $total = 0;
                    foreach($blances as $blance)
                    {
                        if($blance->case_balance == 'deposited')
                        {
                            $total+=$blance->value;
                        }
                        elseif( $blance->case_balance == 'add_balance' || $blance->case_balance == 'Special Ads')
                        {
                            $total= $total;
                        }
                        else
                        {
                            $total-=$blance->value;
                        }
                    }
            $data['balance'] = $total;
            $data['page_title'] = trans('alerts.profile');
            $data['path_I'] = trans('alerts.profile');
            
            Session::flash('alert-class', 'alert-success');
            Session::flash('message',  trans('alerts.updateprofile'));
            return view('members.profile',$data);
        return redirect()->route('member-profile-index')->with('alert-success', trans('alerts.registered'));

    }


    public function myNotifications(){
        $data = [];
        $data['notifications'] = \App\Model\notifications::where(['userid' => \Auth::guard('members')->user()->id])->orderby('id', 'desc')->get();
        $data['page_title'] = trans('notifications.page_title');
        \DB::table('notifications')
            ->where('userid', \Auth::guard('members')->user()->id)
            ->update(['view' => 1]);
        $data['path_I'] = trans('alerts.notifications');
        return view('members.notifications',$data);
    }
}
<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App;
use Hash;
use Validator;
use App\Model\Setting;
use App\Model\ContactUS;
use Session;
use Illuminate\Http\Request;

class ContactUSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = Setting::get();
        $data['setting'] = $page;
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function send_Message(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|max:255|email',
            'message' => 'required',
        ];
        
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        

        $msg = new ContactUS();
        $msg->name = $request->name;
        $msg->email = $request->email;
        $msg->message = $request->message;
        $msg->create_at = date('Y-m-d h:i:s');
        if ($msg->save()) {
            return response()->json(['status'=>(int)200 , 'data'=>trans('alerts.success-message')]);
        } else {
            return response()->json(['status'=>(int)400 , 'data'=>trans('alerts.error-message')]);
        }
    }

}

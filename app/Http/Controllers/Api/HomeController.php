<?php

/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 30/04/18
 * Time: 10:53 ص
 */

namespace App\Http\Controllers\Api;
use App;
use Hash;
use Validator;
use App\Http\Controllers\Controller;
use App\Model\AdsSlider;
use App\Model\Partner;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Model\City;
use App\Model\Country;
use App\Model\FollowAuction;
use App\Model\Auction;
use App\Model\AuctionFile;
use App\Model\AuctionImage;
use App\Model\AuctionOtherSpecification;
use App\Model\CarColor;
use App\Model\CarType;
use App\Model\Category;
use App\Model\EngineType;
use App\Model\Fuel;
use App\Model\Import;
use App\Model\Member;
use App\Model\MotionVector;
use App\Model\notifications;
use App\Model\OtherSpecification;
use App\Model\Specification;
use App\Model\SubCategory;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class HomeController extends Controller {

    public function getCountries(Request $request) {
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['countries'] = Country::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['countries'] = Country::where('is_active', 1)->with('enLang')->get();
        }
        

        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function getCities(Request $request) {
        $rules = [
            'lang'=>'required|in:ar,en',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['cities'] = City::where('is_active', 1)->where('country_id', $request->country_id)->with('arLang')->get();
        }
        else
        {
            $data['cities'] = City::where('is_active', 1)->where('country_id', $request->country_id)->with('enLang')->get();
        }
        

        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function getSliders(Request $request) {

        $now = Carbon::now();
        $sliders = AdsSlider::where(['is_active' => 1])->where('start_at','<=',$now)->where('end_at','>=',$now)->get();
        foreach($sliders as $slider)
        {
            $slider->auctionID = Auction::find($slider->auctionID);
            $slider->image = asset('/uploads/slider/'.$slider->image);
        }
        $data['sliders'] = $sliders;
        
        return response()->json(['status'=>(int)200 , 'data'=>$data]);

    }
    public function getAuctionsClose(Request $request) {

        $user = Member::where('token', $request->header('token'))->first();
        $aus = Auction::where('isWork', 0)->where('is_active', 1)->paginate(10);
        $auctions= [];
        foreach($aus as $au)
        {
            $au->member_id = Member::find($au->member_id);
            if($user)
            {
                $follow = FollowAuction::where('auction_id',$au->id)->where('user_id',$user->id)->first();
                if($follow)
                {
                    $au->isFollow = 1;
                }
                else
                {
                    $au->isFollow = 0;
                }
            }
            $auctions[]=$au;
        }
     
        $data['auctions'] = $auctions;

        return response()->json(['status'=>(int)200 , 'data'=>$data]);
        
    }
    public function getAuctionsOpen(Request $request) {
        $user = Member::where('token', $request->header('token'))->first();
        $aus = Auction::where('isWork', 1)->where('is_active', 1)->paginate(10);
        $auctions= [];
        foreach($aus as $au)
        {
            $au->member_id = Member::find($au->member_id);
            if($user)
            {
                $follow = FollowAuction::where('auction_id',$au->id)->where('user_id',$user->id)->first();
                if($follow)
                {
                    $au->isFollow = 1;
                }
                else
                {
                    $au->isFollow = 0;
                }
            }
            $auctions[]=$au;
        }
     
        $data['auctions'] = $auctions;

        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getAllAuctions(Request $request) {
        $user = Member::where('token', $request->header('token'))->first();
        $aus = Auction::where('is_active', 1)->paginate(10);
        $auctions= [];
        foreach($aus as $au)
        {
            $au->member_id = Member::find($au->member_id);
            if($user)
            {
                $follow = FollowAuction::where('auction_id',$au->id)->where('user_id',$user->id)->first();
                if($follow)
                {
                    $au->isFollow = 1;
                }
                else
                {
                    $au->isFollow = 0;
                }
            }
            $auctions[]=$au;
        }
     
        $data['auctions'] = $auctions;

        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function index(Request $request) {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        $user = Member::where('token', $request->header('token'))->first();
        $now = Carbon::now();
        $sliders = AdsSlider::where(['is_active' => 1])->where('start_at','<=',$now)->where('end_at','>=',$now)->get();
        $auctions = Auction::where(['is_active' => 1])->orderBy('id', 'desc')->get();
        $auctions2 = Auction::where('is_active',1)->get();
        $auctionsBlock = [];
        $auctionsWork = [];
        
        $auss = Auction::where('is_active',1)->get()->toArray();
        $pageI=0;
        if(isset($request->page)&&!empty($request->page))
        {
            $pageI = $request->page;
            $page = 1;
        }
        else
        {
            $page = 1;
        }
        
        
        foreach($sliders as $slider)
        {
            $slider->auctionID = Auction::find($slider->auctionID);
            $slider->image = asset('/uploads/slider/'.$slider->image);
        }
        foreach($auctions2 as $auc)
        {
            $new = Carbon::parse($auc->start_at)->addDays($auc->bid_days);
            if($new >=$now)
            {
                // $au = Auction::find($auc->id);
                // $au->isWork = 1;
                // $au->save();
                $auc->member_id = Member::find($auc->member_id);
                if($user)
                {
                    $follow = FollowAuction::where('auction_id',$auc->id)->where('user_id',$user->id)->first();
                    if($follow)
                    {
                        $auc->isFollow = 1;
                    }
                    else
                    {
                        $auc->isFollow = 0;
                    }
                }
                $auctionsWork[]=$auc;
            }
            else
            {
                // $au = Auction::find($auc->id);
                // $au->isWork = 0;
                // $au->save();
                $auc->member_id = Member::find($auc->member_id);
                if($user)
                {
                    $follow = FollowAuction::where('auction_id',$auc->id)->where('user_id',$user->id)->first();
                    if($follow)
                    {
                        $auc->isFollow = 1;
                    }
                    else
                    {
                        $auc->isFollow = 0;
                    }
                }
                $auctionsBlock[]=$auc;
            }
            
        }
        

        $partners = Partner::get();

        $data['sliders'] = $sliders;
        $data['partners'] = $partners;
        $data['auctionsClose'] = $auctionsBlock;
        $data['auctionsOpen'] = $auctionsWork;
        if($request->lang == 'ar')
        {
            $data['categories'] = Category::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['categories'] = Category::where('is_active', 1)->with('enLang')->get();
            
        }

        //$data['categories'] = Category::where('is_active', 1)->with('currentLanguage')->get();

        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function logout(Request $request) {
        if (!\Auth::guard('members')->check()) {
            return redirect()->route('front-home');
        }
        \Auth::guard('members')->logout();

        $request->session()->invalidate();

        return redirect()->route('front-home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bidDetail(Request $request) {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
            'bid_id' => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        $user =  Member::where('token',$request->header('token'))->first();
        
        
        if($request->lang=='ar')
        {
            $aut_detail = Auction::with([
                    'category' => function($q) {
                        $q->with('arLang');
                    },
                    'subCategory' => function($q) {
                        $q->with('arLang');
                    },
                    'carColor' => function($q) {
                        $q->with('arLang');
                    },
                    'carType' => function($q) {
                        $q->with('arLang');
                    },
                    'engineType' => function($q) {
                        $q->with('arLang');
                    },
                    'fuel' => function($q) {
                        $q->with('arLang');
                    },
                    'motionVector' => function($q) {
                        $q->with('arLang');
                    },
                    'specification' => function($q) {
                        $q->with('arLang');
                    },
                    'import' => function($q) {
                        $q->with('arLang');
                    },
                    'provider',
                    'otherSpecifications' => function($q) {
                        $q->with('arLang');
                    },
                    'auctionFiles',
                    'auctionImages',
                ])->where('id', $request->bid_id)->first();
        }
        else
        {
            $aut_detail = Auction::with([
                    'category' => function($q) {
                        $q->with('enLang');
                    },
                    'subCategory' => function($q) {
                        $q->with('enLang');
                    },
                    'carColor' => function($q) {
                        $q->with('enLang');
                    },
                    'carType' => function($q) {
                        $q->with('enLang');
                    },
                    'engineType' => function($q) {
                        $q->with('enLang');
                    },
                    'fuel' => function($q) {
                        $q->with('enLang');
                    },
                    'motionVector' => function($q) {
                        $q->with('enLang');
                    },
                    'specification' => function($q) {
                        $q->with('enLang');
                    },
                    'import' => function($q) {
                        $q->with('enLang');
                    },
                    'provider',
                    'otherSpecifications' => function($q) {
                        $q->with('enLang');
                    },
                    'auctionFiles',
                    'auctionImages',
                ])->where('id', $request->bid_id)->first();
        }

        if($user)
        {
            $auth_member = $user;
            if ($auth_member->membership_type==1 && $aut_detail->is_active == 0) {
                return response()->json(['status'=>(int)400 , 'error'=>'not found auction']);
            }
            $iam_owner = false;
            if ($auth_member->membership_type==1 && $aut_detail->is_active == 0 &&
                    $aut_detail->member_id != $auth_member->id
            ) {
                $iam_owner = true;
                return response()->json(['status'=>(int)400 , 'error'=>'not found auction']);
            }
            if ($auth_member->membership_type==1 && $iam_owner && ($auth_member && $aut_detail->is_active == 0 &&
                    $aut_detail->provider_id != $auth_member->id)
            ) {
                return response()->json(['status'=>(int)400 , 'error'=>'not found auction']);
            }
            $follow = FollowAuction::where('auction_id',$aut_detail->id)->where('user_id',$user->id)->first();
            if($follow)
            {$data['is_follow'] = 1;}
            else{$data['is_follow'] = 0;}

            \DB::table('auctions')
                ->where('id', $request->bid_id)
                ->update(['views' => ($aut_detail->views + 1)]);
                $aut_detail->provider_id = Member::find($aut_detail->provider_id);
                $aut_detail->member_id = Member::find($aut_detail->member_id);
                $aut_detail->member_id->image = asset($aut_detail->member_id->image);
                $aut_detail->provider_id->image = asset($aut_detail->provider_id->image);
                $data['page_title'] = $aut_detail->title;
               // dd($aut_detail['auction_files']);
               
                $data['aut_detail'] = $aut_detail;
               // dd($aut_detail->member_id->image);
               
                $data['aut_detail']['auctionFiles']['file'] = asset('storage/auction_file/'.$data['aut_detail']['auctionFiles']['file']);
               // dd($data['aut_detail']['auctionFiles']['file']);
               // $data['addedNew'] = Auction::where(['is_active' =>  1,'isWork' =>  1  , 'provider_accept' => 1])->inRandomOrder()->take(3)->get();
            return response()->json(['status'=>(int)200 , 'data'=>$data]);
            
        }
        
        if ($aut_detail->is_active == 0) {
            return response()->json(['status'=>(int)400 , 'error'=>'not found auction']);
        }
        $data['is_follow'] = 0;

        \DB::table('auctions')
                ->where('id', $request->bid_id)
                ->update(['views' => ($aut_detail->views + 1)]);
        $aut_detail->provider_id = Member::find($aut_detail->provider_id);
        $aut_detail->member_id = Member::find($aut_detail->member_id);
        $aut_detail->member_id->image = asset($aut_detail->member_id->image);
        $aut_detail->provider_id->image = asset($aut_detail->provider_id->image);
        //dd($aut_detail->member_id);
        $data['page_title'] = $aut_detail->title;
        $data['aut_detail'] = $aut_detail;
        $data['aut_detail']['auctionFiles']['file'] = asset('storage/auction_file/'.$data['aut_detail']['auctionFiles']['file']);
        $data['addedNew'] = Auction::where(['is_active' =>  1 ,'isWork' =>  1 , 'provider_accept' => 1])->inRandomOrder()->take(3)->get();
      return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function subCategories() {
        $category_id = (int) \Request::input('category_id');
        $c = SubCategory::where('is_active', 1)->where('category_id', $category_id)->with('currentLanguage')->get();
        $ci = [];
        foreach ($c as $item) {
            $ci[] = ['id' => $item->id, 'title' => $item->currentLanguage->title];
        }

        return response()->json($ci);
    }

    public function search(Request $request) {
        if (isset($request->action_type) && !empty($request->action_type) && $request->action_type != -1) {
            $rules = [
                'action_type'=>'in:0,1',
            ];
            App::setLocale('ar');
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}

        }
        $user = Member::where('token', $request->header('token'))->first();
        $items  = Auction::query();
       // var_dump($request->action_type);die();
        if ($request->action_type ==0 || $request->action_type==1) {
                                
            $items = $items->where('isWork', '=', $request->action_type);
        }
        $items = $items->where('is_active', 1);
        
        if (isset($request->bid_type) && !empty($request->bid_type)) {
            $items = $items->where('bid_type', '=', $request->bid_type);
        }
        if (isset($request->category_id) && !empty($request->category_id) && $request->category_id != '') {
            $rules = [
                'category_id'  => 'exists:categories,id',
            ];
             $messages = [
                'category_id.exists'    => 'The selected :category_id is invalid.' ,
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
            
            $items = $items->where('category_id', '=', $request->category_id);
        }
        if ((isset($request->minimum_bid) && !empty($request->minimum_bid)) && (isset($request->max_bid) && !empty($request->max_bid))) {
            $rules = [
            'minimum_bid'=>'after:max_bid',
            'max_bid' => 'before:minimum_bid',
            ];
            App::setLocale('ar');
            $validator = Validator::make($request->all(), $rules);
            if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        }
        if (isset($request->minimum_bid) && !empty($request->minimum_bid)) {
            $items = $items->where('minimum_bid', '>=', $request->minimum_bid);
        }
        if (isset($request->max_bid) && !empty($request->max_bid)) {
            $items = $items->where('minimum_bid', '<=', $request->max_bid);
        }
        if ((isset($request->manufacturing_year_from) && !empty($request->manufacturing_year_from)) && (isset($request->manufacturing_year_to) && !empty($request->manufacturing_year_to))) {
            $rules = [
                'manufacturing_year_from'=>'after:manufacturing_year_to',
                'manufacturing_year_to' => 'before:manufacturing_year_from',
                ];
                App::setLocale('ar');
                $validator = Validator::make($request->all(), $rules);
                if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        }
        if (isset($request->manufacturing_year_from) && !empty($request->manufacturing_year_from)) {
            $items =  $items->where('manufacturing_year', '>=', $request->manufacturing_year_from);
        }
        if (isset($request->manufacturing_year_to) && !empty($request->manufacturing_year_to)) {
            $items->where('manufacturing_year', '<=', $request->manufacturing_year_to);
        }
        if (isset($request->sort) && !empty($request->sort)) {
            $items =  $items->orderBy('id', $request->sort);
        }
        $items = $items->paginate(10);

        $itemsI=[];                
        if($user)
        {
            foreach($items as $auc)
            {
                $follow = FollowAuction::where('auction_id',$auc->id)->where('user_id',$user->id)->first();
                if($follow)
                {
                    $auc->isFollow = 1;
                    $auc->member_id = Member::find($auc->member_id);
                    $itemsI[]=$auc;
                }
                else
                {
                    $auc->isFollow = 0;
                    $auc->member_id = Member::find($auc->member_id);
                    $itemsI[]=$auc;
                }
            }
            $data['items']['data'] = $itemsI;
            return response()->json(['status'=>(int)200 , 'data'=>$data]);
        }
        foreach($items as $auc)
        {
            $auc->member_id = Member::find($auc->member_id);
            $itemsI[]=$auc;  
        }
        $data['items']['data'] = $itemsI;
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

}

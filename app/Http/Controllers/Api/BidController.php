<?php

/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 04/05/18
 * Time: 01:53 ص
 */

namespace App\Http\Controllers\Api;
use App;
use Hash;
use Validator;

use App\Http\Controllers\Controller;
use App\Model\Auction;
use App\Model\AuctionFile;
use App\Model\AuctionImage;
use App\Model\AuctionOtherSpecification;
use App\Model\CarColor;
use App\Model\CarType;
use App\Model\Category;
use App\Model\EngineType;
use App\Model\Fuel;
use App\Model\Import;
use App\Model\Member;
use App\Model\MotionVector;
use App\Model\notifications;
use App\Model\OtherSpecification;
use App\Model\Specification;
use App\Model\SubCategory;
use App\Model\Bidding;
use App\Model\FollowAuction;
use App\Model\BankTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Moyasarphpanonymouscom\MoyasarApi\MoyasarFaced;
//use App\Http\Controllers\Api\test\moyser123;
//use Moyasar;
use Image;
class BidController extends Controller
{
    private $pay_I;

    // public function __construct(moyser123 $x)
    // {
    //     parent::__construct();
    //     $this->pay_I = $x;
    // }
    public function add()
    {

        $data = [];
        $data['page_title'] = trans('global.open-bid');
        $data['categories'] = Category::where('is_active', 1)->with('currentLanguage')->get();
        $data['car_colors'] = CarColor::where('is_active', 1)->with('currentLanguage')->get();
        $data['car_types'] = CarType::where('is_active', 1)->with('currentLanguage')->get();
        $data['engine_types'] = EngineType::where('is_active', 1)->with('currentLanguage')->get();
        $data['fuels'] = Fuel::where('is_active', 1)->with('currentLanguage')->get();
        $data['imports'] = Import::where('is_active', 1)->with('currentLanguage')->get();
        $data['motion_vectors'] = MotionVector::where('is_active', 1)->with('currentLanguage')->get();
        $data['specifications'] = Specification::where('is_active', 1)->with('currentLanguage')->get();
        $data['other_specifications'] = OtherSpecification::where('is_active', 1)->with('currentLanguage')->get();
        if ($user->membership_type == 1) {
            $data['providers'] = Member::where('membership_type', 2)
                ->where('is_active', 1)->orderBy('first_name')->get();
        }

        if (!empty(old('category_id'))) {
            $sub_categories = SubCategory::where('is_active', 1)->where('category_id', old('category_id'))->with('currentLanguage')->get();
            $data['sub_categories'] = [];
            foreach ($sub_categories as $sub_category) {
                $data['sub_categories'][] = ['id' => $sub_category->id, 'title' => $sub_category->currentLanguage->title];
            }
        }
        return view('members.bid.add', $data);
    }
    public function getBidType(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        App::setLocale($request->lang);
        
        $data['BidType'] = [['id'=>1,'title'=>trans('bid.select_bid_type_1')],['id'=>2,'title'=>trans('bid.select_bid_type_2')]];
        
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getCategories(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['categories'] = Category::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['categories'] = Category::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getSubCategories(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['SubCategory'] = SubCategory::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['SubCategory'] = SubCategory::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getSubCategory(Request $request)
    {$request->lang = 'ar';
        $rules = [
            "category_id" => ['required', Rule::exists((new Category())->getTable(), 'id')],
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['SubCategory'] = SubCategory::where('is_active', 1)->where('category_id', $request->category_id)->with('arLang')->get();
        }
        else
        {
            $data['SubCategory'] = SubCategory::where('is_active', 1)->where('category_id', $request->category_id)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getCarColor(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['CarColor'] = CarColor::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['CarColor'] = CarColor::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getCarType(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['CarType'] = CarType::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['CarType'] = CarType::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getEngineType(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['EngineType'] = EngineType::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['EngineType'] = EngineType::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getFuel(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['Fuel'] = Fuel::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['Fuel'] = Fuel::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getImport(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['Import'] = Import::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['Import'] = Import::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getMotionVector(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['MotionVector'] = MotionVector::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['MotionVector'] = MotionVector::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getSpecification(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['Specification'] = Specification::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['Specification'] = Specification::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function getOtherSpecification(Request $request)
    {$request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $data['OtherSpecification'] = OtherSpecification::where('is_active', 1)->with('arLang')->get();
        }
        else
        {
            $data['OtherSpecification'] = OtherSpecification::where('is_active', 1)->with('enLang')->get();
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getProviders(Request $request)
    {
       // $data['Providers'] = Member::where('membership_type', 2)->get();
        $data['Providers'] = Member::where('membership_type', 2)
                ->where('is_active', 1)->orderBy('first_name')->get();
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    public function getAuction(Request $request)
    {
        $rules = [
            "auction_id" => ['required', Rule::exists((new Auction())->getTable(), 'id')],
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        $data['auction'] =  Auction::where('id',$request->auction_id)->first();
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }


    public function addSave(Request $request)
    {
        $rules = [
            "bid_type" => ['required', Rule::in([1, 2])],
            "minimum_bid" => ['required', Rule::in(range(100, 3000, 100))],
            "bid_days" => ['required', Rule::in(range(1, 15))],
            "category_id" => ['required', Rule::exists((new Category())->getTable(), 'id')],
            //"sub_category_id" => ['required', Rule::exists((new SubCategory())->getTable(), 'id')->where('category_id', $request->category_id)],
            "car_color_id" => ['required', Rule::exists((new CarColor())->getTable(), 'id')],
            "car_type_id" => ['required', Rule::exists((new CarType())->getTable(), 'id')],
            "engine_type_id" => ['required', Rule::exists((new EngineType())->getTable(), 'id')],
            "fuel_id" => ['required', Rule::exists((new Fuel())->getTable(), 'id')],
            "import_id" => ['required', Rule::exists((new Import())->getTable(), 'id')],
            "motion_vector_id" => ['required', Rule::exists((new MotionVector())->getTable(), 'id')],
            "specification_id" => ['required', Rule::exists((new Specification())->getTable(), 'id')],
            "manufacturing_year" => ['required', Rule::in(range(1980, date('Y')))],
            
            "form_years" => ['required', Rule::in(range(0, 3))],
            "mileage" => ['required'],
            "chassis_no" => ['required'],
            "engine_size" => ['required'],
            "title" => ['required'],
            "car_price" => ['required'],
            "provider_id" => ['required', Rule::exists((new Member())->getTable(), 'id')->where('membership_type', 2)],
            "description" => ['required'],
           // "images" => ['array','required'],
            //"other_specifications" => ['array'],
           // "files" => ['array','required']
            "images" => ['required','max:500'],
            "files" => ['required','max:500']
        ];

        $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
		if ($user->membership_type == 2) {
		    unset($rules['provider_id']);
		}
	    }
        else{
                return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
            }

        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}

        if(isset($request->sub_category_id) && !empty($request->sub_category_id) && $request->sub_category_id != null)
        {
            $subCategory = SubCategory::where('id',$request->sub_category_id)->where('category_id', $request->category_id)->first();
            if(!$subCategory)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'sub_category_id donot exist']);
            }
        }
        $auctest = Auction::where('chassis_no',$request->chassis_no)->where('manufacturing_year',$request->manufacturing_year)->where('category_id',$request->category_id)->where('sub_category_id',$request->sub_category_id)->first();
        if($auctest)
        {
            return response()->json(['status'=>(int)400 , 'error'=>'هذة المناقصة موجودة من قبل']);
        }

        
	
        
        if (is_array($request->file('images')) && count($request->file('images')) > 0) {
            foreach ($request->file('images') as $k => $v) {
                $rules["images.$k"] = ['file', 'image'];
            }
        }

        if (is_array($request->file('files')) && count($request->file('files')) > 0) {
            foreach ($request->file('files') as $k => $v) {
                $rules["files.$k"] = ['file', 'mimes:jpeg,pdf,rar,zip,bmp,png,jpg'];
            }
        }
        if (is_array($request->other_specifications) && count($request->other_specifications) > 0) {
            foreach ($request->other_specifications as $k => $v) {
                $rules["other_specifications.$k"] = [Rule::exists((new OtherSpecification())->getTable(), 'id')];
            }
        }
	    App::setLocale('ar');
	    


        $file_upload = [];
        if (is_array($request->file('files')) && count($request->file('files')) > 0) {
            $i=0;
            foreach ($request->file('files') as $k => $r) {

                if ($r && $r->isValid()) {
                    $file_upload[$k] = $r->getClientOriginalName();
                    try {
                        $r->storeAs(AuctionFile::FILE_PATH, $file_upload[$k]);
                        ###

                        $img = Image::make($request->file('files')[$i]);
                        $file = $request->file('files')[$i];
                        $filename1 = $file_upload[$k];
                        $img->save(public_path('/storage/auction_file/' . $filename1 ));
                        ###
                    } catch (\Exception $e) {
                        foreach ($file_upload as $l) {
                            \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
                        }
                        return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
                    }
                }
                $i++;
            }
        }

        $image_upload = [];
        if (is_array($request->file('images')) && count($request->file('images')) > 0) {
            $i=0;
            foreach ($request->file('images') as $k => $r) {
                if ($r && $r->isValid()) {
                    $image_upload[$k] =md5(time() . str_random(16)) . '.' . $r->clientExtension();
                    try {
                        $r->storeAs(AuctionImage::IMAGE_PATH, $image_upload[$k]);


                        $img = Image::make($request->file('images')[$i]);

                        // Insert the image above with the watermarked image, and center the watermark
                        $img->insert('http://www.saudibidz.com/beta/public/front/rtl/img/logo.png', 'center');
                        //$img->save(base_path() . '/public/images/gallery/pre/'.$upload->filename); 
                        ###
                        $img->resize(300, 300);
                        $file = $request->file('images')[$i];
                        $filename1 = $image_upload[$k];
                        $path = base_path() . '/storage/auction_image';
                        $img->save(public_path('/storage/auction_image/' . $filename1 ));

                    } catch (\Exception $e) {
                        foreach ($image_upload as $l) {
                            \Storage::delete(AuctionImage::IMAGE_PATH . DIRECTORY_SEPARATOR . $l);
                        }
                        foreach ($file_upload as $l) {
                            \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
                        }
                        return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
                    }
                }
                $i++;
            }
        }
        foreach ($file_upload as $l) {
            \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
        }

        $row = new Auction();
        
        $row->bid_days = $request->bid_days;
        $row->title = $request->title;
        $row->description = $request->description;
        $row->category_id = $request->category_id;
        $row->sub_category_id = $request->sub_category_id;
        $row->car_color_id = $request->car_color_id;
        $row->car_type_id = $request->car_type_id;
        $row->engine_type_id = $request->engine_type_id;
        $row->fuel_id = $request->fuel_id;
        $row->import_id = $request->import_id;
        $row->motion_vector_id = $request->motion_vector_id;
        $row->specification_id = $request->specification_id;
        $row->manufacturing_year = $request->manufacturing_year;
        $row->engine_size = $request->engine_size;
        $row->form_years = $request->form_years;
        $row->mileage = $request->mileage;
        $row->chassis_no = $request->chassis_no;
        $row->bid_type = $request->bid_type;
        if($request->bid_type == 2)
        {
            $row->approve_member = 0;
        }
        $row->minimum_bid = $request->minimum_bid;
        if($request->form_years == 0)
        {
            $row->vaild_form = 0;
        }
        else
        {
            $row->vaild_form = 1;
        }
        
        $row->member_id = $user->id;
        $row->is_periodic_inspection = (int)$request->is_periodic_inspection;
        $row->car_price = $request->car_price;
        $row->isWork = 1;
        if ($user->membership_type == 1) {
            unset($rules['provider_id']);
            $row->provider_id = $request->provider_id;
            $row->provider_accept = 0;
            $row->start_at = Carbon::now();
        } else {
            $row->provider_id = $user->id;
            $row->is_active = 1;
            $row->provider_accept = 1;
            $row->start_at = Carbon::now();
        }
        \DB::beginTransaction();
        try {
            $row->save();
            $files = [];
            foreach ($file_upload as $k => $f) {
                $files[] = ['auction_id' => $row->id, 'order' => $k, 'file' => $f];
            }

            $images = [];
            foreach ($image_upload as $k => $f) {
                $images[] = ['auction_id' => $row->id, 'order' => $k, 'image' => $f];
            }
            if (count($files) > 0) {
                AuctionFile::insert($files);
            }

            if (count($images) > 0) {
                AuctionImage::insert($images);
            }
            $o = [];
            if (is_array($request->other_specifications) && count($request->other_specifications) > 0) {
                foreach ($request->other_specifications as $other_specification) {
                    $o[] = ['auction_id' => $row->id, 'other_specification_id' => $other_specification];
                }
            }
            if (count($o) > 0) {
                AuctionOtherSpecification::insert($o);
            }
            $success_message = trans('alerts.bid-add');
            if ($row->provider_accept == 0) {
                $success_message = trans('alerts.wait-approve');
            }
            \DB::commit();
            if ($user->membership_type == 1) {
                $messa = " طلب موافقة بيع من خلال المعرض للمزايدة رقم  " . $row->id;
                
                $note = new notifications();
                $note->userid = $request->provider_id;
                $note->type = 1;
                $note->auctionID = $row->id;
                $note->message = $messa;
                $note->created_at = Carbon::now();
                $note->save();

                
                $userNot = Member::find($request->provider_id);
                $this->sendGCM($messa,$userNot->fcm_token);
                return response()->json(['status'=>(int)200]);
            }

            return response()->json(['status'=>(int)200]);
        } catch (\PDOException $e) {
            foreach ($image_upload as $l) {
                \Storage::delete(AuctionImage::IMAGE_PATH . DIRECTORY_SEPARATOR . $l);
            }
            foreach ($file_upload as $l) {
                \Storage::delete(AuctionFile::FILE_PATH . DIRECTORY_SEPARATOR . $l);
            }
            \DB::rollBack();
            return response()->json(['status'=>(int)400 , 'error'=>$e->getMessage()]);
        }
    }

    public function myBids(Request $request)
    {
	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
        $auctions=[];
        //provider_accept
        $aus = Auction::where('member_id', $user->id)->where('is_active', 1)->where('provider_accept', 1)->orderby('id', 'desc')->paginate(10);
        foreach($aus as $au)
        {
            $au->member_id = Member::find($au->member_id);
            $follow = FollowAuction::where('auction_id',$au->id)->where('user_id',$user->id)->first();
            if($follow)
            {
                $au->isFollow = 1;
            }
            else
            {
                $au->isFollow = 0;
            }
            $auctions[] = $au;
        }
        if(isset($request->page)&&!empty($request->page))
        {
            $page = $request->page;
        }
        else
        {
            $page = 1;
        }
        //$auctions = $this->paginate($auctions,15,$page);
        $data['auctions'] = $auctions;
        
        $data['items'] = $auctions;
        return response()->json(['status'=>(int)200,'data'=>$data]);	
        }
        else
        {
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }

    public function addbidding($info)
    {
        $data['page_title'] = trans('alerts.Auction');;
        $info = decrypt($info);
        $data['acuid'] = $info[0];
        $data['title'] = $info[1];
        $data['min'] = $info[2];
        //dd($data);
        return view('members.bid.addbidding', $data);
    }

    public function doaddbidding(Request $request)
    {
        $rules = [
            'auction_id'  => 'required|exists:auctions,id',
            'price' => 'required|numeric',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}


         $user = Member::where('token',$request->header('token'))->where('is_active',1)->where('is_active_phone',1)->where('is_active',1)->first();
         if($user->membership_type == 2)
         {
            return response()->json(['status'=>(int)400 , 'error'=>'member is provider he canot bid.']);
         }
         if($user->membership_type == 1 && $user->member_id == $user->id)
         {
            return response()->json(['status'=>(int)400 , 'error'=>'member is owner this auction.']);
         }
            if($user)
            {
                $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                $total = 0;
                foreach($blances as $blance)
                {
                    if($blance->case_balance == 'deposited')
                    {
                        $total+=$blance->value;
                    }
                    elseif( $blance->case_balance == 'add_balance' || $blance->case_balance == 'Special Ads')
                    {
                        $total= $total;
                    }
                    else
                    {
                        $total-=$blance->value;
                    }
                }
                $auct = Auction::where('id',$request->auction_id)->where('is_active',1)->first();
                $lastBid = Bidding::where('auctionID',$request->auction_id)->orderBy('id', 'desc')->first();
                if($auct)
                {
                    if($lastBid)
                    {
                        $myBid = $lastBid->price + $auct->minimum_bid;
                    }
                    else
                    {
                        $myBid = $auct->minimum_bid + $auct->car_price;
                    }
                }
                else
                {
                    return response()->json(['status'=>(int)400 , 'error'=>'auction not active.']);
                }
                

                $now = Carbon::now();
        
                $new = Carbon::parse($auct->start_at)->addDays($auct->bid_days);
               // dd($now,$new);
                if($new >=$now)
                {
                    $au = Auction::find($auct->id);
                    $au->isWork = 1;
                    $au->save();
                }
                
                if($request->price >= $myBid && $new >=$now)
                {
                    $minimum_bid = $request->price/100;
                    if($total >= (5*$minimum_bid))
                    {
                        $auctionID = $request->auction_id;///encrypt($request->id);
                        
                        $Bid = Bidding::where('auctionID',$auctionID)->where('memberID',$user->id)->first();
                        if($Bid)
                        {
                            $Bid->memberID = $user->id;
                            $Bid->auctionID = $auctionID;
                            $Bid->price = $request->price;
                            $Bid->created_at = date('Y-m-d h:i:s');
                            $Bid->save();
                        }
                        else
                        {
                            $Bid = new Bidding();
                            $Bid->memberID = $user->id;
                            $Bid->auctionID = $auctionID;
                            $Bid->price = $request->price;
                            $Bid->created_at = date('Y-m-d h:i:s');
                            $Bid->save();
                        }
                        


                        $aucut = Auction::find($auctionID);
                        $aucut->total_bids = $aucut->total_bids + 1;
                        $aucut->current_price = $request->price;
                        $aucut->save();

                        $CountBids = $auct->total_bids;//Bidding::where('auctionID',$auctionID)->get()->count();
                        if ($CountBids%5 == 0) {
                           
                            $aucNot = Auction::find($auctionID);
                            ///$messa = $aucNot->title . "مزيدات علي مزادك ".$CountBids. " تم عمل ";
                            $messa =  " تم عمل ".$CountBids."مزيدات علي مزادك ".$aucNot->title;
                            $userNot = Member::find($aucNot->member_id);
                            $note = new notifications();
                            $note->userid = $aucNot->provider_id;
                            $note->type = 4;
                            $note->auctionID = $auctionID;
                            $note->message = $messa;
                            $note->created_at = Carbon::now();
                            $note->save();
                           
                            $this->sendGCM($messa,$userNot->fcm_token);
                        }

                        $bala = BankTransaction::where('auctionID',$auctionID)->where('userID',$user->id)->first();
                        if($bala)
                        {
                            $bala->date = date('Y-m-d h:i:s');
                            $bala->value = (5*$minimum_bid);//$request->price;
                            $bala->userID = $user->id;
                            $bala->type_balance = 'bidding';
                            $bala->case_balance = 'freezing';
                            $bala->type = 2;
                            $bala->auctionID = $auctionID;
                            $bala->save();
                        }
                        else
                        {
                            $bala = new BankTransaction;
                            $bala->date = date('Y-m-d h:i:s');
                            $bala->value = (5*$minimum_bid);//$request->price;
                            $bala->userID = $user->id;
                            $bala->type_balance = 'bidding';
                            $bala->case_balance = 'freezing';
                            $bala->type = 2;
                            $bala->auctionID = $auctionID;
                            $bala->save();
                        }

                        return response()->json(['status'=>(int)200,'message'=>'success bid']);
                    }
                    else
                    {
                        return response()->json(['status'=>(int)400 , 'error'=>'please add balance.']);
                    }
                }
                else if($new < $now)
                {
                    return response()->json(['status'=>(int)400 , 'error'=>'Auction is close.']);
                }
                else
                {
                    return response()->json(['status'=>(int)400 , 'error'=>'The :price least minimum_bid.']);
                }
                
                
            }
            else{
                return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
            }

    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function followAuction(Request $request)
    {
	    $rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
	    $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
        $data['user_id'] = $user->id;
        $data['auction_id'] = $request->auction_id;
        $find = FollowAuction::where(['auction_id' => $request->auction_id])->where(['user_id' =>$user->id])->first();
        if ($find) {
            return response()->json(['status'=>(int)200,'data'=>$data,'message'=>' follow auction']);
        } else {
            $add = FollowAuction::Insert($data);
            return response()->json(['status'=>(int)200,'data'=>$data,'message'=>' follow auction']);
        }
	    }
	else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }

    /**
     *
     * @return type
     */
    public function allFollowAuction(Request $request)
    {
	
	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
        $follows = FollowAuction::select('auction_id')->where(['user_id' => $user->id])->orderby('id', 'desc')->get()->toArray();

        $all = array();
        foreach ($follows as $follow) {
            $all[] = $follow['auction_id'];
        }
        $auctions=[];
        
        $aus = Auction::whereIn('id', $all)->paginate(10);
        foreach($aus as $au)
        {
            $au->member_id = Member::find($au->member_id);
            $au->isFollow=1;
            $auctions[] = $au;
        }
        if(isset($request->page)&&!empty($request->page))
        {
            $page = $request->page;
        }
        else
        {
            $page = 1;
        }
       // $auctions = $this->paginate($auctions,15,$page);
        $data['auctions'] = $auctions;
	return response()->json(['status'=>(int)200,'data'=>$data]);
        }
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function deleteFollowAuction(Request $request)
    {
	$rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}

	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
        $delete = FollowAuction::where(['auction_id' => $request->auction_id])->where(['user_id' =>$user->id])->delete();
        if ($delete) {
            return response()->json(['status'=>(int)200,'message'=>'delete follow']);
        }
	}
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }


    // approveAuction
    public function approveAuction(Request $request)
    {
	$id = $request->auction_id;
	$rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
	$id = $request->auction_id;
	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'provider_id' => $user->id])->first();
        
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',2)->where('userid',$Auction->member_id)->first();
            if($notTest)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'this auction is view previously']);
            }
            \DB::table('auctions')
                ->where('id', $id)
                ->update([
                    'provider_accept' => 1,
                    'is_active' => 1,
                    'start_at' => Carbon::now(),
                ]);
            \DB::table('notifications')
                ->where(['type' => 1, 'userid' => $user->id, 'auctionID' => $id])
                ->update([
                    'action' => 'car_approveed',
                    'updated_at' => Carbon::now(),'view' => 1,
                ]);
            $messa = " تهانينا تم الموافقة علي مزايدتك رقم " . $id;    
            
            $note = new notifications();
            $note->userid = $Auction->member_id;
            $note->type = 2;
            $note->auctionID = $id;
            $note->message = $messa;
            $note->created_at = Carbon::now();
            $note->save();
            
            $userNot = Member::find($Auction->member_id);
            
            $this->sendGCM($messa,$userNot->fcm_token);

	
            return response()->json(['status'=>(int)200 , 'message'=>$messa]);
            

        } else {
		return response()->json(['status'=>(int)400 , 'error'=>'no_aucation']);
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	}
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
        
    }

    //rejectAuction
    public function rejectAuction(Request $request)
    {
	$id = $request->auction_id;
	$rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
	$id = $request->auction_id;
	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'provider_id' => $user->id, 'provider_accept' => 0])->first();
            
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',3)->where('userid',$Auction->member_id)->first();
            if($notTest)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'this auction is view previously']);
            }

            \DB::table('auctions')
                ->where('id', $id)
                ->update(['provider_accept' => 0]);

            \DB::table('notifications')
                ->where(['type' => 1, 'userid' => $user->id, 'auctionID' => $id])
                ->update([
                    'action' => 'car_reject',
                    'updated_at' => Carbon::now(),'view' => 1,
                ]);
            $messa = " نأسف لحضرتك تم رفض مزايدتك رقم  " .$id;
            
            $note = new notifications();
            $note->userid = $Auction->member_id;
            $note->type = 3;
            $note->auctionID = $id;
            $note->message = $messa;
            $note->created_at = Carbon::now();
            $note->save();
            
            $userNot = Member::find($Auction->member_id);
            $this->sendGCM($messa,$userNot->fcm_token);
		return response()->json(['status'=>(int)200 , 'message'=>$messa]);

            Session::flash('alert-class', 'alert-success');
            Session::flash('message', sprintf(trans('notifications.car_reject'), $Auction->title));

        } else {
	return response()->json(['status'=>(int)400 , 'error'=>'no_aucation']);
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	}
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }














    public function approveOwnerAuction(Request $request)
    {
	$id = $request->auction_id;
	$rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
	$id = $request->auction_id;
	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'member_id' => $user->id])->first();
        
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',9)->where('userid',$Auction->member_id)->first();
            $now = Carbon::now();
            $new = Carbon::parse($notTest->created_at)->addDays(2);
            if($now > $new)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'time was finish']);
            }
            if($notTest && $notTest->action !=null)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'this auction is view previously']);
            }
            if($notTest && $notTest->action == null)
            {
                \DB::table('auctions')
                ->where('id', $id)
                ->update([
                    'approve_member' => 1,
                    'approve_member_date' => Carbon::now(),
                ]);
                \DB::table('notifications')
                    ->where(['type' => 9, 'userid' => $user->id, 'auctionID' => $id])
                    ->update([
                        'action' => 'owner_approveed',
                        'updated_at' => Carbon::now(),'view' => 1,
                    ]);

                $lastBid = Bidding::where('auctionID',$Auction->id)->where('price',$Auction->current_price)->first();
                

                $messa =   " تهانيناً ,, تم فوزك بالمناقصة رقم#" . $Auction->id ." ويرجي الدفع قبل 48 ساعة والا سيتم خسارة السيارة ومبلغ التأمين"; 
                
                $note = new notifications();
                $note->userid = $lastBid->memberID;
                $note->type = 5;
                $note->auctionID = $id;
                $note->message = $messa;
                $note->created_at = Carbon::now();
                $note->save();
                
                $userNot = Member::find($lastBid->memberID);
                
                $this->sendGCM($messa,$userNot->fcm_token);

        
                return response()->json(['status'=>(int)200 , 'message'=>'تمت الموافقة بنجاح']);
            }
            
            

        } else {
		return response()->json(['status'=>(int)400 , 'error'=>'no_aucation']);
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	}
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
        
    }

    //rejectAuction
    public function rejectOwnerAuction(Request $request)
    {
	$id = $request->auction_id;
	$rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
	$id = $request->auction_id;
	$user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'member_id' => $user->id])->first();
            
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',9)->where('userid',$Auction->member_id)->first();
            $now = Carbon::now();
            $new = Carbon::parse($notTest->created_at)->addDays(2);
            if($now > $new)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'time was finish']);
            }
            if($notTest && $notTest->action !=null)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'this auction is view previously']);
            }

            if($notTest && $notTest->action ==null)
            {
                \DB::table('auctions')
                ->where('id', $id)
                ->update([
                    'approve_member' => 0,
                    'approve_member_date' => Carbon::now(),
                ]);

                \DB::table('notifications')
                    ->where(['type' => 9, 'userid' => $user->id, 'auctionID' => $id])
                    ->update([
                        'action' => 'owner_reject',
                        'updated_at' => Carbon::now(),'view' => 1,
                    ]);

                $lastBid = Bidding::where('auctionID',$Auction->id)->where('price',$Auction->current_price)->first();    
                $deleteAllLoserBidPrice = BankTransaction::where('auctionID',$Auction->id)->where('value',$Auction->current_price)->delete();
                $messa = " نأسف لحضرتك تم رفض البيع من المالك علي المزايدة رقم   " . $id;
                $messaM = " لاعادة نشر المزايدة رقم " . $id;
                
                $note = new notifications();
                $note->userid = $lastBid->memberID;
                $note->type = 7;
                $note->auctionID = $id;
                $note->message = $messa;
                $note->created_at = Carbon::now();
                $note->save();

                $note = new notifications();
                $note->userid = $Auction->member_id;
                $note->type = 10;
                $note->auctionID = $id;
                $note->message = $messaM;
                $note->created_at = Carbon::now();
                $note->save();
                
                $owner = Member::find($Auction->member_id);
                $user = Member::find($lastBid->memberID);

                $this->sendGCM($messa,$user->fcm_token);
                $this->sendGCM($messaM,$owner->fcm_token);

                return response()->json(['status'=>(int)200 , 'message'=>'تم رفض البيع']);

                Session::flash('alert-class', 'alert-success');
                Session::flash('message', sprintf(trans('notifications.car_reject'), $Auction->title));

            }
            
        } else {
	return response()->json(['status'=>(int)400 , 'error'=>'no_aucation']);
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	}
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }



    public function republish(Request $request)
    {
	$id = $request->auction_id;
	$rules = [
            'auction_id'  => 'required|exists:auctions,id',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
	    $id = $request->auction_id;
	    $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {

        $id = intval($id);
        $Auction = Auction::where(['id' => $id, 'member_id' => $user->id])->first();
            
        if ($Auction) {

            $notTest = notifications::where('auctionID',$id)->where('type',10)->where('userid',$Auction->member_id)->first();
            if($notTest && $notTest->action !=null)
            {
                return response()->json(['status'=>(int)400 , 'error'=>'this auction is view previously']);
            }

            if($notTest && $notTest->action ==null)
            {
                    \DB::table('auctions')
                    ->where('id', $id)
                    ->update([
                        'is_active' => 1,
                        'total_bids'=>0,
                        'current_price'=>0,
                        'start_at' => Carbon::now(),
                        'isWork' => 1,
                    ]);

                    \DB::table('notifications')
                        ->where(['type' => 10, 'userid' => $user->id, 'auctionID' => $id])
                        ->update([
                            'action' => 'republich',
                            'updated_at' => Carbon::now(),'view' => 1,
                        ]);

                    $deleteAllBids = Bidding::where('auctionID',$Auction->id)->delete(); 
                    
                    return response()->json(['status'=>(int)200 , 'message'=>'تم النشر']);

                    Session::flash('alert-class', 'alert-success');
                    Session::flash('message', sprintf(trans('notifications.car_reject'), $Auction->title));

                
                
            }
            
        } else {
	        return response()->json(['status'=>(int)400 , 'error'=>'no_aucation']);
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message', trans('notifications.no_aucation'));
        }
	}
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }

















    public function pay(Request $request)
    {//dd($request->all());
        
        //$this->payII($request->amount ,$request->name , $request->number , $request->cvc , $request->month , $request->year , $request->publishable_api_key);
        
        
        
        
        



        MoyasarFaced::setApiKey(config('moyasar.Test_Secret_Key'));

        // $card = [
        //     "type" => MoyasarFaced::CREDIT_CARD,
        //     "name" => $name,
        //     "number" => $number,
        //     "cvc" => $cvc,
        //     "month" => $month,
        //     "year" => $year
        // ];

        // $payment = MoyasarFaced::PayCreate($amount, $card, "", "SAR");
        
        $url = "https://test.oppwa.com/v1/threeDSecure";
        $data = "authentication.userId=8a8294174d0595bb014d05d829e701d1" .
            "&authentication.password=9TnJPc2n9h" .
            "&authentication.entityId=8a8294174d0595bb014d05d82e5b01d2" .
            "&amount=12.50" .
            "&currency=SAR" .
            "&paymentBrand=VISA" .
            "&card.number=".$request->source['number']."" .
            "&card.holder=John Smith" .
            "&card.expiryMonth=".$request->source['month']."" .
            "&card.expiryYear=".$request->source['year']."" .
            "&card.cvv=".$request->source['cvc']."" .
            "&shopperResultUrl=https://hyperpay.docs.oppwa.com/tutorials/threeDSecure#step3";
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    

        
        
        //return url()->previous();
    }


    public function payII($amount ,$name , $number , $cvc , $month , $year ,$publishable_api_key)
    {
        MoyasarFaced::setApiKey(config('moyasar.Test_Secret_Key'));

        // $card = [
        //     "type" => MoyasarFaced::CREDIT_CARD,
        //     "name" => $name,
        //     "number" => $number,
        //     "cvc" => $cvc,
        //     "month" => $month,
        //     "year" => $year
        // ];

        // $payment = MoyasarFaced::PayCreate($amount, $card, "", "SAR");


        $card = [
            "type" => MoyasarFaced::CREDIT_CARD,
           "name" => "Abdulaziz Nasser",
           "number" => "4111111111111111",
           "cvc" => 331,
           "month" => 12,
           "year" => 2018
             ];
       //                        price   cardinfo  description   currency    
         return MoyasarFaced::PayCreate("10000"  ,$card,  "bag payment", "SAR",$request->source,$request->callback_url);
    }



    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    function sendGCM($message, $id) {


        $url = 'https://fcm.googleapis.com/fcm/send';
    
        $fields = array (
                'registration_ids' => array (
                        $id
                ),
                'notification' => array (
                        "body" => $message,
                        'title'     => "saudibidz",
                        'vibrate'   => 1,
                        'sound'     => 1,
                        "icon" => "myicon",
                        "color" => "#2bc0d1"
                )
        );
        $fields = json_encode ( $fields );
    
        $headers = array (
                'Authorization: key=' . "AAAAH6gDKps:APA91bG2gexrxQ0JshjD4I4tWEIGMeMORu77VYH9111OB48Idhc1GDsCmXDfCRrtwjl2pldQCAp1D68G9ENyTaJL8svOYE1xheIsYim8UlDVZGu7sNO4FgTxtKr3lyQkF29RnWntvUKYO6X-WkW0U3n9KlX-PYCfqw",
                'Content-Type: application/json'
        );
    
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
    
        $result = curl_exec ( $ch );
      //  echo $result;dd($result,"ddddddddddd");
        curl_close ( $ch );
    }
}

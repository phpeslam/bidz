<?php

namespace App\Http\Controllers\Api;
use App;
use Hash;
use Validator;
use App\Custum\Sms;
use App\Http\Controllers\Controller;
use App\Mail\RegisterEmail;
use App\Model\City;
use App\Model\Country;
use App\Model\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Password;

use App\Mail\AdminUserSalonEmail;
use App\Mail\WelcomeUserEmail;
use App\Mail\WelcomeUserSalonEmail;

use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;

use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Mail;
use App\Mail\ForgotPassword;
use App\Mail\ResetPassword;

class AuthController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        if (\Auth::guard('members')->check()) {
            return redirect()->route('member-profile-index');
        }
        $data = [];
        $data['countries'] = Country::where('is_active', 1)->with('currentLanguage')->get();
        if (!empty(old('country_id'))) {
            $cities = City::where('is_active', 1)->where('country_id', old('country_id'))->with('currentLanguage')->get();
            $data['cities'] = [];
            foreach ($cities as $city) {
                $data['cities'][] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
            }
        }
        return view('front.auth.index', $data);
    }

    public function cities() {
        $country_id = (int) \Request::input('country_id');
        $cities = City::where('is_active', 1)->where('country_id', $country_id)->with('currentLanguage')->get();
        $ci = [];
        foreach ($cities as $city) {
            $ci[] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
        }

        return response()->json($ci);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function register(Request $request) {

        $request->lang = 'ar';
        $rules = [
            'membership_type' => ['required', Rule::in([1, 2])],
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'terms_cond_accept' => 'required',
            'address' => 'required|max:255',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City())->getTable() . ',id',
            'phone_number' => ['required','regex:/(00966)[0-9]{8}/', 'unique:' . (new Member())->getTable() . ',phone_number'],
            'password' => 'required|min:5|max:20|confirmed',
            'email' => ['required', 'email', 'confirmed', 'unique:' . (new Member())->getTable() . ',email'],
            'fcm_token' => ['required'],
            'lang'=>'required|in:ar,en',
        ];


        
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}


        $row = new Member();
        $row->first_name = $request->get('first_name');
        $row->last_name = $request->get('last_name');
        $row->email = $request->get('email');
        $row->password = \Hash::make($request->get('password'));
        $row->phone_number = $request->get('phone_number');
        $row->membership_type = $request->get('membership_type');
        $row->city_id = $request->get('city_id');
        $row->country_id = $request->get('country_id');
        $row->address = $request->get('address');
        //$row->fcm_token = $request->fcm_token;



        if ($row->membership_type == 2 && $request->hasFile('market_image')) {
            if ($request->hasFile('market_image') && $request->file('market_image')->isValid()) {
                $file_upload = md5(time() . str_random(16)) . '.' . $request->file('market_image')->clientExtension();
                $request->file('market_image')->storeAs(Member::FILE_PATH, $file_upload);
                $row->file = $file_upload;
            }
            $img = Input::file('market_image');
            $ext = $img->getClientOriginalExtension();
            $path = public_path() . '/uploads/markets';
            $fullename = time() . '.' . $ext;
            $img->move($path, $fullename);
            $request->merge(['market_image' => $fullename]);
            $row->market_image = $fullename;
        }
        elseif ($row->membership_type == 2 && (!isset($request->market_image) || empty($request->market_image)) ) {
            return response()->json(['status'=>(int)400 , 'error'=>'put market_image for provider']);
        }
        else
        {
            $row->market_image = asset('storage/global/no_image_found.png');
            $row->image = asset('storage/global/no_image_found.png');
        }
        

        $row->is_active_phone = 0;
        $row->is_active_email = 0;
        $row->email_act_code = str_random(30);
        $row->phone_act_code = rand(1000, 9999);
        $row->is_active = 0;
        $row->token = $this->generateUniqueToken();
        $row->fcm_token = $this->generateUniqueFireBase($request->fcm_token);

        //try {
        $row->save();


        if($row->market_image != null || $row->market_image != asset('storage/global/no_image_found.png'))
        {
            $row->market_image = asset('/uploads/markets/'.$row->market_image) ;
        }
            
         //   \Mail::to($row->email)->send(new RegisterEmail($row));
            Sms::send($row->phone_number, trans('alerts.your_activation_code').$row->phone_act_code );
            
            $user = \Auth::guard('members')->login($row, true); 

            if($request->lang == 'ar')
            {
                $row->country_id = Country::where('id',$row->country_id)->with('arLang')->first();
                $row->city_id = City::where('id',$row->city_id)->with('arLang')->first();
            }
            else
            {
                $row->country_id = Country::where('id',$row->country_id)->with('enLang')->first();
                $row->city_id = City::where('id',$row->city_id)->with('enLang')->first(); 
            }
            

            return response()->json(['status'=>(int)200 , 'user'=>$row]);
           
        // } catch (\PDOException $e) {
        //     return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
        // }
    }

    public function sendResetLinkEmail(Request $request) {
        $rules = ['email' => 'required|email'];

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return response()->json([false, implode(',', $validator->errors()->all())]);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.

        $response = Password::broker()->sendResetLink(
                $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT ? response()->json([true, trans('alerts.email-link-sent')]) : response()->json([false, trans($response)]);
    }

    public function showLogin() {
        $data = [];
        return view('front.auth.login', $data);
    }

    public function resendCode(Request $request)
    {
        
        if( (is_null($request->header('token')) || empty($request->header('token'))))
        {
            return response()->json(['status'=>(int)400 , 'errors'=>'The :token field is required.']);
        }

        $row = Member::where('token',$request->header('token'))->first();
        Sms::send($row->phone_number,trans('alerts.your_activation_code').$row->phone_act_code);
        return response()->json(['status'=>(int)200 ]);
        
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateImage(Request $request)
    {
        $rules = [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];

        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}


        if((is_null($request->header('token')) || empty($request->header('token'))))
        {
            return response()->json(['status'=>(int)400 , 'error'=>'The :token field is required.']);
        }
        App::setLocale('ar');
        $user = Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
            if ($request->hasFile('image')) {
                ###
                $file = $request->image;
                $filename = time() . '_' . $file->getClientOriginalName();
                $path = 'uploads/images';
                $file->move($path, $filename);
                ###
                $user->image = $path . '/' . $filename;
                $user->save();
                $user->image = asset($user->image);
                $user->country_id = Country::where('id', $user->country_id)->with('arLang')->first();
                $user->city_id = City::where('id', $user->city_id)->with('arLang')->first();
                return response()->json(['status'=>(int)200,'data'=>$user]);
            }
        }
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }
    public function doActive(Request $request)
    {

        $rules = [
            'mobile_verify_code' => 'required',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        
        
        if((is_null($request->header('token')) || empty($request->header('token'))))
        {
            return response()->json(['status'=>(int)400 , 'error'=>'The :token field is required.']);
        }

        $phone_act_code = trim($request->get('mobile_verify_code'));

            $user = Member::where('token',$request->header('token'))->first();
            if($user)
            {
                if(trim($user->phone_act_code)==$phone_act_code){
                    $user->phone_act_code = null;
                    $user->is_active_phone = 1;
                    if($user->membership_type == 1 ) {
                    $user->is_active = 1;
                    }
                    $user->save();
                    $this->sendWelcome($user);
                    if($user->membership_type == 2){ return response()->json(['status'=>(int)200 ,'data'=>'بنتظار التأكيد من الادمن']);}
                    return response()->json(['status'=>(int)200]);
                }
                else
                {
                    return response()->json(['status'=>(int)400 , 'error'=>'The :mobile_verify_code field is invalide.']);
                }
            }
            else{
                return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
            }
            
        
    }
    public function sendWelcome(Member $user)
    {
        if($user->is_active_email==1 && $user->is_active_phone) {
            if($user->membership_type == 1 ) {
                $user->is_active = 1;
                $user->save();
                \Mail::to($user->email)->send(new WelcomeUserEmail($user));
            }else{
                if(!empty($user->file) && \Storage::exists(Member::FILE_PATH.DIRECTORY_SEPARATOR.$user->file)) {
                    \Mail::to($user->email)->send(new WelcomeUserSalonEmail($user));
                    \Mail::to(env('ADMIN_EMAIL'))->send(new AdminUserSalonEmail($user));
                }
            }

        }
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {

        $rules = [
            'email'  => 'required|email|exists:members,email',
            'password' => 'required|min:6',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        
        $client = Member::where('email',$request->email)->where('is_active_phone',0)->first();
        if($client)
        {return response()->json(['status'=>(int)400 , 'errors'=>'please active this account']);}

        $credentials = $request->only('email', 'password');
	        
            // attempt to verify the credentials and create a token for the user
            if (! $token = \Auth::guard('members')->attempt($credentials)) {
                return response()->json(['status'=>(int)400 , 'error'=>'invalid_credentials']);
            }
	        
            $data   =  \Auth::guard('members')->user();
            $row = Member::find($data->id);
            $row->token = $this->generateUniqueToken();
            //$row->fcm_token = $request->fcm_token;
            $row->fcm_token = $this->generateUniqueFireBase($request->fcm_token);
            $row->save();  
            $data['market_image'] = asset('/uploads/markets/'.$row->market_image) ;
            if($row->market_image != null || $row->market_image != asset('storage/global/no_image_found.png'))
            {
                $row->market_image = asset('/uploads/markets/'.$row->market_image) ;
            }
            if($request->lang == 'ar')
            {
                $row->country_id = Country::where('id',$row->country_id)->with('arLang')->first();
                $row->city_id = City::where('id',$row->city_id)->with('arLang')->first();
            }
            else
            {
                $row->country_id = Country::where('id',$row->country_id)->with('enLang')->first();
                $row->city_id = City::where('id',$row->city_id)->with('enLang')->first(); 
            }
            
            
            // all good so return the token
            
            return response()->json(['status'=>(int)200 , 'user'=>$row]);
           
    }

    public function forgotPassword(Request $request)
	{
        $rules = [
            'email'  => 'required|email|exists:members,email',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}

		$user = Member::where('email', $request->email)->first();

		$new_password = $request->newPassword;//str_random(8);

		//Generate Random Password
		$user->reset_password_code = $this->generateUniqueCode();
		$user->save();
		//END Generate Random Password

        //Send Email
        \Mail::send('emails.register.forget',['code'=>$user->reset_password_code], function($message) use($user)
                {
                    $message->from('info@saudibidz.com')->subject('forget password');
                    $message->to($user->email);
                });
	//	Mail::to($user->email)->send(new ForgotPassword($user->reset_password_code));
		//END Send Email

		return response()->json(['status'=>(int)200 ]);
    }
    
    public function resetPassword(Request $request)
	{
        $rules = [
            'code'  => 'required|exists:members,reset_password_code',
            'newPassword' => 'required|min:6',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}

		$user = Member::where('reset_password_code', $request->code)->first();

		$new_password = $request->newPassword;//str_random(8);

		//Generate Random Password
		$user->password = bcrypt($new_password);
		$user->save();
		//END Generate Random Password

        //Send Email
        \Mail::send('emails.register.reset',['email' => $user->email,'password' => $new_password], function($message) use($user)
                {
                    $message->from('info@saudibidz.com')->subject('forget password');
                    $message->to($user->email);
                });
		//Mail::to($user->email)->send(new ResetPassword($user->email, $new_password));
		//END Send Email

		return response()->json(['status'=>(int)200 ]);
	}


    public function generateUniqueToken()
	{
		$token = str_random(40);
		$unique = false;

		while(!$unique)
		{
			$authToken = Member::where('token', $token)->first();
			if(!$authToken)
			{
				$unique = true;
			}
		}

		return $token;
    }
    
    public function generateUniqueCode()
	{
		$code = str_random(8);
		$unique = false;

		while(!$unique)
		{
			$generated = Member::where('reset_password_code', $code)->first();
			if(!$generated)
			{
				$unique = true;
			}
		}

		return $code;
    }
    
    public function generateUniqueFireBase($fcm_token)
	{
        $members = Member::where('fcm_token', $fcm_token)->update(['fcm_token'=>'']);
		return $fcm_token;
	}

}

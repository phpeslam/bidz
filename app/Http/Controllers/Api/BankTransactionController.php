<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App;
use Hash;
use Validator;
use App\Model\Member;
use Carbon\Carbon;

use App\Model\BankTransaction;
use Session;
use Illuminate\Http\Request;



class BankTransactionController extends Controller
{

    public function data()
    {
        $data = [];
        $data['bank_name'] = Setting::where('key','bank_name')->first();
        $data['bank_account'] = Setting::where('key','bank_account')->first();
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function index()
    {
        $data = [];
        $data['page_title'] = trans('global.contact_us');
        return view('bank.index', $data);
    }

    public function store(Request $request)
    {

        $rules = [
            'type' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:6048',
           // 'date' => 'required|date',
            'value' => 'required',
        ];

        App::setLocale('ar');
	    $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        $startTime   = new Carbon($request->date);
        $request->date = $startTime->format('Y-m-d H:i:s');
        $now1 = Carbon::now();
        $now = Carbon::now();
        $after = $now->subDays(29); 
        //dd(gettype($after),$after,gettype($startTime),$startTime);
        // if($after > $startTime || $startTime > $now1)
        // {
        //     return response()->json(['status'=>(int)400 , 'error'=>'date is old or in future']);
        // }
        $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->storeAs(BankTransaction::FILE_PATH, $imageName);
        
        $user =  $user->id ?? 0;
//       move(public_path('images'), $imageName);
        $Trans = new BankTransaction();
        $Trans->type = $request->type;
        $Trans->image = $imageName;
        $Trans->date = date('Y-m-d h:i:s');
        $Trans->value = $request->value;
        $Trans->userID = $user;
        $Trans->type_balance ='bankTansaction';
        if($request->type == 3)
        {
            $Trans->case_balance = 'Special Ads';
        }
        else
        {
            $Trans->case_balance = 'add_balance';
        }

        $Trans->create_at = date('Y-m-d h:i:s');
        if ($Trans->save()) {
            return response()->json(['status'=>(int)200]);
        } else {
            return response()->json(['status'=>(int)400 , 'error'=>'Error in save Balance.']);
        }
        }
	    else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }


}
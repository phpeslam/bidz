<?php
/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 04/05/18
 * Time: 01:53 ص
 */

namespace App\Http\Controllers\Api;
use App;
use Hash;
use Validator;

use App\Model\Member;
use App\Model\City;
use App\Model\BankTransaction;
use App\Model\Country;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Password;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class ProfileController extends Controller
{
    public function index()
    {
        $data = [];
        $data['page_title'] = trans('alerts.profile');
        return view('members.profile',$data);
    }
    public function edit(Request $request)
    {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
         $messages = [
            'lang.in'    => 'The selected :lang is invalid.',
            'lang.required'  => 'The :lang field is required.' ,
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        $data = [];
        $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
            if($request->lang == 'ar')
            {
                $data['countries'] = Country::where('is_active', 1)->with('arLang')->get();

                $country_id = $user->country_id  ?? old('country_id');
                if ($country_id) {
                    $cities = City::where('is_active', 1)->where('country_id', $country_id)->with('arLang')->get();
                    $data['cities'] = [];
                    foreach ($cities as $city) {
                        $data['cities'][] = ['id' => $city->id, 'title' => $city->arLang->title];
                    }
                }
            }
            else
            {
                $data['countries'] = Country::where('is_active', 1)->with('enLang')->get();

                $country_id = $user->country_id  ?? old('country_id');
                if ($country_id) {
                    $cities = City::where('is_active', 1)->where('country_id', $country_id)->with('enLang')->get();
                    $data['cities'] = [];
                    foreach ($cities as $city) {
                        $data['cities'][] = ['id' => $city->id, 'title' => $city->enLang->title];
                    }
                }
            }
            $user->country_id = Country::where('id', $user->country_id)->with('arLang')->first();
            $user->city_id = City::where('id', $user->city_id)->with('arLang')->first();
            $user->image = asset($user->image);
            $data['user'] = $user;
            $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                $total = 0;
                foreach($blances as $blance)
                {
                    if($blance->case_balance == 'deposited')
                    {
                        $total+=$blance->value;
                    }
                    elseif( $blance->case_balance == 'add_balance')
                    {
                        $total= $total;
                    }
                    else
                    {
                        $total-=$blance->value;
                    }
                }
                $data['balance'] = $total;
            return response()->json(['status'=>(int)200,'data'=>$data]);
        }
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }

        
       
        
        return view('members.editform',$data);
    }


    public function updateProfie(Request $request){
        $rules = [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'address' => 'required|max:255',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City())->getTable() . ',id',
            'phone_number' => 'required',
            // 'password' => 'required|min:5|max:20',
            // 'email' => ['required', 'email'],
        ];

        App::setLocale('ar');         
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        $userI =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($userI)
        {
            $user = \DB::table('members')
            ->where('token',$request->header('token'))->where('is_active',1)
            ->update([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'address' => $request->address,
                'country_id' => $request->country_id,
                'city_id' => $request->city_id,
                'phone_number' => $request->phone_number,
                // 'password' =>  \Hash::make($request->get('password')),
                // 'email' => $request->email,
            ]);
            $user = Member::find($userI->id);
            $user->country_id = Country::where('id', $userI->country_id)->with('arLang')->first();
            $user->city_id = City::where('id', $userI->city_id)->with('arLang')->first();
            $user->image = asset($user->image);
            return response()->json(['status'=>(int)200,'data'=>$user]);
        }
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }

    public function countNotifications(Request $request){
        $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
            $data = [];
            $data['count'] = \App\Model\notifications::where(['userid' => $user->id,'view'=>0])->orderby('id', 'desc')->get()->count();
  
            return response()->json(['status'=>(int)200,'data'=>$data]);
        }
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }

    public function myNotifications(Request $request){
        $user =  Member::where('token',$request->header('token'))->where('is_active',1)->first();
        if($user)
        {
            $data = [];
            $data['notifications'] = \App\Model\notifications::where(['userid' => $user->id])->orderby('id', 'desc')->paginate(10);
            //$data['notifications'] = $this->paginate($data['notifications'],2,2);
            //return $data['notifications'];
            \DB::table('notifications')
                ->where('userid', $user->id)
                ->update(['view' => 1]);
            return response()->json(['status'=>(int)200,'data'=>$data]);
        }
        else{
            return response()->json(['status'=>(int)400 , 'error'=>'لم يتم الموافقة بعد من الادمن او كود التحقق غير صحيح']);
        }
    }
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);//return $items;
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
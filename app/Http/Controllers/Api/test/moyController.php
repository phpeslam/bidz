<?php
namespace App\Http\Controllers\Api\test;
use App;
use Hash;
use Validator;
require_once 'vendor/autoload.php';

use App\Http\Controllers\Controller;
use App\Model\Auction;
use App\Model\AuctionFile;
use App\Model\AuctionImage;
use App\Model\AuctionOtherSpecification;
use App\Model\CarColor;
use App\Model\CarType;
use App\Model\Category;
use App\Model\EngineType;
use App\Model\Fuel;
use App\Model\Import;
use App\Model\Member;
use App\Model\MotionVector;
use App\Model\notifications;
use App\Model\OtherSpecification;
use App\Model\Specification;
use App\Model\SubCategory;
use App\Model\Bidding;
use App\Model\FollowAuction;
use App\Model\BankTransaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

//use App\Http\Controllers\Api\test\moyser123;

class moyController extends Controller
{

    public function payI(Request $request)
    {
        require_once 'vendor/autoload.php';
        $this->payII($request->amount ,$request->name , $request->number , $request->cvc , $request->month , $request->year , $request->publishable_api_key);
        return url()->previous();
    }

    public function payII($amount ,$name , $number , $cvc , $month , $year ,$publishable_api_key)
    {
        Moyasar\Client::setApiKey($publishable_api_key);

        $card = [
            "type" => Moyasar\Payment::CREDIT_CARD,
            "name" => $name,
            "number" => $number,
            "cvc" => $cvc,
            "month" => $month,
            "year" => $year
        ];

        $payment = Moyasar\Payment::create($amount, $card, "", "SAR");
    }
    
}

?>
<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App;
use Hash;
use Validator;

use App\Model\Page;
use App\Model\Faq;
use App\Model\FaqLanguage;
use Illuminate\Http\Request;

class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutus(Request $request) {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $page = Page::query()->where(['id' => 5])->with('arLang')->first();
            $data['page'] = $page;
        }
        else
        {
            $page = Page::query()->where(['id' => 5])->with('enLang')->first();
            $data['page'] = $page;
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);

        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms(Request $request) {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $page = Page::query()->where(['id' => 6])->with('arLang')->first();
            $data['page'] = $page;
        }
        else
        {
            $page = Page::query()->where(['id' => 6])->with('enLang')->first();
            $data['page'] = $page;
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }
    
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function howSiteWorks(Request $request) {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $page = Page::query()->where(['id' => 7])->with('arLang')->first();
            $data['page'] = $page;
        }
        else
        {
            $page = Page::query()->where(['id' => 7])->with('enLang')->first();
            $data['page'] = $page;
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq(Request $request) {
        $request->lang = 'ar';
        $rules = [
            'lang'=>'required|in:ar,en',
        ];
        App::setLocale('ar');
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
        if($request->lang == 'ar')
        {
            $page = Faq::query()->with('arLang')->get();
            $data['Faqs'] = $page;
        }
        else
        {
            $page = Faq::query()->with('enLang')->get();
            $data['Faqs'] = $page;
        }
        return response()->json(['status'=>(int)200 , 'data'=>$data]);
    }

    public function show($id) {
        $data = [];
        $data['page_title'] = trans('global.contact_us');
        return view('pages.content', $data);
    }

}

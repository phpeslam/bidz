<?php

namespace App\Http\Controllers\CheckPoint;

use App\Custum\Sms;
use App\Mail\AdminUserSalonEmail;
use App\Mail\RegisterEmail;
use App\Mail\WelcomeUserEmail;
use App\Mail\WelcomeUserSalonEmail;
use App\Model\Member;
use App\Model\BankTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class CheckPointController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = \Auth::guard('members')->user();
        // if($user->membership_type==2 && (empty($user->file))){
        //     return view('checkpoint.upload');
        // }
        return view('checkpoint.active');
    }

    public function doUpload(Request $request)
    {
        $rules = [
            'file_upload' => ['required','file','mimes:jpeg,pdf,rar,zip,bmp,png,jpg']
    ]
        ;

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $file_upload = '';
        if ($request->hasFile('file_upload') && $request->file('file_upload')->isValid()) {
            $file_upload = md5(time() . str_random(16)) . '.' . $request->file('file_upload')->clientExtension();
            try {
                $request->file('file_upload')->storeAs(Member::FILE_PATH, $file_upload);
            } catch (\Exception $e) {
                return redirect()->back()->with('alert-danger',trans('alerts.upload-error').' '. $e->getMessage());
            }
        }
        $user = \Auth::guard('members')->user();
        $user->file = $file_upload;
        try{
            $user->save();
            $this->sendWelcome($user);
            return redirect()->back();
        }catch (\PDOException $e){
            return redirect()->back()->with('alert-danger',trans('alerts.upload-error').' '. $e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendCode(Request $request)
    {
        $action = $request->get('action');
        $row = \Auth::guard('members')->user();
        if($action=='e'){
            \Mail::to($row->email)->send(new RegisterEmail($row));
            return response()->json([true,trans('alerts.mail-sent')]);
        }elseif($action=='m'){
            Sms::send($row->phone_number,trans('alerts.your_activation_code').$row->phone_act_code);
            return response()->json([true,trans('alerts.your_activation_code_sent')]);
        }




        return response()->json([false,trans('alerts.undefined')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doActive(Request $request)
    {
        $active_type = $request->get('active_type');
        $phone_act_code = trim($request->get('mobile_verify_code'));
        $email_verify_code = trim($request->get('email_verify_code'));

        if($active_type=='sms' && empty($phone_act_code)){
            return redirect()->back()->with('alert-danger',trans('alerts.type-mobile-activation-code'));
        }elseif($active_type=='email' && empty($email_verify_code)){
            return redirect()->back()->with('alert-danger',trans('alerts.type-email-activation-code'));
        }else{
            $user = \Auth::guard('members')->user();
            if($active_type=='sms' && trim($user->phone_act_code)==$phone_act_code){
                $user->phone_act_code = null;
                $user->is_active_phone = 1;
                if($user->membership_type == 1 ) {
                    $user->is_active = 1;
                    }
                $user->save();
                $this->sendWelcome($user);

                $user = \Auth::guard('members')->user();

                $data = [];
                $data['user'] = $user;
                    $blances = BankTransaction::where('userID',$user->id)->whereNotIn('type', [3])->get();
                        $total = 0;
                        foreach($blances as $blance)
                        {
                            if($blance->case_balance == 'deposited')
                            {
                                $total+=$blance->value;
                            }
                            elseif( $blance->case_balance == 'add_balance')
                            {
                                $total= $total;
                            }
                            else
                            {
                                $total-=$blance->value;
                            }
                        }
                $data['balance'] = $total;
                $data['page_title'] = trans('alerts.profile');
                $data['path_I'] = trans('alerts.profile');

                Session::flash('alert-class', 'alert-success');
                Session::flash('message',  trans('alerts.mobile-activated'));
                return view('members.profile',$data);

                
                return redirect()->route('member-profile-index');
                
            }elseif($active_type=='email' && trim($user->email_act_code)==$email_verify_code){
                $user->email_act_code = null;
                $user->is_active_email = 1;
                $user->save();
                $this->sendWelcome($user);
                return redirect()->back()->with('alert-success',trans('alerts.email-activated'));
            }else{
                return redirect()->back()->with('alert-danger',trans('alerts.Unactivated-activation'));
            }
        }
    }


    public function sendWelcome(Member $user)
    {
        if($user->is_active_email==1 && $user->is_active_phone) {
            if($user->membership_type == 1 ) {
                $user->is_active = 1;
                $user->save();
               // \Mail::to($user->email)->send(new WelcomeUserEmail($user));
                \Mail::send('emails.welcome.user',['user'=>$user], function($message) use($user)
                {
                    $message->from('info@saudibidz.com')->subject('WelcomeUser');
                    $message->to($user->email);
                });
            }else{
                if(!empty($user->file) && \Storage::exists(Member::FILE_PATH.DIRECTORY_SEPARATOR.$user->file)) {
                    \Mail::send('emails.welcome.usersalon',['user'=>$user], function($message) use($user)
                    {
                        $message->from('info@saudibidz.com')->subject('WelcomeUser');
                        $message->to($user->email);
                    });
                    \Mail::send('emails.welcome.adminsalon',['user'=>$user], function($message) use($user)
                    {
                        $message->from('info@saudibidz.com')->subject('WelcomeUser');
                        $message->to(env('ADMIN_EMAIL'));
                    });
                //    \Mail::to($user->email)->send(new WelcomeUserSalonEmail($user));
                //    \Mail::to(env('ADMIN_EMAIL'))->send(new AdminUserSalonEmail($user));
                }
            }

        }
    }
}

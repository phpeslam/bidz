<?php

namespace App\Http\Controllers;

use App\Model\Page;
use App\Model\Faq;
use App\Model\FaqLanguage;
use Illuminate\Http\Request;

class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function aboutus() {
        $data = [];
        $data['page_title'] = trans('global.about_saudibidz');
        $page = Page::query()->where(['id' => 5])->with('currentLanguage')->first();
        
        $data['page'] = $page;
        $data['path_I'] = trans('global.about_saudibidz');
        return view('pages.about', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function terms() {
        $data = [];
        $data['page_title'] = trans('global.terms');
        $page = Page::query()->where(['id' => 6])->with('currentLanguage')->first();
        //dd($page);
        $data['page'] = $page;
        $data['path_I'] = trans('global.terms');
        return view('pages.terms', $data);
    }
    
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function howSiteWorks() {
        $data = [];
        $data['page_title'] =  'شرح استخدام الموقع';
        $page = Page::query()->where(['id' => 7])->with('currentLanguage')->first();
        //dd($page);
        $data['page'] = $page;
        $data['path_I'] = 'شرح استخدام الموقع';
        return view('pages.howsiteworks', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function faq() {
        $data = [];
        $data['page_title'] = trans('global.faq');
        $data['Faqs'] = Faq::query()->with('currentLanguage')->get();
        $data['path_I'] = 'اسئلة الشائعة ';
        return view('pages.faq', $data);
    }

    public function show($id) {
        $data = [];
        $data['page_title'] = trans('global.contact_us');
        $data['path_I'] = trans('global.contact_us');
        return view('pages.content', $data);
    }

}

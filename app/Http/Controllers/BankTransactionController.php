<?php

namespace App\Http\Controllers;

use App\Model\BankTransaction;
use App\Model\Setting;
use Session;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BankTransactionController extends Controller
{

    public function index()
    {
        $data = [];
        $data['page_title'] = trans('global.contact_us');
        $data['bank_name'] = Setting::where('key','bank_name')->first();
        $data['bank_account'] = Setting::where('key','bank_account')->first();
        return view('bank.index', $data);
    }

    public function store(Request $request)
    {

        $request->validate([
            'type' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:6048',
           // 'date' => 'required|date',
            'value' => 'required',
        ]);


        $startTime   = new Carbon($request->date);
        $request->date = $startTime->format('Y-m-d H:i:s');
        $now1 = Carbon::now();
        $now = Carbon::now();
        $after = $now->subDays(29); 
        //dd(gettype($after),$after,gettype($startTime),$startTime);
        // if($after > $startTime || $startTime > $now1)
        // {//dd('ddddddddddddddddd');
        //     Session::flash('alert-class', 'alert-danger');
        //     Session::flash('message','تاريخ التحويل خطأ ');
        //     return back();
        // }

        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->storeAs(BankTransaction::FILE_PATH, $imageName);

        $user =  \Auth::guard('members')->user()->id ?? 0;
//       move(public_path('images'), $imageName);
        $Trans = new BankTransaction();
        $Trans->type = $request->type;
        $Trans->image = $imageName;
        $Trans->date = date('Y-m-d h:i:s');
        $Trans->value = $request->value;
        $Trans->userID =$user;
        $Trans->type_balance ='bankTansaction';
        if($request->type == 3)
        {
            $Trans->case_balance = 'Special Ads';
        }
        else
        {
            $Trans->case_balance = 'add_balance';
        }

        $Trans->create_at = date('Y-m-d h:i:s');
        if ($Trans->save()) {
            Session::flash('alert-class', 'alert-success');
            Session::flash('message', trans('Transfers.msg_success'));
        } else {
            Session::flash('alert-class', 'alert-danger');
            Session::flash('message',trans('Transfers.error-trans'));
        }
        return back();
    }


    public function paytabs(Request $request)
    {

        $values['merchant_email'] = "ayaamohamedaams134@gmail.com";
        $values['secret_key'] = "nVrxRZGX6Ad4pfuGsKcDxm2IpUOMysGVo7tg69ddJrh9GMP8w2M3oPMNKAAi3gesoHIvRWIBn0K4MAdjCZm1P8OzJpSNF6f4OXAG";
        $values['site_url'] = "http://www.saudibidz.com/beta/public/";
        $values['return_url'] = "http://www.saudibidz.com/beta/public/";
        $values['title'] = "Order No 1223";
        $values['cc_first_name'] = "John";
        $values['cc_last_name'] = "Doe";
        $values['cc_phone_number'] = "00971";
        $values['phone_number'] = "39882135";
        $values['email'] = "customer@domain.com";
        $values['products_per_title'] = "Mobile";
        $values['unit_price'] = $request->value;//"21.199";
        $values['quantity'] = "1";
        $values['other_charges'] = "0.0";
        $values['amount'] = $request->value;//"21.199";
        $values['discount'] = "0.0";
        $values['reference_no'] = "ABC-5542";
        $values['currency'] = "SAR";
        $values['ip_customer'] = "1.1.1.0";
        $values['ip_merchant'] = "127.168.1.0";
        $values['billing_address'] = "Flat 3021 Manama Bahrain";
        $values['state'] = "Manama";
        $values['city'] = "Manama";
        $values['postal_code'] = "12345";
        $values['country'] = "BHR";
        $values['shipping_first_name']= "John";
        $values['shipping_last_name'] = "Doe";
        $values['address_shipping'] = "Flat 3021 Manama Bahrain";
        $values['state_shipping'] = "Manama";
        $values['city_shipping'] = "Manama";
        $values['postal_code_shipping']= "12345";
        $values['country_shipping'] = "BHR";
        $values['msg_lang'] = "English";
        $values['cms_with_version'] = "Magento 0.1.9";

        //$res = json_encode($this->my_function_to_post_data('https://www.paytabs.com/apiv2/create_pay_page',$values));
        $res = $this->my_function_to_post_data('https://www.paytabs.com/apiv2/create_pay_page',$values);
        //dd($res);
        return redirect($res->payment_url);
    }

    function my_function_to_post_data($url, $fields)
    {
    // Send Data to PayTabs
    // Here you need to write a function to send the data prepared
    // in the previous function to PayTabs via cURL or any other
    // method.

         $fieldsString = '';
		foreach ($fields as $key => $value) {
			$fieldsString .= $key . '=' . $value . '&';
		}
		rtrim($fieldsString, '&');
		$ch = curl_init();
		$ip = $_SERVER['REMOTE_ADDR'];
		$IPAddress = [
			'REMOTE_ADDR' => $ip,
			'HTTP_X_FORWARDED_FOR' => $ip
		];
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $IPAddress);
		curl_setopt($ch, CURLOPT_POST, count($fields));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, 1);
		$result = curl_exec($ch);
		curl_close($ch);
        $result = json_decode($result);
        return $result;
    }


}
<?php

namespace App\Http\Controllers\Front;
use App;
use App\Custum\Sms;
use App\Http\Controllers\Controller;
use App\Mail\RegisterEmail;
use App\Model\City;
use App\Model\Country;
use App\Model\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Password;

class AuthController extends Controller {

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        if (\Auth::guard('members')->check()) {
            return redirect()->route('member-profile-index');
        }
        $data = [];
        $data['countries'] = Country::where('is_active', 1)->with('currentLanguage')->get();
        if (!empty(old('country_id'))) {
            $cities = City::where('is_active', 1)->where('country_id', old('country_id'))->with('currentLanguage')->get();
            $data['cities'] = [];
            foreach ($cities as $city) {
                $data['cities'][] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
            }
        }
        return view('front.auth.index', $data);
    }

    public function cities() {
        $country_id = (int) \Request::input('country_id');
        $cities = City::where('is_active', 1)->where('country_id', $country_id)->with('currentLanguage')->get();
        $ci = [];
        foreach ($cities as $city) {
            $ci[] = ['id' => $city->id, 'title' => $city->currentLanguage->title];
        }

        return response()->json($ci);
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function register(Request $request) {
        if (\Auth::guard('members')->check()) {
            return redirect()->route('member-profile-index');
        }
        

        $rules = [
            'membership_type' => ['required', Rule::in([1, 2])],
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'terms_cond_accept' => 'required',
            'address' => 'required|max:255',
            'country_id' => 'required|exists:' . (new Country())->getTable() . ',id',
            'city_id' => 'required|exists:' . (new City())->getTable() . ',id',
            'phone_number' => ['required','regex:/(00966)[0-9]{8}/', 'unique:' . (new Member())->getTable() . ',phone_number'],
            'password' => 'required|min:5|max:20|confirmed',
            'email' => ['required', 'email', 'confirmed', 'unique:' . (new Member())->getTable() . ',email'],
        ];
        if($request->get('membership_type') == 2)
        {
            $rules['image']= 'required|mimes:jpeg,jpg,png,gif,pdf,rar,zip,bmp|required|max:10000';
        }

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

        $row = new Member();
        $row->first_name = $request->get('first_name');
        $row->last_name = $request->get('last_name');
        $row->email = $request->get('email');
        $row->password = \Hash::make($request->get('password'));
        $row->phone_number = $request->get('phone_number');
        $row->membership_type = $request->get('membership_type');
        $row->city_id = $request->get('city_id');
        $row->country_id = $request->get('country_id');
        $row->address = $request->get('address');



        if ($request->hasFile('image')) {

            if ($request->hasFile('image') && $request->file('image')->isValid()) {
                $file_upload = md5(time() . str_random(16)) . '.' . $request->file('image')->clientExtension();
                $request->file('image')->storeAs(Member::FILE_PATH, $file_upload);
                $row->file = $file_upload;
            }
            
            $img = Input::file('image');
            $ext = $img->getClientOriginalExtension();
            $path = public_path() . '/uploads/markets';
            $fullename = time() . '.' . $ext;
            $img->move($path, $fullename);
            $request->merge(['image' => $fullename]);
            $row->market_image = $fullename;
        }

        $row->is_active_phone = 0;
        //$row->is_active_email = 0;
        $row->is_active_email = 1;
        $row->email_act_code = str_random(30);
        $row->phone_act_code = $this->generateUniqueCodePhone();

        $row->is_active = 0;

        try {
            $row->save();
           // \Mail::to($row->email)->send(new RegisterEmail($row));
            Sms::send($row->phone_number, trans('alerts.your_activation_code').$row->phone_act_code );
            \Auth::guard('members')->login($row, true);

            return redirect()->route('member-profile-index')->with('alert-success', trans('alerts.registered'));
        } catch (\PDOException $e) {
            return redirect()->back()->with('alert-danger', $e->getMessage())->withInput();
        }
    }

    public function sendResetLinkEmail(Request $request) {
        $rules = ['email' => 'required|email'];

        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return response()->json([false, implode(',', $validator->errors()->all())]);
        }

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.

        $response = Password::broker()->sendResetLink(
                $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT ? response()->json([true, trans('alerts.email-link-sent')]) : response()->json([false, trans($response)]);
    }

    public function showLogin() {
        $data = [];
        return view('front.auth.login', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request) {
        if (\Auth::guard('members')->check()) {
            return redirect()->route('member-profile-index');
        }

        $email = $request->get('email');
        $password = $request->get('password');
        $remember_me = ($request->get('remember_me', 0) == 1) ? true : false;
        if (empty($email)) {
            return redirect()->back()->with('alert-danger', trans('alerts.Email-incorrect'))->withInput();
        }

        if (empty($password)) {
            return redirect()->back()->with('alert-danger',  trans('alerts.Password-required'))->withInput();
        }

        if (\Auth::guard('members')->attempt(['email' => $email, 'password' => $password], $remember_me)) {
            return redirect()->route('member-profile-index');
        }

        return redirect()->back()->with('alert-danger', trans('alerts.Password-email-required'))->withInput();
    }

    public function forgot()
	{
        return view('front.checkpoint.forgot');
    }
    

    public function forgotPassword(Request $request)
	{
        $rules = [
            'email'  => 'required|email|exists:members,email',
        ];
         $messages = [

            'email.required'  => 'The :email field is required.' ,
            'email.email'  => 'The :email must be a valid email address.' ,
            'email.exists'    => 'The selected :email is invalid.' ,
        ];
        App::setLocale('ar');
        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

		$user = Member::where('email', $request->email)->first();

		$new_password = $request->newPassword;//str_random(8);

		//Generate Random Password
		$user->reset_password_code = $this->generateUniqueCode();
		$user->save();
		//END Generate Random Password

        //Send Email
        \Mail::send('emails.register.forget',['code'=>$user->reset_password_code], function($message) use($user)
                {
                    $message->from('info@saudibidz.com')->subject('forget password');
                    $message->to($user->email);
                });
	//	Mail::to($user->email)->send(new ForgotPassword($user->reset_password_code));
		//END Send Email

		return redirect()->route('reset');
    }

    public function reset()
	{
        return view('front.checkpoint.reset');
    }
    
    public function resetPassword(Request $request)
	{
        $rules = [
            'code'  => 'required|exists:members,reset_password_code',
            'password' => 'required|confirmed|min:6',
        ];
        App::setLocale('ar');
        $validator = \Validator::make($request->except(['_token']), $rules);
        if ($validator->fails()) {
            return redirect()
                            ->back()
                            ->withErrors($validator)
                            ->withInput();
        }

		$user = Member::where('reset_password_code', $request->code)->first();

		$password = $request->password;//str_random(8);

		//Generate Random Password
		$user->password = bcrypt($password);
		$user->save();
		//END Generate Random Password

        //Send Email
        \Mail::send('emails.register.reset',['email' => $user->email,'password' => $password], function($message) use($user)
                {
                    $message->from('info@saudibidz.com')->subject('forget password');
                    $message->to($user->email);
                });
		//Mail::to($user->email)->send(new ResetPassword($user->email, $new_password));
        //END Send Email
        
        return redirect()->route('front-login');
	}


    public function generateUniqueCode()
	{
		$code = str_random(8);
		$unique = false;

		while(!$unique)
		{
			$generated = Member::where('reset_password_code', $code)->first();
			if(!$generated)
			{
				$unique = true;
			}
		}

		return $code;
    }

    public function generateUniqueCodePhone()
	{
		$code = rand(1000, 9999);
		$unique = false;

		while(!$unique)
		{
			$generated = Member::where('phone_act_code', $code)->first();
			if(!$generated)
			{
				$unique = true;
			}
		}

		return $code;
    }
}

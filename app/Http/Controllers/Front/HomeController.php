<?php

/**
 * Created by PhpStorm.
 * User: eslam
 * Date: 30/04/18
 * Time: 10:53 ص
 */

namespace App\Http\Controllers\Front;

use App;
use Hash;
use Validator;
use Carbon\Carbon;
use App\Model\BankTransaction;
use App\Http\Controllers\Controller;
use App\Model\Auction;
use App\Model\AdsSlider;
use App\Model\SubCategory;
use App\Model\Partner;
use App\Model\Category;
use App\Model\Ads;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public function index() { 
        
        $sliders = AdsSlider::where(['is_active' => 1])->get();
        
        
        $auctions = Auction::where(['is_active' => 1])->orderBy('id', 'desc')->get();
        $auctions2 = Auction::get();
        $partners = Partner::get();

        $data['ads'] = Ads::where('is_active', 1)->with('currentLanguage')->orderBy('id','desc')->take(2)->get();

        $data['items'] = $auctions;
        $data['sliders'] = $sliders;
        $data['auctions'] = $auctions;
        $data['auctions2'] = $auctions2;
        $data['partners'] = $partners;

        $data['categories'] = Category::where('is_active', 1)->with('currentLanguage')->get();
        return view('front.home.index', $data);
    }

    public function logout(Request $request) {
        if (!\Auth::guard('members')->check()) {
            return redirect()->route('front-home');
        }
        \Auth::guard('members')->logout();

        $request->session()->invalidate();

        return redirect()->route('front-home');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bidDetail($bid_id) {
        $aut_detail = Auction::with([
                    'category' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'subCategory' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'carColor' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'carType' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'engineType' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'fuel' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'motionVector' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'specification' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'import' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'provider',
                    'otherSpecifications' => function($q) {
                        $q->with('currentLanguage');
                    },
                    'auctionFiles',
                    'auctionImages',
                ])->where('id', $bid_id)->first();
        $auth_member = \Auth::guard('members')->user();


        

            if ($auth_member && $auth_member->id != $aut_detail->provider_id) {
                \DB::table('auctions')
                    ->where('id', $bid_id)
                    ->update(['views' => ($aut_detail->views + 1)]);

                    $blances = BankTransaction::where('userID',$auth_member->id)->whereNotIn('type', [3])->get();
                $total = 0;
                foreach($blances as $blance)
                {
                    if($blance->case_balance == 'deposited')
                    {
                        $total+=$blance->value;
                    }
                    elseif( $blance->case_balance == 'add_balance')
                    {
                        $total= $total;
                    }
                    else
                    {
                        $total-=$blance->value;
                    }
                }
            $data['balance'] = $total;
    
                    
                $data['page_title'] = $aut_detail->title;
                $data['aut_detail'] = $aut_detail;
                $data['path_I'] = trans('alerts.profile');
                $data['addedNew'] = Auction::where(['is_active' =>  1 , 'provider_accept' => 1])->inRandomOrder()->take(3)->get();
                return view('front.home.bid_detail', $data);
            }

        if ($auth_member && $auth_member->id == $aut_detail->provider_id) {
            \DB::table('auctions')
                ->where('id', $bid_id)
                ->update(['views' => ($aut_detail->views + 1)]);

                
            $data['balance'] = null;
            $data['page_title'] = $aut_detail->title;
            $data['aut_detail'] = $aut_detail;
            $data['path_I'] = trans('alerts.profile');
            $data['addedNew'] = Auction::where(['is_active' =>  1 , 'provider_accept' => 1])->inRandomOrder()->take(3)->get();
            return view('front.home.bid_detail', $data);
        }
        if (!$auth_member && $aut_detail->is_active == 0) {
            abort(404);
        }
        $iam_owner = false;
        if ($auth_member && $aut_detail->is_active == 0 &&
                $aut_detail->member_id != $auth_member->id
        ) {
            $iam_owner = true;
            abort(404);
        }
        if ($iam_owner && ($auth_member && $aut_detail->is_active == 0 &&
                $aut_detail->provider_id != $auth_member->id)
        ) {
            abort(404);
        }
        \DB::table('auctions')
                ->where('id', $bid_id)
                ->update(['views' => ($aut_detail->views + 1)]);
        $data['balance'] = null;
        $data['page_title'] = $aut_detail->title;
        $data['aut_detail'] = $aut_detail;
        $data['path_I'] = trans('alerts.profile');
        $data['addedNew'] = Auction::where(['is_active' =>  1 ,'isWork' =>  1 , 'provider_accept' => 1])->inRandomOrder()->take(3)->get();
        return view('front.home.bid_detail', $data);
    }

    public function subCategories() {
        $category_id = (int) \Request::input('category_id');
        $c = SubCategory::where('is_active', 1)->where('category_id', $category_id)->with('currentLanguage')->get();
        $ci = [];
        foreach ($c as $item) {
            $ci[] = ['id' => $item->id, 'title' => $item->currentLanguage->title];
        }

        return response()->json($ci);
    }

    public function search(Request $request) {

        // $rules = [
        //     'action_type'=>'in:0,1',
        // ];
        // $messages = [
        //     'action_type.in'    => 'The selected :action_type is invalid.',
        // ];
        // $validator = Validator::make($request->all(), $rules, $messages);
        // if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}


        $auctions2 = Auction::where('is_active',1)->get();
        $now = Carbon::now();
        foreach($auctions2 as $auc)
        {
            $new = Carbon::parse($auc->start_at)->addDays($auc->bid_days);
            if($new >= $now)
            {
                $au = Auction::find($auc->id);
                $au->isWork = 1;
                $au->save();
            }
            else
            {
                $au = Auction::find($auc->id);
                $au->isWork = 0;
                $au->save();
            }
        }

        // $items = Auction::where('is_active', 1)
        //                 ->where(function ($query) use ($request) {
        //                     if ($request->action_type) {
        //                         $query->Where('isWork', '=', $request->action_type);
        //                     }
        //                     if ($request->input('bid_type')) {
        //                         $query->Where('bid_type', '=', $request->input('bid_type'));
        //                     }
        //                     if ($request->input('category_id')) {
        //                         $query->Where('category_id', '=', $request->input('category_id'));
        //                     }
        //                     if ($request->input('category_id')) {
        //                         $query->Where('category_id', '=', $request->input('category_id'));
        //                     }
        //                     if ($request->input('minimum_bid')) {
        //                         $query->Where('minimum_bid', '>=', $request->input('minimum_bid'));
        //                     }
        //                     if ($request->input('max_bid')) {
        //                         $query->Where('minimum_bid', '<=', $request->input('max_bid'));
        //                     }
        //                     if ($request->input('manufacturing_year_from')) {
        //                         $query->Where('manufacturing_year', '>=', $request->input('manufacturing_year_from'));
        //                     }
        //                     if ($request->input('manufacturing_year_to')) {
        //                         $query->Where('manufacturing_year', '<=', $request->input('manufacturing_year_to'));
        //                     }
        //                     if ($request->input('sort')) {
        //                         $query->orderby('id', $request->input('sort'));
        //                     }
        //                 })->paginate(15);



        $items  = Auction::query();
       // var_dump($request->action_type);die();
        if ($request->action_type ==0 || $request->action_type==1) {
                                
            $items = $items->where('isWork', '=', $request->action_type);
        }
        $items = $items->where('is_active', 1);
        
        if (isset($request->bid_type) && !empty($request->bid_type)) {
            $items = $items->where('bid_type', '=', $request->bid_type);
        }
        if (isset($request->category_id) && !empty($request->category_id) && $request->category_id != '') {
            $rules = [
                'category_id'  => 'exists:categories,id',
            ];
             $messages = [
                'category_id.exists'    => 'The selected :category_id is invalid.' ,
            ];
            $validator = Validator::make($request->all(), $rules, $messages);
            if($validator->fails()) {return response()->json(['status'=>(int)400 , 'errors'=>$validator->errors()]);}
            
            $items = $items->where('category_id', '=', $request->category_id);
        }
        if (isset($request->minimum_bid) && !empty($request->minimum_bid)) {
            $items = $items->where('minimum_bid', '>=', $request->minimum_bid);
        }
        if (isset($request->max_bid) && !empty($request->max_bid)) {
            $items = $items->where('minimum_bid', '<=', $request->max_bid);
        }
        if (isset($request->manufacturing_year_from) && !empty($request->manufacturing_year_from)) {
            $items =  $items->where('manufacturing_year', '>=', $request->manufacturing_year_from);
        }
        if (isset($request->manufacturing_year_from) && !empty($request->manufacturing_year_from)) {
            $items->where('manufacturing_year', '<=', $request->manufacturing_year_to);
        }
        if (isset($request->sort) && !empty($request->sort)) {
            $items =  $items->orderBy('id', $request->sort);
        }
        $items = $items->paginate(15);
                       
        $data['items'] = $items;
        $data['path_I'] = trans('alerts.searchresult');
        return view('front.home.search', $data);
        
    }

}

<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // $baseURL = url('/'); 
        // $url = \URL::previous();
        // $arr = explode('/',$url);
        // $trimmed = str_replace($baseURL, '', $url) ;
        // $arr = explode('/',$trimmed);
        $urrl = $_SERVER['REQUEST_URI'];
        $arr = explode('/',$urrl);
        $au = $arr[1];
        if (Auth::guard('members')->check() && $au!='login' ) {
            return redirect('/home');
        }
        if (Auth::guard('members')->check() && $au=='login') {
           return $next($request);
        }
        if (Auth::guard('web')->check()) {
            return redirect('/admin');
        }
        if (Auth::guard($guard)->check()) {
            return redirect('/home');
        }

        return $next($request);
    }
}

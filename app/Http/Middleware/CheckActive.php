<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Front\Members\BaseController;
use Closure;

class CheckActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $current_user = $request->user();
        if ($current_user->is_active != 1 || $current_user->is_active_email == 0 || $current_user->is_active_phone == 0) {
            return redirect()->route('checkpoint-index');
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;

class CheckNotActive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $current_user = $request->user();
        if($current_user->is_active == 1 && $current_user->is_active_email == 1 && $current_user->is_active_phone == 1){
            return redirect()->route('member-profile-index');
        }
        return $next($request);
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\Setting;

class Settings {

    /**
     * 
     * @param type $request
     * @param Closure $next
     * @return type
     */
    public function handle($request, Closure $next) {

        $settings = Setting::get();
        foreach ($settings as $setting) {
            \Config::set($setting->key, $setting->value);
        }
        return $next($request);
    }

}

<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
Route::post('/pay', 'Api\BidController@pay')->name('pay');
Route::get('/paytabs', 'BankTransactionController@paytabs');


Route::get('/CronJob', 'CronJob@cronJob')->name('CronJob');
Route::group(['middleware' => 'settings'], function() {

    Route::get('/forgot', 'Front\AuthController@forgot')->name('forgot');
    Route::post('/forgotPassword', 'Front\AuthController@forgotPassword')->name('forgot-pass');
    Route::post('/resetPassword', 'Front\AuthController@resetPassword')->name('reset-pass');
    Route::get('/reset', 'Front\AuthController@reset')->name('reset');

    $this->get('/', 'Front\HomeController@index')->name('front-home');
    $this->get('/search', 'Front\HomeController@search')->name('search');
    $this->get('/register', 'Front\AuthController@index')->name('front-register');
    $this->get('/members/login', 'Front\AuthController@showLogin')->name('front-login');
    $this->post('/members/login/save', 'Front\AuthController@login')->name('front-login-sub');
    $this->get('/register/getcities', 'Front\AuthController@cities')->name('front-register-cities');
    $this->get('/global/sub_categories', 'Front\HomeController@subCategories')->name('front-sub-categories');
    $this->get('/bid/{id}', 'Front\HomeController@bidDetail')->name('front-bid-detail');
    $this->post('/register/save', 'Front\AuthController@register')->name('front-register-save');
    Route::get('/act', 'HomeController@active')->name('home-act-email');
    Route::get('/contact', 'HomeController@active')->name('home-contact-us');
    Route::get('/members/logout', 'Front\HomeController@logout')->name('member-logout');

    Route::group(['namespace' => 'CheckPoint', 'prefix' => 'checkpoint', 'middleware' => ['auth:members', 'not_active_status']], function () {
        Route::get('/active', 'CheckPointController@index')->name('checkpoint-index');
        Route::get('/salon/blocked', 'CheckPointController@noSalon')->name('checkpoint-no-salon');
        Route::post('/active/do', 'CheckPointController@doActive')->name('checkpoint-do-active');
        Route::post('/upload', 'CheckPointController@doUpload')->name('checkpoint-do-upload');
        Route::post('/resend/code', 'CheckPointController@resendCode')->name('checkpoint-do-resend');
    });
    Route::group(['namespace' => 'Members', 'prefix' => 'members', 'middleware' => ['auth:members', 'active_status']], function () {
        Route::get('/profile', 'ProfileController@index')->name('member-profile-index');
        Route::get('/edit', 'ProfileController@edit')->name('member-profile-edit');
        Route::post('/update', 'ProfileController@updateProfie')->name('member-update-profie');
        Route::get('/follow/{id}', 'BidController@followAuction')->name('member-follow');
        Route::get('/follow-list', 'BidController@allFollowAuction')->name('all-follow');
        Route::get('/myBalance', 'ProfileController@myBalance')->name('myBalance');
        Route::get('/delete-follow/{id}', 'BidController@deleteFollowAuction')->name('delete-follow');
        Route::get('/bids/list', 'BidController@myBids')->name('member-bid-list');
        Route::get('/bids/add', 'BidController@add')->name('member-bid-add');
        Route::post('/bids/add/save', 'BidController@addSave')->name('member-bid-save');
        Route::get('/addbidding/{info}', 'BidController@addbidding')->name('member-addbidding');
        Route::post('/dobidding', 'BidController@doaddbidding')->name('member-dobidding');
        Route::get('/notifications', 'ProfileController@myNotifications')->name('member-notifications');
        Route::get('/approve/{id}', 'BidController@approveAuction')->name('member-approve');
        Route::get('/reject/{id}', 'BidController@rejectAuction')->name('member-reject');



        Route::get('/approve/owner/{id}', 'Api\BidController@approveOwnerAuction')->name('approve-reject');
        Route::get('/reject/owner/{id}', 'Api\BidController@rejectOwnerAuction')->name('owner-reject');
        Route::get('/republish/{id}', 'Api\BidController@republish')->name('owner-republish');
    });
// Authentication Routes...
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
    $this->post('logout', 'Auth\LoginController@logout')->name('logout');


// Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('/home', 'Front\HomeController@index')->name('home');
    Route::get('/contactUs', 'ContactUsController@index')->name('contactUs');
    Route::post('sendmessage', 'ContactUsController@send_Message')->name('sendmessage');
    Route::get('about', 'PagesController@aboutus')->name('about-us');
    Route::get('terms', 'PagesController@terms')->name('terms');
    Route::get('howsiteworks', 'PagesController@howSiteWorks')->name('how-work');
    Route::get('faq', 'PagesController@faq')->name('faq');


    Route::get('OpenedAuctions', 'AuctionsController@opened')->name('OpenedAuctions');
    Route::get('ClosedAuctions', 'AuctionsController@closed')->name('ClosedAuctions');
    Route::get('BankTransfers', 'BankTransactionController@index')->name('BankTransfers');
    Route::post('Transfer', 'BankTransactionController@store')->name('Transfer');

    Route::post('/paytabs', 'BankTransactionController@paytabs')->name('paytabs');

    //---------------------------
// route for view/blade file
//---------------------------
Route::get('addPayment','PaymentController@addPayment')->name('addPayment');

//-------------------------
// route for post request
//-------------------------
Route::post('paypal', 'PaymentController@postPaymentWithpaypal')->name('paypal');

//---------------------------------
// route for check status responce
//---------------------------------
Route::get('paypal','PaymentController@getPaymentStatus')->name('status');

});

<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/////////////////////authintcation///////////////////////
Route::post('/login', 'Api\AuthController@login');
Route::post('/register', 'Api\AuthController@register');
Route::post('/forgotPassword', 'Api\AuthController@forgotPassword');
Route::post('/resetPassword', 'Api\AuthController@resetPassword');
Route::post('/resendCode', 'Api\AuthController@resendCode');
Route::post('/doActive', 'Api\AuthController@doActive');
Route::post('updateImage', 'Api\AuthController@updateImage');
/////////////////////authintcation///////////////////////


/////////////////////home///////////////////////
Route::get('/getSliders', 'Api\HomeController@getSliders');
Route::get('/getAuctionsClose', 'Api\HomeController@getAuctionsClose');
Route::get('/getAuctionsOpen', 'Api\HomeController@getAuctionsOpen');
Route::get('/getAllAuctions', 'Api\HomeController@getAllAuctions');

Route::get('/home', 'Api\HomeController@index');
Route::get('/search', 'Api\HomeController@search');
Route::get('/getCountries', 'Api\HomeController@getCountries');
Route::get('/getCities', 'Api\HomeController@getCities');

Route::get('/bidDetail', 'Api\HomeController@bidDetail');
Route::post('/dobidding', 'Api\BidController@doaddbidding');

//////////////////////profile//////////////////////
Route::get('/profile', 'Api\ProfileController@edit');
Route::post('/updateProfie', 'Api\ProfileController@updateProfie');
Route::get('/getNotifications', 'Api\ProfileController@myNotifications');
Route::get('/countNotifications', 'Api\ProfileController@countNotifications');

///////////////////////////////////////////

Route::get('/approve/owner', 'Api\BidController@approveOwnerAuction');
Route::get('/reject/owner', 'Api\BidController@rejectOwnerAuction');
Route::get('/republish', 'Api\BidController@republish');

Route::post('Transfer', 'Api\BankTransactionController@store');

Route::get('bankData', 'Api\BankTransactionController@data');

Route::get('/follow', 'Api\BidController@followAuction');
Route::get('/follow-list', 'Api\BidController@allFollowAuction');
Route::get('/delete-follow', 'Api\BidController@deleteFollowAuction');
Route::get('/bids/list', 'Api\BidController@myBids');
Route::post('/bids/add/save', 'Api\BidController@addSave');
Route::post('/dobidding', 'Api\BidController@doaddbidding');
Route::get('/approve', 'Api\BidController@approveAuction');
Route::get('/reject', 'Api\BidController@rejectAuction');

/////////////////////ids//////////////////////
Route::get('/getBidType', 'Api\BidController@getBidType');
Route::get('/getCategories', 'Api\BidController@getCategories');
Route::get('/getSubCategories', 'Api\BidController@getSubCategories');
Route::get('/getSubCategory', 'Api\BidController@getSubCategory');
Route::get('/getCarColor', 'Api\BidController@getCarColor');
Route::get('/getCarType', 'Api\BidController@getCarType');
Route::get('/getEngineType', 'Api\BidController@getEngineType');
Route::get('/getFuel', 'Api\BidController@getFuel');
Route::get('/getImport', 'Api\BidController@getImport');
Route::get('/getMotionVector', 'Api\BidController@getMotionVector');
Route::get('/getSpecification', 'Api\BidController@getSpecification');
Route::get('/getOtherSpecification', 'Api\BidController@getOtherSpecification');
Route::get('/getProviders', 'Api\BidController@getProviders');
Route::get('/getAuction', 'Api\BidController@getAuction');


////////////////////Setting///////////////////////

Route::get('/setting', 'Api\ContactUsController@index');
Route::post('sendmessage', 'Api\ContactUsController@send_Message');
Route::get('about', 'Api\PagesController@aboutus');
Route::get('terms', 'Api\PagesController@terms');
Route::get('howsiteworks', 'Api\PagesController@howSiteWorks');
Route::get('faq', 'Api\PagesController@faq');
